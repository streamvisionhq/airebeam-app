<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/*
  |--------------------------------------------------------------------------
  | File and Directory Modes
  |--------------------------------------------------------------------------
  |
  | These prefs are used when checking and setting modes when working
  | with the file system.  The defaults are fine on servers with proper
  | security, but you may wish (or even need) to change the values in
  | certain environments (Apache running a separate process for each
  | user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
  | always be used to set the mode correctly.
  |
 */
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
  |--------------------------------------------------------------------------
  | File Stream Modes
  |--------------------------------------------------------------------------
  |
  | These modes are used when working with fopen()/popen()
  |
 */
define('GOOGLE_MAPS_API_KEY', 'AIzaSyCpDTsYMEaLSpgERF7i-whRnrWLE3Sobdc');
define('FOPEN_READ', 'rb');
define('FOPEN_READ_WRITE', 'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE', 'ab');
define('FOPEN_READ_WRITE_CREATE', 'a+b');
define('FOPEN_WRITE_CREATE_STRICT', 'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

define('EMERALD_REQUEST_URL', "https://billing.airebeam.com/"); //Global
define('EMERALD_USER', "asim"); // emerald user name
define('EMERALD_PASSWORD', "e3f3o9034"); // emerald user password

define('ERROR_DEFAULT', "Error in processing request please try again");
define('PASSWORD_CHANGE', 'Password has been changed successfully');
define('INCIDENT_ADDED', 'Appointment has been added successfully');
define('INCIDENT_ERROR', 'Error in creating appointment');
define('INCIDENT_UPDATED', 'Appointment has been updated successfully');
define('SUCCSESS', 'Please wait, your request is being processed');
define('ERROR', 'Email or Password is invalid. Please double check and retry');
define('PREVIOUS_SLOT_ERROR', 'You cannot schedule an appointment for a slot in the past');
define('NOTE_ADDED', 'Note has been inserted successfully');
define('NOTE_ADDED_ERROR', 'Error in inserting note');
define('UPDATE_ENROUTE', 'Appointment has been updated successfully');
define('UPDATE_START', 'Appointment has been updated successfully');
define('WORKER_DELETED', 'Worker has been deleted successfully');
define('WORKER_FAILED', 'Error in adding worker');
define('WORKER_CANNOT_DELETE', 'This worker cannot be deleted at this time because some appointment(s) to this worker are still active.');
define('ADMIN_OWN_ACCOUNT_DELETE', 'You can not delete your own account');
define('WORKER_EDITED', 'Worker has been edited successfully');
define('NOT_FOUND', 'Page you requested not found');
define('UPDATE_SCHEDULED_ORDERS', 'Please wait, your request is being processed');
define('NO_INTERNET_CONNECTION', 'No Internet Connection');
define('BUSY_SLOT', 'This slot is already busy. You cannot schedule an appointment here');

################## Static Constants ################
define('STATUS_KEY', 'status');
define('MSG_KEY', 'message');
################## Static Constants ################

################## Zip Code Constants ################
define('ZIP_CODE_FAILED', 'Error in adding zip code');
define('ZIP_CODE_NUMBERS_ERROR', 'Zip code can only contains numbers');
define('ZIP_NOT_EXIST', 'Zip Not Exist In The system');
################## Zip Code Constants ################

################## Zip Code Constants ################
define('DELETE_STATUS', '56b1e42d87332');
################## Zip Code Constants ################

################## Available Slots Constants ################
define('DELETE_AVL_SLOTS_STATUS', '46b1e4fd15378');
################## Zip Code Constants ################

################## Config Settings Constants ################
define('CONFIG_UPDATE_FAILED', 'Error in updating config');
define('MANUAL_SUNSET_TIME', 1);
define('API_SUNSET_TIME', 2);
define('API_SUNSET_TIME_NOT_AVAILABLE', 'Current day\'s time is not available right now. Please check sunset time logs');
define('SUNSET_TIME_INSERT_SUCCESS', 'Sunset Time has been inserted successfully');
define('SUNSET_TIME_DELETE_SUCCESS', 'Previous month sunset time has been deleted successfully');
define('SUNSET_TIME_DELETE_FAILED', 'Failed! Previous month sunset time has not been deleted');
define('SUNSET_LOG_WARNING', 'No response from API');
define('SUNSET_LOG_API_FAILED_MSG', 'Sunset Api Failed');
define('SUNSET_LOG_API_EMAIL_MSG', '<p> API is failed. Please find logs in sunset_time_logs.txt in assets folder. </p>');
define('SUNSET_LOG_API_FROM', 'Airebeam Scheduler');
define('FROM_AIREBEAM', 'Airebeam Scheduler');
define('APPOINTMENT_CONFLICT', 'Appointment Conflict');

################## Config Settings Constants ################

define('START', 'Start');
define('ENROUTE', 'En-Route');
define('SCHEDULE', 'Schedule');
define('DONE', 'Done');
define('ABORT', 'Abort');
define('REJECTED', 'Rejected');

define('TOWN_FAILED', 'Error in adding town');
define('TOWN_DELETED', 'Town has been deleted successfully');
define('TOWN_ADDED', 'Town has been added successfully');
define('TOWN_EDITED', 'Town has been edited successfully');
define('TOWN_CANNOT_DELETE', 'Town cannot be deleted');
define('TOWN_NAME_ALREADY_EXISTS', 'Town name already exists');
define('TOWN_CODE_ALREADY_EXISTS', 'Town code already exists');

define('SERVICE_FAILED', 'Error in adding service');
define('SERVICE_DELETED', 'Service has been deleted successfully');
define('SERVICE_ADDED', 'Service has been added successfully');
define('SERVICE_EDITED', 'Service has been edited successfully');
define('SERVICE_CANNOT_DELETE', 'Service cannot be deleted');
define('SERVICE_NAME_ALREADY_EXISTS', 'Service name already exists');
define('SERVICE_CODE_ALREADY_EXISTS', 'Service code already exists');

define('RECORD_NOT_UPDATED', 'Record not updated please try again');
define('ALL_REQUIRED', 'All fields are required');
define('VALID_CUSTOMER', 'Please select a valid customer');
define('ALREADY_HAS_APPOINTMENT', 'Worker you select already has an appoinment on that time');

define('MESSAGE_EDITED', 'Message has been edited successfully');
define('MESSAGE_ERROR', 'Error in updating message');
define('CREATE_ALERT_MSG', '<CLIENT_NAME> needs to be notified of their appointment schedule time from <START_TIMESTAMP> to <END_TIMESTAMP>');
define('CHANGE_ALERT_MSG', '<CLIENT_NAME> needs to be notified of their appointment time change from <START_TIMESTAMP> to <END_TIMESTAMP>');
define('LATE_REJECTION_ALERT_MSG', "<CLIENT_NAME> cannot take <WORKER_NAME>'s appointment at <START_TIMESTAMP> today. Please resolve the situation and call <WORKER_NAME> to update them of the situation.");
define('REJECTION_ALERT_MSG', "The appointment with <CLIENT_NAME> at <START_TIMESTAMP> was rejected by client.");

// twilio
define('EMPTY_PHONE', 'Phone Number is Empty');
define('SENT_SUCCESSFULLY', 'Message sent successfully');
define('EMAIL_SENT_SUCCESSFULL', 'Email sent successfully');


// customer access ok
define('SUCCESSFULL_MBR', 'Success');
define('MISSING_PARAMETERS', 'Missing required parameters');
define('MBR_NOT_EXISTS', 'MBR does not exists');
define('AUTHENTICATION_FAILED', 'Authentication failed');
define('SERVICE_INACTIVE', 'CustomerAccessOK service on this MBR is inactive');
define('SERVICE_NOT_AVAILABLE', 'CustomerAccessOK service on this MBR is not available');
define('SERVICE_NOT_FOUND', 'Service on this MBR is not available');
define('MISSING_PARAMETERS_TWO', 'Missing Phone or Address atleast any one field is required');
define('MISSING_CREDIT_CARD', 'Missing credit card field');
define('INVALID_BASESTRING', 'Password is not in a valid encoded form');
define('EMAIL_NOT_EXISTS', 'Email not exists');
define('PHONE_NOT_EXISTS', 'Phone number not exists');
define('TOKEN_NOT_MATCHED', 'Token not matched');
define('TOKEN_EXPIRED', 'Token has expired');
define('PHONE_LANDLINE', 'Phone number is not a cell phone');
define('NOT_ALLOWED', 'You are not allowed to perform this action');
define('INCORRECT_PASSWORD', 'Incorrect password');
define('MIN_COMMUTE_TIME_CONFIG', 'MIN_COMMUTE_TIME');
define('SIZE_OF_INSTALLATION_CONFIG', 'SIZE_OF_INSTALLATION');
define('LAST_AVAILABLE_START_TIME_CONFIG', 'LAST_AVAILABLE_START_TIME');
define('MAX_NUMBER_OF_APPOINTMENTS_CONFIG', 'MAX_NUMBER_OF_APPOINTMENTS');
define('SUNSET_TIME_HR_CONFIG', 'SUNSET_TIME_HR');
define('SUNSET_TIME_MINS_CONFIG', 'SUNSET_TIME_MINS');
define('SHIFT_START_TIME_HR_CONFIG', 'SHIFT_START_TIME_HR');
define('SUNSET_TIME_MODE_CONFIG', 'SUNSET_TIME_MODE');
define('SHIFT_START_TIME_MINS_CONFIG', 'SHIFT_START_TIME_MINS');
define('MAX_SHIFT_END_TIME_HR_CONFIG', 'MAX_SHIFT_END_TIME_HR');
define('MAX_SHIFT_END_TIME_MINS_CONFIG', 'MAX_SHIFT_END_TIME_MINS');
define('ANTE_MERIDIEM', '0');
define('POST_MERIDIEM', '1');

//File
define('FILE_UPLOADED', 'File uploaded successfully');

################## Create Appointment Constants ################
define('APPOINTMENT_CONFLICT_MSG', 'Cannot create appointment. Given requested time has conflict with another appointment.');
define('CREATE_APPOINTMENT_PARAMS_MISSING', 'Some parameters are missing');
define('API_USER_NAME', 'API');
define('END_TIME_LESS_ERROR', 'End time is greater than Start time');
define('SUCCESS_APPOINTMENT_CREATED', 'Success. Appointment Created');
define('FAILED_APPOINTMENT_CREATED', 'Failed. Appointment is not created due to some problem. Try again later.');
define('WORKER_NOT_EXIST', 'Invalid worker Id.');
define('APPOINTMENT_PARAMS_MISSING_STATUS', '1');
define('APPOINTMENT_CONFLICT_STATUS', '2');
define('END_TIME_LESS_STATUS', '3');
define('SUCCESS_APPOINTMENT_STATUS', '4');
define('FAILED_APPOINTMENT', '5');
define('WORKER_NOT_EXIST_STATUS', '6');
define('ORDER_NOT_PROCESSED', '0');
define('ORDER_PROCESSED', '1');
define('TEMP_ORDERS_INSERTED', 'Success. Orders are inserted successfully');
define('TEMP_ORDERS_NOT_INSERTED', 'Failed. Orders are not inserted');
define('TEMP_ORDERS_NOT_UPDATED', 'Failed. Unprocessed orders are not updated');
define('NO_UNPROCESS_ORDERS', 'There are no unprocess orders found');
################## Create Appointment Constants ################

################## Service Type Constants ################
define('TYPE_INSTALLATION', '56a5c03ad37f5');
################## Service Type Constants ################

################## Status Type Constants ################
define('STATUS_SCHEDULE', '56a5c03ab920f');
################## Status Type Constants ################

################## Available Slots Constants ################
define('START_TIME_KEY', 'start_time');
define('END_TIME_KEY', 'end_time');
define('APPOINTMENT_LOCATION_KEY', 'appointment_loc');
define('WORKER_ID_KEY', 'worker_id');
define('WORKER_NAME_KEY', 'worker_name');
define('WORKER_EMAIL_KEY', 'worker_email');
define('WORKER_PRIORITY_KEY', 'worker_priority');
define('WORKER_BOOKED_SLOTS_KEY', 'worker_booked_slots');
define('DATE_KEY', 'date');
define('BOOKED_SLOTS_KEY', 'booked_slots');
define('AVAILABLE_SLOTS_KEY', 'available_slots');
define('MIN_COMMUTE_TIME_KEY', 'min_commute_time');
define('INSTALLATION_TIME_KEY', 'installation_time');
define('LAST_AVAILABLE_START_TIME_KEY', 'last_available_start_time');
define('MAX_APPOINTMENTS', 'max_appointment_per_day');
define('HOLIDAYS_KEY', 'holidays');
define('SHIFT_END_TIME_KEY', 'shift_end_time');
define('HOLIDAY_NAME_KEY', 'name');
define('HOLIDAY_DATE_KEY', 'date');
define('DATE_RANGE_MSG', 'Date Range is not greater than 31 days');
define('DATE_RANGE_LESS_MSG', '{start_date} is greater than {end_date} parameter');
define('MONTH_DATA_NOT_AVAILABLE_MSG', 'There is no available slots for this month');
define('ZIP_CODE_ERROR_MSG', 'Zip code is missing');
define('ZIP_CODE_NOT_FOUND', 'Zip code does not found');
define('ZIP_CODE_FOUND', 'Zip code found');
define('EMAIL_NOT_FOUND', 'Email not found');
define('EMAIL_FOUND', 'Email found');
define('MONTH', 61);
define('MERGABLE_SLOT', 1);
define('UNMERGABLE_SLOT', 0);

################## Available Slots  Constants ################

################## DATETIME Format Strings ################
define('DATETIME_24_HR', 'Y-m-d H:i:s');
define('DATE_YYYY_MM_DD', 'Y-m-d');
################## DATETIME Format Strings ################

################## Create MBR Constants ################
define('EMERALD_API_USER', "asimapi"); // emerald user name
define('EMERALD_API_PASSWORD', "qw8jq08qw8jq08"); // emerald user password
define('MBR_KEY', 'mbr');
define('MBR_LOG_FAILED', 'Error in adding MBR request log');
define('CREATE_MBR_RETCODE_KEY', "RETCODE");
define('CUSTOMER_ID_KEY', "CUSTOMERID");
define('CREATE_MBR_MSG_KEY', "MESSAGE");
define('CREATE_MBR_PARAMS_MISSING', 'Some parameters are missing');
define('CREATE_MBR_MISSING_STATUS', '1');
define('EMERALD_RET_CODE', "RETCODE");
define('PACKAGE_ID_KEY', "PACKAGEID");

################## Create MBR Constants ################

################## Holidays Constants ################
define('HOLIDAY_NAME', "Name");
define('HOLIDAY_DATE', "Date");
define('SUNDAY', "Sunday");
define('MONDAY', "Monday");
define('TUESDAY', "Tuesday");
define('WEDNESDAY', "Wednesday");
define('THURSDAY', "Thursday");
define('FRIDAY', "Friday");
define('SATURDAY', "Saturday");
define('SUNDAY_INT', 0);
define('MONDAY_INT', 1);
define('TUESDAY_INT', 2);
define('WEDNESDAY_INT', 3);
define('THURSDAY_INT', 4);
define('FRIDAY_INT', 5);
define('SATURDAY_INT', 6);
define('THANKSGIVING', 'Thanksgiving');
define('MEMORIAL_DAY', 'Memorial Day');
define('LABOR_DAY', 'Labor Day');
define('MAY', 'May');
define('SEPTEMBER', 'September');
define('NOVEMBER', 'November');
################## Holidays Constants ################

/* End of file constants.php */
/* Location: ./application/config/constants.php */