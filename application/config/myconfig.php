<?php
$config["site_name"] = "Airebeam";
$config["SCHEDULER_ROLES"] = array( "Admin"=>"56a5c101c9949", "Technician"=> "56a5c10c469f7", "CSR" => "56a5c11822e65");
$config["STATES"] = array( "AZ"=>"Arizona");
$config["FIXED_HOLIDAYS"] = array(
		array(
				'Name' => "New Year's Day",
				'Date' => '-01-01'
		),
		array(
				'Name' => "Independence Day",
				'Date' =>  '-07-04'
		),
		array(
				'Name' => "Christmas Day",
				'Date' => '-12-25'
		)
);