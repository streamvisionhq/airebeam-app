<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends Front_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        add_meta_title("Login");
        $this->load->model("m_worker");
        $this->worker = new M_worker();
        
        if ($this->worker->isWorkerLogin()) {
            redirect(BASE_PATH."orders");
        } else {
            if($this->input->post()){
                $response = $this->worker->doLogin();
                if($response["response"]){
                    $this->data["Msg"] = $response["Msg"];
                    redirect($this->uri->uri_string());
                }else{
                    $this->data["Error"] = $response["Msg"];
                }
            }
            $this->load->view("login", $this->data);
        }
    }

}
