<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Logout extends Front_Controller {

    public function __construct() {
        parent::__construct();
    }

    /**
     * @note destroy all session variables and redirect to home
     */
    public function index() {
        $this->load->model("m_worker");
        $this->worker = new M_worker();
        
        $this->worker->workerLogout();
        redirect(BASE_PATH);
    }

}
