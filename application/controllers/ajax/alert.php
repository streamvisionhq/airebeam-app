<?php
/**
 * this class has all alerts related functionality
 * like generate alert html to show in activity logs
 * remove alert, alerts related to any order
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Alert extends MY_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function generateAlertsHtml($response,$typeID) {
        foreach ($response as $row) {
            if ($row["TypeID"] == $typeID) {
                $orderData = $this->order->getOrder($row["ObjectID"]);
                if ($orderData) {
                    $this->data["order"] = $orderData;
                    $this->data["row"] = $row;
                    $this->load->view("admin/alert", $this->data);
                }
            }
        }
    }

    public function getAlerts() {
        $this->load->model("M_order");
        $this->order = new M_order();
        $response = $this->order->getAllAlertForCustomerCall();
        if ($response) {
            $this->generateAlertsHtml($response, "73dc597bd98e2");
            $this->generateAlertsHtml($response, "7f3c597bd72e3");
            foreach ($response as $row) {
                if ($row["TypeID"] != "73dc597bd98e2" && $row["TypeID"] != "7f3c597bd72e3") {
                    $orderData = $this->order->getOrder($row["ObjectID"]);
                    if ($orderData) {
                        $this->data["order"] = $orderData;
                        $this->data["row"] = $row;
                        $this->load->view("admin/alert", $this->data);
                    }
                }
            }
        }
    }

    public function AlertActionPerform($orderID, $action = false) {
        $this->load->model("M_order");
        $this->order = new M_order();
        $this->order->removeAlert($orderID);
        $this->order->deleteMessageEntry($orderID);

        $activityLogID = $this->order->orderActivity("7f3f38e920c9f", "40d64aeeb308b");

        $activityMetaArray = array(
            "MetaID" => strictUniqueID(),
            "ActivityLogID" => $activityLogID,
            "ObjectID" => $orderID,
            "Name" => "action_performed_by",
            "Value" => $this->order->isAdminLogin()
        );
        $this->order->Insert("activity_log_meta", $activityMetaArray);

        if (!$action) {
            $action = "click_on_done_button";
        }

        $activityMetaArray = array(
            "MetaID" => strictUniqueID(),
            "ActivityLogID" => $activityLogID,
            "ObjectID" => $orderID,
            "Name" => $action,
            "Value" => "yes"
        );
        $this->order->Insert("activity_log_meta", $activityMetaArray);
    }

    public function getAlertsOfOrder($orderID) {
        $this->load->model("M_order");
        $this->order = new M_order();

        $rows = $this->order->selectAllWhere("alert", array("ObjectID" => $orderID, "isVisible" => "1"));
        if ($rows->num_rows() > 0) {
            echo "success";
        } else {
            echo "fail";
        }
    }

    public function removeAlertNotification($orderID) {
        $this->AlertActionPerform($orderID, "click_on_cancel_sms_button");
    }

    public function smsFailNotification(){
        $this->load->model("M_order");
        $this->order = new M_order();
        $orderId = $this->input->post('order');
        $customerId = $this->input->post('customer');        

        $this->order->UpdateAll("customer_notification_log", array("IsNotified" => 1), array(
            "CustomerID" => $customerId,
            "OrderID" => $orderId
        ));

        // Maintain Log
        $activity = array(
                      "OrderID" => $orderId,
                      "AssignedBy" => $this->order->isAdminLogin(),
                      "Name" => "customer_notified",
                      "Message" => 'Customer is notified',
                );
        $this->order->addMessageAllActivities($activity);

    }

    public function getFailureSms(){
        $this->load->model("M_order");
        $this->order = new M_order();
        $response = $this->order->getAllNewFailureSmsNumber();
        if($response){
            echo intval($response);
        }
    }

}
