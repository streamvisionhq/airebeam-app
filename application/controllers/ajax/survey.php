<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Survey extends MY_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function getSurveys() {
        $this->load->model("M_order");
        $this->order = new M_order();
        $response = $this->order->getQueueSurveysNumber();
        if ($response) {
            echo intval($response);
        }
    }

    public function getAllSurveys() {
        $this->load->model("M_order");
        $this->order = new M_order();
        $data["surveys"] = $this->order->getAllQueuedSurveys();
        echo $this->load->view("admin/survey-list-rows",$data,true);
    }

}
