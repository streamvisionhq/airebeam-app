<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Worker extends MY_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function sortWorker() {
        $this->load->model("M_admin");
        $this->admin = new M_admin();
        $response = $this->admin->editWorkerNumber($this->input->post("workerID"));
        if($response === true){
            echo "success";
        }else{
            echo $response;
        }
    }

}
