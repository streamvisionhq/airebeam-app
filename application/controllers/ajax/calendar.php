<?php
/**
 * this class has calendar related functionality
 * like checks last updated field in db to update content on run time
 * gets and make calendar data like all workers and orders and also delete order
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Calendar extends MY_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function checktablesUpdated($pageSlug) {
        $this->load->model("M_order");
        $this->order = new M_order();
        $lastUpdated = date("Y-m-d H:i:s");        
        if(isset($_COOKIE["last_updated_".$pageSlug])) {
            $lastUpdated = $_COOKIE["last_updated_".$pageSlug];
        }
        
        $rows = $this->order->SelectAll("update_table");
        $result = $rows->first_row('array');
        $dbUpdated = $result["LastUpdated"];
        
        if($lastUpdated != $dbUpdated){
            setcookie("last_updated_".$pageSlug, $dbUpdated);
            echo $dbUpdated;
        }else{
            echo "Not Updated";
        }
    }

    public function getCalendarData() {
        $this->load->model("M_order");
        $this->order = new M_order();
        $start = date("Y-m-d", strtotime(substr($_POST["start"], 0, 24)));
        $end = date("Y-m-d", strtotime(substr($_POST["end"], 0, 24)));

        $allOrders = $this->order->getAllOrdersByDate($start,$end);
        $workers = $this->order->getWorkers();
        $javaWorkers = $this->order->javaWorker($workers);
        $string = $this->order->creatOrderString($allOrders, $javaWorkers);
        $array["events"] = $string;

        echo json_encode($array);
    }

    public function getCalendarWorkersData() {
        $this->load->model("M_order");
        $this->order = new M_order();
        $workers = $this->order->getWorkers();

        if ($workers) {
            $workerString = array_values($workers);
            $array["workerString"] = $workerString;
        } else {
            $array["workerString"] = [];
        }
        echo json_encode($array);
    }

    public function deleteEvent() {
        $this->load->model("M_order");
        $this->order = new M_order();
        $creatorID = $this->order->isAdminLogin();
        $orderData = $this->order->getOrder($_POST['orderID']);
        $deleteOrderActivity = array(
            "OrderID" => $_POST['orderID'],
            "AssignedBy" => $creatorID
        );
        $this->order->deleteOrderAllActivities($deleteOrderActivity);
        $this->order->removeAlert($_POST['orderID']);
        $this->order->removeSurveyFromQueue($_POST['orderID']);
        $this->order->deleteOrder($_POST['orderID']);
    }

}
