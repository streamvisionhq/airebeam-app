<?php
/**
 * this class has all twilio related functionality
 * like send and receive sms
 */
if ( !defined('BASEPATH') )
    exit('No direct script access allowed');

class TwilioBase extends MY_Controller {

    function __construct() {
        parent::__construct();
    }

    public function send($orderID) {
        $this->load->model("m_twilio");
        $this->twil = new M_twilio();

        $order = $this->twil->getOrder($orderID);
        $shortCodes = $this->twil->getShortCodes($order);

        if ( !empty($order["PhoneHome"]) ) {
            $result = $this->twil->getMessageEntry($orderID);

            if ( $result ) {
                $message = $this->twil->getMessageBody($result["MessageType"]);
                $message = $this->twil->replaceDateAndService($message, $shortCodes);
                $response = $this->twil->sendSMS(FROM, simplizePhone($order["PhoneHome"]), $message);
            }     


            if ( $response ) {
                if ( $response["Status"] == "success" ) {

                    $this->twil->updateMessageStatus($orderID, "eefe80e2ade65");
                    $array = array(
                          "status" => "success",
                          "message" => SENT_SUCCESSFULLY
                    );
                    $this->twil->UpdateAll("alert", array("isVisible" => "0"), array("ObjectID" => $orderID));

                    $activity = array(
                          "OrderID" => $orderID,
                          "AssignedBy" => $this->twil->isAdminLogin(),
                          "Name" => "sms_sent",
                          "Message" => $message,
                    );
                    $this->twil->addMessageAllActivities($activity);

                    echo json_encode($array);
                }
                else {
                    $status = $response['Status'];
                    $error_message = $response['Message'];
                    $error_code = $response['ErrorCode'];
                    $sid = $response['Sid'];
                    $twilio_sms_log_url = 'https://www.twilio.com/console/sms/logs/'.$sid;

                    if ($error_code == '404') {
                        $message_redirect = $error_code . ' ' . $error_message;
                    }else{
                        $message_redirect = "<a href='".$twilio_sms_log_url."'>" . $error_code . ' '. $error_message . "</a>";
                    }
                    $array = array(
                          "status" => $status,
                          "message" => $error_code.' '.$error_message
                    );

                    $msg = "SMS Sending Failed on " . $order["Name"] . ", " . $message_redirect;
                    $this->twil->removeAlert($orderID);
                    $this->twil->addAlert($order, "sms_failed", false, $msg);
                    $this->twil->addAlertLog($order, "sms_failed", false, $msg);
                    $this->twil->addCustomerNotifyLog($order);

                    $activity = array(
                          "OrderID" => $orderID,
                          "AssignedBy" => $this->twil->isAdminLogin(),
                          "Name" => "sms_failed",
                          "Message" => $msg,
                    );
                    $this->twil->addMessageAllActivities($activity);

                    echo json_encode($array);
                }

                // Maintain Logs
                $messageLog["LogID"] = uniqid();
                $messageLog["OrderID"] = $orderID;
                $messageLog["To"] = $response['To'];
                $messageLog["Sid"] = $response['Sid'];
                $messageLog["Body"] = $response['Body'];
                $messageLog["Status"] = $response['Status'];
                $messageLog["Date"] = date("Y-m-d H:i:s");

                $this->twil->Insert("message_log", $messageLog);
            }
            else {
                $array = array(
                      "status" => "fail",
                      "message" => ERROR_DEFAULT
                );
                echo json_encode($array);
            }    
        }
        else {
            $array = array(
                  "status" => "fail",
                  "message" => EMPTY_PHONE
            );
            echo json_encode($array);
        }
    }

    public function recieve() {
        $this->load->model("m_order");
        $this->order = new M_order();
        $this->load->model("m_curl");
        $this->curl = new M_curl();
        $this->load->model("m_twilio");
        $this->twil = new M_twilio();

        $this->curl->logTime("SMS response recieved = " . json_encode($_POST));

        $phone = str_replace("+92", "", str_replace("+1", "", $_POST["From"]));
        $body = strtolower($_POST["Body"]);

        $detail = $this->order->getCustomerDetailByPhone($phone);

        if ( $detail ) {
            $orderDetail = $this->order->getOrderByCustomerID($detail->CustomerID);
            $shortCodes = $this->order->getShortCodes($orderDetail);
            if ( $body == "no" || $body == "n" ) {
                $this->curl->logTime("SMS recieved customer detail = " . json_encode($detail));

                if ( $orderDetail["Status"] != REJECTED ) {

                    $message = $this->twil->getMessageBody("76d08e3487b57");
                    $message = $this->twil->replaceDateAndService($message, $shortCodes);
                    $response = $this->twil->sendSMS(FROM, $phone, $message);

                    $this->curl->logTime("SMS response = " . json_encode($response));

                    $start = date_create($orderDetail["Date"]);

                    if ( $orderDetail["Status"] == ENROUTE ) {
                        $late_rejection = true;
                    }
                    else {
                        $late_rejection = false;
                    }

                    $this->order->removeAlert($orderDetail["OrderID"]);

                    if ( $late_rejection === true ) {
                        $this->order->addAlert($orderDetail, "late_rejection", $start);
                    }
                    else {
                        $this->order->addAlert($orderDetail, "rejection", $start);
                    }

                    $this->order->setStatus($orderDetail["OrderID"], REJECTED);

                    $activity = array(
                          "OrderID" => $orderDetail["OrderID"],
                          "AssignedBy" => $detail->CustomerID,
                          "Name" => "sms_rejected",
                          "Message" => $_POST["Body"],
                    );
                    $this->order->addMessageAllActivities($activity);
                }
            }
            else {
                $message = $this->twil->getMessageBody("96d0de348fb57");
                $message = $this->twil->replaceDateAndService($message, $shortCodes);
                $response = $this->twil->sendSMS(FROM, $phone, $message);
            }
        }
    }

    public function recieveCallback() {
        $this->load->model("m_curl");
        $this->load->model("m_twilio", "twilio");
        $this->curl = new M_curl();

        $phone = $_POST["To"];
        $this->curl->logTime("SMS Push Recieved = " . json_encode($_POST));

        if ( isset($_POST["SmsSid"]) ) {
            $this->curl->UpdateAll("message_log", array("Status" => $_POST["SmsStatus"]), array("Sid" => $_POST["SmsSid"]));
        }

        if ( isset($_POST["ErrorCode"]) ) {
            $smsResponseDB = $this->curl->getMessageLogBySID($_POST["SmsSid"]);
            $orderDetail = $this->curl->getOrder($smsResponseDB["OrderID"]);
            $detail = $this->curl->getCustomerDetail($orderDetail["CustomerID"]);

            switch ( $_POST["ErrorCode"] ) {
                case "30003":
                    $error_text = "Unreachable destination handset";
                    break;

                case "30004":
                    $error_text = "Message blocked";
                    break;

                case "30005":
                    $error_text = "Unknown destination handset";
                    break;

                case "30006":
                    $error_text = "Landline or unreachable carrier";
                    break;

                default:
                    $error_text = "Unknown error";
                    break;
            }

            $isPhoneHomeLandline = $this->twilio->getPhoneNumberType($orderDetail["PhoneHome"]);

            if ( isset($_POST["ErrorCode"]) && ($_POST["ErrorCode"] == "30005" || $_POST["ErrorCode"] == "30006") && $isPhoneHomeLandline === "1" ) {

                $array = array(
                      "isPhoneHomeLandline" => 1,
                      "Date" => date("Y-m-d H:i:s")
                );

                $this->curl->UpdateAll("customer", $array, array("CustomerID" => $orderDetail["CustomerID"]));

                $alert_text = $orderDetail['CustomerName'] . ", sms sending failed due to <strong><a href='https://www.twilio.com/docs/api/errors/" . $_POST["ErrorCode"] . "' title='click here for error details' target='_blank'>\"" . $error_text . "\"<a/></strong>. Please notify the user manually & press done";
                $this->curl->UpdateAll("alert", array("isVisible" => "1", "Message" => $alert_text, "TypeID" => "7c3d597bd72e4"), array("ObjectID" => $orderDetail["OrderID"]));
            }
            else {
                $alert_text = $orderDetail['CustomerName'] . ", sms sending failed due to <strong><a href='https://www.twilio.com/docs/api/errors/" . $_POST["ErrorCode"] . "' title='click here for error details' target='_blank'>\"" . $error_text . "\"<a/></strong>. Please retry or notify the user manually & press cancel";
                $this->curl->UpdateAll("alert", array("isVisible" => "1", "Message" => $alert_text), array("ObjectID" => $orderDetail["OrderID"]));
            }

            $activity = array(
                  "OrderID" => $orderDetail["OrderID"],
                  "AssignedBy" => $detail["CustomerID"],
                  "Name" => "sms_failed",
                  "Message" => $error_text,
            );

            $this->curl->addMessageAllActivities($activity);
        }
    }

}
