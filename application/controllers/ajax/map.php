<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Map extends MY_Controller {

    public function __construct() {
        parent::__construct();
    }

    /**
     * fetch all necassary data of all orders by date range
     * city,latitude,longitude,order id and time
     */
    public function getMarkersData() {

        $this->load->model("M_order");
        $this->order = new M_order();
        $start = date("Y-m-d", strtotime(substr($this->input->post("start"), 0, 15)));
        $end = date("Y-m-d", strtotime(substr($this->input->post("end"), 0, 15)));
        $workers = $this->order->getWorkersDetail();

        foreach ((array) $workers as $key => $worker) {
            $orders = $this->order->getAllByDateRange($worker["WorkerID"],$start,$end);
            if (!empty($orders)) {
                $n = 1;
                foreach ((array) $orders as $order) {
                    $array[$key]["customer"][] = array($order["City"], $order["Latitude"], $order["Longitude"], $order["OrderID"]);
                    $array[$key]["customerFull"][] = array($n, $order["City"], $order["Latitude"], $order["Longitude"], date("h:i", strtotime($order["Date"])), date("A", strtotime($order["Date"])), $worker["Color"], $order["OrderID"]);
                    $n++;
                }
            }
        }
        if(empty($array)){
            $array = "";
        }
        echo json_encode($array);
    }

    /**
     * fetch latitude and longitude of a order
     */
    public function getNewLngLat() {

        $this->load->model("M_order");
        $this->order = new M_order();

        $orderID = $this->input->post("orderID");
        $data = $this->order->getOrderCustomerLatLng($orderID);
        $data['Date']=  showTime($data['Date']);
        echo json_encode($data);
    }

    /**
     * fetch latitude and longitude of all order
     * and getting center point of all orders
     */
    public function getLngLat() {

        $this->load->model("M_order");
        $this->order = new M_order();

        $allOrders = $this->order->getAll();
        foreach ((array) $allOrders as $order) {
            $array["Longitude"][] = $order["Longitude"];
            $array["Latitude"][] = $order["Latitude"];
        }
        $return["Longitude"] = (max($array["Longitude"]) + min($array["Longitude"])) / 2;
        $return["Latitude"] = (max($array["Latitude"]) + min($array["Latitude"])) / 2;
        echo json_encode($return);
    }

}
