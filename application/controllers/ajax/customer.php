<?php
/**
 * this class has methods for customer related tasks
 * like check if any updated info is there and update it in database
 * search customer by name for auto suggest drop down in search field
 * generates force password reset hit
 * creates log when any user force the customer to reset password
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Customer extends MY_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function getCustomerAddress() {
        $this->load->model("M_admin");
        $this->admin = new M_admin();
        $reponse = $this->admin->getCustomerDetailByName($this->input->post("customerName"));
        if ($reponse) {
            echo json_encode($reponse);
        }
    }

    public function getCustomerUpdates() {
        $this->load->model("M_admin");
        $this->admin = new M_admin();
        $this->load->model("M_curl");
        $this->curl = new M_curl();
        $this->load->model("M_twilio");
        $this->twil = new M_twilio();

        $detail = $mbrDetail = false;

        $service = $this->getServiceFromEmerald($this->input->post("serviceID"));
        $service["city"] = trim($service["city"]);
        $service["zip"] = trim($service["zip"]);
        $service_db = $this->checkExistsService($this->input->post("serviceID"));
        $customer_db = $this->checkExistsCustomer($this->input->post("customerID"));

        $fullName = $this->input->post("customerID");

        //fetch credit card details from backoffic for airebeam customer portal
        $url = EMERALD_REQUEST_URL . "backoffice/mbrdetail.ews?mbr=" . $this->input->post("customerID");
        $detail = $this->curl->fetchBackOfficeMBRDetails($url);

        if (!empty($detail->MBR)) {
            $mbrDetail = [
                "FIRSTNAME" => checkIsZero(simplize($detail->MBR->FIRSTNAME)),
                "LASTNAME" => checkIsZero(simplize($detail->MBR->LASTNAME)),
                "PHONEHOME" => checkIsZero(simplizePhone(simplize($detail->MBR->PHONEHOME))),
                "PHONEWORK" => checkIsZero(simplizePhone(simplize($detail->MBR->PHONEWORK))),
                "PHONEFAX" => checkIsZero(simplizePhone(simplize($detail->MBR->PHONEFAX))),
                "STATE" => checkIsZero(simplize($detail->MBR->STATE)),
                "LASTFOUR" => checkIsZero(simplize($detail->MBR->LASTFOUR))
            ];

            if ($mbrDetail["FIRSTNAME"]) {
                $fullName .= " - " . $mbrDetail["FIRSTNAME"];
                if ($mbrDetail["LASTNAME"]) {
                    $fullName .= " " . $mbrDetail["LASTNAME"] . "";
                }
            }
            $array["FullName"] = $fullName;
        }

        $this->admin->UpdateAll("customer", array("CustomerName" => $fullName, "PhoneWork" => $mbrDetail["PHONEWORK"], "Fax" => $mbrDetail["PHONEFAX"], "CreditCardLastFour" => $mbrDetail["LASTFOUR"], "State" => $mbrDetail["STATE"]), array("CustomerID" => $customer_db["CustomerID"]));

        if ($mbrDetail["PHONEHOME"] != $customer_db["PhoneHome"]) {
            $this->admin->UpdateAll("customer", array("PhoneHome" => $mbrDetail["PHONEHOME"]), array("CustomerID" => $customer_db["CustomerID"]));
            $array["PhoneHome"] = $mbrDetail["PHONEHOME"];
            $array["PhoneUpdated"] = "yes";
        } else {
            $array["PhoneHome"] = $customer_db["PhoneHome"];
            $array["PhoneUpdated"] = "no";
        }
        
        $isPhoneHomeLandline = $this->twil->getPhoneNumberType($mbrDetail["PHONEHOME"]);
        $this->admin->UpdateAll("customer", array("isPhoneHomeLandline" => $isPhoneHomeLandline), array("CustomerID" => $customer_db["CustomerID"]));

        if ($service["email"] != $customer_db["Email"]) {
            $this->admin->UpdateAll("customer", array("Email" => $service["email"]), array("CustomerID" => $customer_db["CustomerID"]));
            $array["Email"] = $service["email"];
            $array["EmailUpdated"] = "yes";
        } else {
            $array["Email"] = $customer_db["Email"];
            $array["EmailUpdated"] = "no";
        }

        if ($service["address1"] != $customer_db["Address1"] || $service["address2"] != $customer_db["Address2"] || $service["zip"] != $customer_db["Zip"] || $service["city"] != $customer_db["City"]) {
            $address = urldecode($service["address1"] . " " . $service["zip"] . " " . $service["city"] . ", AZ, USA");
            $arrayAddress = $this->curl->convertAddressToLatLong($address);

            $cityCode = $this->getCodeOFCity($service["city"]);
            if (!$cityCode) {
                $cityCode = NULL;
            }
            if ($service["address1"] == "0") {
                $service["address1"] = "";
            }
            if ($service["address2"] == "0") {
                $service["address2"] = "";
            }
            if ($service["city"] == "0") {
                $service["city"] = "";
            }
            if ($service["zip"] == "0") {
                $service["zip"] = "";
            }
            $customerArray = array(
                "Email" => $service["email"],
                "Address1" => $service["address1"],
                "Address2" => $service["address2"],
                "City" => $service["city"],
                "CityCode" => $cityCode,
                "Zip" => $service["zip"],
                "Date" => date("Y-m-d H:i:s"),
                "Longitude" => $arrayAddress["longitude"],
                "Latitude" => $arrayAddress["latitude"]
            );
            $address = "";
            if ($this->admin->UpdateAll("customer", $customerArray, array("CustomerID" => $customer_db["CustomerID"]))) {
                if ($service["address1"] == "" || $service["address1"] == "0" || $service["address1"] == null) {
                    
                } else {
                    $address .= $service["address1"];
                }
                if ($service["address2"] == "" || $service["address2"] == "0" || $service["address2"] == null) {
                    
                } else {
                    $address .= ", " . $service["address2"] . "";
                }
                if ($service["city"] == "" || $service["city"] == "0" || $service["city"] == null) {
                    
                } else {
                    $address .= ", " . $service["city"] . "";
                }
                if ($service["zip"] == "" || $service["zip"] == "0" || $service["zip"] == null) {
                    
                } else {
                    $address .= ", " . $service["zip"] . "";
                }

                $array["Address"] = urldecode($address);
                $array["AddressUpdated"] = "yes";
            }
        } else {
            $array["Address"] = urldecode($customer_db["Address1"] . " " . $customer_db["Address2"] . ", " . $customer_db["City"] . ", " . $service["zip"]);
            $array["AddressUpdated"] = "no";
        }

        if (!empty($array)) {
            echo json_encode($array);
        } else {
            echo "Already updated";
        }
    }

    public function searchCustomerByName() {
        $this->load->model("M_admin");
        $this->admin = new M_admin();
        if ($_GET['term']) {
            $customers = $this->admin->searchCustomerByName($_GET['term']);
            foreach ($customers as $customer) {
                $data[] = $customer['CustomerName'];
            }
            echo json_encode($data);
        }
    }

    public function forcePasswordReset($customer) {
        $this->load->model("M_admin");
        $this->admin = new M_admin();
        if ($user = $this->admin->isAdminLogin()) {
            $activity = $this->admin->orderActivity("5fc24e31a7af7", "50206e9a01d5e");
            $activityDetail = array(
                "MetaID" => strictUniqueID(),
                "ActivityLogID" => $activity,
                "ObjectID" => $customer,
                "Name" => "force_password_reset",
                "Value" => $user
            );
            $this->admin->Insert("activity_log_meta", $activityDetail);

            if ($this->admin->UpdateAll("customer", array("isPasswordReset" => 1), array("CustomerID" => $customer))) {
                $activities = $this->admin->getFPRActivities($customer);
                $activities = $this->load->view("admin/password-activity", ["activities" => $activities], TRUE);
                echo $activities;
            } else {
                echo "failed";
            }
        }else{
            echo "failed";
        }
    }

}
