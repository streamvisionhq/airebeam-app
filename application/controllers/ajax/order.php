<?php
/**
 * this class has all order related functionality
 * like add/edit order, get all prefilled fields data to show in appointment
 * all activities of an order, all detail of an order to show in edit appointment
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Order extends MY_Controller {

    public function __construct() {
        parent::__construct();
    }

    /**
     * handle ajax request to add order
     * @return json of all orders
     */
    public function addOrder() {
        $this->load->model("M_order");
        $this->order = new M_order();
        $reponse = $this->order->addOrder();
        if ( $reponse === TRUE ) {
            $allOrders = $this->order->getAll();
            $this->data["allOrders"] = $allOrders;

            $array["reponse"] = "success";
            $this->data["msg"] = "incident has been created";
            $array["showmsg"] = $this->load->view("admin/show-message", $this->data, true);
            echo json_encode($array);
        }
        else {
            $array["reponse"] = "error";
            $this->data["error"] = $reponse;
            $array["showmsg"] = $this->data;
            echo json_encode($array);
        }
    }

    /**
     * handle ajax request to edit order
     * @return json of all orders
     */
    public function editOrder() {
        $this->load->model("M_order");
        $this->order = new M_order();
        $response = $this->order->editOrder($_POST['orderID']);
        if ( $response === TRUE ) {
            echo "success";
        }
        else {
            echo $response;
        }
    }

    /**
     * updating order time when it is dropped in calendar
     */
    public function updateOrder() {
        $this->load->model("M_order");
        $this->order = new M_order();

        $workers = $this->order->getWorkers();
        $javaWorkers = $this->order->javaWorker($workers);

        // decode here ajax posted data
        $array = json_decode($this->input->post("detail"));
        $orderID = $array->orderID;
        $meridium = false;
        $orderData = $this->order->getOrder($orderID);

        $newOrderStart = new DateTime($array->newTime);
        $oldOrderStart = new DateTime($array->oldTime);
        $difference = $newOrderStart->diff($oldOrderStart);

        // calculating interval to add or subtract in duration
        $interval = date("H:i:s", strtotime($difference->h . ":" . $difference->i));

        // after drop any incident getting start & end time of order
        $newOrderStartTime = date("Y-m-d H:i:s", strtotime($array->newTime));
        $newOrderEndTime = date("Y-m-d H:i:s", strtotime(date("Y-m-d H:i:s", strtotime($array->newTime)) . "+" . $orderData["Duration"] . "MINUTES"));
        $currentDate = date("Y-m-d", strtotime($array->newTime));

        // before drop any incident getting start & end time of order
        $oldOrderStartTime = date("Y-m-d H:i:s", strtotime($array->oldTime));

        // if order is dropped on smaller then current time then adding - sign in interval
        if ( $oldOrderStartTime > $newOrderStartTime ) {
            $sign = "-";
        }
        else {
            $sign = FALSE;
        }

        // getting worker id by index in calendar's workers array
        $workerID = $javaWorkers[$array->userID];

        // getting start and end tme of all orders of current date
        $orders = $this->order->getOrderTimeWithDuration($currentDate);

        // check all possible senarios of incident overlapping in calendar
        $response = $this->order->checkOverlappingIncident($orders, $workerID, $orderID, $newOrderStartTime, $newOrderEndTime);

        if ( $response === true ) {
            $data["WorkerID"] = $workerID;
            $data["Date"] = $newOrderStartTime;
            // splitting order name by space
            $orderNameArray = explode(" ", $orderData["Name"]);
            // getting time convention (AM or PM) of dropped incident
            // if it is dropped in AM or PM time
            $orderNameArray[0] = getAMPM($newOrderStartTime);

            if ( $newOrderStartTime != $oldOrderStartTime ) {
                if ( $orderData["StatusID"] == "56a5c03ab920f" || $orderData["StatusID"] == "56a5c03acf58c" || $orderData["StatusID"] == "56a5c03adf54c" ) {
                    if ( $orderData["StatusID"] == "56a5c03adf54c" ) {
                        $data["StatusID"] = "56a5c03ab920f";
                    }

                    $this->order->removeAlert($orderID);
                    if ( $orderData["PhoneHome"] != "" && $orderData["PhoneHome"] != "0" ) {
                        $this->order->addAlert($orderData, "change", $newOrderStartTime);
                    }
                    if ( $orderData["isPhoneHomeLandline"] == "1" ) {
                        $this->order->deleteMessageEntry($orderID);
                    }
                    else {
                        if ( $this->order->getMessageEntry($orderID) ) {
                            if ( $orderData["PhoneHome"] != "" && $orderData["PhoneHome"] != "0" ) {
                                $this->order->updateMessageEntry($orderID);
                            }
                        }
                        else {
                            if ( $orderData["PhoneHome"] != "" && $orderData["PhoneHome"] != "0" ) {
                                $this->order->addMessageEntry($orderID);
                            }
                        }
                    }
                }
            }

            // updating time convention (AM or PM) of dropped incident
            // if it is dropped in AM or PM time
            $data["Name"] = implode(" ", $orderNameArray);
            $this->order->updateOrderTime($orderID, $data);

            $editOrderActivity = array(
                  "OrderID" => $orderID,
                  "AssignedBy" => $this->order->isAdminLogin()
            );
            $this->order->editOrderAllActivities($orderData, $data, $editOrderActivity, $meridium);

            $responsearray["reponse"] = "success";
        }
        else {
            $responsearray["reponse"] = "error";
            $responsearray["msg"] = $response;
        }
        echo json_encode($responsearray);
    }

    /**
     * updating order time when it is extend in calendar
     */
    public function updateOrderNewTime() {
        $this->load->model("M_order");
        $this->order = new M_order();

        // ajax sent parameter contains orderID,addTime,startTime,endTime and userID
        $array = json_decode($this->input->post("detail"));
        $orderID = $array->orderID;
        $orderDetail = $this->order->getOrder($orderID); // fetch order detail from db by order id
        // convert calendar return miliseconds time to
        // minutes to add interval in order duration
        $data["Duration"] = $orderDetail["Duration"] + ($array->addTime / 1000) / 60;

        // converting time in current date and start & end time to formated time
        $currentDate = date("Y-m-d", strtotime($array->startTime));
        $startTime = date("Y-m-d H:i:s", strtotime($array->startTime));
        $endTime = date("Y-m-d H:i:s", strtotime($array->endTime));

        // on 12 pm calendar return 00:00:00, here subtracting
        // 1 second to fit in overlapping conditions
        if ( $endTime >= date("Y-m-d 00:00:00") && $endTime < date("Y-m-d 02:00:00") ) {
            $endTime = date("Y-m-d 23:59:59");
            if ( $endTime == date("Y-m-d 00:00:00") ) {
                $data["Duration"] = $data["Duration"] - 30;
            }
        }

        $workers = $this->order->getWorkers();
        $javaWorkers = $this->order->javaWorker($workers);
        $workerID = $javaWorkers[$array->userID];

        $orders = $this->order->getOrderTimeWithDuration($currentDate);

        // check all possible senarios of incident overlapping in calendar
        $response = $this->order->checkOverlappingIncident($orders, $workerID, $orderID, $startTime, $endTime);
        if ( $response === true ) {
            $data["WorkerID"] = $workerID;
            $this->order->addOrderDuration($orderID, $data);
            if ( $this->order->getAlertOfOrder($orderID) ) {
                $data = $this->order->getAlertOfOrder($orderID);
                if ( $data[0]["TypeID"] == "7e3c597bd72e1" ) {
                    $this->order->removeAlert($orderID);
                    $this->order->addAlert($orderDetail, "create", $endTime);
                }
            }
            $editOrderActivity = array(
                  "OrderID" => $orderID,
                  "AssignedBy" => $this->order->isAdminLogin()
            );
            $this->order->editOrderAllActivities($orderDetail, $data, $editOrderActivity);
            $responsearray["reponse"] = "success";
            echo json_encode($responsearray);
        }
        else {
            $responsearray["reponse"] = "error";
            $responsearray["msg"] = $response;
            echo json_encode($responsearray);
        }
    }

    /**
     * $note getting prefilled data for incident like workers,order types,statuses
     */
    public function getPrefilledData() {
        $this->load->model("M_order");
        $this->order = new M_order();
        $detail = json_decode($this->input->post("detail"));
        $workers = $this->order->getWorkers();
        $javaWorkers = $this->order->javaWorker($workers);
        $workerIndex = $detail->userID;
        $dateArray = explode(" ", $detail->start);
        $date = $dateArray[0];
        $time = $dateArray[1];

        $array["date"] = date("m/d/Y", strtotime($date));
        $array["time"] = date("h:i A", strtotime($time));

        $optionsHtml = "<option value=''>Select Technician</option>";
        foreach ( $workers as $key => $worker ) {
            $selected = "";
            if ( $javaWorkers[$workerIndex] == $key ) {
                $selected = "selected";
            }
            $optionsHtml .= "<option value='$key' $selected>$worker</option>";
        }
        $array["workers"] = $optionsHtml;
        echo json_encode($array);
    }

    public function getAllActivitiesOfOrder($orderID) {
        $this->load->model("M_order");
        $this->order = new M_order();
        $activitiesHtml = NULL;
        if ( $this->order->isAdminLogin() ) {
            $response = $this->order->getAllActivitiesOfOrder($orderID);
            $result = $this->order->mergingAcitivitiesWithNotes($response);
            if ( $result ) {
                $this->data["notesHtml"] = $this->createActivitiesHtml($result);
            }
            echo json_encode($this->data);
        }
    }

    public function getOrderDetail($orderID) {
        $this->load->model("M_order");
        $this->order = new M_order();
        $this->load->model("M_admin");
        $this->admin = new M_admin();
        $address = "";

        $servicesHtml = NULL;
        $statusesHtml = NULL;
        $workersHtml = NULL;
        $durationsHtml = NULL;

        $order = $this->order->getOrder($orderID);

        $order["Previous"] = "1";
        if ( date("Y-m-d H:i:s", strtotime($order["Date"])) > date("Y-m-d H:i:s") ) {
            $order["Previous"] = "0";
        }

        $alerts = $this->order->getAlertOfOrder($orderID);
        if ( !empty($alerts) ) {
            foreach ( (array) $alerts as $row ) {
                $orderData = $this->order->getOrder($row["ObjectID"]);
                $this->data['order'] = $orderData;
                $this->data["row"] = $row;
                $this->data["alertHtml"] = $this->load->view("admin/alert", $this->data, true);
            }
        }
        else {
            $this->data["alertHtml"] = "";
        }

        //Services
        $services = $this->order->getServices();
        $this->data["servicesHtml"] = $this->createSelectOptions($services, $order['TypeID']);

        //Statuses
        $statuses = $this->order->getStatuses();
        $this->data["statusesHtml"] = $this->createSelectOptions($statuses, $order['StatusID']);


        //Workers
        $workers = $this->order->getWorkers();
        $this->data["workersHtml"] = $this->createSelectOptions($workers, $order['WorkerID']);

        //Durations
        $durations = $this->order->getDurations();
        $this->data["durationsHtml"] = $this->createSelectOptionsForDuration($durations, $order['Duration']);

        $this->data["orderName"] = $order['Name'];
        $this->data["orderDetail"] = $order['Detail'];
        $this->data["orderDate"] = date("m/d/Y", strtotime($order["Date"]));
        $this->data["orderTime"] = showTime($order["Date"]);

        $customer = $this->admin->getCustomerDetail($order['CustomerID']);

        $this->data["customerID"] = $order['CustomerID'];
        $this->data["customerName"] = $customer[0]['CustomerName'];

        if ( $customer[0]['Address1'] == "" || $customer[0]['Address1'] == "0" || $customer[0]['Address1'] == null ) {
            
        }
        else {
            $address .= $customer[0]['Address1'];
        }

        if ( $customer[0]['Address2'] == "" || $customer[0]['Address2'] == "0" || $customer[0]['Address2'] == NULL ) {
            
        }
        else {
            $address .= ", " . $customer[0]['Address2'] . "";
        }

        if ( $customer[0]['City'] == "" || $customer[0]['City'] == "0" || $customer[0]['City'] == null ) {
            
        }
        else {
            $address .= ", " . $customer[0]['City'] . "";
        }

        if ( $customer[0]['Zip'] == "" || $customer[0]['Zip'] == "0" || $customer[0]['Zip'] == null ) {
            
        }
        else {
            $address .= ", " . $customer[0]['Zip'] . "";
        }

        $this->data["customerAddress"] = $address;

        if ( empty($customer[0]['Email']) || $customer[0]['Email'] == "0" ) {
            $customer[0]['Email'] = "";
            $this->data["emailErrorHtml"] = "Email address is empty";
        }
        $this->data["customerEmail"] = $customer[0]['Email'];

        if ( empty($customer[0]['PhoneHome']) || $customer[0]['PhoneHome'] == "0" ) {
            $customer[0]['PhoneHome'] = "";
            $this->data["phoneErrorHtml"] = "Phone number is empty";
        }

        $this->data["customerPhone"] = $customer[0]['PhoneHome'];

        if ( $order['isPhoneHomeLandline'] ) {
            $this->data['isPhoneHomeLandline'] = 1;
        }

        $this->data["orderID"] = $order['OrderID'];

        //Notes
        $notes = $this->order->getNotesByOrder($orderID);
        $notesHtml = NULL;
        $this->data["notesHtml"] = $this->createNotesHtml($notes);
        echo json_encode($this->data);
    }

    /**
     * create select drop down html
     * @param array $array
     * @param string $idArray
     * @return html
     */
    public function createSelectOptions($array, $idArray) {
        $html = "";
        foreach ( $array as $key => $arr ) {
            $selected = "";
            if ( $idArray == $key ) {
                $selected = " selected";
            }
            $html .= "<option value='$key'$selected>$arr</option>";
        }
        return $html;
    }

    /**
     * create duration select drop down html
     * @param array $array
     * @param string $idArray
     * @return html
     */
    public function createSelectOptionsForDuration($array, $idArray) {
        $html = "";
        foreach ( $array as $arr ) {
            $selected = "";
            if ( $idArray == $arr ) {
                $selected = " selected";
            }
            $html .= "<option value='$arr'$selected>$arr</option>";
        }
        return $html;
    }

    /**
     * create notes html to show in order popup in calendar
     * @param array $array
     * @return html
     */
    public function createNotesHtml($array) {
        $notesHtml = "";
        foreach ( $array as $note ) {
            $notesHtml .= "<li>";
            $notesHtml .= '<div class="search-info notes-container">';
            switch ( $note["Label"] ) {
                case 'Note':
                    $class = "blue";
                    break;
                case 'Done':
                    $class = "green";
                    break;
                case 'Abort':
                    $class = "red";
                    break;
            }
            $notesHtml .= '<p><b>' . $note['Worker'] . ':</b><span class="' . $class . '">(' . $note["Label"] . ')</span></p>';
            $notesHtml .= '<p>' . $note['Note'] . '</p>';
            $notesHtml .= '<div class="worker-info">';
            $notesHtml .= '<ul>';
            $notesHtml .= '<li>' . getOrderDate($note["Date"]) . '</li>';
            $notesHtml .= '<li>' . showTime($note["Date"]) . '</li>';
            $notesHtml .= '</ul>';
            $notesHtml .= '</div>';
            $notesHtml .= '</div>';
            $notesHtml .= '</li>';
        }
        return $notesHtml;
    }

    public function addOrderEmailActivity($orderID) {
        $this->load->model("M_order");
        $this->order = new M_order();
        $this->order->addSingleActivity("8fc24e31a7af7", "40d64aeeb308b", $orderID, "empty_email_address", "yes");
    }

    /**
     * create activities html to show in order popup in calendar
     * @param array $array
     * @return html
     */
    public function createActivitiesHtml($array) {
        $this->load->model("M_order");
        $this->order = new M_order();
        $assignedBy = "";
        $activityID = "";
        $activitiesHtml = "";
        $firstActivityHtml = "";
        $secondActivityHtml = "";

        foreach ( $array as $val ) {
            if ( isset($val["ActivityLogID"]) ) {
                $activityID = $val["ActivityLogID"];
            }
            $separateActivity[$activityID][] = $val;
        }

        $msgArray = $this->order->createActivitiesArray($separateActivity);

        foreach ( $msgArray as $ke => $va ) {
            if ( isset($va["order_created_by"]) ) {
                unset($msgArray[$ke]);
                $firstActivityHtml = '<li class="activity-log">
                    <div class="activity-title"><b>Order Created by</b> <span>' . $va["order_created_by"] . '</span> ' . $va["time_stamp"] . '</div>
                    <div class="activity-content"></div>';
                '</li>';
            }
        }

        foreach ( $msgArray as $kei => $vai ) {
            if ( isset($vai["assigned_by"]) && isset($vai["assigned_to"]) ) {
                unset($msgArray[$kei]);
                $secondActivityHtml = '<li class="activity-log">
                    <div class="activity-title"><b>Assigned by</b> <span>' . $vai["assigned_by"] . '</span> Assigned To <span>' . $vai["assigned_to"] . '</span>' . $vai["time_stamp"] . '</div>
                    <div class="activity-content"></div>';
                '</li>';
            }
        }

        foreach ( $msgArray as $key => $s ) {
            $string = "";

            if ( isset($s["assigned_by"]) ) {
                $assignedBy = "<b>Assigned by</b> <span>" . $s["assigned_by"] . "</span>";
            }
            elseif ( isset($s["edited_by"]) ) {
                $assignedBy = "<b>Edited by</b> <span>" . $s["edited_by"] . "</span>";
            }
            elseif ( isset($s["action_performed_by"]) ) {
                $assignedBy = "<b>Action performed by</b> <span>" . $s["action_performed_by"] . "</span>";
            }

            if ( isset($s["assigned_to"]) ) {
                $assignedTo = " Assigned To <span>" . $s["assigned_to"] . "</span>";
            }
            else {
                $assignedTo = "";
            }

            if ( isset($s["sms_sent"]) ) {
                $assignedBy = "<b>SMS Sent by</b> <span>" . $msgArray[$key]["assigned_by"] . "</span>";
            }            
            if ( isset($s["sms_failed"]) ) {
                $assignedBy = "<b>SMS Sending Failed:</b> <span>Error Recieved \"" . $msgArray[$key]["sms_failed"] . "\"</span>";
            }
            if ( isset($s["customer_notified"]) ) {
                $assignedBy = "<b>Customer is notified by </b> <span>" . $msgArray[$key]["assigned_by"] . "</span>";
            }
            if ( isset($s["click_on_cancel_sms_button"]) ) {
                $assignedBy = "<b>SMS Notification Removed by</b> <span>" . $msgArray[$key]["action_performed_by"] . "</span>";
            }
            if ( isset($s["enroute_sms_sent"]) ) {
                $assignedBy = "<b>Enroute SMS Sent by</b> <span>" . $msgArray[$key]["assigned_by"] . "</span>";
            }
            if ( isset($s["survey_sms_sent"]) ) {
                $assignedBy = $msgArray[$key]["survey_sms_sent"];
            }
            if ( isset($s["survey_sms_success"]) ) {
                $assignedBy = "<b>Review SMS successfully sent</b> <span>" . $msgArray[$key]["survey_sms_success"] . "</span>";
            }
            if ( isset($s["survey_sms_failed"]) ) {
                $assignedBy = "<b>Review SMS Sending Failed:</b> <span>Error Recieved \"" . $msgArray[$key]["survey_sms_failed"] . "\"</span>";
            }
            if ( isset($s["survey_email_sent"]) ) {
                $assignedBy = "<b>Review Email Sent by</b> <span>" . $msgArray[$key]["assigned_by"] . "</span>";
            }

            if ( isset($s["customer_updated"]) ) {
                $fieldsData = json_decode($s["customer_updated"]);

                foreach ( $fieldsData as $entity ) {
                    if ( $entity == "PhoneHome" ) {
                        $string .= "Home Phone Updated</br>";
                    }
                    else {
                        $string .= $entity . " Updated</br>";
                    }
                }

                $assignedBy = "<b>Customer Information Updated</b> ";
            }
            if ( isset($s["sms_rejected"]) ) {
                $assignedBy = "<b>Order Time Rejected by</b> <span> Customer </span>";
            }
            if ( isset($s["empty_phone_number"]) ) {
                $assignedBy = "<b>Phone Number is empty</b> <span> thats why SMS notification not generated</span>";
            }
            if ( isset($s["empty_email_address"]) ) {
                $assignedBy = "<b>Email Address is empty</b> <span> thats why survey not added in pending survey list</span>";
            }
            if ( isset($s["note_added"]) ) {
                $assignedBy = "<b>Note Added by</b> <span>" . $msgArray[$key]["assigned_by"] . "</span>";
            }
            if ( isset($s["order_created_by"]) ) {
                $assignedBy = "<b>Order Created by</b> <span>" . $s["order_created_by"] . "</span>";
                $assignedTo = false;
            }

            if ( isset($s["fields"]) ) {
                foreach ( $s["fields"] as $k => $f ) {
                    if ( $k == "meridiem" ) {
                        $string .= "Changed " . $k . "</br>";
                    }
                    elseif ( $k == "click_on_done_button" ) {
                        $string .= "Clicked on Done Button</br>";
                    }
                    else {
                        $string .= "Changed " . $k . " to " . $f . "</br>";
                    }
                }
            }

            if ( isset($s["empty_phone_number"]) || isset($s["empty_email_address"]) || isset($s["order_created_by"]) || isset($s["assigned_by"]) || isset($s["assigned_to"]) || isset($s["action_performed_by"]) || isset($s["note_added"]) || isset($s["fields"]) || isset($s["sms_rejected"]) || isset($s["sms_failed"]) || isset($s["survey_sms_sent"]) || isset($s["survey_sms_success"]) || isset($s["survey_sms_failed"]) || isset($s["survey_email_sent"]) ) {
                $activitiesHtml .= '<li class="activity-log">
                    <div class="activity-title">' . $assignedBy . $assignedTo . ' ' . $s["time_stamp"] . '</div>
                    <div class="activity-content">' . $string . '</div>';
                '</li>';
            }

            if ( isset($s["note"]) ) {
                $val = $s["note"];
                $activitiesHtml .= "<li>";
                $activitiesHtml .= '<div class="search-info notes-container">';
                switch ( $val["Label"] ) {
                    case 'Note':
                        $class = "blue";
                        break;
                    case 'Done':
                        $class = "green";
                        break;
                    case 'Abort':
                        $class = "red";
                        break;
                }
                $activitiesHtml .= '<p><b>' . $val['Worker'] . ':</b><span class="' . $class . '">(' . $val["Label"] . ')</span></p>';
                $activitiesHtml .= '<p>' . $val['Note'] . '</p>';
                $activitiesHtml .= '<div class="worker-info">';
                $activitiesHtml .= '<ul>';
                $activitiesHtml .= '<li>' . getOrderDate($val["Date"]) . '</li>';
                $activitiesHtml .= '<li>' . showTime($val["Date"]) . '</li>';
                $activitiesHtml .= '</ul>';
                $activitiesHtml .= '</div>';
                $activitiesHtml .= '</div>';
                $activitiesHtml .= '</li>';
            }
        }
        return $activitiesHtml . $secondActivityHtml . $firstActivityHtml;
    }

    public function doneOrder() {
        $this->load->model("M_order");
        $this->order = new M_order();
        $this->load->model("M_curl");
        $this->curl = new M_curl();

        $this->curl->logTime("start time for process order");
        if ( $this->input->post("label") == "complete" || $this->input->post("label") == "cancel-enroute-complete" || $this->input->post("label") == "enroute-complete" ) {
            $this->curl->pushDoneToEmerald();
        }
        $this->curl->logTime("end time for process order");
        echo "success";
    }

    public function sendSurveyToEmail() {
        $response = $this->sendServey($_POST["orderID"], $_POST["app"]);
        echo $response;
    }

    public function sendAllSurveyToEmail() {
        $this->load->model("M_order");
        $this->order = new M_order();

        if ( $this->input->post("single-survey") ) {
            foreach ( $this->input->post("single-survey") as $item ) {
                if ( !$this->sendServey($item) ) {
                    $error = "Error in sending email.";
                }
            }
            if ( !empty($error) ) {
                echo json_encode(array(
                      "status" => "fail",
                      "message" => $error,
                      "response" => $error
                ));
            }
            else {
                $data["surveys"] = $this->order->getAllQueuedSurveys();
                echo json_encode(array(
                      "status" => "success",
                      "message" => EMAIL_SENT_SUCCESSFULL,
                      "response" => $this->load->view("admin/survey-list-rows", $data, true)
                ));
            }
        }
        else {
            echo json_encode(array(
                  "status" => "fail",
                  "message" => "You did not select any survey.",
                  "response" => "You did not select any survey."
            ));
        }
    }

    public function removeSurvey($orderID) {
        $this->load->model("M_order");
        $this->order = new M_order();
        $order = $this->order->getOrder($orderID);
        $this->order->removeSurveyFromQueue($orderID);
        echo $orderID;
    }

    public function sendSurveySms($orderID){
        $this->load->model("m_twilio");
        $this->twil = new M_twilio();

        $order = $this->twil->getOrder($orderID);

        $message_text = 'Thank you for choosing Airebeam. Please give us a review at ';
        $message = $message_text . 'http://airebeam.com/review';
        
        $response = $this->twil->sendSMS(FROM, simplizePhone($order["PhoneHome"]), $message);

        // Add send survey sms message in logs
        $activity = array(
              "OrderID" => $orderID,
              "AssignedBy" => $this->twil->isAdminLogin(),
              "Name" => "survey_sms_sent",
              "Message" => '<b>Review SMS sent by</b> <span>' . $order['Worker'] . '</span>',
        );
        $this->twil->addMessageAllActivities($activity);

        if ( $response ) {
            if ( $response["Status"] == "success" ) {

                $this->twil->updateMessageStatus($orderID, "eefe80e2ade65");
                $array = array(
                      "status" => "success",
                      "message" => SENT_SUCCESSFULLY
                );
                $this->twil->UpdateAll("alert", array("isVisible" => "0"), array("ObjectID" => $orderID));

                $activity = array(
                      "OrderID" => $orderID,
                      "AssignedBy" => $this->twil->isWorkerLogin(),
                      "Name" => "survey_sms_success",
                      "Message" => $message,
                );
                $this->twil->addMessageAllActivities($activity);

                $this->twil->removeSurveyFromQueue($orderID);

                echo json_encode($array);
            }
            else {
                $status = $response['Status'];
                $error_message = $response['Message'];
                $error_code = $response['ErrorCode'];
                $sid = $response['Sid'];
                $twilio_sms_log_url = 'https://www.twilio.com/console/sms/logs/'.$sid;

                if ($error_code == '404') {
                    $message_redirect = $error_code . ' ' . $error_message;
                }else{
                    $message_redirect = "<a href='".$twilio_sms_log_url."'>" . $error_code . ' '. $error_message . "</a>";
                }
                $array = array(
                      "status" => $status,
                      "message" => $error_code.' '.$error_message
                );

                $msg = "Review SMS Sending Failed on " . $order["Name"] . ", " . $message_redirect;

                $activity = array(
                      "OrderID" => $orderID,
                      "AssignedBy" => $this->twil->isWorkerLogin(),
                      "Name" => "survey_sms_failed",
                      "Message" => $msg,
                );
                $this->twil->addMessageAllActivities($activity);

                // Now send Email
                $subject = 'Survey Form';
                $from = FROM_AIREBEAM;
                $body = $message_text . '<a>http://airebeam.com/review</a>';
                $email = sendEmail($order['Email'], $subject, $body, $from); 

                $this->twil->removeSurveyFromQueue($orderID);

                $addSurveyActivity = array(
                    "OrderID" => $orderID,
                    "AssignedBy" => $this->twil->isWorkerLogin(),
                );
                $this->twil->addSurveyAllActivities($addSurveyActivity);


                echo json_encode($email);
            }
        }
        else {
            $array = array(
                  "status" => "fail",
                  "message" => ERROR_DEFAULT
            );
            echo json_encode($array);
        }
    }

    public function addPendingSurvery($orderID){

        $this->load->model("M_order");
        $this->order = new M_order();

        $data = array(
              "SurveyID" => uniqid(),
              "OrderID" => $orderID,
              "read" => 0,
              "Date" => date("Y-m-d H:i:s")
        );
        $this->order->Insert("survey_queue", $data);
    }

}
