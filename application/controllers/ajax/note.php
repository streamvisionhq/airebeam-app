<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Note extends MY_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function getNotes() {
        $this->load->model("M_order");
        $this->order = new M_order();
        $response = $this->order->getAllNewNotesNumber();
        if($response){
            echo intval($response);
        }
    }

}
