<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Holidays extends MY_Controller {
	public function __construct() {
		parent::__construct();
	}
	
	public function add_holiday() {
		$this->load->model("M_holidays");
		$this->holidays = new M_holidays();
		$holiday_date = $this->input->get("date");
		$holiday_date_obj = new DateTime($holiday_date);
		$holiday_label = $this->input->get("label");
		if( $this->holidays->InsertHoliday($holiday_label, $holiday_date_obj->format('Y-m-d')) ) {
			$lastHolidayId = $this->holidays->LastHoliday()[0]->Id;
			$response = array(
				"date" 		=> $holiday_date_obj->format('Y-m-d'),
				"label" 	=> $holiday_label,
				"Id"		=> $lastHolidayId
			);
			echo json_encode($response);
		} else {
			echo json_encode(array());
		}

	}
}