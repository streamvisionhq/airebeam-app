<?php

/*
 * This class validates the Airebeam customer portal mbr address,phone and credit card digits in our database
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class MBR_Forgot_Password extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library("CustomerAccessOK");
        $this->customerAccessOK = new CustomerAccessOK();
    }

    /**
     * Make response string
     *
     * @desc Found status in statuses array and get message
     *
     * @param <string> Status
     * @return <string> Message
     */
    public function index() {
        if ($this->input->post("credit_card")) {
            $query_param = $condition = $phone_sql = $phone_flag = $address_sql = $validate = $where = $address = $phone = $matches = false;
            $phone = clean_string($this->input->post("phone"));

            if (isBase64Encoded($this->input->post("address"))) {
                $address = base64_decode($this->input->post("address"));
            }

            if ($phone || $address) {

                $sql = 'SELECT * FROM customer
                    INNER JOIN service ON customer.CustomerID = service.CustomerID
                WHERE customer.CreditCardLastFour = "' . $this->input->post("credit_card") . '" AND customer.CreditCardLastFour != "0"';

                if ($this->input->post("phone")) {
                    $phone_flag = true;
                    $phone_sql = $this->make_phone_query($phone);
                }

                if ($address) {
                    $address_sql = $this->make_address_query($address);
                }

                if ($phone_sql && $address_sql) {
                    $sql .= " AND (" . $phone_sql . " OR " . $address_sql . ")";
                } elseif ($address_sql) {
                    $sql .= " AND " . $address_sql;
                } elseif ($phone_sql) {
                    $sql .= " AND " . $phone_sql;
                }

                $query = $this->db->query($sql);
                $result = $query->first_row();

                if (!empty($result)) {
                    $this->db->select('*');
                    $this->db->where(["CustomerID" => $result->CustomerID, "Title" => "0-CustomerAccessOK"]);
                    $service_result = $this->db->get('service')->first_row();
                    if (!empty($service_result)) {
                        if ($service_result->Status != "active") {
                            echo json_encode($this->customerAccessOK->returnResponse("3"));
                            die();
                        }
                    } else {
                        echo json_encode($this->customerAccessOK->returnResponse("3"));
                    }
                    if ($phone_sql && !$address_sql) {
                        $validate = true;
                        if ($phone != clean_string($result->PhoneHome)) {
                            $validate = false;
                        }
                    } else {
                        $validate = true;
                    }
                }

                if ($validate) {
                    if ($address) {
                        $response = $this->validate_address($address, $result);
                        if ($response) {
                            $return = ["MBR" => $result->CustomerID, "status" => "1", "message" => SUCCESSFULL_MBR];
                            $termsData = $this->customerAccessOK->checkTermsAccepted($result->CustomerID);
                            if ($termsData) {
                                $return["terms-conditions"] = $this->customerAccessOK->term_url;
                            }
                            if ($result->isPasswordReset == "1") {
                                if ($token = $this->customerAccessOK->insertGeneralToken($result)) {
                                    $return["token"] = $token;
                                }
                            }
                            echo json_encode($return);
                        } elseif ($phone_flag) {
                            $return = ["MBR" => $result->CustomerID, "status" => "1", "message" => SUCCESSFULL_MBR];
                            $termsData = $this->customerAccessOK->checkTermsAccepted($result->CustomerID);
                            if ($termsData) {
                                $return["terms-conditions"] = $this->customerAccessOK->term_url;
                            }
                            if ($token = $this->customerAccessOK->insertGeneralToken($result)) {
                                $return["token"] = $token;
                            }
                            echo json_encode($return);
                        } else {
                            echo json_encode($this->customerAccessOK->returnResponse("3"));
                        }
                    } else {
                        $return = ["MBR" => $result->CustomerID, "status" => "1", "message" => SUCCESSFULL_MBR];
                        $termsData = $this->customerAccessOK->checkTermsAccepted($result->CustomerID);
                        if ($termsData) {
                            $return["terms-conditions"] = $this->customerAccessOK->term_url;
                        }
                        if ($token = $this->customerAccessOK->insertGeneralToken($result)) {
                            $return["token"] = $token;
                        }
                        echo json_encode($return);
                    }
                } else {
                    echo json_encode($this->customerAccessOK->returnResponse("3"));
                }
            } elseif (!isBase64Encoded($this->input->post("address")) && empty($phone)) {
                echo json_encode($this->customerAccessOK->returnResponse("3"));
            } else {
                echo json_encode($this->customerAccessOK->returnResponse("8"));
            }
        } else {
            echo json_encode($this->customerAccessOK->returnResponse("9"));
        }
    }

    public function make_phone_query($phone) {
        return 'REPLACE(REPLACE(REPLACE(REPLACE(customer.PhoneHome,"(",""),")",""),"-","")," ","") LIKE "%' . $phone . '%"';
    }

    public function make_address_query($address) {
        $matches = $matches_clone = array_unique(explode(" ", strtolower($address)));
        $condition = false;

        foreach ($matches as $en) {
            $this->db->select('Suffix,Abbreviation');
            $this->db->where('Suffix', $en)->or_where('Abbreviation', $en);
            $response = $this->db->get('suffix')->first_row();
            if (!empty($response)) {
                if (($key = array_search($en, $matches_clone)) !== false) {
                    unset($matches_clone[$key]);
                }
                $address_array[] = strtolower($response->Suffix);
                $address_array[] = strtolower($response->Abbreviation);
            }
        }

        foreach ($matches as $en) {
            $this->db->select('Pole,Abbreviation');
            $this->db->where('Pole', $en)->or_where('Abbreviation', $en);
            $response = $this->db->get('pole')->first_row();
            if (!empty($response)) {
                if (($key = array_search($en, $matches_clone)) !== false) {
                    unset($matches_clone[$key]);
                }
                $address_array[] = strtolower($response->Pole);
                $address_array[] = strtolower($response->Abbreviation);
            }
        }

        if (!empty($matches_clone)) {
            foreach ($matches_clone as $param) {
                $query_params[] = "Address1 LIKE '%" . $param . "%'";
            }

            $condition = implode(" AND ", $query_params);
        }

        return $condition;
    }

    public function validate_address($address, $customer) {
        $address_array = array_unique(explode(" ", $address));
        $mbr_address = $customer->Address1;
        $mbr_address_array = explode(" ", $mbr_address);

        foreach ($mbr_address_array as &$value) {
            $value = strtolower($value);
        }

        $total_parts = count($mbr_address_array);
        $matched = 0;

        foreach ($address_array as $key => $p) {
            if (in_array(strtolower($p), $mbr_address_array)) {
                $matched++;
            } else {
                if ($this->match_suffix($p)) {
                    $matched++;
                } elseif ($this->match_pole($p)) {
                    $matched++;
                }
            }
        }

        if ($total_parts == 2 && $matched >= 2) {
            $validate = true;
        } elseif ($total_parts == 3 && $matched >= 3) {
            $validate = true;
        } elseif ($total_parts > 3 && $matched >= 3) {
            $validate = true;
        } else {
            $validate = false;
        }

        if ($validate) {
            return $customer;
        }
    }

    public function match_suffix($param) {
        $this->db->select('Suffix,Abbreviation');
        $this->db->where('Suffix', $param)->or_where('Abbreviation', $param);
        $response = $this->db->get('suffix')->first_row();
        if (!empty($response)) {
            return true;
        } else {
            return false;
        }
    }

    public function match_pole($param) {
        $this->db->select('Pole,Abbreviation');
        $this->db->where('Pole', $param)->or_where('Abbreviation', $param);
        $response = $this->db->get('pole')->first_row();
        if (!empty($response)) {
            return true;
        } else {
            return false;
        }
    }

}
