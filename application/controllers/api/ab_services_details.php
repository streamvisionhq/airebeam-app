<?php

/*
 * This class syncs the Airebeam customer mbr credit card in our database
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class AB_Services_Details extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library("CustomerAccessOK");
        $this->customerAccessOK = new CustomerAccessOK();
    }

    /**
     * Fetch all Services of given MBR
     *
     * @param <string> MBR
     * @return <json> Services
     */
    public function get($MBR) {
        $services = [];
        $result = $this->db->select("*")->get_where("service", ["CustomerID" => $MBR, "Status" => "active"])->result();
        if (!empty($result)) {
            foreach ($result as $service) {
                $services[] = [
                      "ServiceID" => $service->ServiceID,
                      "Title" => $service->Title,
                      "Status" => $service->Status,
                ];
            }
            echo json_encode(["status" => "1", "services" => $services]);
        } else {
            echo json_encode($this->customerAccessOK->returnResponse("7"));
        }
    }

}
