<?php

/*
 * This class syncs the Airebeam customer mbr credit card in our database
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Create_Services extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model("M_curl");
        $this->curl = new M_curl();
        $this->load->model("M_admin");
        $this->admin = new M_admin();
        $this->load->library("Emerald/EmeraldApi");
        $this->emerald_api = new EmeraldApi();
    }

    public function post() {
        if ($this->input->post("first_name") && $this->input->post("last_name") && $this->input->post("email") && $this->input->post("address") && $this->input->post("phone_home") && $this->input->post("state") && $this->input->post("zip") && $this->input->post("city")) {

            $first_name = $this->input->post("first_name");
            $last_name = $this->input->post("last_name");
            $email = $this->input->post("email");
            $address = $this->input->post("address");
            $phone_home = $this->input->post("phone_home");
            $state = $this->input->post("state");
            $zip = $this->input->post("zip");
            $zip_result = $this->admin->IsZipCodeExist((int) $zip);
            $city = $this->input->post("city");
            $services = $this->input->post("services");
            $service_start_date = $this->input->post("service_start_date");
            $package_id = $this->input->post("package_id");
            $package_services = $this->input->post("package_services");
            $ticket_id = $this->input->post("ticket_id");
            $tmp_mbr = $this->input->post("tmp_mbr");

            $this->admin->logTime("Create Services Endpoint Request Data " . json_encode($_POST));

            // Adding cc detail to save with mbr in emerald
            $cc = array(
                'number' => $this->input->post("cc_number"),
                'expiration_date' => $this->input->post("cc_expiration_date"),
                'security_code' => $this->input->post("cc_security_code"),
                'card_holder' => $this->input->post("cc_name")
            );

            $this->admin->logTime("MBR creation process started");

            if ($service_start_date != '') {
                $service_start_date = date('M d y', strtotime($service_start_date));
            }

            $resp = $this->curl->createMBR($first_name, $last_name, $email, $address, $phone_home, $state, $zip, $city, $cc, $service_start_date);

            if ($resp[CREATE_MBR_RETCODE_KEY] == 0) {
                $_POST['customer_id'] = $customer_id = $resp[CUSTOMER_ID_KEY];
                $customerAccessOk = "77";
                $login = $email;
                $password = $phone_home;
                $this->admin->logTime("CustomerAccessOk creation process started");
                $customerAccessServiceResp = $this->emerald_api->addService($customerAccessOk, $customer_id, $first_name, $last_name, $email, $login, $password, $service_start_date);
                $customerAccessServiceResp = xml_to_json($customerAccessServiceResp);
                $this->admin->logTime("CustomerAccessOk Created: " . $customerAccessServiceResp);
                $this->create_other_services($_POST);
                $this->create_market_tag($_POST);
                $this->add_activation_fee($_POST);
            }

            if (isset($customer_id)) {
                $_POST['customer_id'] = $customer_id;
                $this->admin->UpdateTempMbr($tmp_mbr, ['Mbr' => '0', 'Request' => json_encode($_POST)]);
            }

            if (!$zip_result) {
                $key_names = array(
                    'DRIVE_TIME_NOTIFY_EMAIL',
                );
                $Settings = $this->admin->GetConfigSettings($key_names);
                $notify_email = $Settings[0]->Name == 'DRIVE_TIME_NOTIFY_EMAIL' ? $Settings[0]->Value : '';
                $email_array = (explode(",", $notify_email));
                foreach ($email_array as $email) {
                    $subject = ZIP_NOT_EXIST;
                    $body = '<p> Please check this zip code(' . $zip . '). This zip code does not exist in our system. The account(' . $resp[CUSTOMER_ID_KEY] . ') is created with this zip </p>';
                    $from = FROM_AIREBEAM;
                    sendEmail($email, $subject, $body, $from);
                    $this->admin->logTime("Sent zip not found email to : " . $email);
                }
            }

            if ($ticket_id && isset($customer_id)) {
                $data = [
                    "subject" => $customer_id . ", " . $last_name . ", " . $first_name . " - Online Service Order - Broadband"
                ];
                $this->admin->updateTicket($ticket_id, $data);
            }

            $this->admin->logTime("Create services finished");

            echo '<mbr>' . $resp[CUSTOMER_ID_KEY] . '</mbr>';
        }
    }

    public function create_other_services($request) {
        if ($this->input->post("customer_id")) {

            $this->admin->logTime("Create other service process started");

            $this->load->library("Emerald/EmeraldApi");
            $this->emerald_api = new EmeraldApi();

            $customer_id = $request["customer_id"];
            $first_name = $request["first_name"];
            $last_name = $request["last_name"];
            $email = $request["email"];
            $phone = $request["phone_home"];
            $services = isset($request["services"]) ? $request["services"] : [];
            $service_start_date = $request["service_start_date"];
            $package_id = $request["package_id"];
            $package_services = $request["package_services"];
            $detail = $request["note"];
            $service_category = $request['service_category'];
            $login = $customer_id;

            if ($service_start_date != '') {
                $service_start_date = date('M d y', strtotime($service_start_date));
            }

            $x = 1;
            if (!empty($services)) {
                foreach ($services as $key => $service) {
                    $password = $phone;
                    if ($service == 169 || $service == 160 || $service == 157 || $service == 158) {
                        $login = $customer_id . 'AS00' . $x;
                        $x++;
                    } elseif ($service == 35) {
                        $login = $email . 'CA';
                        $password = $this->random_number();
                    } elseif ($service == 33) {
                        $login = $email . 'VB';
                        $password = $this->random_number();
                    } elseif ($service == 176 || $service == 119) {
                        $login = '';
                    } elseif (array_key_exists($service, $service_category)) {
                        $login = $customer_id . 'AS00' . $x;
                        $x++;
                    } else {
                        $login = uniqid();
                    }

                    // $serviceResp = $this->emerald_api->addService($service, $customer_id, $first_name, $last_name, $email, $login, $password, date('M d y', strtotime($service_start_date)));
                    $serviceResp = $this->emerald_api->addService($service, $customer_id, $first_name, $last_name, $email, $login, $password, $service_start_date);
                    $serviceResp = xml_to_json($serviceResp);
                    $this->admin->logTime("Service Created: " . $service);
                }
            }


            $emerald_package_ref = false;

            foreach ($package_services as $key => $package_service) {
                $password = $phone;
                if ($package_service == 184 || $package_service == 185 || $package_service == 277) {
                    $login = $customer_id . $last_name;
                    $password = 'brn1944';
                } elseif (array_key_exists($package_service, $service_category)) {
                    if ($service_category[$package_service] == 'tv' || $service_category[$package_service] == 'dvr') {
                        $login = $customer_id . 'AS00' . $x;
                        $x++;
                    } else if ($service_category[$package_service] == 'phone') {
                        $login = $customer_id . 'AS00' . $x . $last_name;
                        $x++;
                    } else if ($service_category[$package_service] == 'internet') {
                        $login = $customer_id . $last_name;
                        $password = 'brn1944';
                    }
                }

                // $packageServiceResp = $this->emerald_api->addServiceWithPackageRef($package_service, $customer_id, $first_name, $last_name, $email, $emerald_package_ref, $login, $password, $service_start_date);
                $packageServiceResp = $this->emerald_api->addService($package_service, $customer_id, $first_name, $last_name, $email, $login, $password, $service_start_date);
                $packageServiceResp = xml_to_json($packageServiceResp);
                $this->admin->logTime("Package Service Created: " . $package_service);
            }
        }
    }

    public function create_market_tag($request) {
        if ($this->input->post("customer_id")) {

            $this->admin->logTime("Create Market Tag process started");

            $this->load->library("Emerald/EmeraldApi");
            $this->emerald_api = new EmeraldApi();

            $customer_id = $request["customer_id"];
            $package = $request['package'];
            $package_market_tag = $request['package_market_tag'];

            if ($customer_id) {
                if (array_key_exists($package, $package_market_tag)) {
                    $market_tag = $package_market_tag[$package];
                    $marketTagResp = $this->emerald_api->addPackageMarketTag($market_tag, $customer_id);
                    $marketTagResp = xml_to_json($marketTagResp);
                    $this->admin->logTime("Market Tag Created: " . json_encode($marketTagResp));
                }
            }
        }
    }

    public function add_activation_fee($request) {
        if ($this->input->post("customer_id")) {

            $this->admin->logTime("Create Activation Fee process started");

            $this->load->library("Emerald/EmeraldApi");
            $this->emerald_api = new EmeraldApi();

            $customer_id = $request["customer_id"];
            $charge_type = $request['charge_type'];
            $services = isset($request["services"]) ? $request["services"] : [];
            $package_services = $request["package_services"];
            $service_category = $request['service_category'];

            if ($customer_id) {
                $total_services = [];

                if (!empty($services)) {
                    foreach ($services as $key => $service) {
                        if ($service != 176 && $service != 171) { // check if service not equal to roku rental and roku additional
                            if (array_key_exists($service, $service_category)) {
                                $service_type_key = $service_category[$service];
                                $total_services[$service_type_key] = '1';
                            }
                        }
                    }

                    // Additional Roku & Roku Rental one time charge count
                    $roku_rent_count = 0;
                    $roku_additional_count = 0;
                    if (in_array(171, $services)) { // Roku Additional
                        $count_roku = array_count_values($services);
                        $roku_additional_count = $count_roku[171];
                    }
                    if (in_array(176, $services)) { // Roku Rental
                        $count_roku = array_count_values($services);
                        $roku_rent_count = $count_roku[176];
                    }
                }

                foreach ($package_services as $key => $package_services) {

                    // if city is Sundance RV1 and package is CableTV+Internet so charge activation fee $45
                    if ($package_services == 277) {
                        $total_services['tv'] = '1';
                        $total_services['internet'] = '1';
                    }else{
                        if (array_key_exists($package_services, $service_category)) {
                            $service_type_key = $service_category[$package_services];
                            $total_services[$service_type_key] = '1';
                        }                        
                    }
                }

                $total_services_count = count($total_services) + $roku_additional_count + $roku_rent_count;
                $charge_type_label = $charge_type[$total_services_count];

                if ($charge_type_label) {
                    $activaitonFeeResp = $this->emerald_api->addActivationFee($charge_type_label, $customer_id);
                    $activaitonFeeResp = xml_to_json($activaitonFeeResp);
                    $this->admin->logTime("Activation Fee Added Services: " . json_encode($activaitonFeeResp));
                }

                // Wifi Router-Rent Activation charges
                if (!empty($services)) {
                    if (in_array(119, $services)) {
                        $charge_type_label = $charge_type['wifi'];
                        $activaitonFeeResp = $this->emerald_api->addActivationFee($charge_type_label, $customer_id);
                        $activaitonFeeResp = xml_to_json($activaitonFeeResp);
                        $this->admin->logTime("Activation Fee Added for wifi router rent: " . json_encode($activaitonFeeResp));
                    }
                }

                // Wifi Router-Purchase Activation charges
                if (!empty($services)) {
                    if (in_array(150, $services)) {
                        $charge_type_label = $charge_type['wifi_purchase'];
                        $activaitonFeeResp = $this->emerald_api->addActivationFee($charge_type_label, $customer_id);
                        $activaitonFeeResp = xml_to_json($activaitonFeeResp);
                        $this->admin->logTime("Activation Fee Added for wifi router purchase: " . json_encode($activaitonFeeResp));
                    }
                }
            }
        }
    }

    public function random_number() {
        $seed = str_split('abcdefghijklmnopqrstuvwxyz'
                . 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
                . '0123456789');
        shuffle($seed);
        $rand = '';
        foreach (array_rand($seed, 14) as $k)
            $rand .= $seed[$k];
        return $rand;
    }

}
