<?php
/*
 * This class syncs the Airebeam customer mbr credit card in our database
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Process_Temp_Orders extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model ( "M_admin" );
		$this->admin = new M_admin ();
	}
	
	public function get() {
		$unprocessed_orders = $this->admin->GetUnprocessedOrders();
		if( sizeof($unprocessed_orders) ) {
			$unprocessed_orders_array = array();
			$unprocessed_order_ids = array();
			foreach ($unprocessed_orders as $unprocessed_order) {
				$data = array(
					"OrderID" => $unprocessed_order->OrderID,
					"WorkerID" => $unprocessed_order->WorkerID,
					"CustomerID" => $unprocessed_order->CustomerID,
					"StatusID" => $unprocessed_order->StatusID,
					"TypeID" => $unprocessed_order->TypeID,
					"Name" => $unprocessed_order->Name,
					"Detail" => $unprocessed_order->Detail,
					"Duration" => $unprocessed_order->Duration,
					"Date" => $unprocessed_order->Date,
				);
				array_push($unprocessed_orders_array, $data);
				array_push($unprocessed_order_ids, $unprocessed_order->OrderID);
			}
			if( $this->admin->UpdateUnprocessedOrders($unprocessed_order_ids) ) {
				if( $this->admin->InsertUnProcessedOrders($unprocessed_orders_array) ) {
					echo TEMP_ORDERS_INSERTED;
				} else {
					echo TEMP_ORDERS_NOT_INSERTED;
				}
			} else {
				echo TEMP_ORDERS_NOT_UPDATED;
			}
		} else {
			echo NO_UNPROCESS_ORDERS;
		}

	}
}