<?php

/*
 * This class returns the customer info
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Update_Password extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library("CustomerAccessOK");
        $this->customerAccessOK = new CustomerAccessOK();
    }

    /**
     * Fetch Customer of given MBR
     *
     * @param <string> MBR
     * @return <json> Customer
     */
    public function index() {
        if ($this->input->post("mbr") && $this->input->post("password") && $this->input->post("key")) {

            $mbr = $this->input->post("mbr");
            $password = $this->input->post("password");
            $token = $this->input->post("key");
            $medium = $this->input->post("medium");

            if (isBase64Encoded($password)) {
                $password = base64_decode($password);
                $customerData = $this->db->select("*")->get_where("customer", ["CustomerID" => $mbr])->first_row();
                if ($customerData) {
                    $token_data = $this->db->select("*")->get_where("password_reset", ["CustomerID" => $mbr])->first_row();
                    if (!empty($token_data)) {
                        if (strtolower($token_data->Token) == strtolower($token)) {
                            $interval = $this->customerAccessOK->getDateDiff($token_data->Date, gmdate("Y-m-d H:i:s"));
                            if ($interval->h < 6) {
                                $this->customerAccessOK->deleteToken($mbr);
                                $this->db->where('CustomerID', $mbr);
                                $this->db->update('customer', ['isPasswordReset' => 0, 'Password' => simple_encrypt($password, SALT_STRING)]);

                                $activityLogID = strictUniqueID();
                                $activity = array(
                                    "ActivityLogID" => $activityLogID,
                                    "ActionID" => "5fc24e31a7af7",
                                    "ObjectTypeID" => "50206e9a01d5e",
                                    "Date" => date("Y-m-d H:i:s")
                                );
                                $this->db->insert('activity_log', $activity);

                                $activityDetail = array(
                                    "MetaID" => strictUniqueID(),
                                    "ActivityLogID" => $activityLogID,
                                    "ObjectID" => $mbr,
                                    "Name" => "force_password_updated",
                                    "Value" => $customerData->CustomerName
                                );
                                $this->db->insert('activity_log_meta', $activityDetail);

                                if (!empty($customerData->Email) && $customerData->Email != "" && $customerData->Email != "0" && !empty($customerData->PhoneHome) && $customerData->PhoneHome != "" && $customerData->PhoneHome != "0") {
                                    $this->customerAccessOK->confirmMailToCustomer((object) $customerData);
                                    $this->customerAccessOK->confirmSMSToCustomer((object) $customerData);
                                } else {
                                    if ($medium == "email" && !empty($customerData->Email) && $customerData->Email != "" && $customerData->Email != "0") {
                                        $this->customerAccessOK->confirmMailToCustomer((object) $customerData);
                                    } elseif ($medium == "sms" && !empty($customerData->PhoneHome) && $customerData->PhoneHome != "" && $customerData->PhoneHome != "0" && $customerData->isPhoneHomeLandline != "1") {
                                        $this->customerAccessOK->confirmSMSToCustomer((object) $customerData);
                                    }
                                }

                                echo json_encode($this->customerAccessOK->returnResponse("1"));
                            } else {
                                echo json_encode($this->customerAccessOK->returnResponse("15"));
                            }
                        } else {
                            echo json_encode($this->customerAccessOK->returnResponse("12"));
                        }
                    } else {
                        echo json_encode($this->customerAccessOK->returnResponse("12"));
                    }
                } else {
                    echo json_encode($this->customerAccessOK->returnResponse("3"));
                }
            } else {
                echo json_encode($this->customerAccessOK->returnResponse("10"));
            }
        } else {
            echo json_encode($this->customerAccessOK->returnResponse("2"));
        }
    }

}
