<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Populate_Holidays extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model("M_holidays");
		$this->holidays = new M_holidays();
	}
	
	public function index() {
		$year = date('Y', strtotime('+1 years') );
		$new_year_day = date($year . '-01-01');
		$last_day_of_year = date($year . '-12-31');
		$FIXED_HOLIDAYS = config_item('FIXED_HOLIDAYS');
		
		$holidays = array();
		foreach ($FIXED_HOLIDAYS as $fh) {
			$fixed_holiday_data = array(HOLIDAY_NAME => $fh[HOLIDAY_NAME], HOLIDAY_DATE => date($year .$fh[HOLIDAY_DATE]) );	
			array_push($holidays, $fixed_holiday_data);
		}
		
		$mays_monday_arr = array();
		$from_date = $new_year_day;
		$to_date = $last_day_of_year;
		$from_date=strtotime($from_date);
		$to_date=strtotime($to_date);
		// 	Found first monday of september
		$found_first_monday = false;
		
		for($j=$from_date; $j<=$to_date; $j+=86400) {
			$week_day = weekOfMonth($j);
			$p = date('Y-m-d',$j);
			$dt = new DateTime($p);
			$daydate = $dt->format('l');
			$month = $dt->format('F');
		
			if($daydate == SUNDAY) { 
				$daydate = SUNDAY_INT;
			}
			else if($daydate == MONDAY) {
				$daydate = MONDAY_INT;
			}
			else if($daydate == TUESDAY) {
				$daydate = TUESDAY_INT;
			}
			else if($daydate == WEDNESDAY) {
				$daydate = WEDNESDAY_INT;
			}
			else if($daydate == THURSDAY) {
				$daydate = THURSDAY_INT;
			}
			else if($daydate == FRIDAY) {
				$daydate = FRIDAY_INT;
			}
			else {
				$daydate = SATURDAY_INT;
			}
		
			if( $month == SEPTEMBER  && $daydate == 1 && !$found_first_monday) {
				$date_details = date("Y-m-d",$j);
				$e2 = array();
				$e2[HOLIDAY_NAME] = LABOR_DAY;
				$e2[HOLIDAY_DATE] = $date_details;
				array_push($holidays,$e2);
				$found_first_monday = true;
			}
		
			if( $month == MAY  && $daydate == 1) {
				$date_details = date("Y-m-d",$j);
				$mays_monday_arr[] = $date_details;
			}
		
			if( $month == NOVEMBER  && $week_day == 4 && $daydate == 4 ) {
				$date_details = date("Y-m-d",$j);
				$e2 = array();
				$e2[HOLIDAY_NAME] = THANKSGIVING;
				$e2[HOLIDAY_DATE] = $date_details;
				array_push($holidays,$e2);
				$found_first_monday = true;
			}
		
			if ($daydate == 0 || $daydate == 6) {
				if( $daydate == 0 ) {
					$title = SUNDAY;
				} else {
					$title = SATURDAY;
				}
				$date_details = date("Y-m-d", $j);
				$e4=array();
				$e4[HOLIDAY_NAME] = $title;
				$e4[HOLIDAY_DATE] = $date_details;
				array_push( $holidays,$e4 );
			}
		}
		
		$memorial_day = array(HOLIDAY_NAME => MEMORIAL_DAY, HOLIDAY_DATE => $mays_monday_arr[sizeof($mays_monday_arr) - 1]);
		array_push($holidays, $memorial_day);
		$this->holidays->InsertHolidays($holidays);
		echo "Insert Holidays Successfully";
				
	}
}