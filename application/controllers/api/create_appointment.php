<?php
/*
 * This class exposes API to create appointment
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Create_Appointment extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library("SelfAppointmentCreation");
        $this->selfAppointmentCreation = new SelfAppointmentCreation();
        $this->load->model("m_available_slots");
        $this->available_slots = new M_available_slots();
        $this->load->model("m_worker");
        $this->worker_info = new M_worker();        
        $this->load->model("m_admin");
        $this->admin = new M_admin();
        $this->load->model("m_order");
        $this->order = new M_order();
    }

    public function post() {
        if ( $this->input->post("description") && $this->input->post("worker_id") && $this->input->post("customer_id") && $this->input->post("start_time") && $this->input->post("end_time") ) {
            $this->admin->logTime("Appointment Creation Process Started");
            $isWorkerExist = $this->admin->IsWorkerExist($this->input->post("worker_id"));
            if ( $isWorkerExist ) {
                $start_time = $this->input->post("start_time");
                $end_time = $this->input->post("end_time");
                $start_time_obj = new DateTime($start_time);
                $end_time_obj = new DateTime($end_time);
                $temp_start_time = clone $start_time_obj;
                $temp_end_time = clone $end_time_obj;
                $customer_id = $this->input->post("customer_id");
                $worker_id = $this->input->post("worker_id");
                $description = str_replace(array( "\n", "\r" ), '', trim($this->input->post("description")));
                $requested_time_diff = date_diff($start_time_obj, $end_time_obj);
                if ( $requested_time_diff->invert == 0 ) {
                    $duration = ($requested_time_diff->h * 60) + $requested_time_diff->i;
                    $worker_appointments = $this->available_slots->WorkerAppointmentsByDate($start_time, $end_time, $worker_id);
                    $worker_temp_appointments = $this->available_slots->WorkerTempAppointmentsByDate($start_time, $end_time, $worker_id);
                    $worker_appointments = array_merge($worker_appointments, $worker_temp_appointments);

                    //      Add one minute to start time
                    $minutes = 1;
                    $temp_start_time->add(new DateInterval('PT' . $minutes . 'M'));
                    //      Subtract one minute to end time
                    $temp_end_time->sub(new DateInterval('PT' . $minutes . 'M'));

                    $request_appointment = ( object ) array(
                                  START_TIME_KEY => $temp_start_time,
                                  END_TIME_KEY => $temp_end_time
                    );

                    $is_appointment_conflict = false;
                    foreach ( $worker_appointments as $w_appointment ) {
                        $is_appointment_conflict = $this->available_slots->IsAppointmentConflicts($w_appointment, $request_appointment);
                        if ( $is_appointment_conflict ) {
                            break;
                        }
                    }

                    if ( !$is_appointment_conflict ) {
                        //Create appointment code here
                        $this->load->model("M_order");
                        $this->order = new M_order();
                        
                        //Check temporary customer id has MBR != 0 then set $mbr_exist = true else false 
                        $mbr_log = $this->admin->GetMBRLogByTempId($customer_id);
                        
                        if ( $mbr_log[ 0 ]->Mbr != '0' ) {
                            $mbr_exist = true;
                            $customer_id = $mbr_log[ 0 ]->Mbr;
                        }
                        else {
                            $mbr_exist = false;
                        }

                        $response = $this->order->addOrderByCustomer($description, $worker_id, $customer_id, $start_time_obj, $duration, $mbr_exist);

                        if ( $response && array_key_exists("OrderID", $response) ) {
                            
                            $msg = $this->selfAppointmentCreation->returnResponse(SUCCESS_APPOINTMENT_STATUS);
                            $orderId = $response[ "OrderID" ];
                            $orderData = $this->order->getOrder($orderId);

                            if ( $mbr_exist ) {

                                if ( $orderData[ "isPhoneHomeLandline" ] != "1" ) {
                                    //add message entry in database with pending status
                                    if ( $orderData[ "PhoneHome" ] != "" && $orderData[ "PhoneHome" ] != "0" ) {
                                        $this->order->addMessageEntry($orderData[ "OrderID" ]);
                                    }
                                }

                                $this->order->removeAlert($orderId);
                                if ( $orderData[ "PhoneHome" ] != "" && $orderData[ "PhoneHome" ] != "0" ) {
                                    $this->order->addAlert($orderData, "create", $end_time_obj->format('Y-m-d H:i:s'));
                                }
                            }

                            if ( sizeof($worker_appointments) ) {
                                // Get previous appointment location
                                $last_appointment = $this->available_slots->GetLastAppointment($worker_appointments, $start_time_obj);
                                
                                if(!empty($last_appointment)){
                                    $origin = $last_appointment->appointment_loc;
                                    $last_appt_end_time = clone $last_appointment->end_time;

                                    // Get destination location
                                    $destination = $mbr_log[ 0 ]->Address . " " . $mbr_log[ 0 ]->City . ", " . $mbr_log[ 0 ]->State . " " . $mbr_log[ 0 ]->Zip;

                                    $params = array(
                                          "origin" => $origin,
                                          "destination" => $destination,
                                          "departure_time" => $last_appt_end_time->format('U')
                                    );
                                    $this->load->model("m_google_directions");
                                    $this->google_directions = new M_google_directions();
                                    $directions = $this->google_directions->FetchDirections($params);
                                    if ( $directions ) {
                                        $commute_time = $directions[ 'routes' ][ 0 ][ 'legs' ][ 0 ][ 'duration_in_traffic' ][ 'value' ] / 60;
                                        $commute_time = round($commute_time, 0);
                                        if ( $commute_time > 30 ) {
                                            $c_time_remainder = $commute_time % 30;
                                            //Calculate time to get next interval with 30 min
                                            $add_time = 30 - $c_time_remainder;
                                            $commute_time = $add_time + $commute_time;
                                            $minutes = 1;
                                            $minute_to_subtract = $commute_time - $minutes;
                                            $temp_move_slot_start_time = clone $start_time_obj;
                                            $temp_move_slot_start_time->sub(new DateInterval('PT' . $minute_to_subtract . 'M'));

                                            $request_move_appointment = ( object ) array(
                                                          START_TIME_KEY => $temp_move_slot_start_time,
                                                          END_TIME_KEY => $temp_end_time
                                            );

                                            //Before moving appointment test conflict
                                            $move_appointment_conflict = false;
                                            foreach ( $worker_appointments as $w_appointment ) {
                                                $move_appointment_conflict = $this->available_slots->IsAppointmentConflicts($w_appointment, $request_move_appointment);
                                                if ( $move_appointment_conflict ) {
                                                    break;
                                                }
                                            }

                                            if ( $move_appointment_conflict ) {
                                                $move_slot_add_time = $commute_time - 30;
                                                $temp_move_slot_start_time = clone $start_time_obj;
                                                $temp_move_slot_end_time = clone $end_time_obj;
                                                $temp_move_slot_start_time->add(new DateInterval('PT' . $move_slot_add_time . 'M'));
                                                $temp_move_slot_end_time->add(new DateInterval('PT' . $move_slot_add_time . 'M'));
                                                $request_move_appointment = ( object ) array(
                                                              START_TIME_KEY => $temp_move_slot_start_time,
                                                              END_TIME_KEY => $temp_move_slot_end_time
                                                );

                                                //After create appointment request with new time test conflict
                                                $move_slot_appointment_conflict = false;
                                                foreach ( $worker_appointments as $w_appointment ) {
                                                    $move_slot_appointment_conflict = $this->available_slots->IsAppointmentConflicts($w_appointment, $request_move_appointment);
                                                    if ( $move_slot_appointment_conflict ) {
                                                        break;
                                                    }
                                                }

                                                //if it has conflit send email to CSR
                                                //else move appointment
                                                if ( $move_slot_appointment_conflict ) {
                                                    $key_names = array(
                                                          'DRIVE_TIME_NOTIFY_EMAIL',
                                                    );
                                                    $Settings = $this->admin->GetConfigSettings($key_names);
                                                    $notify_email = $Settings[ 0 ]->Name == 'DRIVE_TIME_NOTIFY_EMAIL' ? $Settings[ 0 ]->Value : '';
                                                    $email_array = (explode(",", $notify_email));
                                                    foreach ( $email_array as $email ) {
                                                        $subject = APPOINTMENT_CONFLICT;
                                                        if ( $mbr_exist ) {
                                                            $body = '<p> Customer <' . $mbr_log[ 0 ]->Mbr . ', ' . $mbr_log[ 0 ]->FirstName . ', ' . $mbr_log[ 0 ]->LastName . '>' .
                                                                    '<' . $temp_start_time->format('m/d/y') . '> has an appointment time schedule conflict. Please contact the customer to resolve ASAP. </p>';
                                                        }
                                                        else {
                                                            $body = '<p> Customer <' . $mbr_log[ 0 ]->FirstName . ', ' . $mbr_log[ 0 ]->LastName . '>' .
                                                                    '<' . $temp_start_time->format('m/d/y') . '> has an appointment time schedule conflict. Please contact the customer to resolve ASAP. </p>';
                                                        }

                                                        $from = SUNSET_LOG_API_FROM;
                                                        sendEmail($email, $subject, $body, $from);
                                                    }
                                                }
                                                else {

                                                    //update order date 
                                                    $move_slot_time = $temp_move_slot_start_time->format('Y-m-d H:i:s');
                                                    if ( $mbr_exist ) {
                                                        //Move order time in order table
                                                        $this->order->UpdateOrderDate($orderId, $move_slot_time);
                                                        $this->order->removeAlert($orderId);
                                                        if ( $orderData[ "PhoneHome" ] != "" && $orderData[ "PhoneHome" ] != "0" ) {
                                                            $orderData[ "Date" ] = $move_slot_time;
                                                            $this->order->addAlert($orderData, "change", $temp_move_slot_end_time->format('Y-m-d H:i:s'));
                                                        }
                                                    }
                                                    else {
                                                        //Move order time in temp_order
                                                        $this->order->UpdateTempOrderDate($orderId, $move_slot_time);
                                                        $this->order->UpdateTempOrderAlertTitle($orderId, "change");
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        else {
                            $msg = $this->selfAppointmentCreation->returnResponse(FAILED_APPOINTMENT);
                        }
                        return $this->output->set_header('Access-Control-Allow-Origin:*')->set_content_type('application/json')->set_output(json_encode($msg));
                    }
                    else {
                        $error_msg = $this->selfAppointmentCreation->returnResponse(APPOINTMENT_CONFLICT_STATUS);
                        return $this->output->set_header('Access-Control-Allow-Origin:*')->set_content_type('application/json')->set_output(json_encode($error_msg));
                    }
                }
                else {
                    $error_msg = $this->selfAppointmentCreation->returnResponse(END_TIME_LESS_STATUS);
                    return $this->output->set_header('Access-Control-Allow-Origin:*')->set_content_type('application/json')->set_output(json_encode($error_msg));
                }
            }
            else {
                $error_msg = $this->selfAppointmentCreation->returnResponse(WORKER_NOT_EXIST_STATUS);
                return $this->output->set_header('Access-Control-Allow-Origin:*')->set_content_type('application/json')->set_output(json_encode($error_msg));
            }
        }
        else {
            $error_msg = $this->selfAppointmentCreation->returnResponse(APPOINTMENT_PARAMS_MISSING_STATUS);
            return $this->output->set_header('Access-Control-Allow-Origin:*')->set_content_type('application/json')->set_output(json_encode($error_msg));
        }
    }

    public function update_freshdesk_ticket($ticket_id, $order_date) {
        $this->load->model("M_curl");
        $this->curl = new M_curl();

        $base_url = config_item('base_url');
        $asyncControllerUrl = $base_url . 'api/create_appointment/freshdesk_ticket';

        $params = [
              "ticket_id" => $ticket_id,
              "order_date" => $order_date,
        ];
        $this->curl->curlPostAsync($asyncControllerUrl, $params);
    }

    public function freshdesk_ticket() {
        $this->admin->logTime("Ticket ID: " . $this->input->post("ticket_id"));
        $this->admin->logTime("Order Date: " . $this->input->post("order_date"));

        if ( $this->input->post("ticket_id") && $this->input->post("order_date") ) {

            $ticket_detail = $this->admin->viewTicket($this->input->post("ticket_id"));
            $ticket_detail = json_decode($ticket_detail);

            if ( isset($ticket_detail->description) ) {
                $description = str_replace("[APPT_DATE]", $this->input->post("order_date"), $ticket_detail->description);
                $data = [
                      "description" => $description
                ];
                $this->admin->updateTicket($this->input->post("ticket_id"), $data);
            }
        }
    }

    public function send_appointment_notification($first_name, $last_name, $email) {
        $this->load->library('email');
        $config[ 'mailtype' ] = 'html';
        $this->email->initialize($config);

        $this->email->from('onlineorders@airebeam.com');
        $this->email->to($email);
        $this->email->subject(' Online Service Order Confirmation');

        $html = "Hello " . $first_name . " " . $last_name . ",<br><br>
                We received your broadband Internet service order. AireBeam Customer Service will contact you within 3 business days by email and/or phone.<br><br>
                Thanks for your Order,<br>
                Sincerely,<br>
                AireBeam Customer Service<br>
                FibAire Communications, LLC d/b/a AireBeam<br>
                Phone: 520-233-7400</br>
                Email: support@airebeam.com</br>
                <img src='" . ADMIN_ASSETS_PATH . "img/logo.png' width='280' /><br><br>
                This e-mail was sent from onlineorders@airebeam.com";

        $this->email->message($html);
        $this->email->attach(BASEPATH . '../assets/admin/tc.pdf', 'attachment', 'tc.pdf');
        return $this->email->send();
    }

    public function get_worker(){        
        $worker_id = $this->input->post("worker_id");

        $msg['worker'] = $this->worker_info->getWorkerName($worker_id);

        return $this->output->set_header('Access-Control-Allow-Origin:*')->set_content_type('application/json')->set_output(json_encode($msg));
    }

}