<?php

/*
 * This class syncs the Airebeam customer mbr credit card in our database
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Terms_Accepted extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library("CustomerAccessOK");
        $this->customerAccessOK = new CustomerAccessOK();
    }

    /**
     * Fetch all Services of given MBR
     *
     * @param <string> MBR
     * @return <json> Services
     */
    public function update($MBR) {
        $result = $this->db->select("CustomerID")->get_where("customer", ["CustomerID" => $MBR])->first_row();
        if (!empty($result)) {
            $this->db->where('CustomerID', $MBR);
            $this->db->update('customer', ['TermsAccepted' => "1"]);
            echo json_encode(["status" => "1", "message" => "MBR Terms Accepted Updated Successfully"]);
        } else {
            echo json_encode($this->customerAccessOK->returnResponse("3"));
        }
    }

}
