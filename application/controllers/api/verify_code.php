<?php

/*
 * This class syncs the Airebeam customer mbr credit card in our database
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Verify_Code extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library("CustomerAccessOK");
        $this->customerAccessOK = new CustomerAccessOK();
    }

    /**
     * Fetch all Services of given MBR
     *
     * @param <string> MBR
     * @return <json> Services
     */
    public function index() {
        if ($this->input->get("mbr") && $this->input->get("code")) {
            $mbr = $this->input->get("mbr");
            $code = $this->input->get("code");
            $result = $this->db->select("*")->get_where("password_reset", ["CustomerID" => $mbr])->first_row();
            if (!empty($result) && strtolower($result->Token) == strtolower($code)) {
                echo json_encode(["status" => "1", "message" => "Token verified"]);
            } else {
                echo json_encode($this->customerAccessOK->returnResponse("12"));
            }
        }else{
            echo json_encode($this->customerAccessOK->returnResponse("2"));
        }
    }

}
