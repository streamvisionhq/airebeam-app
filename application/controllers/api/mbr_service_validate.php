<?php

/*
 * This class validates the Airebeam customer portal mbr customer access ok service in our database
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class MBR_Service_Validate extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library("CustomerAccessOK");
        $this->customerAccessOK = new CustomerAccessOK();
    }

    /**
     * Make response string
     *
     * @desc Found status in statuses array and get message
     *
     * @param <string> Status
     * @return <string> Message
     */
    public function index() {
		if ($this->input->get ( "mbr" ) && $this->input->get ( "password" )) {
			$mbr = $this->input->get ( "mbr" );
			if (ctype_digit ( $mbr )) {
				if (isBase64Encoded ( $this->input->get ( "password" ) )) {
					$password = base64_decode ( $this->input->get ( "password" ) );
					$customerData = $this->db->select ( "*" )->get_where ( "customer", [ 
							"CustomerID" => $mbr 
					] )->first_row ();
					if (! empty ( $customerData )) {
						$response = $this->customerAccessOK->checkPassword ( $mbr, $password );
						if ($response ["status"] == "1") {
							$termsData = $this->customerAccessOK->checkTermsAccepted ( $mbr );
							if ($termsData) {
								$response ["terms-conditions"] = $this->customerAccessOK->term_url;
							}
							if ($customerData->isPasswordReset == "1") {
								$response ["force-reset"] = $customerData->isPasswordReset;
								if ($token = $this->customerAccessOK->insertGeneralToken ( $customerData )) {
									$response ["token"] = $token;
								}
							}
							echo json_encode ( $response );
						} else {
							echo json_encode ( $response );
						}
					} else {
						echo json_encode ( $this->customerAccessOK->returnResponse ( "3" ) );
					}
				} else {
					echo json_encode ( $this->customerAccessOK->returnResponse ( "4" ) );
				}
			} else {
				echo json_encode ( $this->customerAccessOK->returnResponse ( "3" ) );
			}
		} else {
			echo json_encode ( $this->customerAccessOK->returnResponse ( "2" ) );
		}
	}
}
