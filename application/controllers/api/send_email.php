<?php

/*
 * This class returns the customer info
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Send_Email extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library("CustomerAccessOK");
        $this->customerAccessOK = new CustomerAccessOK();
    }

    /**
     * Send Reset Password Link in Email
     *
     * @param <string> MBR
     * @return <json> Customer
     */
    public function index() {
        if ($this->input->get("mbr") && $this->input->get("url")) {
            $mbr = $this->input->get("mbr");
            $url = $this->input->get("url");
            $customer = $this->db->select("*")->get_where("customer", ["CustomerID" => $mbr])->first_row();
            if (!empty($customer)) {
                if (!empty($customer->Email) && $customer->Email != "" && $customer->Email != "0") {
                    if($this->customerAccessOK->mailToCustomer($customer, $url)){
                        echo json_encode($this->customerAccessOK->returnResponse("1"));
                    }
                } else {
                    echo json_encode($this->customerAccessOK->returnResponse("16"));
                }
            } else {
                echo json_encode($this->customerAccessOK->returnResponse("3"));
            }
        } else {
            echo json_encode($this->customerAccessOK->returnResponse("2"));
        }
    }

}
