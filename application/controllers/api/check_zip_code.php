<?php
/*
 * This class syncs the Airebeam customer mbr credit card in our database
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Check_Zip_Code extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model ( "M_admin" );
		$this->admin = new M_admin ();
	}
	
	public function index() {
		if ( $this->input->get ( "zip_code" ) ) {
			$zip_code = (int)$this->input->get ( "zip_code" );
			$result = $this->admin->IsZipCodeExist($zip_code);
			if( $result ) {
				// Zip code exists
				$success_msg = [
						"message" => ZIP_CODE_FOUND
				];
				return $this->output->set_header( 'Access-Control-Allow-Origin:*' )->set_content_type ( 'application/json' )->set_output ( json_encode ( $success_msg ) );

				} else {
					$error_msg = [
							"message" => ZIP_CODE_NOT_FOUND
					];
					return $this->output->set_header( 'Access-Control-Allow-Origin:*' )->set_content_type ( 'application/json' )->set_output ( json_encode ( $error_msg ) );
				}
			
			} else {
				$error_msg = ["message" => ZIP_CODE_ERROR_MSG];
				return $this->output->set_header( 'Access-Control-Allow-Origin:*' )
			->set_content_type('application/json')
			->set_output(json_encode($error_msg ));
		}
	}

	public function send_invalid_zip_notification(){
		$old_zip = $this->input->post("old");
		$new_zip = $this->input->post("new");

        $this->admin->logTime("Sending email for invalid zip code (".$old_zip.", ".$new_zip.")");


		$key_names = array(
		    'DRIVE_TIME_NOTIFY_EMAIL',
		);

		$message = 'Please check this zip code ('. $new_zip .')';
		if ($old_zip != $new_zip) {
			$message = 'Please check these zip code (' . $old_zip . ', ' . $new_zip . ')';
		}
		$Settings = $this->admin->GetConfigSettings($key_names);
		$notify_email = $Settings[0]->Name == 'DRIVE_TIME_NOTIFY_EMAIL' ? $Settings[0]->Value : '';
		$email_array = (explode(",", $notify_email));
		foreach ($email_array as $email) {
		    $subject = ZIP_NOT_EXIST;
		    $body = '<p> ' . $message . '. This zip code does not exist in our system. </p>';
		    $from = FROM_AIREBEAM;
		    sendEmail($email, $subject, $body, $from);
		    $this->admin->logTime("Sent zip not found email to : " . $email);
		}
	}

}