<?php

/*
 * This class returns the customer info
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Customer extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library("CustomerAccessOK");
        $this->customerAccessOK = new CustomerAccessOK();
    }

    /**
     * Fetch Customer of given MBR
     *
     * @param <string> MBR
     * @return <json> Customer
     */
    public function index() {
        $mbr = filter_input(INPUT_POST, "mbr");
        if (ctype_digit($mbr)) {
            $url = EMERALD_REQUEST_URL . "backoffice/mbrdetail.ews?mbr=" . $mbr;
            $detail = $this->customerAccessOK->hitBackoffice($url);
            if (!empty($detail->MBR)) {
                $fullName = $mbr;
                $mbrDetail = [
                    "FIRSTNAME" => checkIsZero(simplize($detail->MBR->FIRSTNAME)),
                    "LASTNAME" => checkIsZero(simplize($detail->MBR->LASTNAME)),
                    "EMAIL" => checkIsZero(simplize($detail->MBR->EMAIL)),
                    "PHONEHOME" => checkIsZero(simplizePhone(simplize($detail->MBR->PHONEHOME))),
                    "PHONEWORK" => checkIsZero(simplizePhone(simplize($detail->MBR->PHONEWORK))),
                    "PHONEFAX" => checkIsZero(simplizePhone(simplize($detail->MBR->PHONEFAX))),
                    "ADDRESS1" => checkIsZero(simplize($detail->MBR->ADDRESS1)),
                    "ADDRESS2" => checkIsZero(simplize($detail->MBR->ADDRESS2)),
                    "STATE" => checkIsZero(simplize($detail->MBR->STATE)),
                    "CITY" => checkIsZero(simplize($detail->MBR->CITY)),
                    "ZIP" => checkIsZero(simplize($detail->MBR->ZIP)),
                    "LASTFOUR" => checkIsZero(simplize($detail->MBR->LASTFOUR))
                ];

                if ($mbrDetail["FIRSTNAME"]) {
                    $fullName .= " - " . $mbrDetail["FIRSTNAME"];
                    if ($mbrDetail["LASTNAME"]) {
                        $fullName .= " " . $mbrDetail["LASTNAME"] . "";
                    }
                }
                $array["FullName"] = $fullName;
                $data = [
                    "CustomerName" => $fullName,
                    "Email" => $mbrDetail["EMAIL"],
                    "PhoneHome" => $mbrDetail["PHONEHOME"],
                    "PhoneWork" => $mbrDetail["PHONEWORK"],
                    "Fax" => $mbrDetail["PHONEFAX"],
                    "Address1" => $mbrDetail["ADDRESS1"],
                    "Address2" => $mbrDetail["ADDRESS2"],
                    "City" => $mbrDetail["CITY"],
                    "Zip" => $mbrDetail["ZIP"],
                    "CreditCardLastFour" => $mbrDetail["LASTFOUR"],
                    "State" => $mbrDetail["STATE"]
                ];

                $address = urldecode($data["Address1"] . " " . $data["Zip"] . " " . $data["City"] . ", AZ USA");
                $address_response = $this->customerAccessOK->convertToLatLong($address);
                $data["Latitude"] = $address_response["latitude"];
                $data["Longitude"] = $address_response["longitude"];

                $this->load->model("M_twilio");
                $this->twil = new M_twilio();
                $is_landline = $this->twil->getPhoneNumberType($data["PhoneHome"]);
                $data["isPhoneHomeLandline"] = $is_landline;

                $this->db->where('CustomerID', $mbr);
                $this->db->update('customer', $data);
            }
            $customer = $this->db->select("*")->get_where("customer", ["CustomerID" => $mbr])->first_row();
            if (!empty($customer)) {
                unset($customer->Password);
                echo json_encode(["status" => "1", "customer" => $customer]);
            } else {
                echo json_encode($this->customerAccessOK->returnResponse("3"));
            }
        } else {
            echo json_encode($this->customerAccessOK->returnResponse("3"));
        }
    }

}
