<?php

/*
 * This class returns the customer info
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Send_SMS extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library("CustomerAccessOK");
        $this->customerAccessOK = new CustomerAccessOK();
    }

    /**
     * Fetch Customer of given MBR
     *
     * @param <string> MBR
     * @return <json> Customer
     */
    public function index() {
        if ($this->input->get("mbr")) {
            $mbr = $this->input->get("mbr");
            $customer = $this->db->select("*")->get_where("customer", ["CustomerID" => $mbr])->first_row();
            if (!empty($customer)) {
                if (!empty($customer->PhoneHome) && $customer->isPhoneHomeLandline == "0") {
                    if ($this->customerAccessOK->smsToCustomer($customer)) {
                        echo json_encode($this->customerAccessOK->returnResponse("1"));
                    }
                } else {
                    echo json_encode($this->customerAccessOK->returnResponse("16"));
                }
            } else {
                echo json_encode($this->customerAccessOK->returnResponse("3"));
            }
        } else {
            echo json_encode($this->customerAccessOK->returnResponse("2"));
        }
    }

}
