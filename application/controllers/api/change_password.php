<?php

/*
 * This class returns the customer info
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Change_Password extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library("CustomerAccessOK");
        $this->customerAccessOK = new CustomerAccessOK();
    }

    /**
     * Fetch Customer of given MBR
     *
     * @param <string> MBR
     * @return <json> Customer
     */
    public function index() {
        if ($this->input->post("mbr") && $this->input->post("old_password") && $this->input->post("new_password")) {

            $mbr = $this->input->post("mbr");
            $old_password = $this->input->post("old_password");
            $new_password = $this->input->post("new_password");

            if (isBase64Encoded($old_password) && isBase64Encoded($new_password)) {
                $old_password = base64_decode($old_password);
                $new_password = base64_decode($new_password);
                $customerData = $this->db->select("*")->get_where("customer", ["CustomerID" => $mbr])->first_row();
                $decrypted_password = simple_decrypt($customerData->Password, SALT_STRING);
                if($decrypted_password == $old_password){
                	$this->db->where('CustomerID', $mbr);
                	$this->db->update('customer', ['Password' => simple_encrypt($new_password, SALT_STRING)]);
                	echo json_encode($this->customerAccessOK->returnResponse("1"));
                }else{
                	echo json_encode($this->customerAccessOK->returnResponse("17"));
                }
            } else {
                echo json_encode($this->customerAccessOK->returnResponse("10"));
            }
        } else {
            echo json_encode($this->customerAccessOK->returnResponse("2"));
        }
    }

}
