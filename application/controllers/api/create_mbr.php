<?php
/*
 * This class syncs the Airebeam customer mbr credit card in our database
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Create_Mbr extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model("M_curl");
        $this->curl = new M_curl();
        $this->load->library("SelfAppointmentCreation");
        $this->selfAppointmentCreation = new SelfAppointmentCreation();
        $this->load->model("M_admin");
        $this->admin = new M_admin();
        $this->load->library("Emerald/EmeraldApi");
        $this->emerald_api = new EmeraldApi();
    }

    public function post() {
        if ( $this->input->post("first_name") && $this->input->post("last_name") && $this->input->post("email") && $this->input->post("address") && $this->input->post("phone_home") && $this->input->post("state") && $this->input->post("zip") && $this->input->post("city") && $this->input->post("services") ) {

            $this->admin->logTime("Hit Create MBR endpoint");

            $STATES = config_item('STATES');
            $first_name = $this->input->post("first_name");
            $last_name = $this->input->post("last_name");
            $email = $this->input->post("email");
            $address = $this->input->post("address");
            $phone_home = $this->input->post("phone_home");
            $state = $STATES[$this->input->post("state")];
            $zip = $this->input->post("zip");
            $city = $this->input->post("city");
            $services = json_decode($this->input->post("services"));
            $service_start_date = $this->input->post("service_start_date");
            $package_id = $this->input->post("package_id");
            $package_services = json_decode($this->input->post("package_services"));
            $ticket_id = $this->input->post("ticket_id");
            $note = $this->input->post("note");
            $service_category = $this->input->post("service_category");
            $package_market_tag = $this->input->post("package_market_tag");
            $charge_type = $this->input->post("charge_type");
            $package = $this->input->post("package");

            $cc_number = $this->input->post("cc_number");
            $cc_expiration_date = $this->input->post("cc_expiration_date");
            $cc_security_code = $this->input->post("cc_security_code");
            $cc_name = $this->input->post("cc_name");
            
            $this->admin->InsertMBRRequestParams($address, $email, $zip, $phone_home, $city, $state, $first_name, $last_name);
            $this->admin->logTime("Inserted MBR Request!");
            
            $mbrLogId = $this->admin->GetMBRLogId($address, $zip, $phone_home, $email);

            $base_url = config_item('base_url');
            $asyncControllerUrl = $base_url . 'api/create_services/post';

            //Run background process
            $params = array(
                  "first_name" => $first_name,
                  "last_name" => $last_name,
                  "email" => $email,
                  "address" => $address,
                  "phone_home" => $phone_home,
                  "state" => $state,
                  "zip" => $zip,
                  "city" => $city,
                  "services" => $services,
                  "service_start_date" => $service_start_date,
                  "cc_number" => $cc_number,
                  "cc_expiration_date" => $cc_expiration_date,
                  "cc_security_code" => $cc_security_code,
                  "cc_name" => $cc_name,
                  "ticket_id" => $ticket_id,
                  "note" => $note,
                  "tmp_mbr" => $mbrLogId[0]->Id,
                  "service_category" => $service_category,
                  "package_market_tag" => $package_market_tag,
                  "charge_type" => $charge_type,
                  "package" => $package
            );

            if ( $package_id ) {
                $params['package_id'] = $package_id;
                $params['package_services'] = $package_services;
            }
            $created_mbr = $this->curl->curlPostAsync($asyncControllerUrl, $params);

            // Get newly created mbr
            $new_mbr = $this->get_string_between($created_mbr, '<mbr>', '</mbr>');
            
            $this->admin->logTime("Created MBR " . $new_mbr);

            $success_msg = [
              MBR_KEY => $mbrLogId[0]->Id,
              'emerald_mbr' => $new_mbr
            ];
            
            return $this->output->set_header('Access-Control-Allow-Origin:*')->set_content_type('application/json')->set_output(json_encode($success_msg));
        }
        else {
            //Parameters missing msg
            $error_msg = $this->selfAppointmentCreation->returnCreateMbrResponse(CREATE_MBR_MISSING_STATUS);
            return $this->output->set_header('Access-Control-Allow-Origin:*')->set_content_type('application/json')->set_output(json_encode($error_msg));
        }
    }

    public function check_email(){
      if ( $this->input->post("email") ){

        $this->admin->logTime("Hit Check email already exist");

        $email = $this->input->post("email");

        $url = EMERALD_REQUEST_URL . "backoffice/mbrdetail_byemail.ews?email=" . $email;
        $customer_email_validation_resp = $this->curl->fetchBackOfficeEmailDetails($url);
        
        $this->curl->logTime("back office detail email response:" . json_encode($customer_email_validation_resp));

        if ($customer_email_validation_resp->MBR) {
          $error_msg = ['message' => EMAIL_FOUND];
          return $this->output->set_header('Access-Control-Allow-Origin:*')->set_content_type('application/json')->set_output(json_encode($error_msg));
        }else{
          $success_msg = ['message' => EMAIL_NOT_FOUND];
          return $this->output->set_header('Access-Control-Allow-Origin:*')->set_content_type('application/json')->set_output(json_encode($success_msg));
        }

      }
    }

    public function close_mbr(){
      
      $this->admin->logTime("In close mbr function...");

      if ( $this->input->post("mbr") ){

        $this->admin->logTime("Hit Close MBR");

        $customer_id = $this->input->post("mbr");
        $this->admin->logTime("cLOSING MBR = " . $customer_id);

        $resp = $this->curl->closeMBR($customer_id);

        if ($resp[CREATE_MBR_RETCODE_KEY] == 0) {
          $message = ['message' => 'Closed'];
        }else{
          $message = ['message' => 'Failed'];
        }
        
        return $this->output->set_header('Access-Control-Allow-Origin:*')->set_content_type('application/json')->set_output(json_encode($message));        

      }
    }

    public function get_string_between($string, $start, $end){
      $string = ' ' . $string;
      $ini = strpos($string, $start);
      if ($ini == 0) return '';
      $ini += strlen($start);
      $len = strpos($string, $end, $ini) - $ini;
      return substr($string, $ini, $len);
    }

}
