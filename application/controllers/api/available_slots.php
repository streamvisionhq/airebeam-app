<?php
/*
 * This class syncs the Airebeam customer mbr credit card in our database
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Available_Slots extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model("M_admin");
        $this->admin = new M_admin ();
        $this->load->model("m_available_slots");
        $this->available_slots = new M_available_slots();
        $this->load->model("M_sunset_time");
        $this->sunset_time = new M_sunset_time();
        $this->load->model("M_holidays");
        $this->holidays = new M_holidays();
    }

    public function get() {
        error_reporting(0);
        $key_names = array(
              MIN_COMMUTE_TIME_CONFIG,
              SIZE_OF_INSTALLATION_CONFIG,
              LAST_AVAILABLE_START_TIME_CONFIG,
              MAX_NUMBER_OF_APPOINTMENTS_CONFIG,
              SUNSET_TIME_HR_CONFIG,
              SUNSET_TIME_MINS_CONFIG,
              SHIFT_START_TIME_HR_CONFIG,
              SUNSET_TIME_MODE_CONFIG,
              SHIFT_START_TIME_MINS_CONFIG,
              MAX_SHIFT_END_TIME_HR_CONFIG,
              MAX_SHIFT_END_TIME_MINS_CONFIG
        );

        $settings = $this->admin->GetConfigSettings($key_names);
        $commute_time = $settings [0]->Name == MIN_COMMUTE_TIME_CONFIG ? (int) $settings [0]->Value : '';
        $installation_size = $settings [1]->Name == SIZE_OF_INSTALLATION_CONFIG ? (int) $settings [1]->Value : '';
        $last_avail_start_time = $settings [2]->Name == LAST_AVAILABLE_START_TIME_CONFIG ? (int) $settings [2]->Value : '';
        $max_appoints = $settings [3]->Name == MAX_NUMBER_OF_APPOINTMENTS_CONFIG ? $settings [3]->Value : '';
        $manual_sunset_time_hr = $settings [4]->Name == SUNSET_TIME_HR_CONFIG ? $settings [4]->Value : '';
        $manual_sunset_time_mins = $settings [5]->Name == SUNSET_TIME_MINS_CONFIG ? $settings [5]->Value : '';
        $shift_start_time = $settings [6]->Name == SHIFT_START_TIME_HR_CONFIG ? $settings [6]->Value : '';
        $sunset_time_mode = $settings [7]->Name == SUNSET_TIME_MODE_CONFIG ? $settings [7]->Value : '';
        $shift_start_time_mins = $settings [8]->Name == SHIFT_START_TIME_MINS_CONFIG ? $settings [8]->Value : '';
        $max_shift_end_time_hr = $settings [9]->Name == MAX_SHIFT_END_TIME_HR_CONFIG ? $settings [9]->Value : '';
        $max_shift_end_time_mins = $settings [10]->Name == MAX_SHIFT_END_TIME_MINS_CONFIG ? $settings [10]->Value : '';
        
        $current_date = new DateTime();
        
        //Filter holiday objects
        $holidays_data = $this->holidays->GetHolidays($current_date->format('Y-m-d'));
        $holidays_array = $this->holidays->GetHolidayObjects($holidays_data);

        if ( $this->input->get("zip_code") ) {
            $zip_code = (int) $this->input->get("zip_code");
            $result = $this->admin->IsZipCodeExist($zip_code);
            if ( $result ) {

                if ( $this->input->get("start_date") && $this->input->get("end_date") ) {
                    $start_date = $this->input->get("start_date");
                    $end_date = $this->input->get("end_date");
                }
                else {
                    $start_date = new DateTime ();
                    $start_date->modify(getDayAddIntervalString(1));
                    $start_date = $start_date->format('Y-m-d');
                    $end_date = new DateTime();
                    $end_date->modify(getDayAddIntervalString(60));
                    $end_date = $end_date->format('Y-m-d');
                }

                $diff_date = getDaysDiff($start_date, $end_date);
                
                if ( $diff_date >= 0 && $diff_date < MONTH ) {
                    $available_slots = $this->available_slots->GetAvailableSlotsByDateRange($start_date, $end_date, $manual_sunset_time_hr, $manual_sunset_time_mins, $shift_start_time, $sunset_time_mode, $shift_start_time_mins, $commute_time, $installation_size, $last_avail_start_time, $max_appoints, $zip_code, $holidays_array, $max_shift_end_time_hr, $max_shift_end_time_mins);
                    if ( !($this->input->get("start_date") && $this->input->get("end_date")) ) {
                        $first_available_slot_count = $this->available_slots->GetAvailableSlotIndex($available_slots);
                        if ( $first_available_slot_count != - 1 ) {
                            $num_of_days = 7;
                            $next_num_of_days_available_slots = $this->available_slots->GetNextNumOfDaysAvailableSlot($available_slots, $first_available_slot_count, $num_of_days);
                            $available_slots = $next_num_of_days_available_slots;
                        }
                        else {
                            $error_msg = [
                                  "message" => MONTH_DATA_NOT_AVAILABLE_MSG
                            ];
                        }
                    }
                }
                else {
                    if ( $diff_date < 0 ) {
                        $error_msg = [
                              "message" => DATE_RANGE_LESS_MSG
                        ];
                    }
                    else {
                        $error_msg = [
                              "message" => DATE_RANGE_MSG
                        ];
                    }
                    return $this->output->set_content_type('application/json')->set_output(json_encode($error_msg));
                }
                return $this->output
                                ->set_header('Access-Control-Allow-Origin:*')
                                ->set_content_type('application/json')
                                ->set_output(json_encode($available_slots));
            }
            else {
                $error_msg = [
                      "message" => ZIP_CODE_NOT_FOUND
                ];
                return $this->output->set_content_type('application/json')->set_output(json_encode($error_msg));
            }
        }
        else {
            $error_msg = ["message" => ZIP_CODE_ERROR_MSG];
            return $this->output
                            ->set_content_type('application/json')
                            ->set_output(json_encode($error_msg));
        }
    }

}
