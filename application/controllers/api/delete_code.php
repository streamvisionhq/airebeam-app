<?php

/*
 * This class syncs the Airebeam customer mbr credit card in our database
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Delete_Code extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library("CustomerAccessOK");
        $this->customerAccessOK = new CustomerAccessOK();
    }

    /**
     * Fetch all Services of given MBR
     *
     * @param <string> MBR
     * @return <json> Services
     */
    public function index() {
        if ($this->input->get("mbr")) {
            $this->customerAccessOK->deleteToken($mbr);
            echo json_encode(["status" => "1", "message" => "Token Deleted"]);
        } else {
            echo json_encode($this->customerAccessOK->returnResponse("2"));
        }
    }

}
