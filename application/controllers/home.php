<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Home extends Front_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model("M_worker");
        $this->worker = new M_worker();
    }

    public function index() {
        add_meta_title("Home");
        if ($this->worker->isWorkerLogin()) {
            redirect(BASE_PATH."orders");
        }else{
            redirect(BASE_PATH."login");
        }
        $this->load->view("home",$this->data);
    }

}
