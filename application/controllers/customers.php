<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Customers extends CI_Controller {

    public function __construct() {

        parent::__construct();

        $this->load->model("M_curl");
        $this->curl = new M_curl();
    }

    public function index() {
        $emeraldPush = json_decode(json_encode($_POST));
        $this->curl->logTime("Hit on customer endpoint");
        if (!empty($emeraldPush->CustomerID)) {
            $this->curl->logTime("push recieve for this CustomerID = $emeraldPush->CustomerID");
            // inserting emerald raw push in log table
            $this->logEmeraldPush($emeraldPush);
            $this->curl->logTime("raw push save to log table");
            // check if this customer exists in database
            $customerData = $this->checkExistsCustomer($emeraldPush->CustomerID);

            // Service Handling
            if ($service = $this->getServiceFromEmerald($emeraldPush->AccountID)) {
                // log in assets/logs.txt
                $this->curl->logTime("service " . $emeraldPush->AccountID . " service fetched from emerald.");
            } else {
                // log in assets/logs.txt
                $this->curl->logTime("service " . $emeraldPush->AccountID . " service unable to fetched from emerald");
            }

            if (checkInteger($emeraldPush->CustomerID)) {
                if ($customerData === FALSE) {
                    // add customer in database
                    if ($this->addCustomer($emeraldPush, $service) && $this->addService($emeraldPush, $service)) {
                        $this->data["msg"] = "looks good";
                        $this->load->view("xml-header", $this->data);
                    } else {
                        $this->data["msg"] = "push for $emeraldPush->CustomerID not inserted in database";
                        $this->load->view("xml-header", $this->data);
                        // need to add log here
                    }
                } else {
                    // update customer in database
                    if ($this->updateCustomer($emeraldPush, $service)) {
                        // check if this service exists in database
                        $serviceData = $this->checkExistsService($emeraldPush->AccountID);
                        if ($serviceData === FALSE) {
                            // add service in database
                            if ($this->addService($emeraldPush, $service)) {
                                $this->data["msg"] = "looks good";
                                $this->load->view("xml-header", $this->data);
                            } else {
                                $this->data["msg"] = "service " . $emeraldPush->AccountID . " not inserted in database";
                                $this->load->view("xml-header", $this->data);
                            }
                        } else {
                            // update service in database
                            if ($this->updateService($emeraldPush, $service)) {
                                $this->data["msg"] = "looks good";
                                $this->load->view("xml-header", $this->data);
                            } else {
                                $this->data["msg"] = "service " . $emeraldPush->AccountID . " not updated in database";
                                $this->load->view("xml-header", $this->data);
                            }
                        }
                    } else {
                        $this->data["msg"] = "customer " . $emeraldPush->CustomerID . " not updated in database";
                        $this->load->view("xml-header", $this->data);
                    }
                }
            } else {
                // log in assets/logs.txt
                $this->curl->logTime("customer id is not integer value");

                $this->data["msg"] = "push for $emeraldPush->CustomerID is not integer and not inserted in database";
                $this->load->view("xml-header", $this->data);
                // need to add log here
            }
        } else {
            $this->data["msg"] = "Empty Hit";
            $this->load->view("xml-header", $this->data);
        }
    }

}

?>