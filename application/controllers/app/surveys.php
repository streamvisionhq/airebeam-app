<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Surveys extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model("m_admin");
        $this->admin = new M_admin();
    }

    public function index() {
        add_meta_title("Pending Surveys List");
        $this->data['page_slug'] = "surveys";
        if (!isset($_COOKIE["last_updated_surveys"])) {
            setcookie("last_updated_surveys", date("Y-m-d H:i:s"));
        }
        
        if ($this->admin->isAdminLogin()) {
            if (in_array(CALENDAR, (array) $this->previlages)) {
                $this->load->view("admin/survey-list", $this->data);
            }else{
                redirect(ADMIN_BASE_PATH . "calendar");
            }
        } else {
            redirect(URL_LOGIN, "refresh");
        }
    }

}
