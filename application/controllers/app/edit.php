<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Edit extends Admin_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function worker($workerID) {
        $this->load->model("m_admin");
        $this->admin = new M_admin();        
        add_meta_title("Edit Worker");
        $this->data['page_slug'] = "edit_worker";
        if (!isset($_COOKIE["last_updated_edit_worker"])) {
            setcookie("last_updated_edit_worker", date("Y-m-d H:i:s"));
        }

        if ($this->admin->isAdminLogin()) {
            if (in_array(USER_MANAGEMENT, (array) $this->previlages)) {
                $this->data["roles"] = $this->admin->getRoles();
                $this->data["worker"] = $this->admin->workerDetail($workerID);
                if ($this->input->post("submit")) {
                    $editWorker = $this->admin->editWorker($workerID);
                    if ($editWorker === TRUE) {
                        $this->session->set_userdata(array('msg' => WORKER_EDITED));
                        redirect(ADMIN_BASE_PATH . "edit/worker/" . $workerID);
                    } else {
                        $this->session->set_userdata(array('error' => $editWorker));
                        redirect(ADMIN_BASE_PATH . "edit/worker/" . $workerID);
                    }
                }
                $this->load->view("admin/edit-worker", $this->data);
            } else {
                echo NOT_FOUND;
            }
        } else {
            redirect(URL_LOGIN, "refresh");
        }
    }

    public function order($idOrder) {
        add_meta_title($this->router->class . " Order");
        $this->load->model("M_order");
        $this->order = new M_order();
        $this->data["services"] = $this->order->getServices();
        $this->data["statuses"] = $this->order->getStatuses();
        $this->data["workers"] = $this->order->getWorkers();
        $this->data["customers"] = $this->order->getCustomers();
        $this->data["durations"] = $this->order->getDurations();
        $this->data["order"] = $this->order->getOrder($idOrder);
        if ($this->order->isAdminLogin()) {
            if ($this->input->post("submit")) {
                $result = $this->order->editOrder($idOrder);
                if ($result === TRUE) {
                    $this->session->set_userdata(array('msg' => "Incident has been added"));
                } else {
                    $this->session->set_userdata(array('error' => $result));
                }
            }
            if (!empty($this->data)) {
                $this->load->view("admin/edit-order", $this->data);
            } else {
                $this->load->view("admin/edit-order");
            }
        } else {
            redirect(URL_LOGIN, "refresh");
        }
    }

    public function service($orderServiceID) {
        add_meta_title("Edit Service");
        $this->data['page_slug'] = "edit_service";
        if (!isset($_COOKIE["last_updated_edit_service"])) {
            setcookie("last_updated_edit_service", date("Y-m-d H:i:s"));
        }
        $this->load->model("m_admin");
        $this->admin = new M_admin();

        if ($this->admin->isAdminLogin()) {
            if (in_array(SERVICE_MANAGEMENT, (array) $this->previlages)) {
                $this->data["order_service"] = $this->admin->getOrderService($orderServiceID);
                if ($this->input->post("submit")) {
                    $editOrderService = $this->admin->editOrderService($orderServiceID);
                    if ($editOrderService === TRUE) {
                        $this->session->set_userdata(array('msg' => SERVICE_EDITED));
                    } else {
                        $this->session->set_userdata(array('error' => $editOrderService));
                    }
                }
                $this->load->view("admin/edit-order-service", $this->data);
            } else {
                echo NOT_FOUND;
            }
        } else {
            redirect(URL_LOGIN, "refresh");
        }
    }

    public function town($townID) {
        add_meta_title("Edit Town");
        $this->data['page_slug'] = "edit_town";
        if (!isset($_COOKIE["last_updated_edit_town"])) {
            setcookie("last_updated_edit_town", date("Y-m-d H:i:s"));
        }
        
        $this->load->model("m_admin");
        $this->admin = new M_admin();

        if ($this->admin->isAdminLogin()) {
            if (in_array(SERVICE_MANAGEMENT, (array) $this->previlages)) {
                $this->data["town"] = $this->admin->getTown($townID);
                if ($this->input->post("submit")) {
                    $editTown = $this->admin->editTown($townID);
                    if ($editTown === TRUE) {
                        $this->session->set_userdata(array('msg' => TOWN_EDITED));
                    } else {
                        $this->session->set_userdata(array('error' => $editTown));
                    }
                }
                $this->load->view("admin/edit-town", $this->data);
            } else {
                echo NOT_FOUND;
            }
        } else {
            redirect(URL_LOGIN, "refresh");
        }
    }

    public function messages() {
        add_meta_title("Edit SMS Messages");
        $this->data['page_slug'] = "edit_messages";
        if (!isset($_COOKIE["last_updated_edit_messages"])) {
            setcookie("last_updated_edit_messages", date("Y-m-d H:i:s"));
        }
        $this->load->model("m_admin");
        $this->admin = new M_admin();
        if ($this->input->post()) {
            if ($this->input->post("create-appointment")) {
                $array = array("Message" => $this->input->post("create-appointment"));
                $respons = $this->admin->updateMessage($array, "023b67e15c2b5");
            } elseif ($this->input->post("change-appointment")) {
                $array = array("Message" => $this->input->post("change-appointment"));
                $respons = $this->admin->updateMessage($array, "d6d08e3487b59");
            } elseif ($this->input->post("on-enoroute")) {
                $array = array("Message" => $this->input->post("on-enoroute"));
                $respons = $this->admin->updateMessage($array, "9634e9d91933d");
            } elseif ($this->input->post("replied-no")) {
                $array = array("Message" => $this->input->post("replied-no"));
                $respons = $this->admin->updateMessage($array, "76d08e3487b57");
            } elseif ($this->input->post("replied-other")) {
                $array = array("Message" => $this->input->post("replied-other"));
                $respons = $this->admin->updateMessage($array, "96d0de348fb57");
            } elseif ($this->input->post("password-reset")) {
                $array = array("Message" => $this->input->post("password-reset"));
                $respons = $this->admin->updateMessage($array, "d6d08e3487d53");
            }
            if ($respons === true) {
                $this->session->set_userdata(array('msg' => MESSAGE_EDITED));
            } else {
                $this->session->set_userdata(array('error' => MESSAGE_ERROR));
            }
        }
        $this->data["messages"] = $this->admin->getAllMessages();
        $this->load->view("admin/edit-messages", $this->data);
    }

}
