<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class SunsetTime extends Admin_Controller {

	public function __construct() {
		parent::__construct();
	}
	
	/*
	 * Get next 15 days sunset time data
	 */
	public function fetchtime() {
		$this->load->model("M_sunset_time");
		$this->sunset_time = new M_sunset_time();
		$this->load->model ( "M_admin" );
		$this->admin = new M_admin ();
		$key_names = array (
				'SHIFT_START_TIME_HR',
				'API_FAILED_NOTIFY_EMAIL',
				'SHIFT_START_TIME_MINS'
		);
		$settings = $this->admin->GetConfigSettings ( $key_names );
		$start_time = DateTime::createFromFormat('H:i:s', $settings[1]->Value . ":" . $settings[2]->Value . ":00");
		$start_time = $start_time->format('H:i:s');
		$notify_api_failed_email = $settings[0]->Value;
		$current_date = new DateTime();
// 		Change this to 15 days after testing
		$next_thirty_date = (new DateTime())->modify('+300 day');
		$last_data_date = $this->sunset_time->getLastDate();
		if( is_array($last_data_date) ){
			$last_data_date = $last_data_date[0]->date;
			if( $last_data_date == null) {
				// Call FetchSunsetTime from current date to next 15 days
				$this->sunset_time->StoreInDB ( $start_time, $current_date, $next_thirty_date, $notify_api_failed_email);
				
			}else {
				$last_data_date_obj = new DateTime($last_data_date);
				$diff_last_data_date = (int)date_diff($last_data_date_obj, $next_thirty_date)->format('%R%a');;
				if( $diff_last_data_date > 0){
					$last_data_date_obj->modify('+1 day');
// 					Call FetchSunsetTime from diff_last_data_date to next 15 days from current
					$this->sunset_time->StoreInDB ( $start_time, $last_data_date_obj, $next_thirty_date, $notify_api_failed_email);
				}
			}
			
		}
		else{
// 			Error
		}
	}
	
	/*
	 * Delete previous month's sunset time data
	 */
	
	public function deleteprevioustimes() { 
		$this->load->model("M_sunset_time");
		$this->sunset_time = new M_sunset_time();
		$datetime = new DateTime();
		$datetime->modify('+1 day');
		$datetime_str = $datetime->format('Y-m-d');
		$delete_previous_data = $this->sunset_time->deleteSunsetTimeByDate($datetime_str);
		if($delete_previous_data){
			echo SUNSET_TIME_DELETE_SUCCESS;
		} else {
			echo SUNSET_TIME_DELETE_FAILED;
		}
	}
	

		
}