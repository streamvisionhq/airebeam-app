<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Search extends Admin_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->load->model("M_admin");
        $this->admin = new M_admin();
        add_meta_title("Search MBR");
        $this->data['page_slug'] = "search_mbr";
        if (!isset($_COOKIE["last_updated_search_mbr"])) {
            setcookie("last_updated_search_mbr", date("Y-m-d H:i:s"));
        }
        if ($this->admin->isAdminLogin()) {
            if (!empty($this->previlages) && defined("SEARCH_APPOINTMENT")) {
                $this->data["role"] = $this->admin->getRoleName($this->session->userdata('RoleID'));
                $this->data['worker_data'] = $this->M_admin->AllTechniciansDetail();
                $this->data['service_name'] = $this->M_admin->getAllOrderServices();
                if ($this->input->post("customer") && $this->input->post("mbr_search")) {
                    $this->db->like('CustomerName', $this->input->post("customer"));
                    $query = $this->db->get('customer');
                    $customer = $query->first_row();
                    if (!empty($customer)) {
                        $this->data["customer_detail"]["customer"] = $customer;
                        if ($orders = $this->admin->get90daysOrdersByCustomerID($customer->CustomerID)) {
                            $this->data["customer_detail"]["orders"][] = $orders;
                        }
                        $this->data["customer_detail"]["activities"] = $this->admin->getFPRActivities($customer->CustomerID);
                    } else {
                        $this->session->set_userdata(array('error' => "Customer Not Found"));
                    }
                }
                if ($this->input->post("global_search")) {
                    $input_global = !empty($_POST['global_search_text']) ? $_POST['global_search_text'] : null;
                    $keywords = explode(' ', $input_global);
                    foreach($keywords as $keyword){
                        if($keyword != ''){
                            $data['global'] = $keywords;
                            break;
                        }
                    }
                    if (empty($_POST['start_date']) && empty($_POST['end_date'])) {
                        $data['start_date'] = date('Y-m-d');
                        $data['end_date'] = date('Y-m-d', strtotime("+2 weeks"));
                    } else {
                        $data['start_date'] = !empty($_POST['start_date']) ? date('Y-m-d', strtotime($_POST['start_date'])) : null;
                        $data['end_date'] = !empty($_POST['end_date']) ? date('Y-m-d', strtotime($_POST['end_date'])) : null;
                    }
                    $order = $this->M_admin->global_search($data);
                    if (!empty($order)) {
                        $this->data["global_order_search"] = $order;
                    }
                }
                $this->load->view("admin/search-customer", $this->data);
            } else {
                redirect(ADMIN_BASE_PATH . "404", "refresh");
            }
        } else {
            redirect(URL_LOGIN, "refresh");
        }
    }

}
