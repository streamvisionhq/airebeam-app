<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Sms_Failure_Log extends Admin_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function all($id = 0) {
        $this->load->model("M_twilio");
        $this->twilio = new M_twilio();
        add_meta_title("Sms Failure Log");
        $this->data['page_slug'] = "sms_failure_log";


        // Get Failure sms
        $response = $this->twilio->getAllFailedSMSForCustomer();
        $this->data['failed_sms'] = $response;

        if (!isset($_COOKIE["last_updated_sms_failure_log"])) {
            setcookie("last_updated_sms_failure_log", date("Y-m-d H:i:s"));
        }
        
        if ($this->order->isAdminLogin()) {
            if (in_array(SMS_FAILURE_LOG, (array) $this->previlages)) {
                $this->order->updateAllNewFailureSms();

                $config['base_url'] = ADMIN_BASE_PATH . "sms_failure_log/all";
                
                $this->load->view("admin/sms_failure_log", $this->data);
            }else{
                echo NOT_FOUND;
            }
        } else {
            redirect(URL_LOGIN, "refresh");
        }
    }

}
