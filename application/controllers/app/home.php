<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Home extends Admin_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->load->model("m_admin");
        $this->admin = new M_admin();
        add_meta_title("Worker List");
        $this->data['page_slug'] = "worker_list";
        if (!isset($_COOKIE["last_updated_worker_list"])) {
            setcookie("last_updated_worker_list", date("Y-m-d H:i:s"));
        }
        
        if ($this->admin->isAdminLogin()) {
            if (in_array(USER_MANAGEMENT, (array) $this->previlages)) {
                $AllWorkers = $this->admin->AllWorkerDetail();
                $this->data["AllWorkers"] = $AllWorkers;
                $this->load->view("admin/workers", $this->data);
            }else{
                redirect(ADMIN_BASE_PATH . "calendar");
            }
        } else {
            redirect(URL_LOGIN, "refresh");
        }
    }

}
