<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class TermsConditions extends Admin_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->load->model("M_admin");
        $this->admin = new M_admin();
        add_meta_title("Terms & Conditions");
        $this->data['page_slug'] = "terms_conditions";
        if (!isset($_COOKIE["last_updated_terms_conditions"])) {
            setcookie("last_updated_terms_conditions", date("Y-m-d H:i:s"));
        }

        if ($this->admin->isAdminLogin()) {
            if ($this->input->post("submit")) {
                $config['upload_path'] = BASEPATH . '../assets/admin/terms/';
                $config['allowed_types'] = 'pdf';
                $this->load->library('upload', $config);
                if (!$this->uploadTOC()) {
                    $error = array('error' => $this->upload->display_errors());
                    $this->session->set_userdata(array('error' => strip_tags($error["error"])));
                    redirect($this->uri->uri_string());
                } else {
                    $response = $this->upload->data();
                    $this->uploadTOC(true);
                    $this->admin->Insert("term", ["TermID" => uniqid(), "Filename" => $response["file_name"], "Date" => date("Y-m-d H:i:s")]);
                    $this->admin->UpdateAll("customer", ["TermsAccepted" => "0"]);
                    $this->session->set_userdata(array('msg' => FILE_UPLOADED));
                    redirect($this->uri->uri_string());
                }
            }
            $this->db->select('*');
            $this->db->order_by('Date', 'DESC');
            $this->data["terms"] = $this->db->get('term')->result_array();
            $this->load->view("admin/terms-conditions", $this->data);
        } else {
            redirect(URL_LOGIN, "refresh");
        }
    }

    public function uploadTOC($current = false) {
        $this->load->library("CustomerAccessOK");
        $this->customerAccessOK = new CustomerAccessOK();
        $config['upload_path'] = BASEPATH . '../assets/admin/terms/';
        $config['allowed_types'] = 'pdf';
        if ($current) {
            $config['upload_path'] = BASEPATH . '../assets/admin/terms/current/';
            $config['file_name'] = $this->customerAccessOK->term_name;
            $config['overwrite'] = true;
        }
        $this->upload->initialize($config);
        $this->load->library('upload', $config);
        if (!$this->upload->do_upload('term')) {
            return false;
        }
        return true;
    }

}
