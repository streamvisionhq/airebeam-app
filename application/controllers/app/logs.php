<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Logs extends Admin_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function all($id = 0) {
        if (defined("LOGS") && in_array(LOGS, (array) $this->previlages)) {
            $search_string = false;
            $this->load->model("M_admin");
            $this->admin = new M_admin();
            $this->load->library('pagination');
            add_meta_title("View Logs");
            $this->data['page_slug'] = "view_logs";
            if (!isset($_COOKIE["last_updated_view_logs"])) {
                setcookie("last_updated_view_logs", date("Y-m-d H:i:s"));
            }

            if ($this->admin->isAdminLogin()) {
                if ($this->input->get("customer")) {
                    $search_string = $this->input->get("customer");
                    $search = explode(" ", $search_string);
                    $this->db->where('CustomerID', $search[0]);
                }
                if($this->input->get("from")){
                    $from = $this->input->get("from");
                    $this->db->where('DATE(Date) >=', $from);
                }
                if($this->input->get("to")){
                    $to = $this->input->get("to");
                    $this->db->where('DATE(Date) <=', $to);
                }
                $logs = $this->db->count_all_results("log");
                $config['base_url'] = ADMIN_BASE_PATH . "logs/all" . '?' . http_build_query($_GET);
                $config['page_query_string'] = TRUE;
                $config['query_string_segment'] = "page";
                $config['display_pages'] = TRUE;
                $config['total_rows'] = $logs;
                $config['use_page_numbers'] = TRUE;
                $config['uri_segment'] = 3;
                $config['per_page'] = 10;
                $config['next_link'] = '&gt;&gt;';
                $config['next_tag_open'] = '<li>';
                $config['next_tag_close'] = '</li>';
                $config['prev_link'] = '&lt;&lt;';
                $config['prev_tag_open'] = '<li>';
                $config['prev_tag_close'] = '</li>';
                $config['num_tag_open'] = '<li>';
                $config['num_tag_close'] = '</li>';
                $config['cur_tag_open'] = '<li class="active"><a>';
                $config['cur_tag_close'] = '</a></li>';
                $config['first_link'] = FALSE;
                $config['last_link'] = FALSE;

                $records_per_page = $config['per_page'];
                if ($this->input->get("page") == NULL || $this->input->get("page") == 1) {
                    $offset = 0;
                } else {
                    $offset = $this->input->get("page") - 1;
                    $offset = $offset * $records_per_page;
                }

                $this->db->select("*");
                if ($this->input->get("customer")) {
                    $search_string = $this->input->get("customer");
                    $search = explode(" ", $search_string);
                    $this->db->where('CustomerID', $search[0]);
                }
                if($this->input->get("from")){
                    $from = $this->input->get("from");
                    $this->db->where('DATE(Date) >=', $from);
                }
                if($this->input->get("to")){
                    $to = $this->input->get("to");
                    $this->db->where('DATE(Date) <=', $to);                    
                }
                $this->db->from('log');
                $this->db->limit($records_per_page, $offset);
                $this->db->order_by("Date", "desc");
                $limitedLogs = $this->db->get()->result_array();

                $this->pagination->initialize($config);
                $this->data["pagination"] = $this->pagination->create_links();
                $this->data["logs"] = $limitedLogs;
                $this->load->view("admin/logs", $this->data);
            } else {
                redirect(URL_LOGIN, "refresh");
            }
        } else {
            redirect(URL_LOGIN, "refresh");
        }
    }

}
