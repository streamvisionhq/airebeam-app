<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Services extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model("m_admin");
        $this->admin = new M_admin();
    }

    public function index() {
        add_meta_title("Services List");
        $this->data['page_slug'] = "service_list";
        if (!isset($_COOKIE["last_updated_service_list"])) {
            setcookie("last_updated_service_list", date("Y-m-d H:i:s"));
        }
        
        if ($this->admin->isAdminLogin()) {
            if (in_array(SERVICE_MANAGEMENT, (array) $this->previlages)) {
                $this->data["order_services"] = $this->admin->getAllOrderServices();
                $this->load->view("admin/services", $this->data);
            }else{
                redirect(ADMIN_BASE_PATH . "calendar");
            }
        } else {
            redirect(URL_LOGIN, "refresh");
        }
    }

}
