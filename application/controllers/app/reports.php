<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Reports extends Admin_Controller {

    public function __construct() {
        parent::__construct();
    }

    /**
     * show reports Weekly and Monthly
     */
    public function index() {
        $this->load->model("m_extend");
        $this->extend = new M_extend();
        add_meta_title("Reports");
        $this->data['page_slug'] = "reports";
        if (!isset($_COOKIE["last_updated_reports"])) {
            setcookie("last_updated_reports", date("Y-m-d H:i:s"));
        }
        
        if ($this->extend->isAdminLogin()) {
            if (in_array(CALENDAR, (array) $this->previlages)) {
                $services = $this->extend->getServices();
                if ($this->input->post("date_submit")) {
                    if ($this->input->post("from") || $this->input->post("to")) {
                        foreach ($services as $key => $service) {
                            if ($this->input->post("from") && $this->input->post("to")) {
                                $serviceOrders = $this->extend->getOrdersCountByServiceID($key, false, $this->input->post("from"), $this->input->post("to"));
                            } elseif ($this->input->post("from")) {
                                $serviceOrders = $this->extend->getOrdersCountByServiceID($key, false, $this->input->post("from"));
                            } elseif ($this->input->post("to")) {
                                $serviceOrders = $this->extend->getOrdersCountByServiceID($key, false, false, $this->input->post("to"));
                            }
                            $array["filter"][] = array(
                                "service" => $service,
                                "number" => $serviceOrders
                            );
                        }
                        $this->data["filter_by_date"] = $array;
                    }else{
                        goto script;
                    }
                } else {
                    script:
                    foreach ($services as $key => $service) {
                        $serviceOrders = $this->extend->getOrdersCountByServiceID($key, "last_week");
                        $array["week"]["last_week"][] = array(
                            "service" => $service,
                            "number" => $serviceOrders
                        );
                        $serviceOrders = $this->extend->getOrdersCountByServiceID($key, "this_week");
                        $array["week"]["this_week"][] = array(
                            "service" => $service,
                            "number" => $serviceOrders
                        );
                        $serviceOrders = $this->extend->getOrdersCountByServiceID($key, "next_week");
                        $array["week"]["next_week"][] = array(
                            "service" => $service,
                            "number" => $serviceOrders
                        );
                        $serviceOrders = $this->extend->getOrdersCountByServiceID($key, "last_month");
                        $array["month"]["last_month"][] = array(
                            "service" => $service,
                            "number" => $serviceOrders
                        );
                        $serviceOrders = $this->extend->getOrdersCountByServiceID($key, "this_month");
                        $array["month"]["this_month"][] = array(
                            "service" => $service,
                            "number" => $serviceOrders
                        );
                        $serviceOrders = $this->extend->getOrdersCountByServiceID($key, "next_month");
                        $array["month"]["next_month"][] = array(
                            "service" => $service,
                            "number" => $serviceOrders
                        );
                    }
                    $this->data["week_orders_count"] = $array;
                }
                $this->load->view("admin/reports", $this->data);
            } else {
                echo NOT_FOUND;
            }
        } else {
            redirect(URL_LOGIN, "refresh");
        }
    }

}
