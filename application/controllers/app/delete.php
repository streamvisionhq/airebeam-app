<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/**
 * we are not deleteing any record from database
 * so whenever any delete action  is perform for worker,service and town
 * we just update it in database with the status of delete
 * and this class performs all updation in database for delete worker,service and town
 */
class Delete extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model("M_admin");
        $this->admin = new M_admin();
    }

    /**
     * update status of worker to delete
     * @param string $workerID
     */
    public function worker($workerID) {
        $this->load->model("M_order");
        $this->order = new M_order();

        add_meta_title($this->router->class);
        $this->data['page_slug'] = "delete";
        if (isset($workerID)) {
            if (in_array(USER_MANAGEMENT, (array) $this->previlages)) {
                if (!$this->order->getAllOrders($workerID)) {
                    if ($this->order->isAdminLogin() == $workerID) {
                        $this->session->set_userdata(array('error' => ADMIN_OWN_ACCOUNT_DELETE));
                        redirect(ADMIN_BASE_PATH);
                    } else {
                        $data = array("StatusID" => "56b1e42d87332");
                        $this->admin->UpdateAll("worker", $data, array("WorkerID" => $workerID));
                        $this->session->set_userdata(array('msg' => WORKER_DELETED));
                        redirect(ADMIN_BASE_PATH);
                    }
                } else {
                    $this->session->set_userdata(array('error' => WORKER_CANNOT_DELETE));
                    redirect(ADMIN_BASE_PATH);
                }
            } else {
                echo NOT_FOUND;
            }
        }
    }

    /**
     * update status of service to delete
     * @param string $serviceID
     */
    public function service($serviceID) {
        $this->load->model("M_order");
        $this->order = new M_order();

        add_meta_title($this->router->class);
        if (isset($serviceID)) {
            if (in_array(SERVICE_MANAGEMENT, (array) $this->previlages)) {
                if($this->admin->getOrderService($serviceID)){
                    $data = array("StatusID" => "56b1e42d87332");
                    $this->admin->UpdateAll("order_type", $data, array("TypeID" => $serviceID));
                    $this->session->set_userdata(array('msg' => SERVICE_DELETED));
                    redirect(ADMIN_BASE_PATH . "services");
                }else{
                    $this->session->set_userdata(array('msg' => SERVICE_CANNOT_DELETE));
                }
            } else {
                echo NOT_FOUND;
            }
        }
    }
    
    /**
     * update status of town to delete
     * and update customer city code column if there is any record contain that city code
     * @param string $townID
     */
    public function town($townID) {
        $this->load->model("M_order");
        $this->order = new M_order();

        add_meta_title($this->router->class);
        if (isset($townID)) {
            if (in_array(TOWNS, (array) $this->previlages)) {
                if($town = $this->admin->getTown($townID)){
                    $data = array("StatusID" => "56b1e42d87332");
                    $this->admin->UpdateAll("towns", $data, array("TownID" => $townID));
                    $this->admin->UpdateAll("customer", array("CityCode" => NULL), array("CityCode" => $town["Alias"]));
                    $this->session->set_userdata(array('msg' => TOWN_DELETED));
                    redirect(ADMIN_BASE_PATH . "towns");
                }else{
                    $this->session->set_userdata(array('msg' => TOWN_CANNOT_DELETE));
                }
            } else {
                echo NOT_FOUND;
            }
        }
    }

}
