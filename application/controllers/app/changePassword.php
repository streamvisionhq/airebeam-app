<?php
if ( !defined('BASEPATH') )
      exit('No direct script access allowed');

class ChangePassword extends Admin_Controller {

      public function __construct() {
            parent::__construct();
      }

      public function index() {
            $this->load->model("M_admin");
            $this->admin = new M_admin();
            add_meta_title("Change Password");
            $this->data['page_slug'] = "change_password";
            if ( !isset($_COOKIE["last_updated_change_password"]) ) {
                  setcookie("last_updated_change_password", date("Y-m-d H:i:s"));
            }

            if ( $this->admin->isAdminLogin() ) {
                  $this->data["role"] = $this->admin->getRoleName($this->session->userdata('RoleID'));
                  if ( $this->input->post("submit") ) {
                        $Result = $this->admin->ChangePassword();
                        if ( $Result ) {
                              $this->session->set_userdata(array('error' => $Result));
                        } else {
                              $this->session->set_userdata(array('msg' => PASSWORD_CHANGE));
                        }
                  }
                  $this->load->view("admin/change-password", $this->data);
            } else {
                  redirect(URL_LOGIN, "refresh");
            }
      }

}
