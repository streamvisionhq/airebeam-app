<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Notes extends Admin_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function all($id = 0) {
        $this->load->model("M_order");
        $this->order = new M_order();
        $this->load->library('pagination');
        add_meta_title("Notes");
        $this->data['page_slug'] = "notes";
        if (!isset($_COOKIE["last_updated_notes"])) {
            setcookie("last_updated_notes", date("Y-m-d H:i:s"));
        }
        
        if ($this->order->isAdminLogin()) {
            if (in_array(NOTES, (array) $this->previlages)) {
                $this->order->updateAllNewNotes();
                $notes = $this->order->countAllNotes();

                $config['base_url'] = ADMIN_BASE_PATH . "notes/all";
                $config['total_rows'] = $notes;
                $config['use_page_numbers'] = TRUE;
                $config['uri_segment'] = 4;
                $config['per_page'] = 10;
                $config['next_link'] = '&gt;&gt;';
                $config['next_tag_open'] = '<li>';
                $config['next_tag_close'] = '</li>';
                $config['prev_link'] = '&lt;&lt;';
                $config['prev_tag_open'] = '<li>';
                $config['prev_tag_close'] = '</li>';
                $config['num_tag_open'] = '<li>';
                $config['num_tag_close'] = '</li>';
                $config['cur_tag_open'] = '<li class="active"><a>';
                $config['cur_tag_close'] = '</a></li>';
                $config['first_link'] = FALSE;
                $config['last_link'] = FALSE;

                $records_per_page = $config['per_page'];
                if ($id == NULL || $id == 1) {
                    $offset = 0;
                } else {
                    $offset = $id - 1;
                    $offset = $offset * $records_per_page;
                }
                $limitedNotes = $this->order->getLimitNotes($records_per_page, $offset);

                $this->pagination->initialize($config);
                $this->data["pagination"] = $this->pagination->create_links();
                $this->data["notes"] = $limitedNotes;
                $this->load->view("admin/notes", $this->data);
            }else{
                echo NOT_FOUND;
            }
        } else {
            redirect(URL_LOGIN, "refresh");
        }
    }

}
