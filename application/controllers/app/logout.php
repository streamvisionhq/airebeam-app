<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Logout extends Admin_Controller {

    public function __construct() {
        parent::__construct();
    }

    /**
     * @note destroy all session variables and redirect to home
     * redirect to login
     */
    public function index() {
        $this->load->model("m_worker");
        $this->worker = new M_worker();
        $this->worker->adminLogout();
        redirect(ADMIN_BASE_PATH);
    }

}
