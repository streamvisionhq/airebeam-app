<?php
if ( !defined('BASEPATH') )
      exit('No direct script access allowed');

class Calendar extends Admin_Controller {

      public function __construct() {
            parent::__construct();
      }

      public function index() {
            $this->load->model("M_order");
            $this->order = new M_order();
            add_meta_title("Schedule");

            $this->data['page_slug'] = "calendar";
            if ( !isset($_COOKIE["last_updated_calendar"]) ) {
                  setcookie("last_updated_calendar", date("Y-m-d H:i:s"));
            }

            if ( $this->order->isAdminLogin() ) {
                  if ( defined('CALENDAR') === true ) {
                        if ( !empty($this->previlages) && in_array(CALENDAR, (array) $this->previlages) ) {
                              if ( $this->input->post("submit") ) {
                                    $result = $this->order->addOrder();
                                    if ( $result ) {
                                          $this->session->set_userdata(array('msg' => INCIDENT_ADDED));
                                    } else {
                                          $this->session->set_userdata(array('error' => INCIDENT_ERROR));
                                    }
                              }
                              $this->load->view("admin/calendar", $this->data);
                        }
                  } elseif ( defined('SEARCH_APPOINTMENT') === true ) {
                        redirect(ADMIN_BASE_PATH . "search");
                  }
            } else {
                  redirect(URL_LOGIN, "refresh");
            }
      }

}
