<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class ZipCodes extends Admin_Controller {

	public function __construct() {
		parent::__construct();
	}
	
	public function index() {
		$this->load->model("M_admin");
		$this->admin = new M_admin();
		add_meta_title("Add/Manage Zip Codes");
		$this->data['page_slug'] = "manage_zip_codes";
		if (!isset($_COOKIE["last_add_zip_code"])) {
			setcookie("last_add_zip_code", date("Y-m-d H:i:s"));
		}
		
		if ($this->admin->isAdminLogin()) {
			if (in_array(USER_MANAGEMENT, (array) $this->previlages)) {
				if ($this->input->post("submit")) {
					$AddZipCode = $this->admin->AddZipCode();
					if ($AddZipCode === TRUE) {
						$this->session->set_userdata(array('msg' => "Zip Code has been added"));
					} else {
						$this->session->set_userdata(array('error' => $AddZipCode));
					}
					redirect(URL_ZIPCODES_LIST);
				}
				$AllWorkers = $this->admin->AllTechniciansDetail();
				$AllZipCodes = $this->admin->AllZipDetail(); 
				$AllWorkersMapping = $this->admin->AllTechniciansMappingDetail();
				$this->data["AllWorkers"] = $AllWorkers;
				$this->data["AllZipCodes"] = $AllZipCodes;
				$this->data["AllWorkersMapping"] = $AllWorkersMapping;
				
				$this->load->view("admin/manage-zip-codes", $this->data);
			}else{
				echo NOT_FOUND;
			}
		} else {
			redirect(URL_LOGIN, "refresh");
		}
	}
		
	/*
	 * Add and edit priority
	 */
	public function add() {
		$this->load->model("M_admin");
		$this->admin = new M_admin();
		add_meta_title("Add/Manage Zip Codes");
		$this->data['page_slug'] = "manage_zip_codes";
		if (!isset($_COOKIE["last_update_priority"])) {
			setcookie("last_update_priority", date("Y-m-d H:i:s"));
		}
		
		if ($this->admin->isAdminLogin()) {
			if (in_array(USER_MANAGEMENT, (array) $this->previlages)) {
				if ($this->input->post("submit")) {
					$post_data = $this->input->post();
// 					if parameter is set for updating zip code then update zip code also
// 					Delete old priority and user mapping
// 					Insert new priority and user mapping
						$AllZipCodes = $this->admin->AllZipDetail();
						$AllWorkers = $this->admin->AllTechniciansDetail();
						$zipCodeIdArray = array();
						
						$userMapping = array();
						foreach ( $AllZipCodes as $ZipCode ) {
							$CurrentZipCodeId = $ZipCode->ZipCodeId;
							array_push($zipCodeIdArray, $CurrentZipCodeId);
							foreach ( $AllWorkers as $Worker ) {
								$CurrentWorkerId = $Worker->WorkerID;
								$PriorityKey = 'zipcode-' . $CurrentZipCodeId . '-priority-' . $CurrentWorkerId;
								if( isset($post_data[$PriorityKey]) ){
									$userMapData = array(
											"ZipCodeId"        => 	$CurrentZipCodeId,
											"WorkerId"        => 	$CurrentWorkerId,
											"Priority"        => 	$post_data[$PriorityKey]
									);
									array_push($userMapping, $userMapData);
										
								}
							}
						}
						$this->admin->BatchRemoveZipPriorityMapping($zipCodeIdArray);
						$AddZipPriorityMapping = $this->admin->InsertZipUserMapping( $userMapping );
						if ($AddZipPriorityMapping === TRUE) {
							$this->session->set_userdata(array('msg' => "Zip Code and Priority mapping has been added"));
						} else {
							$this->session->set_userdata(array('error' => $AddZipPriorityMapping));
						}
						echo 'completed';
				}
			}else{
				echo NOT_FOUND;
			}
		} else {
			redirect(URL_LOGIN, "refresh");
		}
	}
	
	/*
	 * Delete mapping and zip code
	 */
	
	public function delete() {
		$this->load->model("M_admin");
		$this->admin = new M_admin();
		add_meta_title("Add/Manage Zip Codes");
		$this->data['page_slug'] = "manage_zip_codes";
		if (!isset($_COOKIE["last_delete_priority"])) {
			setcookie("last_delete_priority", date("Y-m-d H:i:s"));
		}
	
		if ($this->admin->isAdminLogin()) {
			if (in_array(USER_MANAGEMENT, (array) $this->previlages)) {
					$get_data = $this->input->get();
					if(isset($get_data["zip_id"])) {
						$zipCodeId = $get_data["zip_id"];

						$RemoveZipPriority = $this->admin->RemoveZipPriorityMapping( $zipCodeId );
						$RemoveZip = $this->admin->RemoveZip( $zipCodeId );
						if ($RemoveZip === TRUE && $RemoveZipPriority === TRUE) {
							$this->session->set_userdata(array('msg' => "Zip Code and Priority mapping has been deleted"));
						} else {
							$this->session->set_userdata(array('error' => $RemoveZip));
						}
						redirect(URL_ZIPCODES_LIST);
	
					}
					else {
						// 						Show error
					}
			}else{
				echo NOT_FOUND;
			}
		} else {
			redirect(URL_LOGIN, "refresh");
		}
	}
	
	
}