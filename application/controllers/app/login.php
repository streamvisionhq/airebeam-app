<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Login extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model("m_admin");
        $this->admin = new M_admin();

        $this->load->model("m_worker");
        $this->worker = new M_worker();
    }

    public function index() {
        add_meta_title("Sign in");
        $this->data['page_slug'] = "sign_in";
        if ($this->admin->isAdminLogin()) {
            redirect(ADMIN_BASE_PATH);
        } else {
            if($this->input->post()){
                $response = $this->admin->doAdminLogin();
                if($response["response"]){
                    $this->data["Msg"] = $response["Msg"];
                    if($this->session->userdata('RoleID')){
                        $name = $this->admin->getRoleName($this->session->userdata('RoleID'));
                        if($name == INSITE){
                            redirect(ADMIN_BASE_PATH."search");
                        }else{
                            redirect(ADMIN_BASE_PATH."calendar");
                        }
                    }
                }else{
                    $this->data["Error"] = $response["Msg"];
                }
            }
            $this->load->view("admin/login", $this->data);
        }
    }
}