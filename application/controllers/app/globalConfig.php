<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class GlobalConfig extends Admin_Controller {
	public function __construct() {
		parent::__construct();
	}
	
	public function index() {
		$this->load->model ( "M_admin" );
		$this->admin = new M_admin ();
		$this->load->model("M_sunset_time");
		$this->sunset_time = new M_sunset_time();
		$this->load->model("M_holidays");
		$this->holidays = new M_holidays();
		add_meta_title ( "Manage Global Settings" );
		$this->data ['page_slug'] = "manage_global_settings";
		if (! isset ( $_COOKIE ["last_updated_global_settings"] )) {
			setcookie ( "last_updated_global_settings", date ( "Y-m-d H:i:s" ) );
		}
		
		if ($this->admin->isAdminLogin ()) {
			if (in_array ( USER_MANAGEMENT, ( array ) $this->previlages )) {
				if ($this->input->post ( "submit" )) {
					$post_data = $this->input->post();
					if( isset($post_data['holiday_datepick']) && isset($post_data['holiday_label']) &&  !empty($post_data['holiday_label']) ) {
						$holiday_date = $post_data['holiday_datepick'];
						$holiday_date_obj = new DateTime($holiday_date);
						$holiday_label = $post_data['holiday_label'];
						$this->holidays->InsertHoliday($holiday_label, $holiday_date_obj->format('Y-m-d'));
					}
					
					$update_data = array(
							array(
									'Name' => 'MIN_COMMUTE_TIME' ,
									'Value' => $post_data["min_commute_time"]
							),
							array(
									'Name' => 'SIZE_OF_INSTALLATION' ,
									'Value' => $post_data["size_of_installation"]
							),
							array(
									'Name' => 'SUNSET_TIME_HR' ,
									'Value' => $post_data["manual_sunset_period"] == ANTE_MERIDIEM ? $post_data["manual_sunset_time_hr"] : ((int)$post_data["manual_sunset_time_hr"]) + 12
							),
							array(
									'Name' => 'SUNSET_TIME_MINS' ,
									'Value' => $post_data["manual_sunset_time_mins"]
							),
							array(
									'Name' => 'LAST_AVAILABLE_START_TIME' ,
									'Value' => $post_data["last_avail_start_time"]
							),
							array(
									'Name' => 'MAX_NUMBER_OF_APPOINTMENTS' ,
									'Value' => $post_data["max_appoints_per_day"]
							),
							array(
									'Name' => 'DRIVE_TIME_NOTIFY_EMAIL' ,
									'Value' => $post_data["drive_notify_email"]
							),
							array(
									'Name' => 'API_FAILED_NOTIFY_EMAIL' ,
									'Value' => $post_data["api_failed_notify_email"]
							),
							array(
									'Name' => 'SHIFT_START_TIME_HR' ,
									'Value' => $post_data["shift_start_period"] == ANTE_MERIDIEM ? $post_data["shift_start_time_hr"] : ((int)$post_data["shift_start_time_hr"]) + 12
							),
							array(
									'Name' => 'SUNSET_TIME_MODE' ,
									'Value' => $post_data["sunset_time_mode"]
							),
							array(
									'Name' => 'SHIFT_START_TIME_MINS' ,
									'Value' => $post_data["shift_start_time_mins"]
							),
							array(
									'Name' => 'MAX_SHIFT_END_TIME_HR' ,
									'Value' => $post_data["shift_end_period"] == ANTE_MERIDIEM ? $post_data["max_shift_end_time_hr"] : ((int)$post_data["max_shift_end_time_hr"]) + 12
							),
							array(
									'Name' => 'MAX_SHIFT_END_TIME_MINS' ,
									'Value' => $post_data["max_shift_end_time_mins"]
							)
							
							
							
					);
					$UpdateConfigSettings = $this->admin->UpdateConfigSettings($update_data);
					if ($UpdateConfigSettings === TRUE) {
						$this->session->set_userdata(array('msg' => "Config settings has been updated"));
						redirect(ADMIN_BASE_PATH . "globalConfig");
					} 
					
				}
				$key_names = array (
						'MIN_COMMUTE_TIME',
						'SIZE_OF_INSTALLATION',
						'LAST_AVAILABLE_START_TIME',
						'MAX_NUMBER_OF_APPOINTMENTS',
						'DRIVE_TIME_NOTIFY_EMAIL',
						'SUNSET_TIME_HR',
						'SUNSET_TIME_MINS',
						'API_FAILED_NOTIFY_EMAIL',
						'SHIFT_START_TIME_HR',
						'SUNSET_TIME_MODE',
						'SHIFT_START_TIME_MINS',
						'MAX_SHIFT_END_TIME_HR',
						'MAX_SHIFT_END_TIME_MINS'
				);
				$Settings = $this->admin->GetConfigSettings ( $key_names );
				$datetime = new DateTime();
				$timezone = 'America/Phoenix';
				$datetime_format_str = 'Y-m-d';
				$datetime_str = convertToArizonaDateTime($datetime, $timezone, $datetime_format_str);
				$current_day_sunset_time = $this->sunset_time->getDataByDate($datetime_str);
				$this->data ['Commute_Time_Config'] = $Settings [0]->Name == 'MIN_COMMUTE_TIME' ? $Settings [0]->Value : '';
				$this->data ['Size_Installation_Config'] = $Settings [1]->Name == 'SIZE_OF_INSTALLATION' ? $Settings [1]->Value : '';
				$this->data ['Last_Avail_Start_Time_Config'] = $Settings [2]->Name == 'LAST_AVAILABLE_START_TIME' ? $Settings [2]->Value : '';
				$this->data ['Max_Num_Appoints_Config'] = $Settings [3]->Name == 'MAX_NUMBER_OF_APPOINTMENTS' ? $Settings [3]->Value : '';
				$this->data ['Drive_Time_Email_Config'] = $Settings [4]->Name == 'DRIVE_TIME_NOTIFY_EMAIL' ? $Settings [4]->Value : '';
				$this->data ['Sunset_Time_Hr_Config'] = $Settings [5]->Name == 'SUNSET_TIME_HR' ? $Settings [5]->Value : '';
				$this->data ['Sunset_Time_Mins_Config'] = $Settings [6]->Name == 'SUNSET_TIME_MINS' ? $Settings [6]->Value : '';
				$this->data ['Api_Failed_Time_Email_Config'] = $Settings [7]->Name == 'API_FAILED_NOTIFY_EMAIL' ? $Settings [7]->Value : '';
				$this->data ['Shift_Start_Time_Hr_Config'] = $Settings [8]->Name == 'SHIFT_START_TIME_HR' ? $Settings [8]->Value : '';
				$this->data ['Sunset_Time_Mode_Config'] = $Settings [9]->Name == 'SUNSET_TIME_MODE' ? $Settings [9]->Value : '';
				$current_day_sunset_time_obj = DateTime::createFromFormat('H:i:s',$current_day_sunset_time[0]->EndTime);
				$this->data ['Api_Sunset_Time_Config'] = sizeof($current_day_sunset_time) == 1 ? $current_day_sunset_time_obj->format('h:i:s a') : API_SUNSET_TIME_NOT_AVAILABLE;
				$this->data ['Shift_Start_Time_Mins_Config'] = $Settings [10]->Name == 'SHIFT_START_TIME_MINS' ? $Settings [10]->Value : '';
				$this->data ['Max_Shift_End_Time_Hr_Config'] = $Settings [11]->Name == 'MAX_SHIFT_END_TIME_HR' ? $Settings [11]->Value : '';
				$this->data ['Max_Shift_End_Time_Mins_Config'] = $Settings [12]->Name == 'MAX_SHIFT_END_TIME_MINS' ? $Settings [12]->Value : '';
				$current_date = new DateTime();
				$this->data ['Holidays'] = $this->holidays->GetHolidays($current_date->format('Y-m-d'));
				$this->load->view ( "admin/manage-global-settings", $this->data );
			} else {
				echo NOT_FOUND;
			}
		} else {
			redirect ( URL_LOGIN, "refresh" );
		}
	}
	
	public function update() { 
		$this->load->model ( "M_admin" );
		$this->admin = new M_admin ();
		add_meta_title ( "Manage Global Settings" );
		$this->data ['page_slug'] = "manage_global_settings";
		if (! isset ( $_COOKIE ["last_updated_global_settings"] )) {
			setcookie ( "last_updated_global_settings", date ( "Y-m-d H:i:s" ) );
		}
	}
	
	public function delete_holiday() {
		$get_vars = $this->input->get();
		if( isset( $get_vars['id'] ) ) {
			$holiday_id = $get_vars['id'];
			$this->load->model("M_holidays");
			$this->holidays = new M_holidays();
			$this->holidays->RemoveHoliday( $holiday_id );
			$this->session->set_userdata(array('msg' => "Holiday has been deleted"));
			redirect(URL_GLOBAL_CONFIG);
		}
	}
}