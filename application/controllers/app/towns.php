<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Towns extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model("m_admin");
        $this->admin = new M_admin();
    }

    public function index() {
        add_meta_title("Towns List");
        $this->data['page_slug'] = "towns";
        if (!isset($_COOKIE["last_updated_towns"])) {
            setcookie("last_updated_towns", date("Y-m-d H:i:s"));
        }
        
        if ($this->admin->isAdminLogin()) {
            if (in_array(TOWNS, (array) $this->previlages)) {
                $this->data["towns"] = $this->admin->getAllTowns();
                $this->load->view("admin/towns", $this->data);
            }else{
                redirect(ADMIN_BASE_PATH . "calendar");
            }
        } else {
            redirect(URL_LOGIN, "refresh");
        }
    }

}
