<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Add extends Admin_Controller {

    public function __construct() {
        parent::__construct();
    }

    /**
     * add worker to database     * 
     */
    public function index() {
        $this->load->model("m_admin");
        $this->admin = new M_admin();
        add_meta_title("Add Worker");
        $this->data['page_slug'] = "add_worker";
        if (!isset($_COOKIE["last_updated_add_worker"])) {
            setcookie("last_updated_add_worker", date("Y-m-d H:i:s"));
        }
        
        if ($this->admin->isAdminLogin()) {
            if (in_array(USER_MANAGEMENT, (array) $this->previlages)) {
                $this->data["roles"] = $this->admin->getRoles();
                if ($this->input->post("submit")) {
                    $AddWorker = $this->admin->AddWorker();
                    if ($AddWorker === TRUE) {
                        $this->session->set_userdata(array('msg' => "Worker has been added successfully"));
                        redirect(ADMIN_BASE_PATH . "add");
                    } else {
                        $this->session->set_userdata(array('error' => $AddWorker));
                        redirect(ADMIN_BASE_PATH . "add");
                    }
                }
                $this->load->view("admin/add-worker", $this->data);
            }else{
                echo NOT_FOUND;
            }
        } else {
            redirect(URL_LOGIN, "refresh");
        }
    }
    
    /**
     * add service to database     * 
     */
    public function service(){
        add_meta_title("Add Service");
        $this->data['page_slug'] = "add_service";
        if (!isset($_COOKIE["last_updated_add_service"])) {
            setcookie("last_updated_add_service", date("Y-m-d H:i:s"));
        }
        if ($this->admin->isAdminLogin()) {
            if (in_array(SERVICE_MANAGEMENT, (array) $this->previlages)) {
                if ($this->input->post("submit")) {
                    $AddService = $this->admin->addOrderService();
                    if ($AddService === TRUE) {
                        $this->session->set_userdata(array('msg' => SERVICE_ADDED));
                        redirect(ADMIN_BASE_PATH . "add/service");
                    } else {
                        $this->session->set_userdata(array('error' => $AddService));
                        redirect(ADMIN_BASE_PATH . "add/service");
                    }
                }
                $this->load->view("admin/add-order-service", $this->data);
            }else{
                echo NOT_FOUND;
            }
        } else {
            redirect(URL_LOGIN, "refresh");
        }
    }
    
    /**
     * add town to database
     */
    public function town(){
        add_meta_title("Add Town");
        $this->data['page_slug'] = "add_town";
        if (!isset($_COOKIE["last_updated_add_town"])) {
            setcookie("last_updated_add_town", date("Y-m-d H:i:s"));
        }
        if ($this->admin->isAdminLogin()) {
            if (in_array(TOWNS, (array) $this->previlages)) {
                if ($this->input->post("submit")) {
                    $addTown = $this->admin->addTown();
                    if ($addTown === TRUE) {
                        $this->session->set_userdata(array('msg' => TOWN_ADDED));
                        redirect(ADMIN_BASE_PATH . "add/town");
                    } else {
                        $this->session->set_userdata(array('error' => $addTown));
                        redirect(ADMIN_BASE_PATH . "add/town");
                    }
                }
                $this->load->view("admin/add-town", $this->data);
            }else{
                echo NOT_FOUND;
            }
        } else {
            redirect(URL_LOGIN, "refresh");
        }
    }

}
