<?php
/**
 * provides common functionality that other models in this app use
 */
if ( !defined('BASEPATH') )
    exit('No direct script access allowed');

class MY_Model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    /**
     * @param string $workerID
     * @note check from database that worker exists or not with provided id
     * @return boolean true if found record or false if not found worker id in database
     */
    public function IsWorker($workerID) {
        if ( $workerID ) {
            $rows = $this->SelectAllWhere("worker", array("WorkerID" => $workerID));
            $numberOfRecords = $rows->num_rows();
            if ( $numberOfRecords > 0 ) {
                return TRUE;
            }
            else {
                return FALSE;
            }
        }
    }

    /**
     * @note check if worker is authentic
     * @return id if success else return boolean false if not found record in database
     */
    public function isAdminLogin() {
        $workerID = $this->session->userdata('AdminID');
        $Authentic = $this->SelectAllWhere("worker", array("WorkerID" => $workerID, "StatusID" => "44cde42d87552"));
        $Authentic = $Authentic->num_rows();
        if ( $Authentic == "1" ) {
            return $workerID;
        }
        else {
            $array_items = array('AdminID' => '');
            $this->session->unset_userdata($array_items);
            return false;
        }
    }

    /**
     * @note check if worker is authentic
     * @return id if success else return boolean false if not found record in database
     */
    public function isWorkerLogin() {
        $workerID = $this->session->userdata('WorkerID');
        $Authentic = $this->SelectAllWhere("worker", array("WorkerID" => $workerID, "StatusID" => "44cde42d87552"));
        $Authentic = $Authentic->num_rows();
        if ( $Authentic == "1" ) {
            return $workerID;
        }
        else {
            $array_items = array('WorkerID' => '');
            $this->session->unset_userdata($array_items);
            return false;
        }
    }

    /**
     * @note get assigned color to worker
     * color is in rgb format
     * @return string
     */
    public function getWorkerColor($workerID) {
        $sql = "SELECT worker.Color FROM worker WHERE worker.WorkerID = '$workerID'";
        $rows = $this->db->query($sql);
        $result = $rows->first_row('array');
        return $result["Color"];
    }

    /**
     * @note get worker's name
     * @return string
     */
    public function getWorkerName($workerID) {
        $sql = "SELECT worker.Name FROM worker WHERE worker.WorkerID = '$workerID'";
        $rows = $this->db->query($sql);
        $result = $rows->first_row('array');
        return $result["Name"];
    }

    /**
     * @param string $orderID id of order
     * fetch single order from database
     * @return array or boolean false if not found any order on provided id
     */
    public function getOrder($orderID) {
        if ( $orderID ) {
            // join tables order & customer to fetch all necessary data for orders
            $sql = "SELECT `order`.OrderID,`order`.`Name`,`order`.WorkerID,worker.`Name` AS Worker,
                        worker.Email,worker.APIKey,`order`.CustomerID,customer.CustomerName,`order`.StatusID,
                        order_status.`Name` AS `Status`,`order`.TypeID,order_type.`Name` AS Type,
                        `order`.Detail,`order`.Date,`order`.Duration,customer.Address1,customer.Email AS CustomerEmail,
                        customer.Address2,customer.Zip,customer.City,customer.PhoneHome,customer.isPhoneHomeLandline,service.ServiceID
                    FROM `order`
                        INNER JOIN worker ON `order`.WorkerID = worker.WorkerID
                        INNER JOIN customer ON `order`.CustomerID = customer.CustomerID
                        INNER JOIN service ON customer.CustomerID = service.CustomerID
                        INNER JOIN order_type ON `order`.TypeID = order_type.TypeID
                        INNER JOIN order_status ON `order`.StatusID = order_status.StatusID
                    WHERE `order`.OrderID = '{$orderID}' AND `order`.StatusID != '46b1e4fd15378'";

            $rows = $this->db->query($sql);
            $numberOfRecords = $rows->num_rows();

            // check if rows are more then 0
            if ( $numberOfRecords > 0 ) {
                return $result = $rows->first_row('array');
            }
            else {
                return FALSE;
            }
        }
    }

    /**
     * @param string $orderID id of order
     * fetch single order from database
     * @return array or boolean false if not found any order on provided id
     */
    public function getOrderWithoutService($orderID) {
        if ( $orderID ) {
            // join tables order & customer to fetch all necessary data for orders
            $sql = "SELECT `order`.OrderID,`order`.`Name`,`order`.WorkerID,worker.`Name` AS Worker,
    		worker.Email,worker.APIKey,`order`.CustomerID,customer.CustomerName,`order`.StatusID,
    		order_status.`Name` AS `Status`,`order`.TypeID,order_type.`Name` AS Type,
    		`order`.Detail,`order`.Date,`order`.Duration,customer.Address1,customer.Email AS CustomerEmail,
    		customer.Address2,customer.Zip,customer.City,customer.PhoneHome,customer.isPhoneHomeLandline
    		FROM `order`
    		INNER JOIN worker ON `order`.WorkerID = worker.WorkerID
    		INNER JOIN customer ON `order`.CustomerID = customer.CustomerID
    		INNER JOIN order_type ON `order`.TypeID = order_type.TypeID
    		INNER JOIN order_status ON `order`.StatusID = order_status.StatusID
    		WHERE `order`.OrderID = '{$orderID}' AND `order`.StatusID != '46b1e4fd15378'";

            $rows = $this->db->query($sql);
            $numberOfRecords = $rows->num_rows();

            // check if rows are more then 0
            if ( $numberOfRecords > 0 ) {
                return $result = $rows->first_row('array');
            }
            else {
                return FALSE;
            }
        }
    }

    public function getOrderByCustomerID($customerID) {
        if ( $customerID ) {
            // join tables order & customer to fetch all necessary data for orders
            $sql = "SELECT `order`.OrderID,`order`.`Name`,`order`.WorkerID,worker.`Name` AS Worker,
                        worker.Email,worker.APIKey,`order`.CustomerID,customer.CustomerName,`order`.StatusID,
                        order_status.`Name` AS `Status`,`order`.TypeID,order_type.`Name` AS Type,
                        `order`.Detail,`order`.Date,`order`.Duration,customer.Address1,customer.Email AS CustomerEmail,
                        customer.Address2,customer.Zip,customer.City,customer.PhoneHome
                    FROM `order`
                        INNER JOIN worker ON `order`.WorkerID = worker.WorkerID
                        INNER JOIN customer ON `order`.CustomerID = customer.CustomerID
                        INNER JOIN order_type ON `order`.TypeID = order_type.TypeID
                        INNER JOIN order_status ON `order`.StatusID = order_status.StatusID
                    WHERE customer.CustomerID = '{$customerID}' AND `order`.StatusID != '46b1e4fd15378'
                        AND (customer.isPhoneHomeLandline IS NULL OR customer.isPhoneHomeLandline = '0') ORDER BY `order`.Date DESC";

            $rows = $this->db->query($sql);
            $numberOfRecords = $rows->num_rows();

            // check if rows are more then 0
            if ( $numberOfRecords > 0 ) {
                return $result = $rows->first_row('array');
            }
            else {
                return FALSE;
            }
        }
    }

    public function get90daysOrdersByCustomerID($customerID) {
        if ( $customerID ) {
            // join tables order & customer to fetch all necessary data for orders
            $sql = "SELECT `order`.OrderID,`order`.`Name`,`order`.WorkerID,worker.`Name` AS Worker,
                        worker.Email,worker.APIKey,`order`.CustomerID,customer.CustomerName,`order`.StatusID,
                        order_status.`Name` AS `Status`,`order`.TypeID,order_type.`Name` AS Type,
                        `order`.Detail,`order`.Date,`order`.Duration,customer.Address1,customer.Email AS CustomerEmail,
                        customer.Address2,customer.Zip,customer.City,customer.isPasswordReset,service.ServiceID,customer.PhoneHome
                    FROM `order`
                        INNER JOIN worker ON `order`.WorkerID = worker.WorkerID
                        INNER JOIN customer ON `order`.CustomerID = customer.CustomerID
                        INNER JOIN order_type ON `order`.TypeID = order_type.TypeID
                        INNER JOIN order_status ON `order`.StatusID = order_status.StatusID
                        INNER JOIN service ON service.CustomerID = customer.CustomerID
                    WHERE customer.CustomerID = '{$customerID}' AND `order`.StatusID != '46b1e4fd15378'
                        AND `order`.Date >= (LAST_DAY(NOW())+INTERVAL 1 DAY)-INTERVAL 1 MONTH-INTERVAL 1 MONTH
                        AND `order`.Date < (LAST_DAY(NOW())+INTERVAL 1 DAY)-INTERVAL 1 MONTH+INTERVAL 1 MONTH+INTERVAL 1 MONTH
                        GROUP BY `order`.OrderID ORDER BY `order`.Date DESC";

            $rows = $this->db->query($sql);
            $numberOfRecords = $rows->num_rows();

            // check if rows are more then 0
            if ( $numberOfRecords > 0 ) {
                return $result = $rows->result('array');
            }
            else {
                return FALSE;
            }
        }
    }

    public function isValidOrder($ordertime) {

        $currentDate = date("Y-m-d");
        $tomorrowDate = date("Y-m-d", strtotime(date("Y-m-d") . "+24 hours"));
        $currentTime = date("Y-m-d H:i:s");
        $today8pm = date("Y-m-d 20:00:00");
        $tomorrow8pm = date("Y-m-d H:i:s", strtotime(date("Y-m-d 20:00:00") . "+24 hours"));
        $orderDate = date("Y-m-d", strtotime($ordertime));

        if ( $currentTime < $today8pm ) {
            if ( $orderDate == $currentDate ) {
                return TRUE;
            }
            else {
                return FALSE;
            }
        }
        elseif ( $currentTime > $today8pm ) {
            if ( $orderDate == $tomorrowDate ) {
                return TRUE;
            }
            else {
                return FALSE;
            }
        }
    }

    /**
     * it fetches all orders from today till tomorrow
     * @return array or boolean false if not found any today's order
     */
    public function getTodaysOrders($workerID) {
        if ( $workerID ) {
            // join tables order,customer,worker,status,order_type to fetch all necessary data for orders
            $sql = "SELECT
                        `order`.OrderID,`order`.`Name`,`order`.WorkerID,worker.`Name` AS Worker,worker.Email,
                        `order`.CustomerID,customer.CustomerName,`order`.StatusID,order_status.`Name` AS `Status`,
                        `order`.TypeID,order_type.`Name` AS Type,`order`.Detail,`order`.Date,
                        `order`.Duration,customer.Address1,customer.Address2,customer.Zip,customer.City,
                        customer.Longitude,customer.Latitude,customer.Email AS CustomerEmail,survey_queue.SurveyID
                    FROM `order`
                        INNER JOIN worker ON order.WorkerID = worker.WorkerID
                        INNER JOIN customer ON order.CustomerID = customer.CustomerID
                        INNER JOIN order_type ON order.TypeID = order_type.TypeID
                        INNER JOIN order_status ON order.StatusID = order_status.StatusID
                        LEFT JOIN survey_queue ON survey_queue.OrderID = `order`.OrderID
                    WHERE worker.WorkerID = '{$workerID}'
                    AND DATE(`order`.Date) >= DATE(NOW())
                    AND DATE(`order`.Date) <= DATE(NOW() + INTERVAL 1 DAY)
                    AND `order`.StatusID != '46b1e4fd15378'
                    ORDER BY `order`.Date";

            $rows = $this->db->query($sql);
            $numberOfRecords = $rows->num_rows();

            // check if rows are more then 0
            if ( $numberOfRecords > 0 ) {
                $result = $rows->result_array();
                foreach ( $result as $key => $order ) {
                    if ( !$this->isValidOrder($order["Date"]) ) {
                        unset($result[$key]);
                    }
                }
                return $result;
            }
            else {
                return FALSE;
            }
        }
    }

    /**
     * fetch currently enroute or start order of a worker
     * @return array or boolean false if not set any enroute or start order
     */
    public function getKeyOrderofToday($workerID, $status) {
        $allTodaysOrders = $this->getTodaysOrders($workerID);
        foreach ( (array) $allTodaysOrders as $order ) {
            if ( $order["Status"] == $status ) {
                $orderID = $order["OrderID"];
            }
        }
        if ( isset($orderID) ) {
            return $orderID;
        }
        else {
            return FALSE;
        }
    }

    public function logSunsetTime($String) {
        $file = fopen('assets/sunset_time_logs.txt', 'a');
        fwrite($file, date("y-m-d h:i:s A") . " - " . $String . "\r\n");
        fclose($file);
    }

    public function hitForAntenaStats($customerID, $workerKey = NULL) {
        if ( $customerID ) {
            $url = 'https://b.airebeam.com/airecheck?radio=' . $customerID;
            $header = array(
                  'Authorization: Bearer ' . $workerKey
            );
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_FRESH_CONNECT, TRUE);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_URL, $url);
            $json_data = curl_exec($ch);
            $err = curl_error($ch);
            curl_close($ch);
            if ( !$err ) {
                return $json_data;
            }
        }
    }

    public function hitForScheduledSpeedTest($customerID, $workerKey = NULL) {
        if ( $customerID ) {
            $url = 'https://b.airebeam.com/airecheck?radio=' . $customerID . '&schedule=' . strtotime(date("Y-m-d 19:00:00"));
            $header = array(
                  'Authorization: Bearer ' . $workerKey
            );
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_FRESH_CONNECT, TRUE);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_URL, $url);
            $json_data = curl_exec($ch);
            $err = curl_error($ch);
            curl_close($ch);
            if ( !$err ) {
                return $json_data;
            }
        }
    }

    public function passwordEncrypt($password) {
        return $this->betterCrypt($password);
    }

    public function betterCrypt($input, $rounds = 10) {
        $crypt_options = array(
              'cost' => $rounds
        );
        return password_hash($input, PASSWORD_BCRYPT, $crypt_options);
    }

    /**
     * calls orderActivity and addOrderActivityMeta functions
     * for add order activity
     * @param array $addOrderActivity
     */
    public function addOrderAllActivities($addOrderActivity) {
        $actionID = "8fc24e31a7af7"; //id of action in this case it is "add"
        $objectTypeID = "40d64aeeb308b"; //id of object type in this case it is "order"

        if ( $activityLogID = $this->orderActivity($actionID, $objectTypeID) ) {
            $this->addOrderActivityMeta($activityLogID, $addOrderActivity);
            return true;
        }
        else {
            return false;
        }
    }

    /**
     * add initial activity on actions like
     * add,edit,delete in activity_log table
     * @param string $actionID
     * @param string $objectTypeID
     * @return activity id on success or  false if fails
     */
    public function orderActivity($actionID, $objectTypeID) {
        $activityLogID = strictUniqueID();
        $activityArray = array(
              "ActivityLogID" => $activityLogID,
              "ActionID" => $actionID,
              "ObjectTypeID" => $objectTypeID,
              "Date" => date("Y-m-d H:i:s")
        );
        if ( $this->Insert("activity_log", $activityArray) ) {
            return $activityLogID;
        }
        else {
            return false;
        }
    }

    /**
     * add all add order activity meta in activity_log_meta table
     * like who created order and who is assigned on it
     * @param string $activityLogID
     * @param string $addOrderActivity
     */
    public function addOrderActivityMeta($activityLogID, $addOrderActivity) {
        $activityMetaArray[] = array(
              "MetaID" => strictUniqueID(),
              "ActivityLogID" => $activityLogID,
              "ObjectID" => $addOrderActivity["OrderID"],
              "Name" => "order_added",
              "Value" => "yes"
        );
        $activityMetaArray[] = array(
              "MetaID" => strictUniqueID(),
              "ActivityLogID" => $activityLogID,
              "ObjectID" => $addOrderActivity["OrderID"],
              "Name" => "assigned_by",
              "Value" => $addOrderActivity["AssignedBy"]
        );
        $activityMetaArray[] = array(
              "MetaID" => strictUniqueID(),
              "ActivityLogID" => $activityLogID,
              "ObjectID" => $addOrderActivity["OrderID"],
              "Name" => "assigned_to",
              "Value" => $addOrderActivity["AssignedTo"]
        );
        foreach ( $activityMetaArray as $array ) {
            $this->Insert("activity_log_meta", $array);
        }
    }

    /**
     * calls noteActivity and addNoteActivityMeta functions
     * for add note activity
     * @param array $addNoteActivity
     */
    public function addNoteAllActivities($addNoteActivity) {
        $actionID = "8fc24e31a7af7"; //id of action in this case it is "add"
        $objectTypeID = "60206e9a02c5e"; //id of object type in this case it is "note"

        if ( $activityLogID = $this->noteActivity($actionID, $objectTypeID) ) {
            $this->addNoteActivityMeta($activityLogID, $addNoteActivity);
        }
    }

    public function addMessageAllActivities($addMessageActivity) {
        $actionID = "9175eec88598e"; //id of action in this case it is "send"
        $objectTypeID = "9175eec88598e"; //id of object type in this case it is "order"

        if ( $activityLogID = $this->messageActivity($actionID, $objectTypeID) ) {
            $this->addMessageActivityMeta($activityLogID, $addMessageActivity);
        }
    }

    /**
     * add initial activity on actions like
     * add,edit,delete in activity_log table
     * @param string $actionID
     * @param string $objectTypeID
     * @return activity id on success or  false if fails
     */
    public function noteActivity($actionID, $objectTypeID) {
        $activityLogID = strictUniqueID();
        $activityArray = array(
              "ActivityLogID" => $activityLogID,
              "ActionID" => $actionID,
              "ObjectTypeID" => $objectTypeID,
              "Date" => date("Y-m-d H:i:s")
        );
        if ( $this->Insert("activity_log", $activityArray) ) {
            return $activityLogID;
        }
        else {
            return false;
        }
    }

    public function messageActivity($actionID, $objectTypeID) {
        $activityLogID = strictUniqueID();
        $activityArray = array(
              "ActivityLogID" => $activityLogID,
              "ActionID" => $actionID,
              "ObjectTypeID" => $objectTypeID,
              "Date" => date("Y-m-d H:i:s")
        );
        if ( $this->Insert("activity_log", $activityArray) ) {
            return $activityLogID;
        }
        else {
            return false;
        }
    }

    /**
     * add all add order activity meta in activity_log_meta table
     * like who created order and who is assigned on it
     * @param string $activityLogID
     * @param string $addOrderActivity
     */
    public function addNoteActivityMeta($activityLogID, $addNoteActivity) {
        $activityMetaArray[] = array(
              "MetaID" => strictUniqueID(),
              "ActivityLogID" => $activityLogID,
              "ObjectID" => $addNoteActivity["OrderID"],
              "Name" => "note_added",
              "Value" => "yes"
        );
        $activityMetaArray[] = array(
              "MetaID" => strictUniqueID(),
              "ActivityLogID" => $activityLogID,
              "ObjectID" => $addNoteActivity["OrderID"],
              "Name" => "assigned_by",
              "Value" => $addNoteActivity["AssignedBy"]
        );
        $activityMetaArray[] = array(
              "MetaID" => strictUniqueID(),
              "ActivityLogID" => $activityLogID,
              "ObjectID" => $addNoteActivity["OrderID"],
              "Name" => "note_id",
              "Value" => $addNoteActivity["NoteID"]
        );
        foreach ( $activityMetaArray as $array ) {
            $this->Insert("activity_log_meta", $array);
        }
    }

    public function addMessageActivityMeta($activityLogID, $addMessageActivity) {
        $activityMetaArray[] = array(
              "MetaID" => strictUniqueID(),
              "ActivityLogID" => $activityLogID,
              "ObjectID" => $addMessageActivity["OrderID"],
              "Name" => $addMessageActivity["Name"],
              "Value" => $addMessageActivity["Message"]
        );
        $activityMetaArray[] = array(
              "MetaID" => strictUniqueID(),
              "ActivityLogID" => $activityLogID,
              "ObjectID" => $addMessageActivity["OrderID"],
              "Name" => "assigned_by",
              "Value" => $addMessageActivity["AssignedBy"]
        );
        foreach ( $activityMetaArray as $array ) {
            $this->Insert("activity_log_meta", $array);
        }
    }

    /**
     * calls orderActivity and editOrderActivityMeta functions
     * for add order activity
     * @param array $oldStat
     * @param array $newStat
     */
    public function deleteOrderAllActivities($deleteOrderActivity) {
        $actionID = "7d3c5979692e2";
        $objectTypeID = "40d64aeeb308b";

        if ( $activityLogID = $this->orderActivity($actionID, $objectTypeID) ) {
            $activityMetaArray = array(
                  "MetaID" => strictUniqueID(),
                  "ActivityLogID" => $activityLogID,
                  "ObjectID" => $deleteOrderActivity["OrderID"],
                  "Name" => "order_delete",
                  "Value" => "yes"
            );
            $this->Insert("activity_log_meta", $activityMetaArray);
            $activityMetaArray = array(
                  "MetaID" => strictUniqueID(),
                  "ActivityLogID" => $activityLogID,
                  "ObjectID" => $deleteOrderActivity["OrderID"],
                  "Name" => "deleted_by",
                  "Value" => $deleteOrderActivity["AssignedBy"]
            );
            $this->Insert("activity_log_meta", $activityMetaArray);
        }
    }

    public function editOrderAllActivities($oldStat, $newStat, $editOrderActivity, $meridiem = FALSE) {
        $actionID = "7f3f38e920c9f";
        $objectTypeID = "40d64aeeb308b";

        if ( $activityLogID = $this->orderActivity($actionID, $objectTypeID) ) {
            if ( $meridiem ) {
                $activityMetaArray = array(
                      "MetaID" => strictUniqueID(),
                      "ActivityLogID" => $activityLogID,
                      "ObjectID" => $oldStat["OrderID"],
                      "Name" => "change_meridiem",
                      "Value" => "yes"
                );
                $this->Insert("activity_log_meta", $activityMetaArray);
            }
            $activityMetaArray = array(
                  "MetaID" => strictUniqueID(),
                  "ActivityLogID" => $activityLogID,
                  "ObjectID" => $oldStat["OrderID"],
                  "Name" => "order_edited",
                  "Value" => "yes"
            );
            $this->Insert("activity_log_meta", $activityMetaArray);
            $activityMetaArray = array(
                  "MetaID" => strictUniqueID(),
                  "ActivityLogID" => $activityLogID,
                  "ObjectID" => $oldStat["OrderID"],
                  "Name" => "edited_by",
                  "Value" => $editOrderActivity["AssignedBy"]
            );
            $this->Insert("activity_log_meta", $activityMetaArray);
            $this->editOrderActivityMeta($activityLogID, $oldStat, $newStat);
        }
    }

    /**
     * add first activity meta that appointment is updated
     * @param string $activityLogID
     * @param string $orderID
     * @param array $nameValueArray
     */
    public function editOrderActivityMetaField($activityLogID, $orderID, $nameValueArray) {
        foreach ( $nameValueArray as $key => $value ) {
            $array = array(
                  "MetaID" => strictUniqueID(),
                  "ActivityLogID" => $activityLogID,
                  "ObjectID" => $orderID,
                  "Name" => $key,
                  "Value" => $value
            );
            $this->Insert("activity_log_meta", $array);
        }
    }

    /**
     * add every updated field activity in database
     * @param string $activityLogID
     * @param array $oldStat
     * @param array $newStat
     */
    public function editOrderActivityMeta($activityLogID, $oldStat, $newStat) {
        //loop for every new updated field
        foreach ( $newStat as $key => $value ) {

            // checking in old stat which field is updated in new stat
            if ( $oldStat[$key] != $value ) {

                if ( $key == "Name" ) {// if name is changed
                    $nameValueArray = array("order_old_name" => $oldStat[$key], "order_new_name" => $value);
                    $this->editOrderActivityMetaField($activityLogID, $oldStat["OrderID"], $nameValueArray);
                }
                elseif ( $key == "Detail" ) {// if detail is changed
                    $nameValueArray = array("order_old_detail" => $oldStat[$key], "order_new_detail" => $value);
                    $this->editOrderActivityMetaField($activityLogID, $oldStat["OrderID"], $nameValueArray);
                }
                elseif ( $key == "Status" ) {// if status is changed
                    $nameValueArray = array("order_old_status" => $oldStat[$key], "order_new_status" => $value);
                    $this->editOrderActivityMetaField($activityLogID, $oldStat["OrderID"], $nameValueArray);
                }
                elseif ( $key == "TypeID" ) {// if order service is changed
                    $oldType = $this->getServiceTypeNameByID($oldStat[$key]);
                    $newType = $this->getServiceTypeNameByID($value);
                    $nameValueArray = array("order_old_service_type" => $oldType, "order_new_service_type" => $newType);
                    $this->editOrderActivityMetaField($activityLogID, $oldStat["OrderID"], $nameValueArray);
                }
                elseif ( $key == "StatusID" ) {// if status id is changed
                    $oldStatusRow = $this->getStatus($oldStat[$key]);
                    $oldStatus = $oldStatusRow["Name"];

                    $newStatusRow = $this->getStatus($value);
                    $newStatus = $newStatusRow["Name"];

                    $nameValueArray = array("order_old_status" => $oldStatus, "order_new_status" => $newStatus);
                    $this->editOrderActivityMetaField($activityLogID, $oldStat["OrderID"], $nameValueArray);
                }
                elseif ( $key == "Date" ) {// if datetime is changed
                    $oldDateTime = explode(" ", $oldStat[$key]);
                    $oldDate = $oldDateTime[0];
                    $oldTime = $oldDateTime[1];

                    $newDateTime = explode(" ", $value);
                    $newDate = $newDateTime[0];
                    $newTime = $newDateTime[1];

                    if ( $oldDate != $newDate ) {// if date is changed in datetime
                        $nameValueArray = array("order_old_date" => $oldDate, "order_new_date" => $newDate);
                        $this->editOrderActivityMetaField($activityLogID, $oldStat["OrderID"], $nameValueArray);
                    }

                    if ( $oldTime != $newTime ) {// if time is changed in datetime
                        $nameValueArray = array("order_old_time" => $oldTime, "order_new_time" => $newTime);
                        $this->editOrderActivityMetaField($activityLogID, $oldStat["OrderID"], $nameValueArray);
                    }
                }
                elseif ( $key == "Duration" ) {// if duration is changed
                    $nameValueArray = array("order_old_duration" => $oldStat[$key], "order_new_duration" => $value);
                    $this->editOrderActivityMetaField($activityLogID, $oldStat["OrderID"], $nameValueArray);
                }
                elseif ( $key == "WorkerID" ) {// if worker is changed
                    $oldWorkerRow = $this->workerDetail($oldStat[$key]);
                    $oldWorker = $oldWorkerRow["Name"];
                    $newWorkerRow = $this->workerDetail($value);
                    $newWorker = $newWorkerRow["Name"];
                    $nameValueArray = array("order_old_worker" => $oldWorker, "order_new_worker" => $newWorker);
                    $this->editOrderActivityMetaField($activityLogID, $oldStat["OrderID"], $nameValueArray);
                }
            }
        }
    }

    public function getAllActivitiesOfOrder($orderID) {
        $sql = "SELECT
                    activity_log.ActivityLogID,
                    activity_log.Date,
                    activity_log_meta.`Name`,
                    activity_log_meta.`Value`
                FROM
                    activity_log
                INNER JOIN
                    activity_log_meta ON activity_log.ActivityLogID = activity_log_meta.ActivityLogID
                WHERE
                    activity_log_meta.ObjectID = '$orderID'
                ORDER BY
                    activity_log_meta.`Name` = 'order_added', activity_log.Date DESC";
        $rows = $this->db->query($sql);
        $result = $rows->result_array();
        if ( $result ) {
            return $result;
        }
        else {
            return false;
        }
    }

    public function mergingAcitivitiesWithNotes($activities) {
        foreach ( (array) $activities as $key => $val ) {
            if ( $val["Name"] == "note_id" ) {
                $noteID = $val["Value"];
                $response = $this->getNoteByID($noteID);
                if ( $response ) {
                    $activities[$key] = $response;
                }
            }
        }
        return $activities;
    }

    public function createActivitiesArray($separateActivity) {
        foreach ( $separateActivity as $key => $activity ) {
            foreach ( $activity as $fields ) {
                if ( $fields["Name"] == "assigned_by" ) {
                    $worker = $this->workerDetail($fields["Value"]);
                    $msgArray[$key]["assigned_by"] = $worker["Name"];
                }
                elseif ( $fields["Name"] == "edited_by" ) {
                    $worker = $this->workerDetail($fields["Value"]);
                    $msgArray[$key]["edited_by"] = $worker["Name"];
                }
                elseif ( $fields["Name"] == "action_performed_by" ) {
                    $worker = $this->workerDetail($fields["Value"]);
                    $msgArray[$key]["action_performed_by"] = $worker["Name"];
                }

                if ( $fields["Name"] == "assigned_to" ) {
                    $worker = $this->workerDetail($fields["Value"]);
                    $msgArray[$key]["assigned_to"] = $worker["Name"];
                }
                elseif ( $fields["Name"] == "order_new_worker" ) {
                    $msgArray[$key]["assigned_to"] = $fields["Value"];
                }

                if ( $fields["Name"] == "note_added" ) {
                    $msgArray[$key]["note_added"] = "yes";
                }

                if ( $fields["Name"] == "order_created_by" ) {
                    if ( $fields["Value"] == API_USER_NAME ) {
                        $msgArray[$key]["order_created_by"] = API_USER_NAME;
                    }
                    else {
                        $worker = $this->workerDetail($fields["Value"]);
                        $msgArray[$key]["order_created_by"] = $worker["Name"];
                    }
                }

                if ( $fields["Name"] == "empty_phone_number" ) {
                    $msgArray[$key]["empty_phone_number"] = "yes";
                }

                if ( $fields["Name"] == "empty_email_address" ) {
                    $msgArray[$key]["empty_email_address"] = "yes";
                }

                if ( $fields["Name"] == "survey_email_sent" ) {
                    $msgArray[$key]["survey_email_sent"] = "yes";
                }

                if ( $fields["Name"] == "sms_sent" ) {
                    $msgArray[$key]["sms_sent"] = "yes";
                }

                if ( $fields["Name"] == "customer_notified" ) {
                    $msgArray[$key]["customer_notified"] = "yes";
                }
                
                if ( $fields["Name"] == "enroute_sms_sent" ) {
                    $msgArray[$key]["enroute_sms_sent"] = "yes";
                }

                if ( $fields["Name"] == "sms_rejected" ) {
                    $msgArray[$key]["sms_rejected"] = "yes";
                }

                if ( $fields["Name"] == "sms_failed" ) {
                    $msgArray[$key]["sms_failed"] = $fields["Value"];
                }

                if ( $fields["Name"] == "survey_sms_sent" ) {
                    $msgArray[$key]["survey_sms_sent"] = $fields["Value"];
                }

                if ( $fields["Name"] == "survey_sms_success" ) {
                    $msgArray[$key]["survey_sms_success"] = $fields["Value"];
                }

                if ( $fields["Name"] == "survey_sms_failed" ) {
                    $msgArray[$key]["survey_sms_failed"] = $fields["Value"];
                }

                if ( $fields["Name"] == "click_on_cancel_sms_button" ) {
                    $msgArray[$key]["click_on_cancel_sms_button"] = $fields["Value"];
                }

                if ( $fields["Name"] == "customer_updated" ) {
                    $msgArray[$key]["customer_updated"] = $fields["Value"];
                }

                if ( isset($fields["NoteID"]) ) {
                    $msgArray[$key]["note"] = $fields;
                }

                if ( $fields["Name"] == "order_new_detail" ) {
                    $msgArray[$key]["fields"]["detail"] = $fields["Value"];
                }

                if ( $fields["Name"] == "click_on_done_button" ) {
                    $msgArray[$key]["fields"]["click_on_done_button"] = $fields["Value"];
                }

                if ( $fields["Name"] == "change_meridiem" ) {
                    $msgArray[$key]["fields"]["meridiem"] = $fields["Value"];
                }
                if ( $fields["Name"] == "order_new_service_type" ) {
                    $msgArray[$key]["fields"]["service"] = $fields["Value"];
                }
                if ( $fields["Name"] == "order_new_status" ) {
                    $msgArray[$key]["fields"]["status"] = $fields["Value"];
                }
                if ( $fields["Name"] == "order_new_date" ) {
                    $msgArray[$key]["fields"]["date"] = getOrderDate($fields["Value"]);
                }
                if ( $fields["Name"] == "order_new_time" ) {
                    $msgArray[$key]["fields"]["time"] = date('h:i A', strtotime($fields["Value"]));
                }
                if ( $fields["Name"] == "order_new_duration" ) {
                    $msgArray[$key]["fields"]["duration"] = $fields["Value"];
                }
                $msgArray[$key]["time_stamp"] = defaultDateFormat($fields["Date"]);
            }
        }
        return $msgArray;
    }

    public function addMessageEntry($orderID) {
        $array = array(
              "MessageLogID" => uniqid(),
              "MessageType" => "023b67e15c2b5",
              "OrderID" => $orderID,
              "MessageStatus" => "22377f6f23d49",
              "Date" => date("Y-m-d H:i:s"),
        );
        $this->Insert("message_sent_recieved", $array);
    }

    public function updateMessageEntry($orderID) {
        $result = $this->getMessageEntry($orderID);
        if ( $result["MessageType"] == "023b67e15c2b5" && $result["MessageStatus"] == "eefe80e2ade65" ) {
            $this->UpdateAll("message_sent_recieved", array("MessageType" => "d6d08e3487b59", "MessageStatus" => "22377f6f23d49"), array("OrderID" => $orderID));
        }
        elseif ( $result["MessageType"] == "d6d08e3487b59" && $result["MessageStatus"] == "eefe80e2ade65" ) {
            $this->UpdateAll("message_sent_recieved", array("MessageType" => "d6d08e3487b59", "MessageStatus" => "22377f6f23d49"), array("OrderID" => $orderID));
        }
    }

    public function deleteMessageEntry($orderID) {
        $this->Delete("message_sent_recieved", array("OrderID" => $orderID));
    }

    public function getMessageEntry($orderID, $message_type = null) {
        $array = array("OrderID" => $orderID);
        if ( $message_type ) {
            $array["MessageType"] = $message_type;
        }
        $rows = $this->SelectAllWhere("message_sent_recieved", $array);
        if ( $rows->num_rows() > 0 ) {
            return $rows->first_row('array');
        }
        else {
            return false;
        }
    }

    public function removeAlert($orderID) {
        $rows = $this->selectAllWhere("alert", array("ObjectID" => $orderID));
        if ( $rows->num_rows() > 0 ) {
            $this->Delete("alert", array("ObjectID" => $orderID));
        }
    }

    public function getAlertOfOrder($orderID) {
        $rows = $this->selectAllWhere("alert", array("ObjectID" => $orderID, "isVisible" => "1"));
        if ( $rows->num_rows() > 0 ) {
            return $rows->result('array');
        }
        else {
            return false;
        }
    }

    public function convertAlertMessage($msg, $params) {
        if ( isset($params["customer"]) && strpos($msg, "<CLIENT_NAME>") !== false ) {
            $msg = str_replace("<CLIENT_NAME>", $params["customer"], $msg);
        }
        if ( isset($params["worker"]) && strpos($msg, "<WORKER_NAME>") !== false ) {
            $msg = str_replace("<WORKER_NAME>", $params["worker"], $msg);
        }
        if ( isset($params["start_time"]) && strpos($msg, "<START_TIMESTAMP>") !== false ) {
            $msg = str_replace("<START_TIMESTAMP>", $params["start_time"], $msg);
        }
        if ( isset($params["end_time"]) && strpos($msg, "<END_TIMESTAMP>") !== false ) {
            $msg = str_replace("<END_TIMESTAMP>", $params["end_time"], $msg);
        }
        if ( isset($params["time"]) && strpos($msg, "<START_TIMESTAMP>") !== false ) {
            $msg = str_replace("<START_TIMESTAMP>", $params["time"], $msg);
        }
        return $msg;
    }

    public function addAlert($orderData, $type, $endTime = false, $msg = false) {
        $alertType = "7d3c597bd72e2";
        $params = array(
              "customer" => $orderData["CustomerName"],
              "start_time" => defaultDateFormat($orderData["Date"])
        );

        if ( $type == "create" && $endTime ) {
            $params["end_time"] = defaultDateFormat($endTime);
        }
        elseif ( $type == "change" && $endTime ) {
            $params["end_time"] = defaultDateFormat($endTime);
        }
        elseif ( "rejection" ) {
            $params["customerID"] = $orderData["CustomerID"];
            $params["worker"] = $orderData["Worker"];
        }
        else {
            $params["customerID"] = $orderData["CustomerID"];
            $params["worker"] = $orderData["Worker"];
        }

        if ( $type == "create" ) {
            $alertType = "7e3c597bd72e1";
            $msg = CREATE_ALERT_MSG;
        }
        elseif ( $type == "change" ) {
            $alertType = "7d3c597bd72e2";
            $msg = CHANGE_ALERT_MSG;
        }
        elseif ( $type == "rejection" ) {
            $alertType = "7f3c597bd72e3";
            $msg = REJECTION_ALERT_MSG;
        }
        elseif ( $type == "sms_failed" ) {
            $alertType = "7c3d597bd72e4";
        }
        else {
            $alertType = "73dc597bd98e2";
            $msg = LATE_REJECTION_ALERT_MSG;
        }

        $arrayAlert = array(
              "AlertID" => uniqid(),
              "TypeID" => $alertType,
              "Message" => $this->convertAlertMessage($msg, $params),
              "BehaviorID" => "7d3cd97bd72e2",
              "isVisible" => 1,
              "ObjectID" => $orderData["OrderID"],
              "Date" => date("Y-m-d H:i:s"),
        );
        $this->Insert("alert", $arrayAlert);
    }

    public function addAlertLog($orderData = false, $type = false, $endTime = false, $msg = false) {
        $alertType = "7d3c597bd72e2";
        $orderId = $orderData['OrderID'];        
        $orderLogExist = false;
        $rows = $this->SelectAllWhere("alert_log", array("AlertOrderID" => $orderId));
        if ($rows->num_rows() > 0) {
            $result = $rows->result('array');
            $updatedContent = json_decode($result[0]['Content']);
            $orderLogExist = true;
        }

        $params = array(
              "customer" => $orderData["CustomerName"],
              "start_time" => defaultDateFormat($orderData["Date"])
        );

        if ( $type == "create" && $endTime ) {
            $params["end_time"] = defaultDateFormat($endTime);
        }
        elseif ( $type == "change" && $endTime ) {
            $params["end_time"] = defaultDateFormat($endTime);
        }
        elseif ( "rejection" ) {
            $params["customerID"] = $orderData["CustomerID"];
            $params["worker"] = $orderData["Worker"];
        }
        else {
            $params["customerID"] = $orderData["CustomerID"];
            $params["worker"] = $orderData["Worker"];
        }

        if ( $type == "create" ) {
            $alertType = "7e3c597bd72e1";
            $msg = CREATE_ALERT_MSG;
        }
        elseif ( $type == "change" ) {
            $alertType = "7d3c597bd72e2";
            $msg = CHANGE_ALERT_MSG;
        }
        elseif ( $type == "rejection" ) {
            $alertType = "7f3c597bd72e3";
            $msg = REJECTION_ALERT_MSG;
        }
        elseif ( $type == "sms_failed" ) {
            $alertType = "7c3d597bd72e4";
        }
        else {
            $alertType = "73dc597bd98e2";
            $msg = LATE_REJECTION_ALERT_MSG;
        }
        $newMessage = [
                        'message' => $this->convertAlertMessage($msg, $params),
                        'date' => defaultDateFormat(date("Y-m-d H:i:s"))
                    ];

        if ($orderLogExist) {
            array_push($updatedContent, $newMessage);
            $this->UpdateAll("alert_log", array("Content" => json_encode($updatedContent)), array("AlertOrderID" => $orderId));
        }else{
            $newContent = [];
            array_push($newContent, $newMessage);

            $arrayAlertLog = array(
                  "AlertLogID" => uniqid(),
                  "Content" => json_encode($newContent),
                  "AlertOrderID" => $orderId,
            );

            $this->Insert("alert_log", $arrayAlertLog);
        }
    }

    public function addCustomerNotifyLog($order){
        $rows = $this->SelectAllWhere("customer_notification_log", array("OrderID" => $order['OrderID']));
        if ( $rows->num_rows() <= 0 ) {
            $customerNotificationLog = array(
                "SmsFailedID" => strictUniqueID(),
                "CustomerID" => $order['CustomerID'],
                "OrderID" => $order['OrderID'],
                "IsNotified" => 0
            );
            $response = $this->db->insert('customer_notification_log', $customerNotificationLog);
        }      
    }
    
    public function sendSMS($from, $to, $message) {
        $this->load->library('twilio');
        $response = $this->twilio->sms($from, $to, $message);
        $response = json_decode(json_encode($response), true);        
        if ( $response["IsError"] ) {
            return array(
                "To" => $to,
                "Body" => $response["ErrorMessage"],
                "Status" => "fail",
                "ErrorCode" => '404',
                "Message" => $response["ErrorMessage"],
                "Sid" => ''
            );
        }
        else {
            $messageData = $response["ResponseXml"]["Message"];

            // Get the recently created Message Response
            $messageResponse = $this->twilio->getMessageResponse($messageData["Sid"]);

            $jsonData = json_decode($messageResponse->ResponseText);
            $error_code = $jsonData->error_code;
            $error_message = $jsonData->error_message;
            $status = $jsonData->status;
            $sid = $jsonData->sid;
            if ($status == 'sent') {
                $status = 'delivered';
            }
            if ($status == 'delivered') {
                return array(
                      "To" => $messageData["To"],
                      "Sid" => $messageData["Sid"],
                      "Body" => $messageData["Body"],
                      "Status" => 'success'
                );
            }else{
                return array(
                    "To" => $messageData["To"],
                    "Status" => $status,
                    "Body" => $messageData["Body"],
                    'Message' => $error_message,
                    'ErrorCode' => $error_code,
                    'Sid' => $sid
                );
            }
        }
    }

    public function getShortCodes($order) {
        $array["Worker"] = $order["Worker"];
        $array["Type"] = $order["Type"];
        $array["Date"] = $order["Date"];
        return $array;
    }

    public function getMessageBody($message_type) {
        $rows = $this->SelectAllWhere("message", array("MessageTypeID" => $message_type));
        $result = $rows->first_row("array");
        return $result["Message"];
    }

    public function replaceDateAndService($message, $shortCodes) {
        $date = defaultDateFormat($shortCodes["Date"]);
        $message = str_replace("{TIME_STAMP}", $date, $message);
        $message = str_replace("{WORKER}", $shortCodes["Worker"], $message);
        return str_replace("{SERVICE_TYPED}", $shortCodes["Type"], $message);
    }

    public function getMessageLogBySID($sid) {
        $rows = $this->SelectAllWhere("message_log", array("Sid" => $sid));
        if ( $rows->num_rows() > 0 ) {
            return $rows->first_row('array');
        }
        else {
            return false;
        }
    }

    /**
     * // Get all activities of password reset for a particaular customer
     * @param string $id
     * @return boolean or array
     */
    public function getFPRActivities($id) {
        $activity = $this->db->query("SELECT * FROM activity_log
            INNER JOIN activity_log_meta ON activity_log.ActivityLogID = activity_log_meta.ActivityLogID
            WHERE ObjectID = '" . $id . "' AND (activity_log_meta.`Name` = 'force_password_reset' OR activity_log_meta.`Name` = 'force_password_updated')
            ORDER BY activity_log.Date DESC")->result_array();

        if ( count($activity) > 0 ) {
            foreach ( $activity as $key => $act ) {
                if ( $act["Name"] == "force_password_reset" ) {
                    $worker = $this->workerDetail($act["Value"]);
                    $activity[$key]["Worker"] = $worker["Name"];
                }
            }
            return $activity;
        }
        else {
            return false;
        }
    }

    public function updateTicket($ticket_id, $fields) {
        $curl = curl_init();

        curl_setopt_array($curl, array(
              CURLOPT_URL => "https://airebeam.freshdesk.com/api/v2/tickets/" . $ticket_id,
              CURLOPT_CUSTOMREQUEST => "PUT",
              CURLOPT_POSTFIELDS => json_encode($fields),
              CURLOPT_HTTPHEADER => array(
                    "Content-type: application/json",
                    "Authorization: Basic cFAxdEh6UkF0MjAxbTBCaTljMjo="
              ),
        ));

        $resp = curl_exec($curl);

        $this->logTime("Search Ticket By MBR : " . $resp);

        $err = curl_error($curl);
        curl_close($curl);
    }

    public function viewTicket($ticket_id) {
        $curl = curl_init();

        curl_setopt_array($curl, array(
              CURLOPT_URL => "https://airebeam.freshdesk.com/api/v2/tickets/" . $ticket_id,
              CURLOPT_CUSTOMREQUEST => "GET",
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_HTTPHEADER => array(
                    "Content-type: application/json",
                    "Authorization: Basic cFAxdEh6UkF0MjAxbTBCaTljMjo="
              ),
        ));

        $resp = curl_exec($curl);
        curl_close($curl);
        
        if ( !curl_error($resp) ) {
            return $resp;
        }
        return false;
    }

}
