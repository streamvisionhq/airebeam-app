<?php

/**
 * this class is working as a center layer between the core controller class and child classes
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function sendServey($orderID, $app = false) {
        $this->load->model("M_order");
        $this->order = new M_order();
        $user = null;
        $response = $this->order->getOrder($orderID);
        $subject = "Airebeam - How did we do?";
        
        $headers = "From: Airebeam\r\n";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
        
        $body = 'Thank you for choosing Airebeam. Please give us a review at <a>http://airebeam.com/review</a>';
        
        mail($response["CustomerEmail"], $subject, $body, $headers);
        
        $this->order->removeSurveyFromQueue($orderID);

        //start add activity for survey sent
        if (isset($app)) {
            if ($app == "mobile-app") {
                $user = $this->order->isWorkerLogin();
            } else {
                $user = $this->order->isAdminLogin();
            }
        }else{
            $user = $this->order->isAdminLogin();
        }
        
        $addSurveyActivity = array(
            "OrderID" => $orderID,
            "AssignedBy" => $user,
        );
        $this->order->addSurveyAllActivities($addSurveyActivity);
        //end add activity for survey sent

        return "Email has been sent to " . $response["CustomerEmail"];
    }

}
