<?php

/**
 * Name:         EmeraldApi
 */
class EmeraldApi {

    private $Emerald_Base_Url = "https://billing.airebeam.com/";
    private $Emerald_User = "asimapi";
    private $Emerald_Password = "qw8jq08qw8jq08";

    public function addPackage($customer_id, $package_id, $start_date = false) {
        $emeraldEndPoint = $this->Emerald_Base_Url . "api.ews";
        $parameters = array(
              "emerald_user" => $this->Emerald_User,
              "emerald_password" => $this->Emerald_Password,
              "action" => "package_add",
              "CustomerID" => $customer_id,
              "DiscountID" => "1",
              "PayPeriodID" => "1",
              "setupcharge" => "1",
              "PackageTypeID" => $package_id,
              "DiffCost" => "0",
              "Active" => "1",
              "Extension" => "3"
        );
        if($start_date){
            $parameters['StartDate'] = $start_date;
        }
        $resp = $this->curlExecutePost($emeraldEndPoint, $parameters);
        return $resp;
    }

    public function addServiceWithPackageRef($service_id, $customer_id, $first_name, $last_name, $email = "", $package_id, $login, $password, $start_date = false) {
        $emeraldEndPoint = $this->Emerald_Base_Url . "api.ews";
        $parameters = array(
              "emerald_user" => $this->Emerald_User,
              "emerald_password" => $this->Emerald_Password,
              "action" => "service_add",
              "AccountTypeID" => $service_id,
              "CustomerID" => $customer_id,
              "DiscountID" => "1",
              "DomainID" => "1",
              "PayPeriodID" => "1",
              "setupcharge" => "0",
              "FirstName" => $first_name,
              "LastName" => $last_name,
              "Login" => $login,
              "Password" => $password,
              "ServiceCategoryID" => "95",
              "Email" => $email
        );
        if($start_date){
            $parameters['StartDate'] = $start_date;
        }
        $resp = $this->curlExecutePost($emeraldEndPoint, $parameters);
        return $resp;
    }

    public function addService($service_id, $customer_id, $first_name, $last_name, $email = "", $login = "", $password, $start_date = false) {
        $emeraldEndPoint = $this->Emerald_Base_Url . "api.ews";
        $parameters = array(
              "emerald_user" => $this->Emerald_User,
              "emerald_password" => $this->Emerald_Password,
              "action" => "service_add",
              "AccountTypeID" => $service_id,
              "ServiceCategoryID" => '94',
              "CustomerID" => $customer_id,
              "DiscountID" => "1",
              "DomainID" => "1",
              "PayPeriodID" => "1",
              "setupcharge" => "0",
              "FirstName" => $first_name,
              "LastName" => $last_name,
              "Login" => $login,
              "Password" => $password,
              "Email" => $email
        );
        if($start_date){
            $parameters['StartDate'] = $start_date;
        }
        $resp = $this->curlExecutePost($emeraldEndPoint, $parameters);
        return $resp;
    }

    public function addPackageMarketTag($market_tag, $customer_id){
      $emeraldEndPoint = $this->Emerald_Base_Url . "api.ews";
      $parameters = array(
            "emerald_user" => $this->Emerald_User,
            "emerald_password" => $this->Emerald_Password,
            "action" => "mbr_tag_modify",
            "CustomerID" => $customer_id,
            "MarketTag" => $market_tag
      );
      
      $resp = $this->curlExecutePost($emeraldEndPoint, $parameters);
      return $resp;
    }

    public function addActivationFee($charge_type, $customer_id){
      $emeraldEndPoint = $this->Emerald_Base_Url . "api.ews";
      $parameters = array(
            "emerald_user" => $this->Emerald_User,
            "emerald_password" => $this->Emerald_Password,
            "action" => "adjustment_add",
            "CustomerID" => $customer_id,
            "ChargeType" => $charge_type,
            "Quantity" => 1
      );
      
      $resp = $this->curlExecutePost($emeraldEndPoint, $parameters);
      return $resp;
    }    

    private function curlExecutePost($end_point, $parameters) {
        if ( $end_point ) {
            $curl = curl_init();
            // Set some options - we are passing in a useragent too here
            curl_setopt_array($curl, array(
                  CURLOPT_RETURNTRANSFER => 1,
                  CURLOPT_URL => $end_point,
                  CURLOPT_USERAGENT => 'curl Request',
                  CURLOPT_POST => 1,
                  CURLOPT_POSTFIELDS => $parameters,
            ));
            // Send the request & save response to $resp
            $resp = curl_exec($curl);
            // Close request to clear up some resources
            curl_close($curl);
            return $resp;
        }
    }

}
