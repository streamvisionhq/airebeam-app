<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Name:         CustomerAccessOK
 * Author:       Faraz:faraz@codup.io
 * Created:      12.01.2016
 * Description:  Validate & fetch the Service from database and return responses
 */
class CustomerAccessOK {

    protected $ci;
    protected $statuses;
    public $term_name = "TOC.pdf";
    public $term_url;

    function __construct() {
        //initialize the CI super-object
        $this->ci = & get_instance();
        $this->term_url = base_url() . 'assets/admin/terms/current/' . $this->term_name;
        $this->statuses = [
            "1" => ["status" => "1", "message" => SUCCESSFULL_MBR],
            "2" => ["status" => "2", "message" => MISSING_PARAMETERS],
            "3" => ["status" => "3", "message" => MBR_NOT_EXISTS],
            "4" => ["status" => "4", "message" => AUTHENTICATION_FAILED],
            "5" => ["status" => "5", "message" => SERVICE_INACTIVE],
            "6" => ["status" => "6", "message" => SERVICE_NOT_AVAILABLE],
            "7" => ["status" => "7", "message" => SERVICE_NOT_FOUND],
            "8" => ["status" => "8", "message" => MISSING_PARAMETERS_TWO],
            "9" => ["status" => "9", "message" => MISSING_CREDIT_CARD],
            "10" => ["status" => "10", "message" => INVALID_BASESTRING],
            "11" => ["status" => "11", "message" => EMAIL_NOT_EXISTS],
            "12" => ["status" => "12", "message" => TOKEN_NOT_MATCHED],
            "13" => ["status" => "13", "message" => PHONE_NOT_EXISTS],
            "14" => ["status" => "14", "message" => PHONE_LANDLINE],
            "15" => ["status" => "15", "message" => TOKEN_EXPIRED],
            "16" => ["status" => "16", "message" => NOT_ALLOWED],
        	"17" => ["status" => "17", "message" => INCORRECT_PASSWORD],
        		
        ];
    }

    /**
     * Make response string
     *
     * @desc Found status in statuses array and get message
     *
     * @param <string> Status
     * @return <string> Message
     */
    public function returnResponse($status) {
        return $this->statuses[$status];
    }

    /**
     * Check Service And Password
     *
     * @desc checks if Customer Access OK service is active or inactive and matches the password
     *
     * @param <string> Status
     * @return <string> Message
     */
    public function checkPassword($mbr, $password) {
        $this->ci->db->select('*');
        $this->ci->db->where(["CustomerID" => $mbr]);
        $result = $this->ci->db->get('customer')->first_row();
        if (!empty($result)) {
            if (simple_decrypt($result->Password, SALT_STRING) == $password) {
                $returnResponse = $this->statuses["1"];
            } else {
                $returnResponse = $this->statuses["4"];
            }
        } else {
            $returnResponse = $this->statuses["3"];
        }
        return $returnResponse;
    }

    public function hitBackOffice($url) {
        $detail = "";
        $params = array(
            "emerald_user" => EMERALD_USER,
            "emerald_password" => EMERALD_PASSWORD
        );
        $cookiejar = substr(strictUniqueID(), 0, 6);
        $cookiejar = __DIR__ . '/cookie.txt';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_COOKIEJAR, $cookiejar);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($params));
        ob_start();
        curl_exec($ch);
        ob_end_clean();
        curl_close($ch);
        unset($ch);
        if (file_exists($cookiejar)) {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
            curl_setopt($ch, CURLOPT_COOKIEFILE, $cookiejar);
            curl_setopt($ch, CURLOPT_URL, $url);
            $data = curl_exec($ch);
            curl_close($ch);
            if (file_exists($cookiejar)) {
                unlink($cookiejar);
            }
            if (!empty($data)) {
                libxml_use_internal_errors(true);
                $detail = simplexml_load_string($data);
            }
        }
        return $detail;
    }

    public function checkTermsAccepted($mbr) {
        $this->ci->db->select('*');
        $this->ci->db->where(["CustomerID" => $mbr, "TermsAccepted" => "1"]);
        if ($this->ci->db->get('customer')->num_rows() == 0) {
            $this->ci->db->select('*');
            $this->ci->db->order_by('Date', 'DESC');
            $term = $this->ci->db->get('term')->first_row();
            if (!empty($term)) {
                return $term;
            }
            return false;
        }
        return false;
    }

    public function mailToCustomer($customer, $url) {
        $token = $this->insertToken($customer->CustomerID);
        $subject = "Reset Password | AireBeam Customer Portal";
        $headers = "From: noreply@airebeam.net\r\n";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

        $url = $url . "?login=" . $customer->CustomerID . "&key=" . $token . "&ex=" . base64_encode(gmdate("Y-m-d H:i:s"));
        
        $customer_name = $this->removeCustomerMBR($customer->CustomerName);

        $body = "Hi " . $customer_name . ",<br><br>
                You recently requested to reset your password for your AireBeam Customer Portal account. You can reset your password by clicking the link below:
                <br><br>
                <a href='" . $url . "' target='_blank'>" . $url . "</a>
                <br><br>
                This password reset link is only valid for next 6 hours from now. If you did not request a password reset, please ignore this email.
                <br><br>
                Sincerely,<br>
                AireBeam Customer Service<br>
                FibAire Communications, LLC d/b/a AireBeam<br>
                520-233-7400</br>
                <img src='" . ADMIN_ASSETS_PATH . "img/logo.png' width='280' />";

        return mail($customer->Email, $subject, $body, $headers);
    }

    public function confirmMailToCustomer($customer) {
        $subject = "Your password reset | AireBeam Customer Portal";
        $headers = "From: noreply@airebeam.net\r\n";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
        
        $customer_name = $this->removeCustomerMBR($customer->CustomerName);

        $body = "Hi " . $customer_name . ",<br><br>
                Your password for AireBeam Customer Portal account has been reset successfully.
                
                <br><br>
                Sincerely,<br>
                AireBeam Customer Service<br>
                FibAire Communications, LLC d/b/a AireBeam<br>
                520-233-7400</br>
                <img src='" . ADMIN_ASSETS_PATH . "img/logo.png' width='280' />";

        return mail($customer->Email, $subject, $body, $headers);
    }

    public function smsToCustomer($customer) {
        $this->ci->load->library("Twilio");
        $twilio = new Twilio();
        $messageBody = $this->ci->db->select("*")->get_where("message", ["MessageTypeID" => "d6d08e3487d53"])->first_row();
        $token = $this->insertToken($customer->CustomerID, "code");
        $messageBody = str_replace("{CODE}", $token, $messageBody->Message);
        return $twilio->sms(FROM, $customer->PhoneHome, $messageBody);
    }

    public function confirmSMSToCustomer($customer) {
        $this->ci->load->library("Twilio");
        $twilio = new Twilio();
        return $twilio->sms(FROM, $customer->PhoneHome, "Your password for AireBeam Customer Portal account has been reset successfully.");
    }

    public function insertGeneralToken($customer_info) {
        if (!empty($customer_info)) {
            return $this->insertToken($customer_info->CustomerID);
        }
        return false;
    }

    public function insertToken($mbr, $type = false) {
        $this->ci->db->where('CustomerID', $mbr);
        $this->ci->db->delete('password_reset');
        $token = strictUniqueID();
        if ($type == "code") {
            $token = strtoupper(substr($token, 0, 5));
        }
        $this->ci->db->insert('password_reset', ['CustomerID' => $mbr, "Token" => $token, "Date" => gmdate("Y-m-d H:i:s")]);
        return $token;
    }

    public function deleteToken($mbr) {
        $this->ci->db->where('CustomerID', $mbr);
        $this->ci->db->delete('password_reset');
    }

    /**
     * returns the datetime difference object of date1 from date2
     * @param type $date1
     * @param type $date2
     * @return type
     */
    public function getDateDiff($date1, $date2) {
        $datetime1 = new DateTime($date1);
        $datetime2 = new DateTime($date2);
        return $datetime1->diff($datetime2);
    }

    public function convertToLatLong($address) {
        $address = urlencode($address);
        $url = "https://maps.googleapis.com/maps/api/geocode/json?address=$address&key=" . GEOCODE_API;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $response = curl_exec($ch);
        curl_close($ch);
        $response_a = json_decode($response);

        $array["latitude"] = "";
        $array["longitude"] = "";

        if (!isset($response_a->error_message) && $response_a->status != "ZERO_RESULTS") {
            if (!empty($response_a->results[0]->geometry->location->lat) || $response_a->results[0]->geometry->location->lng) {
                $array["latitude"] = $response_a->results[0]->geometry->location->lat;
                $array["longitude"] = $response_a->results[0]->geometry->location->lng;
            }
        }

        return $array;
    }
    
    public function removeCustomerMBR($customer_name){
        $customer_name = explode(" - ", $customer_name);
        return $customer_name[1];
    }

}

/* End of file CustomerAccessOK.php */