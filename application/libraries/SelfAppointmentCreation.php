<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

/**
 * Name:         SelfAppointmentCreation
 */
	
class SelfAppointmentCreation {
	protected $statuses;
	
	function __construct() {
		$this->statuses = [
						APPOINTMENT_PARAMS_MISSING_STATUS => [STATUS_KEY => APPOINTMENT_PARAMS_MISSING_STATUS, MSG_KEY => CREATE_APPOINTMENT_PARAMS_MISSING],
						APPOINTMENT_CONFLICT_STATUS => [STATUS_KEY => APPOINTMENT_CONFLICT_STATUS, MSG_KEY => APPOINTMENT_CONFLICT_MSG],
						END_TIME_LESS_STATUS => [STATUS_KEY => END_TIME_LESS_STATUS, MSG_KEY => END_TIME_LESS_ERROR],
						SUCCESS_APPOINTMENT_STATUS => [STATUS_KEY => SUCCESS_APPOINTMENT_STATUS, MSG_KEY => SUCCESS_APPOINTMENT_CREATED],
						FAILED_APPOINTMENT => [STATUS_KEY => FAILED_APPOINTMENT, MSG_KEY => FAILED_APPOINTMENT_CREATED],
						WORKER_NOT_EXIST_STATUS => [STATUS_KEY => WORKER_NOT_EXIST_STATUS, MSG_KEY => WORKER_NOT_EXIST],
				];
		
		$this->create_mbr_statuses = [
				CREATE_MBR_MISSING_STATUS => [STATUS_KEY => CREATE_MBR_MISSING_STATUS, MSG_KEY => CREATE_MBR_PARAMS_MISSING],
		];
	}
	
	public function returnResponse($status) {
		return $this->statuses[$status];
	}
	
	public function returnCreateMbrResponse($status) {
		return $this->create_mbr_statuses[$status];
	}
}