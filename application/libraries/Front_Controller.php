<?php

class Front_Controller extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->data['meta_title'] = config_item('site_name');
    }

}