<?php

class Admin_Controller extends MY_Controller {

    protected $previlages;

    public function __construct() {
        parent::__construct();
        $this->load->model("m_admin");
        $this->admin = new M_admin();
        $this->load->model("m_order");
        $this->order = new M_order();
        $this->load->model("m_extend");
        $this->extend = new M_extend();

        $this->data["services"] = $this->extend->getServices();
        $this->data["statuses"] = $this->extend->getStatuses();
        $this->data["workers"] = $this->extend->getWorkers();
        $this->data["durations"] = $this->extend->getDurations();
        $this->data["customers"] = $this->extend->getCustomers();
        $this->data['meta_title'] = config_item('site_name');
        $this->data['surveys'] = $this->order->getAllQueuedSurveys();

        if ($this->admin->isAdminLogin()) {
            $workerID = $this->admin->isAdminLogin();
            $detail = $this->admin->workerDetail($workerID);
            $this->data['userName'] = $detail["Name"];
            $this->data['isLogin'] = TRUE;

            $response = $this->order->getAllAlertForCustomerCall();
            if ($response) {
                $this->data['alerts'] = "";
                foreach ($response as $row) {
                    if ($row["TypeID"] == "73dc597bd98e2") {
                        $orderData = $this->order->getOrder($row["ObjectID"]);
                        if ($orderData) {
                            $this->data['order'] = $orderData;
                            $this->data['row'] = $row;
                            $this->data['alerts'] .= $this->load->view("admin/alert", $this->data, true);
                        }
                    }
                }
                foreach ($response as $row) {
                    if ($row["TypeID"] == "7f3c597bd72e3") {
                        $orderData = $this->order->getOrder($row["ObjectID"]);
                        if ($orderData) {
                            $this->data['order'] = $orderData;
                            $this->data['row'] = $row;
                            $this->data['alerts'] .= $this->load->view("admin/alert", $this->data, true);
                        }
                    }
                }
                foreach ($response as $row) {
                    if ($row["TypeID"] != "73dc597bd98e2" && $row["TypeID"] != "7f3c597bd72e3") {
                        $orderData = $this->order->getOrder($row["ObjectID"]);
                        if ($orderData) {
                            $this->data['order'] = $orderData;
                            $this->data['row'] = $row;
                            $this->data['alerts'] .= $this->load->view("admin/alert", $this->data, true);
                        }
                    }
                }
            }

            $privilages = $this->admin->getPrivilages($this->session->userdata('RoleID'));

            //define constant for all previlages
            foreach ($privilages as $priv) {
                define(strtoupper($priv), $priv);
            }

            $this->previlages = $privilages;
            if (!empty($privilages)) {
                $this->data['privilages'] = $privilages;
            }
        }
    }

}
