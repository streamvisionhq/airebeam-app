<div id='popup-cancel-enroute-<?php echo $OrderID; ?>' class='popup-container' style='display: none;'>
    <h3 class='popup-title'>Cancel Enroute</h3>
    <p>Please assign a status to this appointment</p>

    <div class="header-btn">                
        <a class='cancel-enroute-done bg-green'>Done</a>        
        <a class='cancel-enroute-abort bg-red'>Abort</a>
        <a class='cancel-enroute-schedule bg-blue'>Reschedule</a>
    </div>   
    
    <form class='form popup-form display-none' id='cancel-enroute-form' method='post'>
        <div class='error msgs error-msg display-none'></div>
        <input type="hidden" name="order-id" value="<?php echo $OrderID; ?>">
        <input type="hidden" name="label" value="">
        <textarea class='textarea popup-textarea' name='note' placeholder='Enter note'></textarea>
        <span class="dropdown-container">
            <select class='wrapper-dropdown-5' name='next-order-enroute'>
                <option value=''>Select next appointment (optional)</option>
                <?php
                foreach ($allScheduledOrders as $scheduledOrder) {
                    ?>
                    <option value='<?php echo $scheduledOrder["OrderID"] ?>'><?php echo $scheduledOrder["Name"] ?></option>
                    <?php
                }
                ?>
            </select>
        </span>
        <input type='button' name='status' value='Reschedule' class='btn loadOverlay' />
    </form>
</div>