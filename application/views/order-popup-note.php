<div id='popup-abort-<?php echo $OrderID; ?>' class='popup-container' style='display: none;'>
    <h3 class='popup-title'>You are now aborting this appointment...</h3>
    <p>Please add a note to tell us why you are aborting this appointment</p>
    <form class='popup-form form' method='post'>
        <div class='error msgs error-msg display-none'></div>
        <input type="hidden" name="order-id" value="<?php echo $OrderID; ?>">
        <input type="hidden" name="label" value="abort">
        <textarea class='textarea popup-textarea report-popup-textarea' name='note' placeholder='Enter note (required)' ></textarea>
        <?php
        if ($Status == ENROUTE || $Status == START) {
            ?>
            <span class="dropdown-container">
                <select class='wrapper-dropdown-5' name='next-order-enroute'>
                    <option value=''>Select next appointment (optional)</option>
                    <?php
                    foreach ($allScheduledOrders as $scheduledOrder) {
                        ?>
                        <option value='<?php echo $scheduledOrder["OrderID"] ?>'><?php echo $scheduledOrder["Name"] ?></option>
                        <?php
                    }
                    ?>
                </select>
            </span>
            <?php
        }
        ?>
        <input type='button' name='status' value='Abort' class='bg-red btn' />
    </form>
</div>