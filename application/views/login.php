<?php $this->load->view("header"); ?>
<div id="container" class="wrapper login-wrapper">
    <div class="logo">
        <a href="<?php echo base_url(); ?>"><img src="<?php echo base_url() . "assets/" ?>img/logo.png"/></a>
    </div>
    <form method="post" class="login-form" id="login-form">
        <div class="login-container">
            <?php
            if (!empty($Error)) {
                ?>
                <div class="field-container">
                    <div class="msgs error-msg">
                        <p>Warning! <?php echo $Error; ?></p>
                    </div>
                </div>
                <?php
            }
            ?>
            <div class="field-container">                
                <input type="email" name="email" id="Email" placeholder="Email Address" class="text-field" />
            </div>
            <div class="field-container">
                <input type="password" name="password" id="password" placeholder="Password" class="text-field" />                
            </div>
            <div class="space"></div>
            <div class="field-container">
                <input type="submit" name="submit" class="sub-btn" value="Login"/>
            </div>
        </div>
    </form>
</div>
</body>
</html>