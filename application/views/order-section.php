<?php
$container_class = false;
if ($Alerts && $Alerts[0]["TypeID"] == "73dc597bd98e2") {
    $container_class = "wrapper-late-rejected";
} elseif ($Status == REJECTED) {
    $container_class = "wrapper-rejected";
}
?>
<div class="list-container <?php echo $container_class; ?>">
    <div class="list-head">
        <div class="list-title"><?php echo $Name; ?></div>
        <div class="list-status">
            <div class="status-text <?php
            if ($Status == DONE && !empty($SurveyID) && !empty($CustomerEmail)) {
                echo "pending-survey";
            } else {
                echo $class;
            }
            ?>">
             <?php
             if ($Status == DONE && !empty($SurveyID) && !empty($CustomerEmail)) {
                 echo "Pending Review";
             } else {
                 if ($Alerts && $Alerts[0]["TypeID"] == "73dc597bd98e2") {
                     echo "Late Rejected";
                 }else {
                     echo $Status;
                 }
             }
             ?>
            </div>
            <i class="icon icon-<?php
            if ($Status == DONE && !empty($SurveyID) && !empty($CustomerEmail)) {
                echo "pending-survey";
            } else {
                echo $class;
            }
            ?>"></i>
        </div>
        <div class="field-address">
            <i class="icon icon-address"></i>
            <div class="listing-text"><?php echo $Address1 . " " . $Address2 . " " . $City . " " . $Zip; ?></div>
        </div>
    </div>
    <div class="list-leftcontent">
        <div class="incident-info">
            <i class="icon icon-time"></i>
            <div class="listing-text"><?php echo showTime($Date); ?></div>
        </div>
        <div class="incident-info">
            <i class="icon icon-service"></i>
            <div class="listing-text"><?php echo $Type; ?></div>
        </div>
    </div>
    <div class="list-rightcontent">
        <?php
        if ($container_class) {
            ?>
            <div class="rejected-container">
                <p>This appointment has been rejected by the client. Please co-ordinate with CSR for more info</p>
            </div>
            <?php
        } else {
            ?>
            <a href="<?php echo base_url() . "orders/detail/" . $OrderID; ?>" class="button1 bg-blue anchor-no-internet">Details</a>

            <?php
            // started button html
            if ($Status == ENROUTE) {
                ?>
                <div class="button4 no-margin started-btn">
                    <ul>
                        <li class="onclick"><a class="f-click" data-id="<?php echo $OrderID ?>">Start</a></li>
                        <li class="onclick-hide"><a href="#popup-cancel-enroute-<?php echo $OrderID ?>" class="fancybox">Cancel En-Route</a></li>
                    </ul>
                </div>
                <?php
            } elseif ($Status == START) {
                
            } else {
                if ($Status == DONE && !empty($SurveyID) && !empty($CustomerEmail)) {
                    $en_class = "fancybox button2 no-margin bg-grey";
                } else {
                    $en_class = "fancybox button4 no-margin bg-grey";
                }
                if ($enrouteOrderID) {
                    ?>
                    <a href='#popup-en-route-<?php echo $OrderID; ?>' class='<?php echo $en_class; ?>'>En-Route</a>
                    <?php
                } else {
                    $en_class .= " anchor-no-internet first-enroute";
                    ?>
                    <a href='<?php echo BASE_PATH; ?>orders/enroute/<?php echo $OrderID; ?>' class='<?php echo $en_class; ?>'>En-Route</a>
                    <?php
                }
            }

            if ($Status != ABORT && $Status != DONE && $Status != START) {
                $href = "href='#popup-done-" . $OrderID . "'";
                $class = "fancybox button5 btn-margin bg-green";
            } elseif ($Status == START) {
                $href = "href='#popup-done-" . $OrderID . "'";
                $class = "fancybox button2 nomarginLeft bg-green";
            } else {
                $href = "";
                $class = "button5 btn-margin button-disabled";
            }

            if ($Status == DONE && !empty($SurveyID) && !empty($CustomerEmail)) {
                ?>
                <a data-id="<?php echo (!empty($OrderID)) ? $OrderID : false; ?>" data-app="mobile-app" class="send-to-email button2 nomarginRight">Send Review</a>
                <?php
            } else {
                ?>
                <a <?php echo $href; ?> class="<?php echo $class; ?>">Done</a>
                <?php
                if ($Status != ABORT && $Status != DONE && $Status != START) {
                    $href = "href='#popup-abort-" . $OrderID . "'";
                    $class = "fancybox button5 no-margin bg-red";
                } elseif ($Status == START) {
                    $href = "href='#popup-abort-" . $OrderID . "'";
                    $class = "fancybox button2 nomarginRight bg-red";
                } else {
                    $href = "";
                    $class = "button5 no-margin button-disabled";
                }
                ?>
                <a <?php echo $href; ?> class="<?php echo $class; ?>">Abort</a>
                <?php
            }
            if ($Status == ENROUTE) {
                $this->load->view("order-popup-cancel-enroute", $OrderID);
            } elseif ($Status == START) {
                
            } else {
                $this->load->view("order-popup-enroute", $OrderID);
            }
            $this->load->view("order-popup-done", $OrderID);
            $this->load->view("order-popup-note", $OrderID);
        }
        ?>
    </div>
    <div class="clear"></div>
</div>