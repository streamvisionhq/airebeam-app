<?php

if ($this->session->userdata("error")) {
    $this->load->view("error");
} elseif ($this->session->userdata("msg")) {
    $this->load->view("success");
}
?>