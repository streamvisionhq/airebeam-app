<?php
$this->load->view("header");
$address = $detail["Address1"] . " " . $detail["Address2"] . " " . $detail["City"] . " " . $detail["Zip"];
?>
<div id="container" class="wrapper">

    <div class="header">
        <div class="logout back">
            <a href="<?php echo BASE_PATH; ?>" title="Back" class="anchor-no-internet">
                <i class='icon icon-back'></i>
            </a>
        </div>
        <div class="header-title float-left">Today's Task</div>
    </div>

    <div id="cus-loading-overlay" class="msgs info-msg" style="display: none;">
        <img src="<?php echo BASE_PATH; ?>assets/img/animation.gif"><span><?php echo UPDATE_SCHEDULED_ORDERS; ?></span>
    </div>
    <div class="msgs error-msg" style="display: none;">
        <p><?php echo NO_INTERNET_CONNECTION; ?></p>
    </div>
    <?php
    $this->load->view("show-message");
    ?>

    <div class="list-container header-btn">

        <!-- Done Button -->
        <?php
        if ($detail["Status"] == DONE || $detail["Status"] == ABORT) {
            $href = "";
            $class = "button-disabled";
        } else {
            $href = "href='#popup-done-" . $detail["OrderID"] . "'";
            $class = "fancybox bg-green";
        }
        ?>
        <a <?php echo $href; ?> class="<?php echo $class; ?>">Done</a>
        <!-- End Done Button -->

        <!-- Abort Button -->
        <?php
        if ($detail["Status"] == DONE || $detail["Status"] == ABORT) {
            $href = "";
            $class = "button-disabled";
        } else {
            $href = "href='#popup-abort-" . $detail["OrderID"] . "'";
            $class = "fancybox bg-red";
        }
        ?>
        <a <?php echo $href; ?> class="<?php echo $class; ?>">Abort</a>
        <!-- End Abort Button -->


        <!-- Start Button -->
        <?php
        if ($detail["Status"] == ENROUTE) {
            ?>
            <div class="started-btn">
                <ul>                  
                    <li class="onclick"><a class='f-click' data-id="<?php echo $detail["OrderID"]; ?>">Start</a></li>
                    <li class="onclick-hide"><a href="#popup-cancel-enroute-<?php echo $detail["OrderID"]; ?>" class="fancybox">Cancel En-Route</a></li>                                   
                </ul>
            </div>
            <?php
        } elseif ($detail["Status"] == START) {
            ?>
            <div class="started-btn disabled">
                <ul class="disabled">
                    <li><a onclick='javascript:void();'>Start</a></li>               
                </ul>
            </div>
            <?php
        } else {
            if (!$enrouteOrderID) {
                ?>
                <a href="<?php echo BASE_PATH . "orders/enroute/" . $detail['OrderID']; ?>" class='fancybox bg-grey loadOverlaySetEnroute anchor-no-internet'>En-Route</a>
            <?php } else {
                ?>
                <a href="#popup-en-route-<?php echo $detail['OrderID']; ?>" class='fancybox bg-grey'>En-Route</a>
                <?php
            }
        }
        ?>
        <!-- Start Button End -->

        <a target="_blank" href='http://geo.airebeam.net/ac/?address=<?php echo $address; ?>' class="bg-blue">Geo</a>        
        <?php
        if (!empty($iPhone) && $iPhone == "Yes") {
            ?>
            <a href='<?php echo "http://maps.apple.com/?daddr=" . $address . "&directionsmode=driving"; ?>' target='_blank' class="bg-blue">Map</a>
            <?php
        } elseif (!empty($Windows) && $Windows == "Yes") {
            ?>
            <a href='<?php echo "http://maps.google.com/maps?saddr=Current%20Location&daddr=" . $address . "&directionsmode=driving"; ?>' target='_blank' class="bg-blue">Map</a>
            <?php
        } else {
            ?>
            <a href='<?php echo "google.navigation:q=" . urlencode($address); ?>' target='_blank' class="bg-blue">Map</a>
            <?php
        }
        ?>
        <a target="_blank" href='https://billing.airebeam.com/mbr_detail.ews?CustomerID=<?php echo $detail["CustomerID"]; ?>' class="bg-blue">Emerald</a>
    </div>   

    <?php
    if ($detail["Status"] != "") {
        if ($detail["Status"] == START) {
            $array["class"] = "started";
            $array["text"] = START;
        } elseif ($detail["Status"] == SCHEDULE) {
            $array["class"] = "scheduled";
            $array["text"] = SCHEDULE;
        } elseif ($detail["Status"] == DONE) {
            $array["class"] = "complete";
            $array["text"] = DONE;
        } elseif ($detail["Status"] == ABORT) {
            $array["class"] = "reported";
            $array["text"] = ABORT;
        } elseif ($detail["Status"] == ENROUTE) {
            $array["class"] = "enroute";
            $array["text"] = ENROUTE;
        }
    }
    ?>

    <div class="list-header bg-grey">
        <i class="icon icon-<?php echo $array["class"] ?>"></i> <?php echo $array["text"]; ?>
        <div class="clear"></div>
    </div>

    <?php if (!empty($detail["CustomerName"])) { ?>
        <div class="list-content">
            <div class="list-top">
                <i class="icon icon-user"></i> Customer Name
            </div>
            <div class="list-bottom"><?php echo $detail["CustomerName"]; ?></div>
            <div class="clear"></div>
        </div>
        <?php
    }
    if (!empty($detail["Name"])) {
        ?>
        <div class="list-content">
            <div class="list-top">
                <i class="icon icon-order"></i> Appointment Name
            </div>
            <div class="list-bottom"><?php echo $detail["Name"]; ?></div>
            <div class="clear"></div>
        </div>
        <?php
    }
    if (!empty($detail["StartDate"])) {
        ?>
        <div class="list-content">
            <div class="list-top">
                <i class="icon icon-time"></i> Time
            </div>
            <div class="list-bottom"><?php echo getOrderTime($detail["StartDate"]); ?></div>
            <div class="clear"></div>
        </div>
        <?php
    }
    if (!empty($detail["Type"])) {
        ?>
        <div class="list-content">
            <div class="list-top">
                <i class="icon icon-service"></i> Service
            </div>
            <div class="list-bottom"><?php echo $detail["Type"]; ?></div>
            <div class="clear"></div>
        </div>
        <?php
    }
    if (!empty($detail["PhoneHome"])) {
        ?>
        <div class="list-content">
            <div class="list-top">
                <i class="icon icon-phone"></i> Phone
            </div>
            <div class="list-bottom"><?php echo $detail["PhoneHome"]; ?></div>
            <div class="clear"></div>
        </div>
        <?php
    }
    if (!empty($detail["Address1"])) {
        ?>
        <div class="list-content">
            <div class="list-top">
                <i class="icon icon-address"></i> Address
            </div>
            <div class="list-bottom"><?php echo $address; ?></div>
            <div class="clear"></div>
        </div>
        <?php
    }
    if (!empty($detail["Detail"])) {
        ?>
        <div class="list-content">
            <div class="list-top">
                <i class="icon icon-desc"></i> Description
            </div>
            <div class="list-bottom"><?php echo $detail["Detail"]; ?></div>
            <div class="clear"></div>
        </div>
        <?php
    }
    ?>

    <!------------------ Notes  -------------------->
    <div class="list-container note-list">
        <div class="notes-title">
            <div class="line nomarginLeft"></div>
            <div class="title">Notes</div>
            <div class="line nomarginRight"></div>
        </div>
    </div>
    <form class="form note-form" method="post">
        <div class='error msgs error-msg display-none'></div>
        <input type="hidden" name="order-id" value="<?php echo $detail["OrderID"]; ?>">
        <input type="hidden" name="worker-id" value="<?php echo $detail["WorkerID"]; ?>">
        <input type="hidden" name="label" value="detail">
        <textarea class="textarea" name="note" placeholder="Enter Message"></textarea>        
        <input type="button" name="status" value="Submit" class="bg-green btn">
    </form>
    <div class="list-container">
        <?php
        foreach ($allNotes as $n) {?>
        <div class="list-head list-notes">
            <div class="list-title">                
                <?php
                echo $n['Name'];
                
                switch ($n['Label']){
                    case 'Note':
                        $class="blue";
                    break;
                    case 'Done':
                        $class="green";
                    break;
                    case 'Abort':
                        $class="red";
                    break;                                            
                }
                ?>
                <p class="<?php echo $class; ?>">(<?php echo $n['Label']; ?>)</p>
            </div>
            <div class="list-status">
                <div class="notes-detail"><?php echo getOrderDate($n["Date"]); ?></div>
                <div class="notes-detail"><?php echo showTime($n["Date"]); ?></div>
            </div>
            <div class="clear"></div>
            <div class="notes-detail wname"><?php echo $n['Worker']; ?></div>
            <div class="field-address">
                <div class="listing-text"><?php echo $n["Note"] ?></div>
            </div>
        </div>
        <?php } ?>
        <div class="clear"></div>
    </div>
</div>
<a href="#order-popup-survey" class="display-none order-popup-survey fancybox">Launch Survey Popup</a>
<?php
$this->data["OrderID"] = $detail["OrderID"];
$this->data["Status"] = $detail["Status"];
if ($detail["Status"] == ENROUTE) {
    $this->load->view("order-popup-cancel-enroute", $this->data);
} elseif ($detail["Status"] == START) {
    
} else {
    $this->load->view("order-popup-enroute", $this->data);
}
$this->load->view("order-popup-done", $this->data);
$this->load->view("order-popup-note", $this->data);
$this->load->view("order-popup-survey");
?>
</body>
</html>