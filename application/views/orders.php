	<?php
$this->load->view("header");
?>
<div id="container" class="wrapper">
    <div class="header">
        <div class="header-title">Today's Task</div>
        <div class="logout"> <a href="<?php echo base_url() . "index.php/logout" ?>" title="Logout"> <i class='icon icon-logout'></i> </a> </div>
    </div>
    <!-- End Header -->    
    <div id="cus-loading-overlay" class="msgs info-msg" style="display: none;">
        <img src="<?php echo base_url(); ?>assets/img/animation.gif"><span><?php echo UPDATE_SCHEDULED_ORDERS; ?></span>
    </div>
    <div class="msgs error-msg" style="display: none;">
        <p><?php echo NO_INTERNET_CONNECTION; ?></p>
    </div>
    <?php
    $this->load->view("show-message");
    ?>
    <?php
    if (!empty($allOrders)) {
        foreach ($allOrders as $key => $order) {
            foreach ($order as $o) {
                if ($o["Status"] == REJECTED) {
                    $o["class"] = "rejected";
                } elseif ($o["Status"] == START) {
                    $o["class"] = "started";
                } elseif ($o["Status"] == SCHEDULE) {
                    $o["class"] = "scheduled";
                } elseif ($o["Status"] == DONE) {
                    $o["class"] = "complete";
                } elseif ($o["Status"] == ABORT) {
                    $o["class"] = "reported";
                } elseif ($o["Status"] == ENROUTE) {
                    $o["class"] = "enroute";
                }
                $this->load->view("order-section", $o);
            }
        }
    }
    ?>
</div>
<a href="#order-popup-survey" class="display-none order-popup-survey fancybox">Launch Survey Popup</a>
<?php
$this->load->view("order-popup-survey");
?>
<p style="padding:5px 20px;">2.7.2.1</p>
</body>
</html>
