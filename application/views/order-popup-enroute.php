<div id='popup-en-route-<?php echo $OrderID; ?>' class='popup-container' style='display: none;'>
    <h3 class='popup-title'>Enroute</h3>
    <p>You have indicated that you are already enroute to your next appointment. Please update the status on your previous appointment. Please choose a status below</p>

    <div class="header-btn">
        <a class='bg-green enroute-done'>Done</a>
        <a class='bg-red enroute-note'>Abort</a>
        <a class='enroute-schedule bg-blue loadOverlay'>Schedule</a>
    </div>

    <form class='form popup-form enroute-schedule-form display-none' method='post'>
        <input type="hidden" name="order-id" value="<?php echo $OrderID; ?>">
        <input type="hidden" name="label" value="enroute-schedule">
    </form>

    <form class='form popup-form enroute-done-form display-none' method='post'>
        <div class='error msgs error-msg display-none'></div>
        <input type="hidden" name="order-id" value="<?php echo $OrderID; ?>">
        <input type="hidden" name="label" value="enroute-complete">
        <textarea class='textarea popup-textarea' name='note' placeholder='Enter note'></textarea>
        <input type='button' name='status' value='Done' class='bg-green btn loadOverlay' />
    </form>

    <form class='form popup-form enroute-note-form display-none' method='post'>
        <div class='error msgs error-msg display-none'></div>
        <input type="hidden" name="order-id" value="<?php echo $OrderID; ?>">
        <input type="hidden" name="label" value="enroute-abort">
        <textarea class='textarea popup-textarea report-popup-textarea' name='note' placeholder='Enter note (required)'></textarea>
        <input type='button' name='status' value='Abort' class='bg-red btn loadOverlay' />
    </form>
</div>