<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!doctype html>
<html>
    <head>
        <meta charset="utf8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <title><?php echo $meta_title; ?></title>
        <link rel="stylesheet" href="<?php echo ASSETS_PATH; ?>css/style.css">        
        <script type="text/javascript" src="<?php echo ASSETS_PATH; ?>js/jquery-1.10.1.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>
        <script type="text/javascript" src="<?php echo ASSETS_PATH; ?>js/jquery.fancybox.js?v=2.1.5"></script>
        <script src="<?php echo ASSETS_PATH; ?>js/jquery.validate.min.js"></script>
        <script src="<?php echo ASSETS_PATH; ?>js/FormValidation.js"></script>        
        <script language="javascript">
            var base_url = "<?php echo base_url(); ?>";
        </script>
        <script type="text/javascript" src="<?php echo ASSETS_PATH; ?>js/main.js"></script>
        <script type="text/javascript" src="<?php echo ASSETS_PATH; ?>js/order.js"></script>
        <script type="text/javascript" src="<?php echo ASSETS_PATH; ?>js/survey.js"></script>
        <link rel="stylesheet" type="text/css" href="<?php echo ASSETS_PATH; ?>css/jquery.fancybox.css?v=2.1.5" media="screen" />
        <script type="text/javascript" src="<?php echo ASSETS_PATH; ?>js/head.js"></script>
        <script src="<?php echo ASSETS_PATH; ?>js/script.php"></script>
    </head>
    <body>
        <div class="cus-overlay"></div>