<div id='popup-done-<?php echo $OrderID; ?>' class='popup-container' style='display: none;'>
    <h3 class='popup-title'>Mark Done</h3>
    <p>This action will change the appointment status to "complete".</p>    
    <form class='popup-form form' method='post'>
        <div class='error msgs error-msg display-none'></div>
        <input type="hidden" name="order-id" value="<?php echo $OrderID; ?>">
        <input type="hidden" name="label" value="complete">
        <textarea class='textarea popup-textarea' name='note' placeholder='Enter note'></textarea>
        <?php
        if ($Status == ENROUTE || $Status == START) {
            ?>        
            <span class="dropdown-container">
                <select class='wrapper-dropdown-5' name='next-order-enroute'>
                    <option value=''>Select next appointment (optional)</option>
                    <?php
                    foreach ($allScheduledOrders as $scheduledOrder) {
                        ?>
                        <option value='<?php echo $scheduledOrder["OrderID"] ?>'><?php echo $scheduledOrder["Name"] ?></option>
                        <?php
                    }
                    ?>
                </select>
            </span>
            <?php
        }
        ?>
        <input type='button' name='status' value='Done' class='bg-green btn' />
    </form>
</div>