<?php
$this->load->view("admin/header");
?>
<div class="note-container">
    <div class="container-fluid">
        <div class="space"></div>
        <div class="row-fluid">
            <div class="span12">
                <div class="box">
                    <div class="box-content nopadding">
                        <div class="search-results">
                            <table id="paginated_table" class="display table" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>Appointment</th>
                                        <th>Twilio Response</th>
                                        <th>Action</th>
                                        <th>Logs</th>
                                    </tr>                                    
                                </thead>
                                <tbody>
                                    <?php foreach ($failed_sms as $key => $sms): ?>
                                        <?php 
                                            $failure_logs = json_decode($sms['Content']);
                                            $btn_text = 'Notify Customer';
                                            $class = 'notify-customer btn btn-danger';
                                            if ($sms['IsNotified'] == 1) {
                                                $btn_text = 'Notified';
                                                $class = 'btn btn-notified';
                                            }
                                        ?>
                                        <tr>
                                            <td>
                                                <a href="#modal-editIncident" data-eventid="<?= $sms['OrderID']; ?>" class="note-editIncident notes_edit_popup modal-open" role="button" data-toggle="modal" data-backdrop="static" data-keyboard="false">
                                                <?= $sms['Name'] ?>
                                                </a>                                                    
                                            </td>
                                            <td><?= $sms['Message'] ?></td>
                                            <td>
                                                <input data-id="<?= $sms['OrderID']; ?>" type="submit" value="Resend SMS" class="send-sms btn-primary append-alert-id" style="background-color: #006dcc;">
                                                <input data-order-id="<?= $sms['OrderID']; ?>" data-customer-id="<?= $sms['CustomerID']; ?>" type="submit" value="<?= $btn_text ?>" class="<?= $class ?>">
                                                
                                            </td>
                                            <td class="failed_logs">
                                                <?php foreach ($failure_logs as $key => $log): ?>
                                                    <b>Message :    </b>
                                                    <?php 
                                                        $twilio_msg = explode(',', $log->message);
                                                        echo $twilio_msg[1];
                                                    ?>
                                                    <b>Date :    </b><?= $log->date ?>
                                                    <br>
                                                <?php endforeach ?>                                                    
                                            </td>
                                        </tr>                                        
                                    <?php endforeach ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
    $this->load->view("admin/create-incident");
    $this->load->view("admin/edit-incident");
    ?>
</div>
</body>
</html>