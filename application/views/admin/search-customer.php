<?php
$this->load->view("admin/header");
?>
<?php
$this->load->view("admin/show-message");
?>
<?php
if (!empty($privilages)) {
    if (in_array(SEARCH_APPOINTMENT, (array) $privilages)) {
        ?>
        <div class="wrapper customer-search-wrapper">
            <div class="space"></div>
            <div class="span12">
                <div class="box">
                    <div class="box-content">
                        <form method="POST" class='form-horizontal'>
                            <div class="control-group">
                                <div class="span6">
                                    <label for="textfield" class="control-label">Search Here</label>
                                    <div class="controls">
                                        <input autocomplete="off" name="customer" type="text" value="<?php
                                        if ($this->input->post("customer")) {
                                            echo $this->input->post("customer");
                                        }
                                        ?>" id="customer_search" class="input-xlarge" placeholder="MBR/Customer Name" data-rule-required="true" data-rule-minlength="1">
                                        <button type="submit" name="mbr_search" class="btn btn-primary" value="mbr_search">Search</button>
                                        <span class="help-block">Type Customer MBR or Name to find appointments.</span>
                                    </div>
                                    <div class="clear"></div>
                                </div><!-- /.span6 -->
                                <div class="clear"></div>
                                <hr>
                                <div class="multi-search-fields-wrapper">
                                    <div class="span4">
                                        <label for="global_search_text" class="control-label lbl-appt">Search Appointments</label>
                                        <div class="controls">
                                            <input autocomplete="off" name="global_search_text" type="text" value="<?php
                                            if ($this->input->post("global_search_text")) {
                                                echo $this->input->post("global_search_text");
                                            }
                                            ?>" id="global_search_text" class="input-xlarge" placeholder="Email/Address/Phone/Description" data-rule-required="true" data-rule-minlength="1">
                                            <span class="help-block">Type Customer Email/Phone/Address or description to find appointments.</span>
                                        </div>
                                    </div><!-- /.span4 -->
                                    <div class="span2">
                                        <label for="start_date" class="control-label lbl-start-date">Start Date</label>
                                        <div class="controls">
                                            <input type="text" id="start_date" name="start_date" class="datepick input-xlarge" autocomplete="off" value="<?php
                                            if ($this->input->post("start_date")) {
                                                echo $this->input->post("start_date");
                                            }
                                            ?>"> 
                                            <span class="help-block">Select the start date to filter from.</span>
                                        </div>
                                    </div><!-- /.span2 -->
                                    <div class="span2">
                                        <label for="end_date" class="control-label lbl-end-date">End Date</label>
                                        <div class="controls">
                                            <input type="text" id="end_date" name="end_date" class="datepick input-xlarge" autocomplete="off" value="<?php
                                            if ($this->input->post("end_date")) {
                                                echo $this->input->post("end_date");
                                            }
                                            ?>"> 
                                            <span class="help-block">Select the end date to filter from.</span>
                                            <button type="submit" id="global_search_button" name="global_search" class="btn btn-primary btn-search-date" value="global_search">Search</button>
                                        </div>
                                    </div><!-- /.span2 -->
                                    <div class="clear"></div>
                                </div><!-- /.multi-search-fields-wrapper -->
                                <div class="clear"></div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <?php
            if (!empty($customer_detail)) {
                $customer_name = $customer_detail["customer"]->CustomerName;
                $customer_id = $customer_detail["customer"]->CustomerID;
                $is_reset = $customer_detail["customer"]->isPasswordReset;
                ?>
                <div class="container-fluid">
                    <div class="row-fluid">
                        <div class="span12">
                            <div class="box box-color box-bordered">
                                <div class="box-title">
                                    <h3> <i class="icon-table"></i> <?php echo $customer_name; ?> </h3>
                                    <div class="actions">
                                        <?php
                                        $disabled = false;
                                        $title = false;
                                        if ($is_reset == "1") {
                                            $disabled = ' disabled="disabled"';
                                            $title = ' title="Force Password Reset is still active"';
                                        }
                                        ?>
                                        <?php
                                        if (INSITE != $role) {
                                            ?>
                                            <button<?php echo $disabled; ?><?php echo $title; ?> type="button" class="btn force-reset-hit" data-toggle="modal" data-target="#myModal">Force Password Reset</button>
                                            <?php
                                        }
                                        ?>
                                        <a href="#modal-3" role="button" class="btn" data-toggle="modal" title="View Force Password Reset Logs"><i class="icon-th-list"></i></a>
                                        <div id="modal-3" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                <h4 class="modal-title">Force Password Reset Activity Logs</h4>
                                            </div>
                                            <div class="modal-body">
                                                <ul class="password-activity">
                                                    <?php
                                                    $this->load->view("admin/password-activity", ["activities" => $customer_detail["activities"]]);
                                                    ?>
                                                </ul>
                                            </div>
                                            <div class="modal-footer">
                                                <button class="btn btn-primary" data-dismiss="modal" aria-hidden="true">Close</button>
                                            </div>
                                        </div>

                                        <!-- Modal -->
                                        <div id="myModal" class="modal fade" role="dialog">
                                            <div class="modal-dialog">
                                                <!-- Modal content-->
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        <h4 class="modal-title">Force Password Reset</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <p>Are you sure you want to force this customer to reset password?</p>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                                                        <button type="button" data-id="<?php echo $customer_id; ?>" class="btn btn-primary force-reset" data-dismiss="modal">Yes</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="box-content nopadding">
                                    <table class="table table-hover table-nomargin dataTable table-bordered">
                                        <thead>
                                            <tr>
                                                <th width="40">ID</th>
                                                <th>Customer</th>
                                                <th>Date</th>
                                                <th>Time</th>
                                                <th>City</th>
                                                <th>Service</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $x = 1;
                                            if (isset($customer_detail["orders"])) {
                                                foreach ($customer_detail["orders"] as $orders) {
                                                    foreach ($orders as $order) {
                                                        ?>
                                                        <tr>
                                                            <td><?php echo $x++; ?></td>
                                                            <td><a href="#modal-editIncident" class="alert-editIncident" data-eventid="<?php echo $order["OrderID"]; ?>" data-toggle="modal" data-backdrop="static" data-keyboard="false"><?php echo $order["Name"]; ?></a></td>
                                                            <td><?php echo getOrderDate($order["Date"]); ?></td>
                                                            <td><?php echo showTime($order["Date"]); ?></td>
                                                            <td><?php echo $order["City"]; ?></td>
                                                            <td><?php echo $order["Type"]; ?></td>
                                                            <td>
                                                                <button class="alert-editIncident btn btn-primary" href="#modal-editIncident" role="button" data-toggle="modal" data-backdrop="static" data-keyboard="false" data-eventid="<?php echo $order["OrderID"]; ?>" title="View Appointment"><i class="icon-eye-open"></i></button>
                                                            </td>
                                                        </tr>
                                                        <?php
                                                    }
                                                }
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
            }
            if($this->input->post("global_search")):
                ?>
                <div class="box-content nopadding">
                    <table class="table table-hover table-nomargin dataTable table-bordered">
                        <thead>
                            <tr>
                                <th width="40">ID</th>
                                <th>Customer</th>
                                <th>Date</th>
                                <th>Time</th>
                                <th>City</th>
                                <th>Service</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            if (isset($global_order_search)):
                            $x = 1;
                            foreach ($global_order_search as $order) {
                                ?>
                                <tr>
                                    <td><?php echo $x++; ?></td>
                                    <td><a href="#modal-editIncident" class="alert-editIncident" data-eventid="<?php echo $order->OrderID; ?>" data-toggle="modal" data-backdrop="static" data-keyboard="false"><?php echo $order->orderName; ?></a></td>
                                    <td><?php echo getOrderDate($order->Date); ?></td>
                                    <td><?php echo showTime($order->Date); ?></td>
                                    <td><?php echo $order->City; ?></td>
                                    <td><?php echo $order->Name; ?></td>
                                    <td>
                                        <button class="alert-editIncident btn btn-primary" href="#modal-editIncident" role="button" data-toggle="modal" data-backdrop="static" data-keyboard="false" data-eventid="<?php echo $order->OrderID; ?>" title="View Appointment"><i class="icon-eye-open"></i></button>
                                    </td>
                                </tr>
                                <?php
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
                <div class="space"></div>
            </div>
        <?php endif; endif; ?>
        <?php
    }
    $this->load->view("admin/create-incident");
    $this->load->view("admin/edit-incident");
}
?>
</body>
</html>