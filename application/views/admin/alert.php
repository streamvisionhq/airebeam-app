<?php
$class = $cancelSMS = $href = false;
$button = "Done";
$buttonClass = "done-btn ";
$class = " alert-bg-light-red";
$buttonClass = "done-btn rejection-btn ";
$href = ' href="#modal-a3"';

if ($row["TypeID"] == "73dc597bd98e2") {
    $class = " alert-bg-dark-red";
    $button = "OK";
    $buttonClass = "done-btn late-rejection-btn ";
} elseif ($row["TypeID"] == "7f3c597bd72e3") {
    $button = "OK";
} elseif ($row["TypeID"] == "7e3c597bd72e1" || $row["TypeID"] == "7d3c597bd72e2") {
    if ($order["isPhoneHomeLandline"] != "1") {
        $buttonClass = "send-sms rejection-btn ";
        $button = "Send SMS";
        $href = false;
        $cancelSMS = true;
    }
}
?>
<div class="row-fluid alert-container">
    <div class="span12">
        <div class="alert alert-error bg-red<?php echo $class; ?>">
            <div class="alert-content float-left">
                <a href="#modal-editIncident" class="alert-editIncident" role="button" data-toggle="modal" data-backdrop="static" data-keyboard="false" data-eventid="<?php echo $row["ObjectID"]; ?>"><?php echo $row["Message"]; ?></a>
            </div>
            <div class="alert-btn float-right">
                <a<?php echo $href; ?> role="button" data-toggle="modal"><input data-id="<?php echo $row["ObjectID"]; ?>" type="submit" value="<?php echo $button; ?>" class="<?php echo $buttonClass; ?>append-alert-id" /></a>
                <?php
                if($cancelSMS){
                    ?>
                    <input data-id="<?php echo $row["ObjectID"]; ?>" type="button" value="Cancel" class="cancel-send-sms append-alert-id" /></a>
                    <?php
                }
                ?>
            </div>
            <div class="clear"></div>
        </div>
    </div>
</div>