<?php
$this->load->view("admin/header");
$this->load->view("admin/show-message");
?>

<div class="wrapper">
    <div class="space"></div>
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span12">
                <div class="box box-color box-bordered">
                    <div class="box-title">
                        <h3><i class="icon-lock"></i> Change Password</h3>
                    </div>
                    <div class="box-content nopadding">
                        <form method="POST" class='form-horizontal form-bordered form-validate' id="bb" enctype='multipart/form-data'>
                            <div class="control-group">
                                <label for="oldpwfield" class="control-label">Old Password</label>
                                <div class="controls">
                                    <input type="password" name="oldpassword" id="oldpwfield" class="input-xlarge" data-rule-required="true">
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="pwfield" class="control-label">New Password</label>
                                <div class="controls">
                                    <input type="password" name="password" id="pwfield" class="input-xlarge" data-rule-required="true" data-rule-minlength="6">
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="pwfield" class="control-label">Confirm Password</label>
                                <div class="controls">
                                    <input type="password" name="confirmpassword" id="confirmfield" class="input-xlarge" data-rule-equalto="#pwfield" data-rule-required="true">
                                </div>
                            </div>
                            <div class="form-actions">
                                <input type="submit" name="submit" class="btn btn-primary" value="Save changes">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
$this->load->view("admin/create-incident");
$this->load->view("admin/edit-incident");
?>
</body>
</html>