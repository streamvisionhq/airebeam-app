<?php
if ($surveys) {
    $x = 1;
    foreach ($surveys as $survey) {
        ?>
        <tr>
            <td><?php echo $x++; ?></td>
            <td><?php echo $survey->Name; ?></td>
            <td><?php echo $survey->Email; ?></td>
            <td>
                <a data-id="<?php echo (!empty($survey->OrderID)) ? $survey->OrderID : false; ?>" data-app="admin-app" class="btn btn-primary send-to-email" title="Send Survey"><img src="<?php echo base_url() . "assets/admin/img/icon-send-survey.png" ?>" /></a>
            </td>
            <td><input type="checkbox" name="single-survey[]" value="<?php echo (!empty($survey->OrderID)) ? $survey->OrderID : false; ?>" /></td>
        </tr>
        <?php
    }
    ?>
    <?php
} else {
    ?>
    <tr class="odd"><td valign="top" colspan="5" class="dataTables_empty">No data available in table</td></tr>
    <?php
}
?>