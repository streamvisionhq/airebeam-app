Hi <?php echo $CustomerName; ?>,<br>
<p>Hope you are doing well.</p>
<p>We just wanted to reach out to you real quick and ask you how we did at our recent appointment with you.</p>
<p>We'd really appreciate if you can fill out this quick survey. It will only take a minute but will go a long way
in help us do better next time.</p>
<br>
<a href="https://www.surveymonkey.com/r/V8QXPQV" target="_blank">https://www.surveymonkey.com/r/V8QXPQV</a>
<br><br>
Looking forward to your feedback.
<br><br>
Thank you,
<br>
Gregory A Friedman