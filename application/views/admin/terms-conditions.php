<?php
$this->load->view("admin/header");
$this->load->view("admin/show-message");
?>

<div class="wrapper">
    <div class="space"></div>
    <div class="container-fluid absa-tc-section">


                <div class="row-fluid">
                    <div class="span5">
                        <div class="box box-color box-bordered">
                            <div class="box-title">
                                <h3>
                                    <!--<i class="fa fa-table"></i>-->
                                    Update Terms & Conditions
                                </h3>
                            </div>

                            <div class="box-content absa-tc-upload-file nopadding">
                                <form method="POST" class='form-validate' enctype='multipart/form-data'>
                                <div class="row-fluid absa-form-row">
                                    <div class="span3 absa-uf-label"><span>Upload pdf file</span></div>
                                    <div class="span9 absa-uf-dialog">
                                        <div class="control-group">
                                            <div class="controls">
                                                <input type="file" name="term" class="input-xlarge">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                    <div class="row-fluid">
                                        <div class="span3"></div>
                                        <div class="span9 absa-submit">
                                            <input type="submit" name="submit" class="btn btn-primary" value="Publish">
                                        </div>
                                    </div>




                               <!--     <a href="<?php /*echo base_url() . "app/calendar"; */?>"><input type="button" name="cancel" class="btn" value="Cancel"></a>-->
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="span7">
                        <div class="box box-color box-bordered">
                            <div class="box-title">
                                <h3>
                                    <!--<i class="fa fa-table"></i>-->
                                  Terms & Conditions History
                                </h3>
                            </div>
                            <div class="box-content nopadding">

                                    <?php

                                    if (!empty($terms)) {

                                        ?>
                                <table data-page-length="8" class="table table-hover table-nomargin table-bordered dataTable" id="absa-tc-file-list">
                                    <thead>
                                    <tr>
                                        <th data-orderable ="false" style="text-align:center">ID</th>
                                        <th class="max-256">Filename</th>
                                        <th>Timestamp</th>
                                        <th data-orderable ="false" style="text-align:center">View</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                        <?php

                                        $i =1;

                                        foreach ($terms as $term) {
                                            ?>
                                            <tr>
                                                <td style="text-align:center"><?php echo $i; ?></td>
                                                <td class="max-256" title="<?php echo $term["Filename"]; ?>"><?php echo $term["Filename"]; ?></td>
                                                <td><p class="details"><span class="date"><?php echo date("M j, Y h:i:s A", strtotime($term["Date"])); ?></span></p></td>
                                                <td style="text-align:center"><a class="btn btn-primary" href="<?php echo base_url() . 'assets/admin/terms/' . $term["Filename"]; ?>" target="_blank" title="View File"><i class="icon-eye-open"></i></a></td>
                                            </tr>
                                            <?php
                                            $i++;
                                        } ?>
                                    </tbody>
                                </table>
                                        <?php
                                    } else {
                                        ?>
                                        <span class="absa-no-data">
                                            No Data Found
                                        </span>
                                        <?php
                                    }
                                    ?>


                            </div>
                        </div>
                    </div>
                </div>


    </div>
</div>
<?php
$this->load->view("admin/create-incident");
$this->load->view("admin/edit-incident");
?>
</body>
</html>