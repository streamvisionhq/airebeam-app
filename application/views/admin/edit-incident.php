<div id="modal-editIncident" class="modal hide fade view-popup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div id="edit_popup_cont" style="display:none">
        <div class="modal-header">
            <button type="button" class="btn btn-remove pull-right" data-dismiss="modal" aria-hidden="true">
                <i class="icon-remove"></i>
            </button>
            <?php
            if ( (isset($role) ? $role : null) != INSITE ) {
                  ?>
                  <button type="button" class="btn btn-primary btn-edit pull-right" title="Edit Appointment">
                      <i class="icon-edit"></i>
                  </button>
                  <?php
            }
            ?>
            <a href="#activitylog" class="activitylog-btn"><button type="button" id="activity-log" class="btn btn-primary1 btn-activity pull-right" data-href="#activitylog" data-id="" title="Activity Log">
                    <i class="icon-activitylog"></i>
                </button>
            </a>
            <h3 id="myModalLabel">View Appointment</h3>
            <?php
            if ( (isset($role) ? $role : null) != INSITE ) {
                  ?>
                  <div class="alertbox-notification-appointment"></div>
                  <?php
            }
            ?>
            <div class="alert alert-error cs-general-error">
                <strong>Warning!</strong> <span id="email-error-edit"></span>
            </div>
            <div class="alert alert-error cs-general-error">
                <strong>Warning!</strong> <span id="phone-error-edit"></span>
            </div>
        </div>
        <form action="#" method="POST" class='form-validate' id="editIncidentForm">
            <div class="modal-body">
                <div id="error-php" class="error"></div>
                <div class="controls control-group">
                    <input type="text" name="name" id="textfield" class="input-block-level jquery-validation null-val" placeholder="Order Name" data-rule-required="true" data-rule-minlength="1" disabled>
                </div>
                <div class="controls control-group">
                    <input type="text" name="customer" class="input-block-level jquery-validation null-val" placeholder="MBR/Customer Name" data-rule-required="true" data-rule-minlength="1" disabled>
                </div>
                <div class="controls control-group">
                    <textarea name="description" id="textarea" rows="5" data-rule-required="true" class="input-block-level jquery-validation null-val" placeholder="Description" disabled></textarea>
                </div>

                <div class="controls control-group">
                    <select data-rule-required="true" id="service" name="service" class="input-block-level jquery-validation null-val" disabled>
                        <option value="">-- Services --</option>
                    </select>
                </div>
                <div class="controls control-group">
                    <select data-rule-required="true" name="status" id="status" class="input-block-level jquery-validation null-val" disabled>
                        <option value="">-- Status --</option>
                    </select>
                </div>

                <div class="controls control-group">
                    <input type="text" name="date" id="datepick" data-rule-required="true" placeholder="Select Date" class="input-block-level jquery-validation datepick null-val" disabled autocomplete="off">
                    <i class="icon-popup icon-calendar"></i>
                </div>
                <div class="controls control-group popuphalf-width pull-left">
                    <div class="bootstrap-timepicker">
                        <input type="text" data-rule-required="true" placeholder="Select Time" name="time" id="timepicker1" class="input-block-level jquery-validation timepick" disabled>
                        <i class="icon-popup icon-time"></i>
                    </div>
                </div>

                <div class="controls control-group popuphalf-width pull-right">
                    <select data-rule-required="true" name="duration" id="duration" class="input-block-level jquery-validation null-val" disabled>
                        <option value="">-- Duration --</option>
                    </select>
                </div>
                <div class="clearfix"></div>

                <div class="controls control-group">
                    <select data-rule-required="true" name="worker" id="assignworker" class="input-block-level jquery-validation null-val" disabled>
                        <option value="">-- Assign Worker --</option>
                    </select>
                </div>
                <div class="controls control-group">
                    <input type="text" name="address" id="caddress" class="input-block-level" placeholder="Customer Address" data-rule-required="true" data-rule-minlength="1" disabled>
                </div>
                <div class="controls control-group c-add">
                    <input type="text" name="email" id="cemail" class="input-block-level" placeholder="Customer Email" data-rule-required="true" data-rule-minlength="1" disabled>
                </div>
                <div class="controls control-group c-add">
                    <input type="text" name="phone" id="cphonenum" class="input-block-level" placeholder="Customer Phone No" data-rule-required="true" data-rule-minlength="1" disabled>
                </div>
                <!--- POPUP Notes --->
                <div class="search-results" id="activitylog">
                    <div class="note-title">
                        <div class="l-noteborder"></div>
                        <div class="text-note">Notes</div>
                        <div class="r-noteborder"></div>
                    </div>
                    <ul id="editIncidentNotes">
                    </ul>
                </div>
            </div>
            <div class="modal-footer">
                <input type="hidden" name="orderID" value="">
                <?php
                if ( (isset($role) ? $role : null) != INSITE ) {
                      ?>
                      <input type="submit" name="edit-incident" class="btn btn-submit btn-primary" value="Save Changes">
                      <?php
                }
                ?>
                <button type="button" class="btn btn-remove" data-dismiss="modal" aria-hidden="true">Cancel</button>
            </div>
        </form>
    </div>    
</div>    