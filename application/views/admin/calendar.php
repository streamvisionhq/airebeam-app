<?php
$this->load->view("admin/header");
$this->load->view("admin/show-message");
?>
<!-- Notifications -->
<div id="gritter-notice-wrapper" class="alert-warning">
    <div class="alert alert-error">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>Warning!</strong> <span id="schedule-error"><?php echo PREVIOUS_SLOT_ERROR; ?></span>
    </div>
</div>

<!-- Notifications -->
<div id="gritter-notice-wrapper" class="alert-nthsuccess">
    <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>Success!</strong> <span id="message-success"></span>
    </div>
</div>
<div class="container-fluid schedule-container" id="content">       
    <div class="space"></div>
    <div class="inner-container">
        <div id="left" class="map-container">
            <div id="map"></div>
        </div>
        <div id="main">
            <div id="calendar"></div>
        </div>
        <div class="clear"></div>
    </div>
</div>
<a href="#modal-editIncident" class="display-none marker-click alert-editIncident" data-toggle="modal" data-eventid=""></a>
<?php
$this->load->view("admin/delete-incident");
$this->load->view("admin/create-incident");
$this->load->view("admin/edit-incident");
?>    
</div>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDQWA29c0wqxsQpQJiEyJ9NZZL6YRhLihc"></script>
<script type="text/javascript" src="<?php echo ADMIN_ASSETS_PATH; ?>js/markerwithlabel.js"></script>
<script src="<?php echo ADMIN_ASSETS_PATH; ?>js/calendar/calendar.js"></script>
</body>
</html>