<head>
    <meta charset="utf8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <!-- Apple devices fullscreen -->
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <!-- Apple devices fullscreen -->
    <meta names="apple-mobile-web-app-status-bar-style" content="black-translucent" />
    <title><?php echo $meta_title; ?></title>
    <meta name="page" id="page" content="<?php echo $page_slug; ?>" />
    <!-- Favicon -->
    <link rel="shortcut icon" href="<?php echo ADMIN_ASSETS_PATH; ?>img/favicon.ico" />
    <!-- Apple devices Homescreen icon -->
    <link rel="apple-touch-icon-precomposed" href="<?php echo ADMIN_ASSETS_PATH; ?>img/apple-touch-icon-precomposed.png" />

    <link rel="stylesheet" href="<?php echo ADMIN_ASSETS_PATH; ?>css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo ADMIN_ASSETS_PATH; ?>css/style.css">
    <link rel="stylesheet" href="<?php echo ADMIN_ASSETS_PATH; ?>css/plugins/timepicker/bootstrap-timepicker.min.css">
    <link rel="stylesheet" href="<?php echo ADMIN_ASSETS_PATH; ?>css/plugins/datepicker/datepicker.css">
    <link rel="stylesheet" href="<?php echo ADMIN_ASSETS_PATH; ?>css/plugins/chosen/chosen.css">
    <link rel="stylesheet" href="<?php echo ADMIN_ASSETS_PATH; ?>css/plugins/gritter/jquery.gritter.css">
    <link rel="stylesheet" href="<?php echo ADMIN_ASSETS_PATH; ?>css/plugins/xeditable/bootstrap-editable.css">
    <link rel="stylesheet" href="<?php echo ADMIN_ASSETS_PATH; ?>css/plugins/tagsinput/jquery.tagsinput.css">

    


    <script language="javascript">
        var base_url = "<?php echo BASE_PATH; ?>";
    </script>

    <script src="<?php echo ADMIN_ASSETS_PATH; ?>js/jquery.min.js"></script>

    <script src="<?php echo ADMIN_ASSETS_PATH; ?>js/bootstrap.min.js"></script>
    <script src="<?php echo ADMIN_ASSETS_PATH; ?>js/plugins/datatable/jquery.dataTables.min.js"></script>
    <script src="<?php echo ADMIN_ASSETS_PATH; ?>js/jquery-ui.js"></script>
    <script src="<?php echo ADMIN_ASSETS_PATH; ?>js/plugins/xeditable/bootstrap-editable.min.js"></script>
    <script src="<?php echo ADMIN_ASSETS_PATH; ?>js/plugins/xeditable/demo.js"></script>
    <script src="<?php echo ADMIN_ASSETS_PATH; ?>js/eakroko.js"></script>
    <script src="<?php echo ADMIN_ASSETS_PATH; ?>js/plugins/chosen/chosen.jquery.min.js"></script>
    <script src="<?php echo ADMIN_ASSETS_PATH; ?>js/plugins/datepicker/bootstrap-datepicker.js"></script>
    <script src="<?php echo ADMIN_ASSETS_PATH; ?>js/plugins/timepicker/bootstrap-timepicker.min.js"></script>
    <script src="<?php echo ADMIN_ASSETS_PATH; ?>js/plugins/validation/jquery.validate.min.js"></script>
    <script src="<?php echo ADMIN_ASSETS_PATH; ?>js/plugins/gritter/jquery.gritter.min.js"></script>
    <script src="<?php echo ADMIN_ASSETS_PATH; ?>js/plugins/tagsinput/jquery.tagsinput.min.js"></script>
    <script src="<?php echo ADMIN_ASSETS_PATH; ?>js/jquery.are-you-sure.js"></script>
    <script src="<?php echo ADMIN_ASSETS_PATH; ?>js/custom.js"></script>
    <script src="<?php echo ASSETS_PATH; ?>js/order.js"></script>
    <?php
    if (isset($isLogin)) {
        ?>
        <script src="<?php echo ASSETS_PATH; ?>js/note.js"></script>
        <?php
    }
    ?>

    <script type="text/javascript" src="<?php echo ADMIN_ASSETS_PATH; ?>js/head.js"></script>
    <!--Calendar Links -->

    <link type = "text/css" href='<?php echo ADMIN_ASSETS_PATH; ?>js/plugins/calendar/css/smoothness/jquery-ui-1.8.11.custom.css' rel = 'stylesheet'/>
    <?php
    if ($this->router->class == "surveys") {
        ?>
        <script src="<?php echo ASSETS_PATH; ?>js/survey.js"></script>
        <?php
    }
    if ($this->router->class == "calendar") {
        ?>
        <link type = "text/css" href="<?php echo ADMIN_ASSETS_PATH; ?>js/plugins/calendar/jquery.weekcalendar.css" rel = "stylesheet"/>
        <link type = "text/css" href="<?php echo ADMIN_ASSETS_PATH; ?>js/plugins/calendar/default.css" rel = "stylesheet"/>
        <link type = "text/css" href="<?php echo ADMIN_ASSETS_PATH; ?>js/plugins/calendar/gcalendar.css" rel = "stylesheet"/>
        <script type='text/javascript' src='<?php echo ADMIN_ASSETS_PATH; ?>js/plugins/calendar/jquery-1.4.4.min.js'></script>
        <script type='text/javascript' src='<?php echo ADMIN_ASSETS_PATH; ?>js/plugins/calendar/jquery-ui-1.8.11.custom.min.js'></script>
        <script type='text/javascript' src='<?php echo ADMIN_ASSETS_PATH; ?>js/plugins/calendar/jquery-ui-i18n.js'></script>
        <script type="text/javascript" src="<?php echo ADMIN_ASSETS_PATH; ?>js/plugins/calendar/date.js"></script>
        <script type="text/javascript" src="<?php echo ADMIN_ASSETS_PATH; ?>js/plugins/calendar/jquery.weekcalendar.js"></script>
        <?php
    }
    ?>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>
</head>