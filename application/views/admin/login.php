<?php
$this->load->view("admin/header");
$this->load->view("admin/show-message");
?>
<div class="wrapper">
    <h1><a><img src="<?php echo base_url() . "assets/" ?>img/logo.png" alt="" class='retina-ready'></a></h1>
    <div class="login-body">
        <h2>SIGN IN</h2>
        <form method="post" class="form-validate" id="bb">
            <?php
            if (!empty($Error)) {
                ?>
                <div class = "alert alert-error">
                    <button type = "button" class = "close" data-dismiss = "alert">×</button>
                    <strong>Warning!</strong>
                    <?php echo $Error; ?>
                </div>
                <?php
            }
            ?>
            <div class="email controls control-group">
                <input type="text" name="email" id="emailfield" placeholder="Email address" class='input-block-level' data-rule-email="true" data-rule-required="true">
            </div>
            <div class="pw controls control-group">
                <input type="password" name="password" id="pwfield" placeholder="Password" class='input-block-level' data-rule-required="true">
            </div>
            <div class="submit">
                <input type="submit" name="submit" value="Sign in" class='btn btn-primary'>
            </div>
        </form>
    </div>
</div>
</body>
</html>