<?php
$this->load->view("admin/header");
$this->load->view("admin/show-message");
?>

<div class="wrapper">
    <div class="space"></div>
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span12">
                <div class="box box-color box-bordered">
                    <div class="box-title">
                        <h3><i class="icon-user"></i> Add Worker</h3>
                    </div>
                    <div class="box-content nopadding">
                        <form method="post" class='form-horizontal form-bordered form-validate' id="bb">
                            <div class="control-group">
                                <label for="fullname" class="control-label">Full Name</label>
                                <div class="controls">
                                    <input type="text" name="name" value="<?php echo $this->input->post("name"); ?>" id="fullname" class="input-xlarge" data-rule-required="true" data-rule-minlength="1">
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="textfield" class="control-label">Worker Role</label>
                                <div class="controls">
                                    <select name="role" id="bbb" data-rule-required="true" class="addworker-select" >
                                        <?php
                                        foreach ($roles as $key => $role) {
                                            ?>
                                            <option value="<?php echo $key; ?>"><?php echo $role; ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="api" class="control-label">API Key</label>
                                <div class="controls">
                                    <input type="text" name="api-key" id="api" class="input-xlarge">
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="emailfield" class="control-label">Email Address</label>
                                <div class="controls">
                                    <input type="text" name="email" id="emailfield" value="<?php echo $this->input->post("email"); ?>" class="input-xlarge" data-rule-email="true" data-rule-required="true">
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="pwfield" class="control-label">Password</label>
                                <div class="controls">
                                    <input type="password" name="password" id="pwfield" class="input-xlarge" data-rule-required="true" data-rule-minlength="6">
                                </div>
                            </div>
                            
                            <div class="control-group address-auto-suggest no-display">
                                <label for="pac-input" class="control-label">Address</label>
                                <div id="pac-container" class="controls">
                                    <input placeholder="Enter a location" type="text" name="address" value="<?php echo $this->input->post("name"); ?>" id="pac-input" class="input-xlarge" data-rule-required="true" data-rule-minlength="1">
                                    <input type="hidden" name="lat" id="lat" />
                                    <input type="hidden" name="lng" id="lng" />
                                    <input type="hidden" name="place_icon" id="place_icon" />
                                    <input type="hidden" name="place_name" id="place_name" />
                                    <input type="hidden" name="place_address" id="place_address" />
                                </div>
                            </div>
                            
                            
                            <div class="form-actions">
                                <input type="submit" name="submit" value="Add Worker" class="btn btn-primary">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
< 
<div id="map" style="display:none; width:50%; height:50%"></div>
    <div style="display:none;" id="infowindow-content">
      <img src="" width="16" height="16" id="place-icon">
      <span id="place-name"  class="title"></span><br>
      <span id="place-address"></span>
    </div>
 
    <script>
		
		$('[name="role"]').change(function(){
			var role_enum = {"Technician": "56a5c10c469f7"};
			var role_val = $(this).val();
			if( role_val == role_enum["Technician"]) {
				$('.address-auto-suggest').show();
			} else {
				$('.address-auto-suggest').hide();
			}
		});
    

      function initMap() {
        var map = new google.maps.Map(document.getElementById('map'), {
          center: {lat: -33.8688, lng: 151.2195},
          zoom: 13
        });
        var input = document.getElementById('pac-input');
        var strictBounds = document.getElementById('strict-bounds-selector');
        var autocomplete = new google.maps.places.Autocomplete(input);

        // Bind the map's bounds (viewport) property to the autocomplete object,
        // so that the autocomplete requests use the current map bounds for the
        // bounds option in the request.
        autocomplete.bindTo('bounds', map);

        var infowindow = new google.maps.InfoWindow();
        var infowindowContent = document.getElementById('infowindow-content');
        infowindow.setContent(infowindowContent);
        var marker = new google.maps.Marker({
          map: map,
          anchorPoint: new google.maps.Point(0, -29)
        });

        autocomplete.addListener('place_changed', function() {
          infowindow.close();
          marker.setVisible(false);
          var place = autocomplete.getPlace();
          if (!place.geometry) {
            // User entered the name of a Place that was not suggested and
            // pressed the Enter key, or the Place Details request failed.
            window.alert("No details available for input: '" + place.name + "'");
            return;
          }
          $('#lat').val( place.geometry.location.lat() );
          $('#lng').val( place.geometry.location.lng() );

          // If the place has a geometry, then present it on a map.
          if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
          } else {
            map.setCenter(place.geometry.location);
            map.setZoom(17);  // Why 17? Because it looks good.
          }
          marker.setPosition(place.geometry.location);
          marker.setVisible(true);

          var address = '';
          if (place.address_components) {
            address = [
              (place.address_components[0] && place.address_components[0].short_name || ''),
              (place.address_components[1] && place.address_components[1].short_name || ''),
              (place.address_components[2] && place.address_components[2].short_name || '')
            ].join(' ');
          }

          infowindowContent.children['place-icon'].src = place.icon;
          infowindowContent.children['place-name'].textContent = place.name;
          infowindowContent.children['place-address'].textContent = address;
          $('#place_icon').val( place.icon );
          $('#place_name').val( place.name );
          $('#place_address').val( address );
          infowindow.open(map, marker);
        });

      }
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDQWA29c0wqxsQpQJiEyJ9NZZL6YRhLihc&libraries=places&callback=initMap"
        async defer></script>
<?php
$this->load->view("admin/create-incident");
$this->load->view("admin/edit-incident");
?>
</body>
</html>