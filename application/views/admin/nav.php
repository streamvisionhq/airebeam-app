<?php
if ( !isset($userName) ) {
      $userName = FALSE;
}
?>
<?php $class = $this->router->class; ?>
<div id="navigation" <?php echo $showNav; ?>>
    <div class="container-fluid">
        <a href="<?php echo ADMIN_BASE_PATH . "calendar" ?>" id="brand">AireBeam</a>
        <ul class='main-nav'>
            <?php
            if ( defined('CALENDAR') === true ) {
                  if ( !empty($privilages) && in_array(CALENDAR, (array) $privilages) ) {
                        ?>
                        <li <?php echo ($class == "calendar") ? "class='active'" : '' ?>>
                            <a href="<?php echo ADMIN_BASE_PATH . "calendar" ?>">
                                <span>Schedule</span>
                            </a>
                        </li>
                        <?php
                  }
            }
            if ( defined('CALENDAR') === true ) {
                  if ( !empty($privilages) && in_array(CALENDAR, (array) $privilages) ) {
                        ?>
                        <li>
                            <a href="#modal-1" class="modal-open" data-backdrop="static" data-keyboard="false" role="button" data-toggle="modal">
                                <span>Create Appointment</span>
                            </a>
                        </li>
                        <?php
                  }
            }
            if ( defined('SEARCH_APPOINTMENT') === true ) {
                  if ( !empty($privilages) && in_array(SEARCH_APPOINTMENT, (array) $privilages) ) {
                        ?>
                        <li <?php echo ($class == "search") ? "class='active'" : '' ?>>
                            <a href="<?php echo ADMIN_BASE_PATH . "search" ?>">
                                <span>Search</span>
                            </a>
                        </li>
                        <?php
                  }
            }
            if ( defined('NOTES') === true ) {
                  if ( !empty($privilages) && in_array(NOTES, (array) $privilages) ) {
                        ?>
                        <li <?php echo ($class == "notes") ? "class='active'" : '' ?>>
                            <a href="<?php echo ADMIN_BASE_PATH . "notes/all" ?>">
                                <span>Notes <p class="notification" id="new-notes-number"><?php echo NEW_NOTES; ?></p></span>
                            </a>
                        </li>
                        <?php
                  }
            }
            if ( defined('SURVEY') === true ) {
                  if ( !empty($privilages) && in_array(CALENDAR, (array) $privilages) ) {
                        ?>
                        <li <?php echo ($class == "surveys") ? "class='active'" : '' ?>>
                            <a href="<?php echo ADMIN_BASE_PATH . "surveys" ?>">
                                <span>Pending Reviews <p class="notification" id="pending-survey-number"><?php echo QUEUED_SURVEYS; ?></p></span>
                            </a>
                        </li>
                        <?php
                  }
            }
            if ( defined('REPORTS') === true ) {
                  if ( !empty($privilages) && in_array(REPORTS, (array) $privilages) ) {
                        ?>
                        <li <?php echo ($class == "reports") ? "class='active'" : '' ?>>
                            <a href="<?php echo ADMIN_BASE_PATH . "reports" ?>">
                                <span>Reports</span>
                            </a>
                        </li>
                        <?php
                  }
            }
            if ( defined('SMS_FAILURE_LOG') === true ) {
                if ( !empty($privilages) && in_array(SMS_FAILURE_LOG, (array) $privilages) ) {
                    ?>
                    <li <?php echo ($class == "sms_failure_log") ? "class='active'" : '' ?>>
                        <a href="<?php echo ADMIN_BASE_PATH . "sms_failure_log/all" ?>">
                            <span>SMS Failure Log <p class="notification" id="new-failure-sms-number"><?php echo NEW_FAILURE_SMS; ?></p></span>
                        </a>
                    </li>
                    <?php
                }
            }
            ?>
        </ul>
        <div class="user">
            <div class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo $userName; ?></a>
                <ul class="dropdown-menu pull-right">
                    <?php
                    if ( defined('USER_MANAGEMENT') === true ) {
                          if ( !empty($privilages) && in_array(USER_MANAGEMENT, (array) $privilages) ) {
                                ?>
                                <li <?php echo ($class == "home") ? "class='active'" : '' ?>>
                                    <a href="<?php echo ADMIN_BASE_PATH; ?>">
                                        <span>Worker List</span>
                                    </a>
                                </li>
                                <?php
                          }
                          if ( !empty($privilages) && in_array(USER_MANAGEMENT, (array) $privilages) ) {
                                ?>
                                <li <?php echo ($class == "add" && $this->router->method == "index") ? "class='active'" : '' ?>>
                                    <a href="<?php echo ADMIN_BASE_PATH . "add" ?>">
                                        <span>Add Worker</span>
                                    </a>
                                </li>
                                <?php
                          }
                          if ( !empty($privilages) && in_array(USER_MANAGEMENT, (array) $privilages) ) {
                          	?>
								<li
									<?php echo ($class == "zipCodes") ? "class='active'" : '' ?>>
									<a href="<?php echo URL_ZIPCODES_LIST ?>"> <span> Add/Manage Zip Codes </span>
								</a>
								</li>
								<?php
								}
						  if ( !empty($privilages) && in_array(USER_MANAGEMENT, (array) $privilages) ) {
							?>
								<li
									<?php echo ($class == "globalConfig") ? "class='active'" : '' ?>>
									<a href="<?php echo ADMIN_BASE_PATH . "globalConfig" ?>"> <span> Manage Global Settings </span>
								</a>
								</li>
								<?php
								}	
							
							}
																				if (defined ( 'SERVICE_MANAGEMENT' ) === true) {
																					if (! empty ( $privilages ) && in_array ( SERVICE_MANAGEMENT, ( array ) $privilages )) {
                                ?>
                                <li <?php echo ($class == "services") ? "class='active'" : '' ?>>
                                    <a href="<?php echo ADMIN_BASE_PATH; ?>services">
                                        <span>Service List</span>
                                    </a>
                                </li>
                                <?php
                          }
                          if ( !empty($privilages) && in_array(SERVICE_MANAGEMENT, (array) $privilages) ) {
                                ?>
                                <li <?php echo ($this->router->method == "service") ? "class='active'" : '' ?>>
                                    <a href="<?php echo ADMIN_BASE_PATH . "add/service" ?>">
                                        <span>Add Service</span>
                                    </a>
                                </li>
                                <?php
                          }
                    }
                    if ( defined('TOWNS') === true ) {
                          if ( !empty($privilages) && in_array(TOWNS, (array) $privilages) ) {
                                ?>
                                <li <?php echo ($class == "towns") ? "class='active'" : '' ?>>
                                    <a href="<?php echo ADMIN_BASE_PATH; ?>towns">
                                        <span>Towns List</span>
                                    </a>
                                </li>
                                <?php
                          }
                          if ( !empty($privilages) && in_array(TOWNS, (array) $privilages) ) {
                                ?>
                                <li <?php echo ($this->router->method == "town") ? "class='active'" : '' ?>>
                                    <a href="<?php echo ADMIN_BASE_PATH . "add/town" ?>">
                                        <span>Add Town</span>
                                    </a>
                                </li>
                                <?php
                          }
                    }
                    if ( defined('EDIT_MESSAGES') === true ) {
                          if ( !empty($privilages) && in_array(EDIT_MESSAGES, (array) $privilages) ) {
                                ?>
                                <li <?php echo ($this->router->method == "edit") ? "class='active'" : '' ?>>
                                    <a href="<?php echo ADMIN_BASE_PATH . "edit/messages" ?>">
                                        <span>Edit SMS Messages</span>
                                    </a>
                                </li>
                                <?php
                          }
                    }
                    if ( defined('TERMS_CONDITIONS') === true ) {
                          if ( !empty($privilages) && in_array(TERMS_CONDITIONS, (array) $privilages) ) {
                                ?>
                                <li <?php echo ($class == "terms-conditions") ? "class='active'" : '' ?>>
                                    <a href="<?php echo ADMIN_BASE_PATH . "termsConditions" ?>">
                                        <span>Terms & Conditions</span>
                                    </a>
                                </li>
                                <?php
                          }
                    }
                    ?>
                    <li <?php echo ($class == "changePassword") ? "class='active'" : '' ?>>
                        <a href="<?php echo ADMIN_BASE_PATH . "changePassword" ?>" class="signout-btn">Change Password</a>
                    </li>
                    <li>
                        <a href="<?php echo ADMIN_BASE_PATH . "logout" ?>" class="signout-btn">Sign out</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>