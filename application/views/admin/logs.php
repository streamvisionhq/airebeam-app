<?php
$this->load->view("admin/header");
$this->load->view("admin/show-message");
?>
<div class="wrapper">
    <div class="space"></div>
    <div class="container-fluid">
        <div class="space"></div>
        <div class="row-fluid">
            <div class="span12">
                <form method="GET" class='form-horizontal'>
                    <div class="control-group">
                        <label for="textfield" class="control-label">Search MBR</label>
                        <div class="controls">
                            <input autocomplete="off" name="customer" type="text" value="<?php
                            if ($this->input->get("customer")) {
                                echo $this->input->get("customer");
                            }
                            ?>" id="customer_search" class="input-xlarge" placeholder="MBR/Customer Name" data-rule-required="true" data-rule-minlength="1">                            
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Date From</label>
                        <div class="controls">
                            <input type="date" name="from" value="<?php echo $this->input->get("from"); ?>">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Date To</label>
                        <div class="controls">
                            <input type="date" name="to" value="<?php echo $this->input->get("to"); ?>">
                        </div>
                    </div>
                    <div class="control-group">
                        <button type="submit" class="btn btn-primary">Search</button>
                    </div>
                </form>
                <div class="box">
                    <div class="box-content nopadding">
                        <div class="search-results">
                            <ul>
                                <?php
                                if (!empty($logs)) {
                                    foreach ($logs as $log) {
                                        $content = json_decode(simple_decrypt($log["Content"], SALT_STRING));
                                        foreach ($content as $k => $con) {
                                            ?>
                                            <p style="display:inline-block; padding: 3px;"><?php echo "<strong>" . $k . "</strong>: " . $con; ?></p>
                                            <?php
                                        }
                                        ?>
                                        <p style="color: #0000ff;"><strong>Date:</strong> <?php echo date("M d Y h:i:s A", strtotime($log["Date"])); ?> | <strong>MBR:</strong> <?php echo $log["CustomerID"]; ?></p>
                                        <hr>
                                        <?php
                                    }
                                } else {
                                    ?>
                                    <p>No Logs Found</p>
                                    <?php
                                }
                                ?>
                            </ul>
                        </div>
                        <div class="highlight-toolbar bottom">
                            <div class="pull-right">
                                <div class="btn-toolbar">
                                    <div class="btn-group">
                                        <div class="pagination pagination-custom">
                                            <ul>
                                                <?php
                                                echo $pagination;
                                                ?>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
$this->load->view("admin/create-incident");
$this->load->view("admin/edit-incident");
?>
</body>
</html>