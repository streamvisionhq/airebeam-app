<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$this->data["Method"] = $this->router->method;
?>
<!doctype html>
<html>
    <?php $this->load->view("admin/head"); ?>
    <body <?php
    if ( $this->router->class == "login" ) {
          echo "class='login'";
          $this->data["showNav"] = "style='display:none;'";
    } else {
          echo "class=''";
          $this->data["showNav"] = "style='display:block;'";
    }
    ?>>
            <?php $this->load->view("admin/nav", $this->data); ?>
            <?php
            if ( (isset($role) ? $role : null) != INSITE ) {
                  ?>
              <div class="alertbox-notification">
                  <?php
                  if ( !empty($alerts) ) {
                        echo $alerts;
                  }
                  ?>
              </div>
              <?php
        }
        ?>
        <div id="modal-a3" data-id="" class="modal hide fade" tabindex="-1" role="dialog"  data-backdrop="static" data-keyboard="false" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-header">
                <h3 id="myModalLabel">Please Confirm</h3>
            </div>
            <div class="modal-body">
                <p>Are you sure this customer has been notified already?</p>
            </div>
            <div class="modal-footer">
                <button class="btn cancel-done" data-dismiss="modal" aria-hidden="true">Umm, Not sure...</button>
                <a><button data-dismiss="modal" aria-hidden="true" class="btn btn-primary submit_alert">Absolutely!</button></a>
            </div>
        </div>