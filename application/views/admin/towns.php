<?php
$this->load->view("admin/header");
?>
<?php
$this->load->view("admin/show-message");
?>
<?php
if (!empty($privilages)) {
    if (in_array(TOWNS, (array) $privilages)) {
        ?>
        <div class="wrapper">
            <div class="space"></div>
            <div class="container-fluid">
                <div class="row-fluid">
                    <div class="span12">
                        <div class="box box-color box-bordered">
                            <div class="box-title">
                                <h3> <i class="icon-table"></i> Towns List </h3>
                            </div>
                            <div class="box-content nopadding">
                                <table class="table table-hover table-nomargin dataTable table-bordered">
                                    <thead>
                                        <tr>
                                            <th width="40">ID</th>
                                            <th>Name</th>
                                            <th>Abbreviation</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        if ($towns) {
                                            $x = 1;
                                            foreach ($towns as $t) {
                                                ?>
                                                <tr>
                                                    <td><?php echo $x++; ?></td>
                                                    <td><?php echo $t->Name; ?></td>
                                                    <td><?php echo $t->Alias; ?></td>
                                                    <td><a href="<?php echo ADMIN_BASE_PATH . "edit/town/" . $t->TownID; ?>" class="btn btn-primary" title="Edit Town"><i class="icon-edit"></i></a>
                                                        <button class="btn btn-danger" href="#modal-3<?php echo $t->TownID; ?>" role="button" data-toggle="modal" title="Delete Town"><i class="icon-trash"></i></button>
                                                    </td>
                                                </tr>
                                                
                                                <!-- Delete POPUP Service -->
                                                <div id="modal-3<?php echo $t->TownID; ?>" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                        <h3 id="myModalLabel">Delete Town</h3>
                                                    </div>
                                                    <div class="modal-body">
                                                        <p>Are you sure you want to delete this Town?</p>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button class="btn" data-dismiss="modal" aria-hidden="true">No</button>
                                                        <a href="<?php echo ADMIN_BASE_PATH."delete/town/".$t->TownID; ?>"><button class="btn btn-primary">Yes</button></a>
                                                    </div>
                                                </div>
                                    
                                            <?php
                                        }
                                        ?>
                                        <?php
                                    }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php
    }
$this->load->view("admin/create-incident");
$this->load->view("admin/edit-incident");
}
?>
</body>
</html>