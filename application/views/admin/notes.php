<?php
$this->load->view("admin/header");
?>
<div class="note-container">
    <div class="container-fluid">
        <div class="space"></div>
        <div class="row-fluid">
            <div class="span6">
                <div class="box">
                    <div class="box-content nopadding">
                        <div class="search-results">
                            <ul>
                                <?php
                                if ($notes) {
                                    foreach ($notes as $note) {
                                        if($note["Label"] == "Note"){
                                            $class = "blue";
                                        }elseif($note["Label"] == "Done"){
                                            $class = "green";
                                        }else{
                                            $class = "red";
                                        }
                                        ?>
                                        <li>
                                            <div class="search-info notes-container">
                                                <a href="#modal-editIncident" data-eventid="<?php echo $note['OrderID']; ?>" class="note-editIncident notes_edit_popup modal-open" role="button" data-toggle="modal" data-backdrop="static" data-keyboard="false"><?php echo $note["Name"]; ?></a>
                                                <div class="pull-right worker-info">
                                                    <ul>
                                                        <li><?php echo getOrderDate($note["Date"]); ?></li>
                                                        <li><?php echo showTime($note["Date"]); ?></li>
                                                    </ul>
                                                    <p><?php echo $note["Worker"]; ?></p>
                                                    <p class="<?php echo $class; ?> status-text">(<?php echo $note["Label"]; ?>)</p>
                                                </div>
                                                <p class="pull-left worker-notes">
                                                    <?php echo $note["Note"]; ?>
                                                </p>
                                            </div>
                                        </li>
                                        <?php
                                    }
                                } else {
                                    ?>
                                    <p>No Notes Found</p>
                                    <?php
                                }
                                ?>
                            </ul>
                        </div>
                        <div class="highlight-toolbar bottom">
                            <div class="pull-right">
                                <div class="btn-toolbar">
                                    <div class="btn-group">
                                        <div class="pagination pagination-custom">
                                            <ul>
                                                <?php
                                                echo $pagination;
                                                ?>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
    $this->load->view("admin/create-incident");
    $this->load->view("admin/edit-incident");
    ?>
</div>
</body>
</html>