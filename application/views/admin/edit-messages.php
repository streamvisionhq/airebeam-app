<?php
$this->load->view("admin/header");
$this->load->view("admin/show-message");
?>

<div class="wrapper">
    <div class="space"></div>
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span6">
                <div class="box box-bordered box-color">
                    <div class="box-title">
                        <h3><i class="icon-edit"></i> Edit SMS Messages</h3>
                    </div>
                    <div class="box-content nopadding">
                        <?php
                        if ($messages) {
                            foreach ($messages as $message) {
                                if ($message->MessageTypeID == "023b67e15c2b5") {
                                    ?>
                                    <form method="POST" class='form-vertical form-bordered form-striped'>
                                        <div class="control-group">
                                            <label for="textarea" class="control-label">Create Appointment Message</label>
                                            <div class="controls">
                                                <textarea name="create-appointment" id="create-appointment" rows="5" class="input-block-level"><?php echo $message->Message; ?></textarea>
                                            </div>
                                            <button type="submit" class="btn btn-primary">Update</button>
                                        </div>
                                    </form>
                                    <?php
                                }
                                if ($message->MessageTypeID == "d6d08e3487b59") {
                                    ?>
                                    <form method="POST" class='form-vertical form-bordered form-striped'>
                                        <div class="control-group">
                                            <label for="textarea" class="control-label">Change Appointment Message</label>
                                            <div class="controls">
                                                <textarea name="change-appointment" id="change-appointment" rows="5" class="input-block-level"><?php echo $message->Message; ?></textarea>
                                            </div>
                                            <button type="submit" class="btn btn-primary">Update</button>
                                        </div>
                                    </form>
                                    <?php
                                }
                                if ($message->MessageTypeID == "9634e9d91933d") {
                                    ?>
                                    <form method="POST" class='form-vertical form-bordered form-striped'>
                                        <div class="control-group">
                                            <label for="textarea" class="control-label">On Enroute Appointment Message</label>
                                            <div class="controls">
                                                <textarea name="on-enoroute" id="on-enoroute" rows="5" class="input-block-level"><?php echo $message->Message; ?></textarea>
                                            </div>
                                            <button type="submit" class="btn btn-primary">Update</button>
                                        </div>
                                    </form>
                                    <?php
                                }
                                if ($message->MessageTypeID == "76d08e3487b57") {
                                    ?>
                                    <form method="POST" class='form-vertical form-bordered form-striped'>
                                        <div class="control-group">
                                            <label for="textarea" class="control-label">Customer replied NO Message</label>
                                            <div class="controls">
                                                <textarea name="replied-no" id="replied-no" rows="5" class="input-block-level"><?php echo $message->Message; ?></textarea>
                                            </div>
                                            <button type="submit" class="btn btn-primary">Update</button>
                                        </div>
                                    </form>
                                    <?php
                                }
                                if ($message->MessageTypeID == "96d0de348fb57") {
                                    ?>
                                    <form method="POST" class='form-vertical form-bordered form-striped'>
                                        <div class="control-group">
                                            <label for="textarea" class="control-label">Customer replied other then NO Message</label>
                                            <div class="controls">
                                                <textarea name="replied-other" id="replied-other" rows="5" class="input-block-level"><?php echo $message->Message; ?></textarea>
                                            </div>
                                            <button type="submit" class="btn btn-primary">Update</button>
                                        </div>
                                    </form>
                                    <?php
                                }
                                if ($message->MessageTypeID == "d6d08e3487d53") {
                                    ?>
                                    <form method="POST" class='form-vertical form-bordered form-striped'>
                                        <div class="control-group">
                                            <label for="textarea" class="control-label">Password Reset</label>
                                            <div class="controls">
                                                <textarea name="password-reset" id="password-reset" rows="5" class="input-block-level"><?php echo $message->Message; ?></textarea>
                                            </div>
                                            <button type="submit" class="btn btn-primary">Update</button>
                                        </div>
                                    </form>
                                    <?php
                                }
                            }
                        }
                        ?>
                    </div>
                </div>
            </div>
            <div class="span6">
                <div class="box box-color box-bordered">
                    <div class="box-title">
                        <h3>
                            <i class="icon-table"></i>
                            Shortcodes
                        </h3>
                    </div>
                    <div class="box-content nopadding">
                        <table class="table table-hover table-nomargin">
                            <thead>
                                <tr>
                                    <th>SNo</th>
                                    <th>Shortcode</th>
                                    <th>Description</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>1</td>
                                    <td>{TIME_STAMP}</td>
                                    <td>Adds date time in alert of appointment</td>
                                </tr>
                                <tr>
                                    <td>2</td>
                                    <td>{SERVICE_TYPED}</td>
                                    <td>Adds the service type in alert of appointmnet</td>
                                </tr>
                                <tr>
                                    <td>3</td>
                                    <td>{WORKER}</td>
                                    <td>Adds the worker name in alert of appointment</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
$this->load->view("admin/create-incident");
$this->load->view("admin/edit-incident");
?>
</body>
</html>