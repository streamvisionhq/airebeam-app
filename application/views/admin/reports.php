<?php
$this->load->view("admin/header");
?>
<?php
$this->load->view("admin/show-message");
?>
<?php
if (!empty($privilages)) {
    if (in_array(CALENDAR, (array) $privilages)) {
        ?>
        <div class="wrapper">
            <div class="space"></div>
            <div class="span12">
                <div class="box">
                    <div class="box-content">
                        <form method="post">
                            <div class="step" id="firstStep">
                                <div class="control-group">
                                    <div class="controls">
                                        <label class="control-label">From</label>
                                        <input type="date" name="from" value="<?php echo $this->input->post("from"); ?>" class="input-xlarge">
                                        <label class="control-label">To</label>
                                        <input type="date" name="to" value="<?php echo $this->input->post("to"); ?>" class="input-xlarge">
                                    </div>
                                </div>
                            </div>
                            <input type="submit" name="date_submit" class="btn btn-primary" value="Submit">
                        </form>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="space"></div>
            <?php
            $this->load->view("admin/reports-table", $this->data);
            ?>
        </div>
        <?php
    }
    $this->load->view("admin/create-incident");
    $this->load->view("admin/edit-incident");
}
?>
</body>
</html>