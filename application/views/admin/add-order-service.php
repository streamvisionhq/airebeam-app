<?php
$this->load->view("admin/header");
$this->load->view("admin/show-message");
?>

<div class="wrapper">
    <div class="space"></div>
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span12">
                <div class="box box-color box-bordered">
                    <div class="box-title">
                        <h3><i class="icon-user"></i> Add Service</h3>
                    </div>
                    <div class="box-content nopadding">
                        <form method="post" class='form-horizontal form-bordered form-validate' id="bb">
                            <div class="control-group">
                                <label for="service" class="control-label">Name</label>
                                <div class="controls">
                                    <input type="text" name="service" value="<?php echo $this->input->post("service"); ?>" class="input-xlarge" data-rule-required="true" data-rule-minlength="1">
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="abbr" class="control-label">Abbreviation</label>
                                <div class="controls">
                                    <input type="text" name="abbr" class="input-xlarge" data-rule-required="true">
                                </div>
                            </div>
                            <div class="form-actions">
                                <input type="submit" name="submit" value="Add Service" class="btn btn-primary">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
$this->load->view("admin/create-incident");
$this->load->view("admin/edit-incident");
?>
</body>
</html>