<?php
$this->load->view("admin/header");
$this->load->view("admin/show-message");
?>
<?php

function is_priority_exist_lookup_table($allMappings, $zipCodeId, $workerId, $priority) {
    $foundMapping = false;
    if ( is_array($allMappings) ) {
        foreach ( $allMappings as $mapping ) {
            if ( $mapping->ZipCodeId == $zipCodeId && $mapping->WorkerID == $workerId && $mapping->Priority == $priority ) {
                $foundMapping = true;
                break;
            }
        }
    }
    return $foundMapping;
}
?>
<div class="wrapper">
    <div class="space"></div>
    <div class="container-fluid">
        <div class="row-fluid">

            <div class="span12">
                <form method="post" id="set-priority"
                      action="<?php echo ADMIN_BASE_PATH . 'zipCodes/add'; ?>"
                      class='form-horizontal form-bordered form-validate' onsubmit="setFormSubmitting()">

                    <input type="hidden" id="made_changes" value="0" />
                    <div class="box box-color box-bordered">
                        <div class="box-title">
                            <h3>
                                <i class="icon-table"></i>Manage Priorities
                            </h3>
                        </div>
                        <div class="box-content nopadding">

                            <table
                                class="table table-hover table-nomargin dataTable table-bordered table-zip-codes">
                                <thead>
                                    <tr>
                                        <th width="100">Zip Codes</th>
                                        <!-- List all technician users -->
                                        <?php
                                        if ( $AllWorkers ) :
                                            foreach ( $AllWorkers as $Worker ) :
                                                ?>
                                                <th><?php echo $Worker->Name; ?></th>

                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    <?php
                                    if ( $AllZipCodes ) :
                                        foreach ( $AllZipCodes as $ZipCode ) :
                                            ?>
                                            <tr>

                                                <td>
                                                    <?php echo $ZipCode->Code; ?>

                                                    <input name="zipcode_id_<?php echo $ZipCode->Code; ?>" type="hidden"
                                                           value="<?php echo $ZipCode->ZipCodeId; ?>" />

                                                    <input id="zipcode-<?php echo $ZipCode->ZipCodeId; ?>" type="text" class="no-display"
                                                           value="<?php echo $ZipCode->Code; ?>" />
                                                </td>
                                                <?php
                                                foreach ( $AllWorkers as $Worker ) :
                                                    ?>
                                                    <td>
                                                        <select class="span6 technician-priority priority-select-<?php echo $ZipCode->ZipCodeId; ?>"
                                                                name="<?php echo 'zipcode-' . $ZipCode->ZipCodeId . '-priority-' . $Worker->WorkerID; ?>">
                                                                    <?php
                                                                    //$priorityCount = 1;
                                                                    ?>
                                                            <option value="0">Select Priority</option>
                                                            <?php foreach ( range(1, count($AllWorkers)) as $number ): ?>
                                                                <option value="<?php echo $number; ?>"
                                                                <?php $priority_check = is_priority_exist_lookup_table($AllWorkersMapping, $ZipCode->ZipCodeId, $Worker->WorkerID, $number); ?>
                                                                <?php echo $priority_check == true ? 'selected' : ''; ?>
                                                                        > 
                                                                    <?php echo getOrdinal($number) ?> </option>
                                                                <?php //$priorityCount++; ?>
                                                            <?php endforeach; ?>
                                                        </select>
                                                    </td>
                                                <?php endforeach; ?>






                                                <td>
                                                    <!-- button name="submit" value="Add Priority" type="submit"
                                                            class="btn btn-primary" title="Add Priority">
                                                            <i class="icon-ok"></i>
                                                    </button -->
                                                    <a  data-zip_id="<?php echo $ZipCode->ZipCodeId; ?>" class="btn btn-del-zip btn-danger" href="#delete-warning-modal" role="button"
                                                        data-toggle="modal" title="Delete Worker">
                                                        <i class="icon-trash"></i>
                                                    </a>
                                                </td>

                                            </tr>
                                        <?php endforeach; ?>
                                    <?php endif; ?>



                                </tbody>
                            </table>



                        </div>
                    </div>
                    <button type="button" value="Add Zip Code"
                            class="btn btn-primary btn-add-zip"> Add Zip Code </button>

                    <button value="Add Priority" <?php echo $AllZipCodes == false ? 'disabled' : '' ?> name="submit" type="button"
                            class="btn btn-primary set-priority-button" title="Save Priority">
                        Save Priority
                    </button>
                </form>
            </div>

            <div class="span12">
                <div class="box box-color box-bordered">





                    <div id="add-zip-modal" class="modal hide" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                        <form method="post"
                              class='form-horizontal form-bordered form-validate' id="bb" onsubmit="setFormSubmitting()">
                            <div class="modal-header">
                                <h3 id="myModalLabel">Add Zip Code</h3>
                            </div>
                            <div class="modal-body">
                                <input type="text" name="zip_code"
                                       value="<?php echo $this->input->post("zip_code"); ?>"
                                       id="zip_code" class="input-xlarge" data-rule-required="true"
                                       data-rule-minlength="1">

                            </div>
                            <div class="modal-footer">
                                <input type="submit" name="submit" value="Add Zip Code"
                                       class="btn btn-primary">
                            </div>
                        </form>
                    </div>


                    <div id="zip-modal" class="modal hide" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                        <div class="modal-header">
                            <h3 id="myModalLabel">Warning!</h3>
                        </div>
                        <div class="modal-body">
                            <p>Selected priority is already assigned to another technician. Do you want to assign selected priority to this agent? 
                                If you proceed then selected is removed from previously assigned agent.</p>
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-yes" data-dismiss="modal" aria-hidden="true">Yes</button>
                            <button class="btn btn-no btn-primary" data-dismiss="modal">No</button>
                        </div>
                    </div>

                    <div id="delete-warning-modal" class="modal hide" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                        <div class="modal-header">
                            <h3 id="myModalLabel">Warning!</h3>
                        </div>
                        <div class="modal-body">
                            <p>Are you sure you want to delete this zip code?</p>
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-del-modal-yes" data-dismiss="modal" aria-hidden="true">Yes</button>
                            <button class="btn btn-del-modal-no btn-primary" data-dismiss="modal">No</button>
                        </div>
                    </div>




                </div>
            </div>
        </div>
    </div>
</div>
<?php
$this->load->view("admin/create-incident");
$this->load->view("admin/edit-incident");
?>
</body>
</html>

<script type="text/javascript">
    find_match_select_ref = null;
    select_clicked_ref = null;
    select_clicked_previous_val = null;
    $('.table-zip-codes select').on('focus', function () {
        // Store the current value on focus and on change
        select_clicked_ref = this;
        select_clicked_previous_val = this.value;
    }).change(function () {
        var selected_val = $(this).val();
        var selected_name_attr = $(this).attr('name');
        var $select_elems_ref = $(this).parent().parent().find('select');
        $('#made_changes').val(1);
        if (selected_val != 0) {
            for (var i = 0; i < $select_elems_ref.length; i++) {
                var elem_name_attr = $($select_elems_ref[i]).attr('name');
                if (selected_name_attr != elem_name_attr) {
                    var elem_value = $($select_elems_ref[i]).val();
                    if (elem_value == selected_val) {
                        find_match_select_ref = $select_elems_ref[i];
                        $('#zip-modal').modal('show');
                    }
                }

            }
        }
    });

    $('.btn.btn-yes').click(function () {
        $(find_match_select_ref).val(0);
    });

    $('.btn.btn-no').click(function () {
        $(select_clicked_ref).val(select_clicked_previous_val);
    });

    $('.btn-add-zip').click(function () {
        $('#add-zip-modal').modal('show');
    });

    selected_del_zip_id = null;
    $('.btn-del-zip').click(function () {
        selected_del_zip_id = $(this).data('zip_id');
    });

    $('#set-priority .set-priority-button').click(function () {
        var table = $('#set-priority table').dataTable();
        var data = table.$('input,select,textarea').serializeArray();
        data.push({
            name: 'submit',
            value: 'Add Priority'
        });
        $.ajax({
            type: "POST",
            url: '<?php echo ADMIN_BASE_PATH . 'zipCodes/add'; ?>',
            data: data,
            success: function (response) {
                $('#made_changes').val("");
                if (response == "completed") {
                    location.reload();
                }
            }
        });
    });

    $('.btn-del-modal-yes').click(function () {
        window.location = "<?php echo ADMIN_BASE_PATH . 'zipCodes/delete?zip_id='; ?>" + selected_del_zip_id;
    });

    var formSubmitting = false;
    var setFormSubmitting = function () {
        formSubmitting = true;
    };

    window.onbeforeunload = function (e) {
        if ($('#made_changes').val() == 0 || formSubmitting) {
            return undefined;
        }
        var message = 'It looks like you have been editing something. '
                + 'If you leave before saving, your changes will be lost.';
        var e = e || window.event;
        // For IE and Firefox
        if (e) {
            e.returnValue = message;
        }

        // For Safari
        return message;
    };
</script>
