<?php
$this->load->view("admin/header");
?>
<?php
$this->load->view("admin/show-message");
?>
<?php
if (!empty($privilages)) {
    if (in_array(USER_MANAGEMENT, (array) $privilages)) {
        ?>
        <div class="wrapper">
            <div class="space"></div>
            <div class="container-fluid">
                <div class="row-fluid">
                    <div class="span12">
                        <div class="box box-color box-bordered">
                            <div class="box-title">
                                <h3> <i class="icon-table"></i> Workers List </h3>
                            </div>
                            <div class="box-content nopadding">
                                <table class="table table-hover table-nomargin dataTable table-bordered">
                                    <thead>
                                        <tr>
                                            <th width="40">ID</th>
                                            <th>Full Name</th>
                                            <th>Email</th>
                                            <th>Role</th>
                                            <th>Key</th>
                                            <th>Sort Number</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        if ($AllWorkers) {
                                            $x = 1;
                                            foreach ($AllWorkers as $Worker) {
                                                ?>
                                                <tr>
                                                    <td><?php echo $x++; ?></td>
                                                    <td><?php echo $Worker->Name; ?></td>
                                                    <td><?php echo $Worker->Email; ?></td>
                                                    <td><?php echo $Worker->Role; ?></td>
                                                    <td><?php echo $Worker->APIKey; ?></td>
                                                    <td>
                                                        <?php
                                                        if($Worker->Role == "Technician"){
                                                            ?>
                                                        <a href="<?php echo $Worker->WorkerID; ?>" data-type="text" data-placement="top" data-placeholder="Required" data-original-title="Enter Sorting Order" data-worker-id="<?php echo $Worker->WorkerID; ?>" class="order-value"><?php echo $Worker->SortOrder; ?></a>
                                                        <?php
                                                        }else{
                                                            echo $Worker->Role;
                                                        }
                                                        ?>
                                                    </td>
                                                    <td><a href="<?php echo ADMIN_BASE_PATH . "edit/worker/" . $Worker->WorkerID; ?>" class="btn btn-primary" title="Edit Worker"><i class="icon-edit"></i></a>
                                                        <button class="btn btn-danger" href="#modal-3<?php echo $Worker->WorkerID; ?>" role="button" data-toggle="modal" title="Delete Worker"><i class="icon-trash"></i></button>
                                                    </td>
                                                </tr>
                                                
                                                <!-- Delete POPUP Worker -->
                                                <div id="modal-3<?php echo $Worker->WorkerID; ?>" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                        <h3 id="myModalLabel">Delete Worker</h3>
                                                    </div>
                                                    <div class="modal-body">
                                                        <p>Are you sure you want to delete this Worker?</p>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button class="btn" data-dismiss="modal" aria-hidden="true">No</button>
                                                        <a href="<?php echo ADMIN_BASE_PATH."delete/worker/".$Worker->WorkerID; ?>"><button class="btn btn-primary">Yes</button></a>
                                                    </div>
                                                </div>
                                    
                                            <?php
                                        }
                                        ?>
                                        <?php
                                    }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php
    }
$this->load->view("admin/create-incident");
$this->load->view("admin/edit-incident");
}
?>
</body>
</html>