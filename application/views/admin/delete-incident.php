<!-- Delete POPUP Worker -->
<div id="modal-3" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">Delete Appointment</h3>
    </div>
    <div class="modal-body">
        <p>Are you sure you want to delete this Appointment?</p>
    </div>
    <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">No</button>
        <a href="javascript:void(0);"><button id="deleteIncident" data-event="" class="btn btn-primary" data-dismiss="modal">Yes</button></a>
    </div>
</div>