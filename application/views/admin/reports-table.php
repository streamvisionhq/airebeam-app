<?php
if (!empty($filter_by_date)) {
    foreach ($filter_by_date as $key => $data) {
        ?>
        <div class="span12">
            <div class="box box-color box-bordered">
                <div class="box-title">
                    <h3>
                        <i class="icon-table"></i>
                        Filters By Date Range
                    </h3>
                </div>
                <div class="box-content nopadding">
                    <table class="table table-hover table-nomargin  table-bordered">
                        <thead>
                            <tr>
                                <th width="35">S No</th>
                                <th>Service Type</th>
                                <th>No of Appt</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $x = 1;
                            foreach ($data as $key => $val) {
                                ?>
                                <tr>
                                    <td><?php echo $x; ?></td>
                                    <td><?php echo $val["service"] ?></td>
                                    <td><?php echo $val["number"] ?></td>
                                </tr>
                                <?php
                                $x++;
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <?php
    }
} elseif ($week_orders_count) {
    foreach ($week_orders_count as $key => $ndata) {
        ?>
        <div class="row-fluid">
            <?php
            foreach ($ndata as $key => $data) {
                ?>
                <div class="span4">
                    <div class="box box-color box-bordered">
                        <div class="box-title">
                            <h3>
                                <i class="icon-table"></i>
                                <?php
                                if ($key == "last_week") {
                                    ?>
                                    Last Week
                                    <?php
                                } elseif ($key == "this_week") {
                                    ?>
                                    This Week
                                    <?php
                                } elseif ($key == "next_week") {
                                    ?>
                                    Next Week
                                    <?php
                                } elseif ($key == "last_month") {
                                    ?>
                                    Last Month
                                    <?php
                                } elseif ($key == "this_month") {
                                    ?>
                                    This Month
                                    <?php
                                } elseif ($key == "next_month") {
                                    ?>
                                    Next Month
                                    <?php
                                }
                                ?>
                            </h3>
                        </div>
                        <div class="box-content nopadding">
                            <table class="table table-hover table-nomargin  table-bordered">
                                <thead>
                                    <tr>
                                        <th width="35">S No</th>
                                        <th>Service Type</th>
                                        <th>No of Appt</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $x = 1;
                                    foreach ($data as $key => $val) {
                                        ?>
                                        <tr>
                                            <td><?php echo $x; ?></td>
                                            <td><?php echo $val["service"] ?></td>
                                            <td><?php echo $val["number"] ?></td>
                                        </tr>
                                        <?php
                                        $x++;
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <?php
            }
            ?>
        </div>
        <?php
    }
}
?>