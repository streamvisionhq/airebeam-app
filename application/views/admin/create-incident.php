<!------------- POPUP Incident ------------->
<div id="modal-1" class="modal fade hide" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="btn btn-remove pull-right" data-dismiss="modal" aria-hidden="true"><i class="icon-remove"></i></button>
        <h3 id="myModalLabel">Create Appointment</h3>
        <br>
        <div class="alert alert-error cs-general-error">
            <strong>Warning!</strong> <span id="email-error"></span>
        </div>
        <div class="alert alert-error cs-general-error">
            <strong>Warning!</strong> <span id="phone-error"></span>
        </div>
    </div>
    <form method="POST" class='form-validate' id="addIncidentForm">
        <div class="modal-body">
            <div id="error-php" class="help-block error"></div>
            <div class="controls control-group">
                <input autocomplete="off" type="text" id="customer_add" name="customer" class="input-block-level" placeholder="MBR/Customer Name" data-rule-required="true" data-rule-minlength="1">
                <input type="hidden" id="address-updated" name="address-updated" value="">
                <input type="hidden" id="phone-updated" name="phone-updated" value="">
                <input type="hidden" id="email-updated" name="email-updated" value="">
                <div class="loading_customer"><span>Checking updated info</span><img src="<?php echo ADMIN_ASSETS_PATH ?>img/loader.gif" width="15" /></div>
            </div>
            <div class="controls control-group">
                <textarea name="description" id="textarea" rows="5" data-rule-required="true" class="input-block-level" placeholder="Description"></textarea>
            </div>

            <div class="controls control-group">
                <select name="service" data-rule-required="true" id="service" class="input-block-level">
                    <option value="">Select Service Type</option>
                    <?php
                    foreach ( $services as $key => $service ) {
                        ?>
                        <option value="<?php echo $key; ?>"><?php echo $service; ?></option>
                        <?php
                    }
                    ?>
                </select>
            </div>
            <div class="controls control-group">
                <select data-rule-required="true" name="status" id="status" class="input-block-level">
                    <?php
                    foreach ( $statuses as $key => $status ) {
                        ?>
                        <option value="<?php echo $key; ?>">
                            <?php echo $status; ?>
                        </option>
                        <?php
                    }
                    ?>
                </select>
            </div>

            <div class="controls control-group">
                <input type="text" name="date" id="datepick" data-rule-required="true" placeholder="Select Date" class="input-block-level datepick" autocomplete="off">
                <i class="icon-popup icon-calendar"></i>
            </div>
            <div class="controls control-group popuphalf-width pull-left">
                <div class="bootstrap-timepicker">
                    <input type="text" data-rule-required="true" name="time" id="timepicker" class="input-block-level timepick" placeholder="Select Time" >
                    <i class="icon-popup icon-time"></i>
                </div>
            </div>

            <div class="controls control-group popuphalf-width pull-right">
                <select data-rule-required="true" name="duration" id="duration" class="input-block-level">
                    <?php
                    foreach ( $durations as $v ) {
                        $selected = NULL;
                        if ( $v == "60" ) {
                            $selected = " selected";
                        }
                        ?>
                        <option value="<?php echo $v; ?>" <?php echo $selected; ?>><?php echo $v; ?></option>
                        <?php
                    }
                    ?>
                </select>
            </div>
            <div class="clearfix"></div>
            <div class="controls control-group">
                <select data-rule-required="true" name="worker" id="assignworker" class="input-block-level">
                    <option value="">Select Technician</option>
                    <?php
                    foreach ( $workers as $key => $worker ) {
                        ?>
                        <option value="<?php echo $key; ?>"><?php echo $worker; ?></option>
                        <?php
                    }
                    ?>
                </select>
            </div>
            <div class="controls control-group c-add">
                <input type="text" name="address" id="caddress" class="input-block-level" placeholder="Customer Address" data-rule-required="true" data-rule-minlength="1" disabled>
            </div>
            <div class="controls control-group c-add">
                <input type="text" name="email" id="cemail" class="input-block-level" placeholder="Customer Email" data-rule-required="true" data-rule-minlength="1" disabled>
            </div>
            <div class="controls control-group c-add">
                <input type="text" name="phone" id="cphonenum" class="input-block-level" placeholder="Customer Phone No" data-rule-required="true" data-rule-minlength="1" disabled>
            </div>
        </div>
        <div class="modal-footer">
            <input type="submit" name="add-incident" class="btn btn-primary" value="Create Appointment">
            <button type="button" class="btn btn-remove" data-dismiss="modal" aria-hidden="true">Cancel</button>
        </div>
    </form>
</div>