<!-- Notifications -->
<div id="gritter-notice-wrapper" class="alert-warning" style="display: block;">
    <div class="alert alert-error">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>Warning!</strong>
        <?php
        if ($this->session->userdata("error")) {
            echo $this->session->userdata("error");
            $this->session->unset_userdata("error");
        } else {
            echo $error;
        }
        ?>
    </div>
</div>