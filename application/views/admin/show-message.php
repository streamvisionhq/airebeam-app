<?php
if ($this->session->userdata("error") || isset($error)) {
    $this->load->view("admin/error");
} elseif ($this->session->userdata("msg") || isset($msg)) {
    $this->load->view("admin/success");
}
?>