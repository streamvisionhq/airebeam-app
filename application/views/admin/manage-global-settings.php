<?php
$this->load->view("admin/header");
$this->load->view("admin/show-message");
?>

<div class="wrapper">
    <div class="space"></div>
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span12">
                <form method="post" class='form-horizontal form-bordered form-validate' id="frm-global-settings">
                    <!-- MANAGE GLOBAL SETTINGS STARTS -->
                    <div class="box box-color box-bordered">
                        <div class="box-title">
                            <h3><i class="icon-user"></i> Manage Global Settings</h3>
                        </div>
                        <div class="box-content nopadding">
                            <div class="control-group">
                                <label for="textfield" class="control-label">Minimum Commute Time </label>
                                <div class="controls">
                                    <select name="min_commute_time" id="min_commute_time" data-rule-required="true" class="addworker-select">
                                        <?php
                                        foreach (range(30, 300, 30) as $number) {
                                            ?>
                                            <option value="<?php echo $number; ?>" <?php echo $Commute_Time_Config == $number ? 'selected':''; ?> ><?php echo $number; ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>

                            <div class="control-group">
                                <label for="textfield" class="control-label"> Installation Time </label>
                                <div class="controls">
                                    <select name="size_of_installation" id="size_of_installation" data-rule-required="true" class="addworker-select">
                                        <?php
                                        foreach (range(30, 300, 30) as $number) {
                                            ?>
                                            <option value="<?php echo $number; ?>" <?php echo $Size_Installation_Config == $number ? 'selected':''; ?> > <?php echo $number; ?> </option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>

                            <div class="control-group">
                                <label for="textfield" class="control-label"> Sunset Time Mode </label>
                                <div class="controls">
                                    <select name="sunset_time_mode" id="sunset_time_mode" data-rule-required="true" class="addworker-select">
                                       <option value="<?php echo MANUAL_SUNSET_TIME; ?>" <?php echo $Sunset_Time_Mode_Config == MANUAL_SUNSET_TIME ? 'selected':''; ?> > Manual Sunset Time </option>
                                       <option value="<?php echo API_SUNSET_TIME; ?>" <?php echo $Sunset_Time_Mode_Config == API_SUNSET_TIME ? 'selected':''; ?> > API Sunset Time </option>
                                    </select>
                                </div>
                            </div>

                            <div class="control-group api_sunset <?php echo $Sunset_Time_Mode_Config == MANUAL_SUNSET_TIME ? 'no-display':''; ?>">
                                <label for="textfield" class="control-label"> API Sunset Time </label>
                                <div class="controls">
                                    <span> <?php echo $Api_Sunset_Time_Config ?>  </span>
                                </div>
                            </div>

                            <div class="control-group manual_sunset <?php echo $Sunset_Time_Mode_Config == API_SUNSET_TIME ? 'no-display':''; ?>">
                                <label for="textfield" class="control-label">Manual Sunset Time  </label>
                                <div class="controls">
                                    <select name="manual_sunset_time_hr" id="manual_sunset_time_hr" data-rule-required="true" class="addworker-select">
                                        <?php
                                        foreach (range(1, 12) as $number) {
                                            if($Sunset_Time_Hr_Config > 12){
                                                $selected_sunset_condition = (int)$Sunset_Time_Hr_Config - 12;
                                            }
                                            else{
                                                $selected_sunset_condition = (int)$Sunset_Time_Hr_Config;
                                            }
                                            ?>
                                            <option value="<?php echo $number; ?>" <?php echo $selected_sunset_condition == $number ? 'selected':''; ?> ><?php echo $number; ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                    <span>Hr</span>
                                    <select name="manual_sunset_time_mins" id="manual_sunset_time_mins" data-rule-required="true" class="addworker-select">
                                        <?php
                                        foreach (range(0, 55, 5) as $number) {
                                            ?>
                                            <option value="<?php echo prependZeroToUnits($number); ?>" <?php echo $Sunset_Time_Mins_Config == $number ? 'selected':''; ?> ><?php echo $number; ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                    <span>Min</span>
                                    <select name="manual_sunset_period" id="manual_sunset_period" data-rule-required="true" class="addworker-select">
                                        <option value="<?php echo ANTE_MERIDIEM ?>" <?php echo $Sunset_Time_Hr_Config <= 12 ? 'selected':''; ?> > am </option>
                                        <option value="<?php echo POST_MERIDIEM ?>" <?php echo $Sunset_Time_Hr_Config > 12 ? 'selected':''; ?> > pm </option>
                                    </select>
                                </div>
                            </div>

                            <div class="control-group">
                                <label for="textfield" class="control-label">Last Available Start Time  </label>
                                <div class="controls">
                                    <select name="last_avail_start_time" id="last_avail_start_time" data-rule-required="true" class="addworker-select">
                                        <?php
                                        foreach (range(30, 300, 30) as $number) {
                                            ?>
                                            <option value="<?php echo $number; ?>" <?php echo $Last_Avail_Start_Time_Config == $number ? 'selected':''; ?> ><?php echo $number; ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                    <span class="help-block"> The time at which technician can start their last installation before day end or sunset. </span>
                                </div>
                            </div>

                            <div class="control-group">
                                <label for="textfield" class="control-label">Max Number of Appointments/Day </label>
                                <div class="controls">
                                    <select name="max_appoints_per_day" id="max_appoints_per_day" data-rule-required="true" class="addworker-select">
                                        <?php
                                        foreach (range(1, 8) as $number) {
                                            ?>
                                            <option value="<?php echo $number; ?>" <?php echo $Max_Num_Appoints_Config == $number ? 'selected':''; ?> ><?php echo $number; ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>

                            <div class="row"><div class="span12" style="min-height: inherit;"><input id="dummy_tags" type="text" class="tagsinput" value=""></div></div>

                            <div class="control-group">
                                <label for="textfield" class="control-label">Drive Time Notify Email</label>
                                <div class="controls">
                                    <div class="span12"><input type="text" name="drive_notify_email" id="drive_notify_email" value="<?php echo !empty($Drive_Time_Email_Config) ? $Drive_Time_Email_Config:''  ?>" data-rule-required="true"></div>
                                </div>

                            </div>

                            <div class="control-group">
                                <label for="api_failed_notify_email" class="control-label">API Failed Notify Email</label>
                                <div class="controls">
                                    <div class="span12"><input type="text" name="api_failed_notify_email" id="api_failed_notify_email" value="<?php echo !empty($Api_Failed_Time_Email_Config) ? $Api_Failed_Time_Email_Config:''  ?>" data-rule-required="true"></div>
                                </div>
                            </div>

                            <div class="control-group">
                                <label for="textfield" class="control-label">Shift Start Time  </label>
                                <div class="controls">
                                    <select name="shift_start_time_hr" id="shift_start_time_hr" data-rule-required="true" class="addworker-select">
                                        <?php
                                        foreach (range(1, 12) as $number) {
                                            if($Shift_Start_Time_Hr_Config> 12){
                                                $selected_shift_start_condition = (int)$Shift_Start_Time_Hr_Config- 12;
                                            }
                                            else{
                                                $selected_shift_start_condition= (int)$Shift_Start_Time_Hr_Config;
                                            }
                                            ?>
                                            <option value="<?php echo $number; ?>" <?php echo $selected_shift_start_condition == $number ? 'selected':''; ?> ><?php echo $number; ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                    <span>Hr</span>
                                    <select name="shift_start_time_mins" id="shift_start_time_mins" data-rule-required="true" class="addworker-select">
                                        <?php
                                        foreach (range(0, 55, 5) as $number) {
                                            ?>
                                            <option value="<?php echo prependZeroToUnits($number); ?>" <?php echo $Shift_Start_Time_Mins_Config == $number ? 'selected':''; ?> ><?php echo $number; ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                    <span>Min</span>
                                    <select name="shift_start_period" id="shift_start_period" data-rule-required="true" class="addworker-select">
                                        <option value="<?php echo ANTE_MERIDIEM ?>" <?php echo $Shift_Start_Time_Hr_Config <= 12 ? 'selected':''; ?> > am </option>
                                        <option value="<?php echo POST_MERIDIEM ?>" <?php echo $Shift_Start_Time_Hr_Config > 12 ? 'selected':''; ?> > pm </option>
                                    </select>
                                </div>

                            </div>

                            <div class="control-group">
                                <label for="textfield" class="control-label">Max Shift End Time  </label>
                                <div class="controls">
                                    <select name="max_shift_end_time_hr" id="max_shift_end_time_hr" data-rule-required="true" class="addworker-select">
                                        <?php
                                        foreach (range(1, 12) as $number) {
                                            if($Max_Shift_End_Time_Hr_Config> 12){
                                                $selected_shift_end_condition = (int)$Max_Shift_End_Time_Hr_Config- 12;
                                            }
                                            else{
                                                $selected_shift_end_condition= (int)$Max_Shift_End_Time_Hr_Config;
                                            }
                                            ?>
                                            <option value="<?php echo $number; ?>" <?php echo $selected_shift_end_condition == $number ? 'selected':''; ?> ><?php echo $number; ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                    <span>Hr</span>
                                    <select name="max_shift_end_time_mins" id="max_shift_end_time_mins" data-rule-required="true" class="addworker-select">
                                        <?php
                                        foreach (range(0, 55, 5) as $number) {
                                            ?>
                                            <option value="<?php echo prependZeroToUnits($number); ?>" <?php echo $Max_Shift_End_Time_Mins_Config == $number ? 'selected':''; ?> ><?php echo $number; ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                    <span>Min</span>
                                    <select name="shift_end_period" id="shift_end_period" data-rule-required="true" class="addworker-select">
                                        <option value="<?php echo ANTE_MERIDIEM ?>" <?php echo $Max_Shift_End_Time_Hr_Config <= 12 ? 'selected':''; ?> > am </option>
                                        <option value="<?php echo POST_MERIDIEM ?>" <?php echo $Max_Shift_End_Time_Hr_Config > 12 ? 'selected':''; ?> > pm </option>
                                    </select>
                                    <div class="holiday-btn-container">
                                        
                                    </div>
                                    <!-- div class="holiday-container">
                                        <label for="holiday" class="control-label"> Holiday </label>
                                        <div class="controls">
                                            <input type="text" name="holiday_datepick" id="holiday_datepick" placeholder="Select Date" class="input-block-level datepick addworker-select" autocomplete="off">
                                            <i class="icon-popup icon-calendar"></i>

                                            <span> Label </span>
                                            <input type="text" id="holiday_label" name="holiday_label" class="addworker-select" />
                                        </div>
                                    </div-->
                                </div>

                            </div>
                            
                            <div class="form-actions">
                                <input type="submit" name="submit" value="Update" class="btn btn-primary">
                            </div>
                            
                           <div id="delete-warning-modal" class="modal hide" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                <div class="modal-header">
                                    <h3 id="myModalLabel">Warning!</h3>
                                </div>
                                <div class="modal-body">
                                    <p>Are you sure you want to delete <span></span> holiday?</p>
                                </div>
                                <div class="modal-footer">
                                    <button class="btn btn-del-modal-yes" data-dismiss="modal" aria-hidden="true">Yes</button>
                                    <button class="btn btn-del-modal-no btn-primary" data-dismiss="modal">No</button>
                                </div>
                            </div>

                        </div>
                    </div>
                    </form>
                    <!-- HOLIDAY LISTINGS TABLE STARTS -->
                    <div class="box box-color box-bordered tbl-holidays">
                        <div class="box-title">
                            <h3><i class="icon-user"></i> Holiday Listings</h3>
                        </div>
                        
                        <div class="box-content nopadding">
                        
                         <div class="control-group">
                            	<label for="holiday" class="control-label"> Holiday </label>
								<div class="controls">
									<input type="text" name="holiday_datepick"
										id="holiday_datepick" placeholder="Select Date"
										class="input-block-level datepick addworker-select"
										autocomplete="off"> <i class="icon-popup icon-calendar"></i> 
										
										<span> Label </span> 
										<input type="text" id="holiday_label"
										name="holiday_label" class="addworker-select" />
										
										<button type="button" class="btn-add-holiday btn btn-primary" > Add Holiday </button>
										<span class="holiday-success no-display" > Success! Holiday has been added </span>
										<span class="holiday-validation no-display" > Holiday and Label should not be empty. </span>
								</div>
							</div>
							
                            <table class="table table-hover table-nomargin table-bordered table-holidays">
                                <thead>
                                    <tr>
                                        <th width="100">Name</th>
                                        <th>Date</th>
                                        <th> Action </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        foreach ( $Holidays as $Holiday ) :
                                    ?>
                                        <tr>
                                            <td>
                                                <?php echo $Holiday->Name ?>
                                            </td>
                                            <td>
                                                <?php echo $Holiday->Date ?> </td>
                                            <td>
                                                <a data-holiday_date="<?php echo $Holiday->Date ?>" data-holiday_id="<?php echo $Holiday->Id ?>" href="#delete-warning-modal" class="btn btn-del-holiday btn-danger" role="button" data-toggle="modal" title="Delete Date">
                                                <i class="icon-trash"></i>
                                            </a>
                                            </td>
                                        </tr>
                                        <?php endforeach; ?>

                                </tbody>
                            </table>

                            <div id="delete-warning-modal" class="modal hide" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                <div class="modal-header">
                                    <h3 id="myModalLabel">Warning!</h3>
                                </div>
                                <div class="modal-body">
                                    <p>Are you sure you want to delete <span></span> holiday?</p>
                                </div>
                                <div class="modal-footer">
                                    <button class="btn btn-del-modal-yes" data-dismiss="modal" aria-hidden="true">Yes</button>
                                    <button class="btn btn-del-modal-no btn-primary" data-dismiss="modal">No</button>
                                </div>
                            </div>

                        </div>
                    </div>
                <!-- /form-->
            </div>
        </div>
    </div>
</div>
<?php
$this->load->view("admin/create-incident");
$this->load->view("admin/edit-incident");
?>
</body>

</html>

<script>
    function show_holiday() {
            $('.holiday-btn-container').hide();
            $('.holiday-container').show();
        }
    
        $('#api_failed_notify_email').tagsInput({
            'defaultText':'Add an Email',
            'width': 'auto',
            'height': 'auto'
        });
        $('#drive_notify_email').tagsInput({
            'defaultText':'Add an Email',
            'width': 'auto',
            'height': 'auto'
        });
        $('#sunset_time_mode').change(function(){
            var sunset_time_mode_val = $(this).val();
            if( sunset_time_mode_val == <?php echo MANUAL_SUNSET_TIME?> ) {
                $('.manual_sunset').show();
                $('.api_sunset').hide();
            }
            else if( sunset_time_mode_val == <?php echo API_SUNSET_TIME?> ) {
                $('.manual_sunset').hide();
                $('.api_sunset').show();
            }
        });
        
        selected_del_holiday_id = null;
        selected_holiday_date = null;
        $('.btn-del-holiday').click(function(){
            selected_del_holiday_id = $(this).data('holiday_id');
            selected_holiday_date = $(this).data('holiday_date');
            $('#delete-warning-modal p span').text( "\"" + selected_holiday_date + "\"" );
        });
    
        $('.btn-del-modal-yes').click(function(){
            window.location = "<?php echo ADMIN_BASE_PATH . 'globalConfig/delete_holiday?id='; ?>" + selected_del_holiday_id;
        });
    
        $( document ).ready(function() {
			var t = $('.table-holidays').dataTable();
        	$('.btn-add-holiday').on( 'click', function () {
        		$holiday_datepicker_ref = $('#holiday_datepick');
            	$holiday_label_ref = $('#holiday_label');
            	if($holiday_datepicker_ref.val().trim() != "" && $holiday_label_ref.val().trim() != "" ) {
	        		var data = {"date" : $holiday_datepicker_ref.val(), "label": $holiday_label_ref.val()};
	        		$.ajax({url: "<?php echo AJAX_BASE_PATH . 'holidays/add_holiday'; ?>", data:data, type: 'GET', success: function(result){
	        			result = JSON.parse(result)
	        			if(result.hasOwnProperty('Id')) {
	            			$('.holiday-success').show();
	        				$('.table-holidays').dataTable().fnAddData( [
	               	        	result['label'],
	               	        	result['date'],
	               	        	'<a data-holiday_date="' + result['date'] + '" data-holiday_id="' + result['Id'] + 
	               	        	'" href="#delete-warning-modal" class="btn btn-del-holiday btn-danger" role="button" data-toggle="modal" title="Delete Date">' +
	                            '<i class="icon-trash"></i>' +
	                            '</a>' ], false
	               	        	                                    );
	            			
		                    $holiday_datepicker_ref.val('');
		                    $holiday_label_ref.val('');
		                    setTimeout(function(){
		                    		window.location = '<?php echo ADMIN_BASE_PATH . "globalConfig"; ?>';
								}, 1000);
		                    
	        			}
	                }});        		
            	} else {
					$(".holiday-validation").show();
					setTimeout(function(){
						$(".holiday-validation").hide("normal");
						}, 2000);
					
                }
         
            } );
            
             $('#dummy_tags_tagsinput').addClass('no-display');
             $('#frm-global-settings').areYouSure();
        });
</script>