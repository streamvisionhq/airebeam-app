<div id="gritter-notice-wrapper" class="alert-contsuccess" style="display:block;">
    <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>Success!</strong>
        <?php
        if ($this->session->userdata("msg")) {
            echo $this->session->userdata("msg");
            $this->session->unset_userdata("msg");
        } else {
            echo $msg;
        }
        ?>
    </div>
</div>