<?php
$this->load->view("admin/header");
?>
<?php
$this->load->view("admin/show-message");
?>
<?php
if (!empty($privilages)) {
    if (in_array(CALENDAR, (array) $privilages)) {
        ?>
        <div class="wrapper">
            <div class="space"></div>
            <div class="container-fluid">
                <div class="row-fluid">
                    <div class="span12">
                        <div class="box box-color box-bordered">
                            <div class="box-title">
                                <h3> <i class="icon-table"></i> Pending Review List </h3>
                            </div>
                            <div class="box-content nopadding">
                                <form method="post" id="all-surveys">
                                    <table class="table table-hover table-nomargin dataTable table-bordered">
                                        <thead>
                                            <tr>
                                                <th width="40">ID</th>
                                                <th>Name</th>
                                                <th>Customer Email</th>
                                                <th>Action</th>
                                                <th><input type="checkbox" name="all-survey" /></th>
                                            </tr>
                                        </thead>
                                        <tbody class="survey-list">
                                            <?php
                                            if ($surveys) {
                                                $x = 1;
                                                foreach ($surveys as $survey) {
                                                    ?>
                                                    <tr>
                                                        <td><?php echo $x++; ?></td>
                                                        <td><?php echo $survey->Name; ?></td>
                                                        <td><?php echo $survey->Email; ?></td>
                                                        <td>
                                                            <a data-id="<?php echo (!empty($survey->OrderID)) ? $survey->OrderID : false; ?>" data-app="admin-app" class="btn btn-primary send-to-email" title="Send Survey"><img src="<?php echo base_url() . "assets/admin/img/icon-send-survey.png" ?>" /></a>
                                                        </td>
                                                        <td><input type="checkbox" name="single-survey[]" value="<?php echo (!empty($survey->OrderID)) ? $survey->OrderID : false; ?>" /></td>
                                                    </tr>
                                                    <?php
                                                }
                                                ?>
                                                <?php
                                            }
                                            ?>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                                <th><input type="submit" class="btn btn-primary" name="send-all" value="Send All" /></th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php
    }
    $this->load->view("admin/create-incident");
    $this->load->view("admin/edit-incident");
}
?>
</body>
</html>