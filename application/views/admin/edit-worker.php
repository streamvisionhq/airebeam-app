<?php
$this->load->view("admin/header");
$this->load->view("admin/show-message");
?>
<div class="wrapper">
    <div class="space"></div>
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span12">
                <div class="box box-color box-bordered">
                    <div class="box-title">
                        <h3><i class="icon-user"></i> Edit Worker</h3>
                    </div>
                    <div class="box-content nopadding">
                        <form method="POST" class='form-horizontal form-bordered form-validate' id="bb" enctype='multipart/form-data'>
                            <div class="control-group">
                                <label for="fullname" class="control-label">Full Name</label>
                                <div class="controls">
                                    <input type="text" name="name" id="fullname" value="<?php echo ($this->input->post()) ? $this->input->post("name") : $worker["Name"] ?>" class="input-xlarge" data-rule-required="true" data-rule-minlength="1">
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="textfield" class="control-label">Worker Role</label>
                                <div class="controls">
                                    <?php
                                    $js = 'id="bbb" data-rule-required="true" class="addworker-select"';
                                    echo form_dropdown('role', $roles, $worker["RoleID"], $js);
                                    ?>
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="api" class="control-label">API Key</label>
                                <div class="controls">
                                    <input type="text" name="api-key" id="api" value="<?php echo ($this->input->post()) ? $this->input->post("api-key") : $worker["APIKey"] ?>" class="input-xlarge" data-rule-required="false">
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="emailfield" class="control-label">Email Address</label>
                                <div class="controls">
                                    <input type="text" name="email" id="emailfield" value="<?php echo ($this->input->post()) ? $this->input->post("email") : $worker["Email"] ?>" class="input-xlarge" data-rule-email="true" data-rule-required="true">
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="pwfield" class="control-label">Password</label>
                                <div class="controls">
                                    <input type="password" name="password" id="pwfield" value="" class="input-xlarge" data-rule-minlength="6">
                                </div>
                            </div>
                            <?php $SCHEDULER_ROLES = config_item('SCHEDULER_ROLES');?>
                            <div class="control-group address-auto-suggest <?php echo $worker["RoleID"] != $SCHEDULER_ROLES["Technician"] ? 'no-display':'' ?>">
                                <label for="pac-input" class="control-label">Address</label>
                                <div id="pac-container" class="controls">
                                    <input placeholder="Enter a location" type="text" name="address" value="<?php echo ($this->input->post()) ? $this->input->post("address") : $worker["Address"] ?>" id="pac-input" class="input-xlarge" data-rule-required="true" data-rule-minlength="1">
                                    <input type="hidden" name="lat" id="lat" value="<?php echo ($this->input->post()) ? $this->input->post("Lat") : $worker["Lat"] ?>" />
                                    <input type="hidden" name="lng" id="lng" value="<?php echo ($this->input->post()) ? $this->input->post("Lng") : $worker["Lng"] ?>" />
                                    <input type="hidden" name="place_icon" id="place_icon" value="<?php echo ($this->input->post()) ? $this->input->post("PlaceIcon") : $worker["PlaceIcon"] ?>" />
                                    <input type="hidden" name="place_name" id="place_name" value="<?php echo ($this->input->post()) ? $this->input->post("PlaceName") : $worker["PlaceName"] ?>" />
                                    <input type="hidden" name="place_address" id="place_address" value="<?php echo ($this->input->post()) ? $this->input->post("PlaceAddress") : $worker["PlaceAddress"] ?>" />
                                </div>
                            </div>
                            
                            <div class="form-actions">
                                <input type="submit" name="submit" value="Save Changes" class="btn btn-primary"/>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="map" style="display:none; width:50%; height:50%"></div>
    <div style="display:none;" id="infowindow-content">
      <img src="" width="16" height="16" id="place-icon">
      <span id="place-name"  class="title"></span><br>
      <span id="place-address"></span>
    </div>
    <script>

	    $('[name="role"]').change(function() {
			var role_enum = {"Technician": "56a5c10c469f7"};
			var role_val = $(this).val();
			if( role_val == role_enum["Technician"]) {
				$('.address-auto-suggest').show();
			} else {
				$('.address-auto-suggest').hide();
			}
		});

    function setInfoContent( infowindow, infowindowContent, place_icon, place_name, place_address, map, marker){
    	infowindowContent.children['place-icon'].src = place_icon;
        infowindowContent.children['place-name'].textContent = place_name;
        infowindowContent.children['place-address'].textContent = place_address;
        infowindow.open(map, marker);
    }

      function initMap() {
        var myLatLng = {lat: <?php echo !empty($worker["Lat"]) && $worker["Lat"] != 0 ? $worker["Lat"]:-33.8688 ?>, 
                  		  lng: <?php echo !empty($worker["Lng"]) && $worker["Lng"] != 0 ? $worker["Lng"]:151.2195 ?>};
  		var place_icon = <?php echo !empty($worker['PlaceIcon']) == true ? "'". $worker['PlaceIcon'] . "'": "''"?>;
  		var place_name = <?php echo !empty($worker['PlaceName']) == true ? "'". $worker['PlaceName'] . "'": "''"?>;
  		var place_address = <?php echo !empty($worker['PlaceAddress']) == true ? "'".  $worker['PlaceAddress'] . "'": "''"?>;
        var map = new google.maps.Map(document.getElementById('map'), {
          center: myLatLng,
          zoom: 17
        });
        var input = document.getElementById('pac-input');
        var strictBounds = document.getElementById('strict-bounds-selector');
        var autocomplete = new google.maps.places.Autocomplete(input);
       

        // Bind the map's bounds (viewport) property to the autocomplete object,
        // so that the autocomplete requests use the current map bounds for the
        // bounds option in the request.
        autocomplete.bindTo('bounds', map);

        var infowindow = new google.maps.InfoWindow();
        var infowindowContent = document.getElementById('infowindow-content');
        
        infowindow.setContent(infowindowContent);
        var marker = new google.maps.Marker({
          position: myLatLng,
          map: map,
          anchorPoint: new google.maps.Point(0, -29)
        });

		if( place_icon != '' && place_name != '' && place_address != '') {
        	setInfoContent( infowindow, infowindowContent, place_icon, place_name, place_address, map, marker );
		}

        autocomplete.addListener('place_changed', function() {
          infowindow.close();
          marker.setVisible(false);
          var place = autocomplete.getPlace();
          console.log(place.geometry.location.lat());
          console.log(place.geometry.location.lng());
          if (!place.geometry) {
            // User entered the name of a Place that was not suggested and
            // pressed the Enter key, or the Place Details request failed.
            window.alert("No details available for input: '" + place.name + "'");
            return;
          }

          $('#lat').val( place.geometry.location.lat() );
          $('#lng').val( place.geometry.location.lng() );
          // If the place has a geometry, then present it on a map.
          if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
          } else {
            map.setCenter(place.geometry.location);
            map.setZoom(17);  // Why 17? Because it looks good.
          }
          marker.setPosition(place.geometry.location);
          marker.setVisible(true);

          var address = '';
          if (place.address_components) {
            address = [
              (place.address_components[0] && place.address_components[0].short_name || ''),
              (place.address_components[1] && place.address_components[1].short_name || ''),
              (place.address_components[2] && place.address_components[2].short_name || '')
            ].join(' ');
          }

          infowindowContent.children['place-icon'].src = place.icon;
          infowindowContent.children['place-name'].textContent = place.name;
          infowindowContent.children['place-address'].textContent = address;
          $('#place_icon').val( place.icon );
          $('#place_name').val( place.name );
          $('#place_address').val( address );
          infowindow.open(map, marker);
        });

        

      }
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAs8rFC1q8ZsLFR2RWOiaS6i9hCCRZdu3Q&libraries=places&callback=initMap"
        async defer></script>

<?php
$this->load->view("admin/create-incident");
$this->load->view("admin/edit-incident");
?>
</body>
</html>