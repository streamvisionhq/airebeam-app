<?php
if (!empty($activities)) {
    foreach ($activities as $meta) {
        if ($meta["Name"] == "force_password_updated") {
            ?>
            <li>
                <div class="activity-title"><b>Password updated by customer</b> <span><?php echo $meta["Value"]; ?></span> <?php echo defaultDateFormat($meta["Date"]); ?></div>
            </li>
            <?php
        } else {
            ?>
            <li>
                <div class="activity-title"><b>Forced reset password by</b> <span><?php echo $meta["Worker"]; ?></span> <?php echo defaultDateFormat($meta["Date"]); ?></div>
            </li>
            <?php
        }
    }
} else {
    ?>
    <li>
        <div class="activity-title">No Activity Log Found</div>
    </li>
    <?php
}
?>