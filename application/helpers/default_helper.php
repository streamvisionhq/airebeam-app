<?php

/**
 * @author Faraz Ahmed
 * @version 1.1
 */
//URL

define('BASE_PATH', base_url());
define('ADMIN_BASE_PATH', base_url() . "app/");
define('ADMIN_ASSETS_PATH', base_url() . "assets/admin/");
define('ASSETS_PATH', base_url() . "assets/");
define('ADMINISTRATOR', "Admin");
define('CSR', "CSR");
define('WORKER', "Technician");
define('INSITE', "Read Only Tech");
define('NEW_NOTES', "<script type='text/javascript'>if(localStorage.newNotes != undefined){document.write(localStorage.newNotes)}</script>");
define('QUEUED_SURVEYS', "<script type='text/javascript'>if(localStorage.queuedSurveys != undefined){document.write(localStorage.queuedSurveys)}</script>");
define('CLEAR_NEW_NOTES', "<script type='text/javascript'>window.localStorage.removeItem('newNotes');</script>");
define('CLEAR_PENDING_SURVEYS', "<script type='text/javascript'>window.localStorage.removeItem('queuedSurveys');</script>");
define('NEW_FAILURE_SMS', "<script type='text/javascript'>if(localStorage.newFailureSms != undefined){document.write(localStorage.newFailureSms)}</script>");
define('CLEAR_NEW_FAILURE_SMS', "<script type='text/javascript'>window.localStorage.removeItem('newFailureSms');</script>");
define('URL_ZIPCODES_LIST', ADMIN_BASE_PATH . "zipCodes");
define('URL_GLOBAL_CONFIG', ADMIN_BASE_PATH . "globalConfig");
define('URL_LOGIN', ADMIN_BASE_PATH . "login");
define('AJAX_BASE_PATH', base_url() . "ajax/");

define('COUNTRYCODE', '+1');

if (ENVIRONMENT == "production") {
    define('FROM', '+15202519120'); //production
    define('TWILIO_SMS_CALLBACK', "https://dispatch1.airebeam.net/ajax/twilioBase/recieveCallback"); //production
} elseif (ENVIRONMENT == "staging") {
    define('FROM', '+15202519120'); //staging
    define('TWILIO_SMS_CALLBACK', "https://staging-dispatch.airebeam.net/ajax/twilioBase/recieveCallback");  //staging
} else {
    define('FROM', '+15202519120'); //dev,localhost
    define('TWILIO_SMS_CALLBACK', "https://dev-dispatch.airebeam.net/ajax/twilioBase/recieveCallback");  //dev,localhost
}

if (ENVIRONMENT != "production") {
    define('GEOCODE_API', "AIzaSyDO6MKElrS8n-P6Rs3jqrH_Iyp635mQKV0"); //staging
} else {
    define('GEOCODE_API', "AIzaSyCSJwTkjb0J-kheZ7ZXH88vL66FqOZMkfE"); //production
}


define('NEGATIVE_REMARK_EMAIL', 'negativesurveys@airebeam.net,oliviathomas002@gmail.com');
define('SALT_STRING', 'b725f09627a955f6cba77b852bdb9a3d');
define('CIPHER_STRING', 'AES-128-CTR');
define('ENCRYPTION_IV_STRING', '1234567891011121');

/**
 * append a page title to the main site title
 * and set a global config variable meta_tile
 */
function add_meta_title($string) {
    $CI = & get_instance();
    $CI->data['meta_title'] = e($string) . ' - ' . $CI->data['meta_title'];
}

function clean_string($string) {
    return preg_replace('/[^0-9]/', '', $string); // Removes special chars.
}

/**
 * convert html tags to html entities
 * @param string $string
 * @return string
 */
function e($string) {
    return htmlentities($string);
}

/**
 * show an array in pre formated formate
 * this function is only for developmenet purpose only
 * @param array $Array
 */
function p($Array) {
    echo "<pre>";
    print_r($Array);
    echo "</pre>";
}

/**
 * convert an object to string
 * @param xml object $obj
 * @return string
 */
function simplize($obj) {
    return substr($obj, 0);
}

function checkIsZero($string) {
    return ($string === "0") ? false : $string;
}

function defaultDateFormat($date) {
    $date = strtotime($date);
    return date("m/d/Y h:i:s A", $date);
}

function checkInteger($string) {
    if (preg_match('/^\d+$/', $string)) {
        return TRUE;
    } else {
        return FALSE;
    }
}

/**
 * convert time to 24 hour format
 * @param string $time
 * @return string
 */
function formatTime($time) {
    $time = date("H:i:s", strtotime($time));
    return $time;
}

/**
 * convert a date to desired format
 * @param string $date
 * @return string
 */
function formatDate($date) {
    $date = date("Y-m-d", strtotime($date));
    return $date;
}

/**
 * convert a time to 12 hour format
 * @param string $time
 * @return string
 */
function showTime($time) {
    $time = date("h:i A", strtotime($time));
    return $time;
}

/**
 * convert a time to 12 hour format
 * @param string $time
 * @return string
 */
function getAMPM($date) {
    return date("A", strtotime($date));
}

/**
 * convert a date to desired format
 * @param string $date
 * @return string
 */
function getOrderDate($date) {
    $date = date("m/d/Y", strtotime($date));
    return $date;
}

function simplizePhone($string) {
    return preg_replace("/[^0-9,.+()-]/", "", $string);
}

/**
 * Dump helper. Functions to dump variables to
 * the screen, in a nicley formatted manner.
 */
if (!function_exists('dump')) {

    function dump($var, $label = 'Dump', $echo = TRUE) {
        // Store dump in variable 
        ob_start();
        var_dump($var);
        $output = ob_get_clean();

        // Add formatting
        $output = preg_replace("/\]\=\>\n(\s+)/m", "] => ", $output);
        $output = '<pre style="background: #FFFEEF; color: #000; border: 1px dotted #000; padding: 10px; margin: 10px 0; text-align: left;">' . $label . ' => ' . $output . '</pre>';

        // Output
        if ($echo == TRUE) {
            echo $output;
        } else {
            return $output;
        }
    }

}

/**
 * convert Date Formate 2016-01-18 05:00:00 to Time 05:00 AM OR PM
 * @return string
 */
function dateToTime($date) {
    $date = strtotime($date);
    $date = date("H:i A", $date);
    return $date;
}

function strictUniqueID() {
    return substr(md5(uniqid(rand(), true)), 0, 13);
}

function logRepsonse($String) {
    $file = fopen('assets/logs.txt', 'a');
    fwrite($file, $String . " " . date("y-m-d h:i:s A") . "\r\n");
    fclose($file);
}

function isBase64Encoded($str) {
    try {
        $decoded = base64_decode($str, true);

        if (base64_encode($decoded) === $str) {
            return true;
        } else {
            return false;
        }
    } catch (Exception $e) {
        // If exception is caught, then it is not a base64 encoded string
        return false;
    }
}

function simple_encrypt($text) {
    // Use OpenSSl Encryption method 
    $iv_length = openssl_cipher_iv_length(CIPHER_STRING);

    // Use openssl_encrypt() function to encrypt the data 
    return openssl_encrypt($text, CIPHER_STRING, SALT_STRING, 0, ENCRYPTION_IV_STRING);
}

function simple_decrypt($text) {
    // Use openssl_decrypt() function to decrypt the data 
    return openssl_decrypt($text, CIPHER_STRING, SALT_STRING, 0, ENCRYPTION_IV_STRING);
}

function sendEmail($email, $subject, $body, $from) {
    $headers = "From: " . $from . "\r\n";
    $headers .= "MIME-Version: 1.0\r\n";
    $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
    if (mail($email, $subject, $body, $headers)) {
        return true;
    }
    return false;
}

function getOrdinal($number) {
    $ends = array('th', 'st', 'nd', 'rd', 'th', 'th', 'th', 'th', 'th', 'th');
    if ((($number % 100) >= 11) && (($number % 100) <= 13))
        return $number . 'th';
    else
        return $number . $ends[$number % 10];
}

function convertToArizonaDateTime($datetime, $timezone, $dateTimeFormatStr = NULL) {
    $arizona_time = new DateTimeZone($timezone);
    $datetime->setTimezone($arizona_time);
    if (!is_null($dateTimeFormatStr)) {
        return $datetime->format($dateTimeFormatStr);
    } else {
        return $datetime->format('Y-m-d H:i:s');
    }
}

function prependZeroToUnits($number) {
    return ($number / 10) < 1 ? "0" . $number : $number;
}

function getDayAddIntervalString($numberofday) {
    return '+' . $numberofday . ' day';
}

function get24HrDateTime($date_str, $hr_string, $mins_string = NULL) {
    if (!is_null($mins_string)) {
        return $date_str . " " . $hr_string . ":" . $mins_string . ":00";
    } else {
        return $date_str . " " . $hr_string;
    }
}

function getDaysDiff($start_date_str, $end_date_str) {
    $start_date_obj = new DateTime($start_date_str);
    $end_date_obj = new DateTime($end_date_str);
    $diff_date = (int) date_diff($start_date_obj, $end_date_obj)->format('%R%a');
    return $diff_date;
}

function xml_to_json($xmlstring) {
    $xml = simplexml_load_string($xmlstring, "SimpleXMLElement", LIBXML_NOCDATA);
    $json = json_encode($xml);
    $array = json_decode($json, TRUE);
    return $array;
}

function weekOfMonth($date) {
    $firstOfMonth = strtotime(date("Y-m-01", $date));
    return intval(date("W", $date)) - intval(date("W", $firstOfMonth)) + 1;
}
