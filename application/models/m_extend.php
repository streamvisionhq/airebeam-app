<?php

/**
 * this class has methods that are related to admin
 * like change password,all workers detail,add edit worker,admin & CSR login
 * customer detail,all colors list for worker, assign a color to worker
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class M_extend extends MY_Model {

    public function __construct() {
        parent::__construct();
    }

}
