<?php
/**
 * this class has methods that are related to workers
 * like login
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class M_worker extends MY_Model {
    
    public function __construct() {
        parent::__construct();
    }

    /**
     * @note get login form values and match in database
     * if creadentials match then redirect to orders listing page
     * if not then show error
     * @return array
     */
    public function doLogin() {
        if ($this->input->post("submit")) {

            $email = $this->input->post("email");
            $password = $this->input->post("password");
            $data = $this->checkExistWorker($email);
            if ($data) {
                if (password_verify($password, $data->Password)) {
                    if ($data->RoleID) {
                        $role = $this->getRole($data->WorkerID);
                        $roleName = $this->getRoleName($role);
                        if($roleName == WORKER) {
                            $items = array(
                                'WorkerID' => $data->WorkerID,
                                "WorkerRoleID" => $role,
                            );
                            $this->session->set_userdata($items);
                        }else{
                            return array("response" => FALSE, "Msg" => ERROR);
                        }
                    }
                    return array("response" => TRUE, "Msg" => SUCCSESS);
                } else {
                    return array("response" => FALSE, "Msg" => ERROR);
                }
            } else {
                return array("response" => FALSE, "Msg" => ERROR);
            }
        }
    }

}
