<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_mergable_slot extends MY_Model{
	public $mergable_slot;
	private $start_time;
	private $end_time;
	private $mergable;
	
	public function __construct() {
		parent::__construct();
	}
	
	public function make_object($start_time, $end_time, $mergable) {
		$this->start_time = $start_time;
		$this->end_time = $end_time;
		$this->mergable = $mergable;
		$this->mergable_slot = array(
									"start_time" => $this->start_time, 
									"end_time" => $this->end_time, 
									"mergable" => $this->mergable
								);
		$this->mergable_slot = (object)$this->mergable_slot;
	}
		
}