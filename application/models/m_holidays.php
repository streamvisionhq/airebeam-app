<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_holidays extends MY_Model {
	public function __construct() {
		parent::__construct();
	}
	
	public function InsertHolidays($holidays) {
		$result = $this->db->insert_batch('holidays', $holidays);
		return $result;
	}
	
	public function InsertHoliday( $name, $date ) {
		$data = array(
				'Name' => $name,
				'Date' => $date
		);
		
		$result = $this->db->insert('holidays', $data);
		return $result;
	}
	/*
	 * Get upcoming holidays
	 * @param date
	 */
	public function GetHolidays( $date ) {
		$this->db->where('Date >=', $date);
		$result = $this->db->get('holidays')->result();
		return $result;
	}
	
	public function RemoveHoliday( $id=null ) {
		$result = false;
		if( !is_null($id) ) {
			$result = $this->db->delete('holidays', array('Id' => $id));
		}
		return $result;
	}
	
	public function LastHoliday() {
		return $this->db->select_max('Id')->get('holidays')->result();
	}
	
	/**
	 * @param holidays_data
	 */
	public function GetHolidayObjects($holidays_data) {
		$this->load->model ( "M_holiday" );
		$this->holiday_model = new M_holiday();
		$holidays_array = array();
		foreach ($holidays_data as $holiday) {
			$this->holiday_model->make_object($holiday->Name, $holiday->Date);
			array_push($holidays_array, $this->holiday_model->holiday);
		}
		return $holidays_array;
	}
}