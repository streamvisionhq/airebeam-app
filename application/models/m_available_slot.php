<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_available_slot extends MY_Model{
	public $available_slot;
	private $start_time;
	private $end_time;
	
	public function __construct() {
		parent::__construct();
	}
	
	public function make_object($start_time, $end_time) {
		$this->start_time = $start_time;
		$this->end_time = $end_time;
		$this->available_slot = array(START_TIME_KEY => $this->start_time, END_TIME_KEY => $this->end_time);
		$this->available_slot = (object)$this->available_slot;
	}
		
}