<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_google_directions extends MY_Model {
	public function __construct() {
		parent::__construct();
	}
	
	public function FetchDirections($params) {
		$response = false;
		if( isset($params['origin']) && isset($params['destination']) ){
			$this->load->model("M_curl");
// 			origin = 10135+W+Arvada+Dr,+Arizona+City,+AZ+85123	
// 			dest = '40772 W Maricopa-Casa Grande Hwy, Maricopa, AZ 85138, USA'
			$origin =  urlencode( $params['origin'] );
			$destination = urlencode( $params['destination'] );
			$this->curl = new M_curl();
			$url = 'https://maps.googleapis.com/maps/api/directions/json?origin=' . $origin .
			'&destination=' . $destination .
			'&mode=driving' .
			'&key=' . GOOGLE_MAPS_API_KEY .
			'&traffic_model=best_guess' .
			'&departure_time=' . $params['departure_time'];
			$response = $this->curl->curlExecute( $url );
			$response = json_decode($response, true);
		}
		if( $response != null && array_key_exists("routes", $response) ) {
			return $response;
		} else {
			return false;
		}
		 
	}
	

}