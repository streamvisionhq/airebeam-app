<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_available_slots extends MY_Model {

    public function __construct() {
        parent::__construct();
        $this->load->model("M_admin");
        $this->admin = new M_admin ();
        $this->load->model("M_appointment");
        $this->appointment_model = new M_appointment();
        $this->load->model("M_available_slot");
        $this->available_slot_model = new M_available_slot();
        $this->load->model("M_worker_appointments");
        $this->load->model("M_mergable_slot");
        $this->mergable_slot_model = new M_mergable_slot();
        $this->worker_appointments = new M_worker_appointments();
        $this->load->model("M_day_appointments");
        $this->day_appointments = new M_day_appointments();
        $this->load->model("M_sunset_time");
        $this->sunset_time = new M_sunset_time();
    }
    /*
     * Get Worker appointments from order table
     */

    public function WorkerAppointmentsByDate($start_date, $end_date, $worker_id) {
        $SCHEDULER_ROLES = config_item('SCHEDULER_ROLES');
        $sql = "SELECT
				`order`.`OrderID`,`order`.`WorkerID`,`order`.`CustomerID`,`order`.`StatusID`,
				`order`.`TypeID`, `order`.`Name`,`order`.`Detail`,`order`.`Duration`,
				`order`.`Date`, 
				customer.CustomerName,
                    customer.Longitude,customer.Latitude,
                    customer.Address1, customer.Address2 , customer.City , customer.State , customer.CityCode , customer.Zip 
				FROM `order`
				INNER JOIN customer ON `order`.CustomerID = customer.CustomerID
                INNER JOIN worker ON `order`.WorkerID = worker.WorkerID
				WHERE DATE(`order`.`Date`) >= DATE('" . $start_date . "') AND DATE(`order`.`Date`) <= DATE('" . $end_date . "')
				AND `order`.`StatusID` != '" . DELETE_AVL_SLOTS_STATUS . "' AND `order`.`WorkerID` ='" . $worker_id . "' " .
                "ORDER BY `order`.`Date`";
        $rows = $this->db->query($sql);
        $numberOfRecords = $rows->num_rows();
        $result = $rows->result();

        if ( $numberOfRecords > 0 ) {
            return $this->MakeWorkerAppointments($result);
        }
        else {
            return array();
        }
    }
    /*
     * Get Worker appointments from temp_order table
     */

    public function WorkerTempAppointmentsByDate($start_date, $end_date, $worker_id) {
        $SCHEDULER_ROLES = config_item('SCHEDULER_ROLES');
        $sql = "SELECT
				`temp_order`.`OrderID`,`temp_order`.`WorkerID`,`temp_order`.`CustomerID`,`temp_order`.`StatusID`,
				`temp_order`.`TypeID`, `temp_order`.`Name`,`temp_order`.`Detail`,`temp_order`.`Duration`,
				`temp_order`.`Date`,
				customer.CustomerName,
                customer.Longitude,customer.Latitude,
                customer.Address1, customer.Address2 , customer.City , customer.State , customer.CityCode , customer.Zip
				FROM `temp_order`
				INNER JOIN customer ON `temp_order`.CustomerID = customer.CustomerID
                INNER JOIN worker ON `temp_order`.WorkerID = worker.WorkerID
				WHERE DATE(`temp_order`.`Date`) >= DATE('" . $start_date . "') AND DATE(`temp_order`.`Date`) <= DATE('" . $end_date . "')
				AND `temp_order`.`StatusID` != '" . DELETE_AVL_SLOTS_STATUS . "' AND `temp_order`.`WorkerID` ='" . $worker_id . "' " .
                "AND `temp_order`.`IsProcessed` ='" . ORDER_NOT_PROCESSED . "' " .
                "ORDER BY `temp_order`.`Date`";
        $rows = $this->db->query($sql);
        $numberOfRecords = $rows->num_rows();
        $result = $rows->result();

        if ( $numberOfRecords > 0 ) {
            return $this->MakeWorkerAppointments($result);
        }
        else {
            return array();
        }
    }

    public function GetLastAppointment($appointments, $start_time) {
        $temp_appointments = null;
        foreach ( $appointments as $app_detail ) {
            if ( $app_detail->end_time < $start_time ) {
                $temp_appointments = $app_detail;
            }
        }
        return $temp_appointments;
    }

    /**
     * Convert worker appointment into objects
     * @param worker_appointments
     */
    private function MakeWorkerAppointments($worker_appointments) {
        $appointments = array();
        foreach ( $worker_appointments as $w_appointment ) {
            $start_time = new DateTime($w_appointment->Date);
            $end_time = clone $start_time;
            $minutes_to_add = $w_appointment->Duration;
            $end_time->add(new DateInterval('PT' . $minutes_to_add . 'M'));
            // Made location address
            $address = $w_appointment->Address1;
//         	if( !empty( $w_appointment->Address2 ) ) {
//         		$address .= $w_appointment->Address2;
//         	}
            $appointment_location = $address . " " . $w_appointment->City . ", " . $w_appointment->State . " " . $w_appointment->Zip;

            $this->appointment_model->make_object($start_time, $end_time, $appointment_location);
            $appointment_data = $this->appointment_model->appointment;
            array_push($appointments, $appointment_data);
        }
        return $appointments;
    }

    /**
     * Check if given create appointment request conflicts with any existing appointments
     * @param slot1
     * @param slot2
     */
    public function IsAppointmentConflicts($slot1, $slot2) {
        if ( ($slot2->start_time >= $slot1->start_time && $slot2->start_time <= $slot1->end_time) ||
                ($slot2->end_time >= $slot1->start_time && $slot2->end_time <= $slot1->end_time) ) {
            return true;
        }
        else {
            return false;
        }
    }

    public function AllAppointmentsDetailByZipCode($start_date, $end_date, $zip_code) {
        $SCHEDULER_ROLES = config_item('SCHEDULER_ROLES');
        $sql = "SELECT 
				`order`.`OrderID`,`order`.`WorkerID`,`order`.`CustomerID`,`order`.`StatusID`, 
				`order`.`TypeID`, `order`.`Name`,`order`.`Detail`,`order`.`Duration`, 
				`order`.`Date` FROM `order` 
				INNER JOIN zip_user_map ON `order`.WorkerID = `zip_user_map`.WorkerID 
				INNER JOIN zip_codes ON `zip_codes`.ZipCodeId = `zip_user_map`.ZipCodeId 
				WHERE DATE(`order`.`Date`) >= DATE('" . $start_date . "') AND DATE(`order`.`Date`) <= DATE('" . $end_date . "') 
						AND `order`.`StatusID` != '" . DELETE_AVL_SLOTS_STATUS . "' AND `zip_codes`.Code = " . $zip_code .
                " AND `zip_user_map`.Priority > 0 ORDER BY `order`.`Date`";
        $rows = $this->db->query($sql);
        $numberOfRecords = $rows->num_rows();
        $result = $rows->result();

        if ( $numberOfRecords > 0 ) {
            return $result;
        }
        else {
            return false;
        }
    }

    public function AllTempAppointmentsDetailByZipCode($start_date, $end_date, $zip_code) {
        $SCHEDULER_ROLES = config_item('SCHEDULER_ROLES');
        $sql = "SELECT
				`temp_order`.`OrderID`,`temp_order`.`WorkerID`,`temp_order`.`CustomerID`,`temp_order`.`StatusID`,
				`temp_order`.`TypeID`, `temp_order`.`Name`,`temp_order`.`Detail`,`temp_order`.`Duration`,
				`temp_order`.`Date` FROM `temp_order`
				INNER JOIN zip_user_map ON `temp_order`.WorkerID = `zip_user_map`.WorkerID
				INNER JOIN zip_codes ON `zip_codes`.ZipCodeId = `zip_user_map`.ZipCodeId
				WHERE DATE(`temp_order`.`Date`) >= DATE('" . $start_date . "') AND DATE(`temp_order`.`Date`) <= DATE('" . $end_date . "')
						AND `temp_order`.`StatusID` != '" . DELETE_AVL_SLOTS_STATUS . "' AND `zip_codes`.Code = " . $zip_code .
                " AND `zip_user_map`.Priority > 0 ORDER BY `temp_order`.`Date`";
        $rows = $this->db->query($sql);
        $numberOfRecords = $rows->num_rows();
        $result = $rows->result();

        if ( $numberOfRecords > 0 ) {
            return $result;
        }
        else {
            return false;
        }
    }

    /**
     * Get Single technician booked slots
     * @param appointments_deatil
     */
    private function GetTechnicianBookedSlots($appointments_deatil) {
        $booked_slots = array();
        foreach ( $appointments_deatil as $app_detail ) {
            $temp_start_time = new DateTime($app_detail->Date);
            $minutes_to_add = (int) $app_detail->Duration;
            $temp_end_time = new DateTime($app_detail->Date);
            $temp_end_time->add(new DateInterval('PT' . $minutes_to_add . 'M'));
            $this->appointment_model->make_object($temp_start_time, $temp_end_time);
            $temp_array = $this->appointment_model->appointment;
            array_push($booked_slots, $temp_array);
        }
        return $booked_slots;
    }

    /**
     * Get Single technician available slots
     * @param appointments_deatil
     * @param start_time
     * @param end_time
     */
    private function GetTechnicianAvailableSlots($appointments_deatil, $start_time, $end_time, $commute_time, $installation_size, $last_avail_start_time) {
        $available_slot_time = $commute_time + $installation_size;
        $minutes_to_subtract = $commute_time;
        $free_tech_time_slots = array();
        $temp_start_time = clone $start_time;
        foreach ( $appointments_deatil as $app_detail ) {
            $app_date_time = new DateTime($app_detail->Date);
            $app_date_time->sub(new DateInterval('PT' . $minutes_to_subtract . 'M'));
            $free_slot_diff = date_diff($temp_start_time, $app_date_time);
            if ( $free_slot_diff->invert == 0 && (($free_slot_diff->h * 60) + $free_slot_diff->i) >= ($available_slot_time) ) {
                $temp_free_slot_start_time = clone $temp_start_time;
                $temp_minutes = ($free_slot_diff->h * 60) + $free_slot_diff->i;
                $temp_free_slot_end_time = clone $temp_free_slot_start_time;
                $temp_free_slot_end_time->add(new DateInterval('PT' . $temp_minutes . 'M'));
                $this->appointment_model->make_object($temp_free_slot_start_time, $temp_free_slot_end_time);
                $temp_array = $this->appointment_model->appointment;
                array_push($free_tech_time_slots, $temp_array);
            }

            $minutes_to_add = (int) $app_detail->Duration;
            $temp_start_time = new DateTime($app_detail->Date);
            $temp_start_time->add(new DateInterval('PT' . $minutes_to_add . 'M'));
        }

        $free_slot_diff = date_diff($temp_start_time, $end_time);
        if ( $free_slot_diff->invert == 0 && (($free_slot_diff->h * 60) + $free_slot_diff->i) >= ($available_slot_time) ) {
            $temp_free_slot_start_time = clone $temp_start_time;
            $this->appointment_model->make_object($temp_free_slot_start_time, $end_time);
            $temp_array = $this->appointment_model->appointment;
            array_push($free_tech_time_slots, $temp_array);
        }
        return $free_tech_time_slots;
    }

    /**
     * Filter technician appointments by date
     * @param date_str
     * @param worker
     * @param appointments_deatil
     */
    public function GetWorkerAppointmentByDate($date_str, $worker, $appointments_deatil) {
        $worker_appointments = array();
        if ( $appointments_deatil ) {
            foreach ( $appointments_deatil as $appointment ) {
                $appointment_date = new DateTime($appointment->Date);
                if ( $worker->WorkerID == $appointment->WorkerID && $date_str == $appointment_date->format(DATE_YYYY_MM_DD) ) {
                    array_push($worker_appointments, $appointment);
                }
            }
        }
        return $worker_appointments;
    }

    /**
     * Get next thirty days booked and available slots by default. Otherwise it gets data within date range provided
     * @param manual_sunset_time_hr
     * @param manual_sunset_time_mins
     * @param shift_start_time
     * @param sunset_time_mode
     * @param shift_start_time_mins
     */
    public function GetAvailableSlotsByDateRange($start_date, $end_date, $manual_sunset_time_hr, $manual_sunset_time_mins, $shift_start_time, $sunset_time_mode, $shift_start_time_mins, $commute_time, $installation_size, $last_avail_start_time, $max_appoints, $zip_code, $holidays_data, $max_shift_end_time_hr, $max_shift_end_time_mins) {
        $diff_date = getDaysDiff($start_date, $end_date);
        $current_day_date = new DateTime($start_date);
        $end_day_date = (new DateTime($start_date))->modify(getDayAddIntervalString($diff_date));
        $start_date_str = $current_day_date->format(DATE_YYYY_MM_DD);
        $end_date_str = $end_day_date->format(DATE_YYYY_MM_DD);

        $all_workers = $this->admin->AllTechniciansDetailByZipCode($zip_code);
        $appointments_deatil = $this->AllAppointmentsDetailByZipCode($start_date_str, $end_date_str, $zip_code);
        $temp_appointments_deatil = $this->AllTempAppointmentsDetailByZipCode($start_date_str, $end_date_str, $zip_code);

        if ( $appointments_deatil == false ) {
            $appointments_deatil = array();
        }

        if ( $temp_appointments_deatil == false ) {
            $temp_appointments_deatil = array();
        }

        $appointments_deatil = array_merge($appointments_deatil, $temp_appointments_deatil);

        $five_days_sunset_time = $this->sunset_time->getDataByDateRange($start_date_str, $end_date_str);
        $five_days_available_slots = array();

        while ( $current_day_date <= $end_day_date ) {
            $start_date_str = $current_day_date->format(DATE_YYYY_MM_DD);
            $start_time = DateTime::createFromFormat(DATETIME_24_HR, get24HrDateTime($start_date_str, $shift_start_time, $shift_start_time_mins));
            $shift_end_time = DateTime::createFromFormat(DATETIME_24_HR, get24HrDateTime($start_date_str, $max_shift_end_time_hr, $max_shift_end_time_mins));
            $end_time = $this->GetEndTime($five_days_sunset_time, $manual_sunset_time_hr, $manual_sunset_time_mins, $sunset_time_mode, $start_date_str, $shift_end_time);

            if(!empty($end_time)){
                 $day_available_slots_data = $this->GetDayBookedSlots($appointments_deatil, $all_workers, $start_time, $end_time, $start_date_str, $commute_time, $installation_size, $last_avail_start_time, $max_appoints, $holidays_data, $shift_end_time);

                array_push($five_days_available_slots, $day_available_slots_data);   
            }
            $current_day_date->modify(getDayAddIntervalString(1));
        }

        return $five_days_available_slots;
    }

    /**
     * This function is no more used
     * Get next five day booked and available slots
     * @param manual_sunset_time_hr
     * @param manual_sunset_time_mins
     * @param shift_start_time
     * @param sunset_time_mode
     * @param shift_start_time_mins
     */
    public function GetFiveDayAvailableSlots($manual_sunset_time_hr, $manual_sunset_time_mins, $shift_start_time, $sunset_time_mode, $shift_start_time_mins, $commute_time, $installation_size, $last_avail_start_time) {
        $next_day_date = (new DateTime())->modify(getDayAddIntervalString(1));
        $next_five_day_date = (new DateTime())->modify(getDayAddIntervalString(5));
        $start_date_str = $next_day_date->format(DATE_YYYY_MM_DD);
        $end_date_str = $next_five_day_date->format(DATE_YYYY_MM_DD);
        $all_workers = $this->admin->AllTechniciansDetail();
        $appointments_deatil = $this->AllAppointmentsDetailByZipCode($start_date_str, $end_date_str);
        $five_days_sunset_time = $this->sunset_time->getDataByDateRange($start_date_str, $end_date_str);
        $five_days_available_slots = array();

        while ( $next_day_date <= $next_five_day_date ) {
            $start_date_str = $next_day_date->format(DATE_YYYY_MM_DD);
            $start_time = DateTime::createFromFormat(DATETIME_24_HR, get24HrDateTime($start_date_str, $shift_start_time, $shift_start_time_mins));
            $end_time = $this->GetEndTime($five_days_sunset_time, $manual_sunset_time_hr, $manual_sunset_time_mins, $sunset_time_mode, $start_date_str);

            $day_available_slots_data = $this->GetDayBookedSlots($appointments_deatil, $all_workers, $start_time, $end_time, $start_date_str, $commute_time, $installation_size, $last_avail_start_time);

            array_push($five_days_available_slots, $day_available_slots_data);
            $next_day_date->modify(getDayAddIntervalString(1));
        }

        return $five_days_available_slots;
    }

    private function MakeSlots($duration, $break, $stTime, $enTime) {
        $start = clone $stTime;
        $end = clone $enTime;
        $interval = new DateInterval("PT" . $duration . "M");
        $breakInterval = new DateInterval("PT" . $break . "M");

        for ( $intStart = $start; $intStart < $end; $intStart->add($interval)->add($breakInterval) ) {

            $endPeriod = clone $intStart;
            $endPeriod->add($interval);
            if ( $endPeriod > $end ) {
                $endPeriod = $end;
            }
            $start_time = new DateTime($intStart->format('Y-m-d H:i:s'));
            $end_time = new DateTime($endPeriod->format('Y-m-d H:i:s'));
            $mergable = 0;
            $this->mergable_slot_model->make_object($start_time, $end_time, $mergable);
            $periods[] = $this->mergable_slot_model->mergable_slot;
        }

        return $periods;
    }

    private function IsSlotMergeable($slot1, $slot2) {
        if ( $slot2->start_time >= $slot1->start_time && $slot2->end_time <= $slot1->end_time ) {
            return MERGABLE_SLOT;
        }
        else {
            return UNMERGABLE_SLOT;
        }
    }

    /**
     * Get technician booked slots and use that data to make available slots of the day
     * @param all_workers
     * @param start_time
     * @param end_time
     */
    private function GetDayBookedSlots($appointments_deatil, $all_workers, $start_time, $end_time, $start_date_str, $commute_time, $installation_size, $last_avail_start_time, $max_appoints, $holidays_data, $shift_end_time) {
        $day_available_slots_data;
        $worker_free_slots_array = array();
        $day_booked_slots = array();

        foreach ( $all_workers as $worker ) {
            $worker_appointments = $this->GetWorkerAppointmentByDate($start_date_str, $worker, $appointments_deatil);
            $settings = $this->admin->GetConfigSettings([MAX_NUMBER_OF_APPOINTMENTS_CONFIG]);
            $max_appoints = $settings [0]->Name == MAX_NUMBER_OF_APPOINTMENTS_CONFIG ? $settings [0]->Value : '';
            if ( $max_appoints != '' && count($worker_appointments) < $max_appoints ) {
                $free_tech_time_slots = $this->GetTechnicianAvailableSlots($worker_appointments, $start_time, $end_time, $commute_time, $installation_size, $last_avail_start_time);
                $tech_booked_slots = $this->GetTechnicianBookedSlots($worker_appointments);
                $this->worker_appointments->make_object($worker, $tech_booked_slots);
                $slots_data = $this->worker_appointments->appointments;
                array_push($day_booked_slots, $slots_data);
                if ( sizeof($free_tech_time_slots) ) {
                    array_push($worker_free_slots_array, $free_tech_time_slots);
                }
            }
        }
// 		No free slot for any worker
        if ( sizeof($worker_free_slots_array) != 0 ) {
// 			Merged available slots if all workers have some free slots
            $unique_day_available_slots = $this->GetMergedAvailableSlots($worker_free_slots_array, $start_time, $end_time);
        }
        else {
            $unique_day_available_slots = array();
        }

        $day_available_slots_data = $this->PrepareDayAvailableSlotsData($start_time, $end_time, $unique_day_available_slots, $day_booked_slots, $commute_time, $installation_size, $last_avail_start_time, $max_appoints, $start_date_str, $holidays_data, $shift_end_time);
        return $day_available_slots_data;
    }

    /**
     * Generate 15 mins slots then merged all slots if it is available in worker's free slot
     * @param worker_free_slots_array
     */
    private function GetMergedAvailableSlots($worker_free_slots_array, $start_time, $end_time) {
        $unique_day_available_slots = array();
        $fifteen_mins_slots = $this->MakeSlots(15, 0, $start_time, $end_time);
        foreach ( $fifteen_mins_slots as &$ms ) {
            $is_slot_mergable_array = array();
            foreach ( $worker_free_slots_array as $worker_fs ) {
                foreach ( $worker_fs as $fs ) {
                    array_push($is_slot_mergable_array, $this->IsSlotMergeable($fs, $ms));
                }
            }
            if ( in_array(MERGABLE_SLOT, $is_slot_mergable_array) ) {
                $ms->mergable = MERGABLE_SLOT;
            }
        }
        $merge_slots_set = array();
        foreach ( $fifteen_mins_slots as $ms ) {
            if ( $ms->mergable == MERGABLE_SLOT ) {
                array_push($merge_slots_set, $ms);
            }
            else {
                if ( sizeof($merge_slots_set) ) {
                    $merge_start_time = $merge_slots_set[0]->start_time;
                    $merge_end_time = $merge_slots_set[sizeof($merge_slots_set) - 1]->end_time;
                    $this->available_slot_model->make_object($merge_start_time, $merge_end_time);
                    $merge_slots_data = $this->available_slot_model->available_slot;
                    array_push($unique_day_available_slots, $merge_slots_data);
                    $merge_slots_set = array();
                }
            }
        }
        if ( sizeof($merge_slots_set) ) {
            $merge_start_time = $merge_slots_set[0]->start_time;
            $merge_end_time = $end_time;
            $this->available_slot_model->make_object($merge_start_time, $merge_end_time);
            $merge_slots_data = $this->available_slot_model->available_slot;
            array_push($unique_day_available_slots, $merge_slots_data);
            $merge_slots_set = array();
        }

        return $unique_day_available_slots;
    }

    /**
     * Find available slots for a day
     * @param start_time
     * @param end_time
     * @param unique_day_available_slots
     * @param day_available_slots
     */
    private function PrepareDayAvailableSlotsData($start_time, $end_time, $unique_day_available_slots, $day_booked_slots, $commute_time, $installation_size, $last_avail_start_time, $max_appoints, $start_date_str, $holidays_data, $shift_end_time) {

        $this->day_appointments->make_object($start_date_str, $day_booked_slots, $unique_day_available_slots, $commute_time, $installation_size, $last_avail_start_time, $max_appoints, $start_time, $end_time, $holidays_data, $shift_end_time);
        $day_slots_data = $this->day_appointments->appointments;

        return $day_slots_data;
    }

    /**
     * Find if full slot is available, in other words we have a user with no appointment for a specific day
     * @param start_time
     * @param end_time
     * @param unique_day_available_slots
     */
    private function FindFullAvailableSlot($start_time, $end_time, $unique_day_available_slots) {
        $found_full_available_slot = false;
        foreach ( $unique_day_available_slots as $uslot ) {
            if ( $uslot->start_time == $start_time && $uslot->end_time == $end_time ) {
                $found_full_available_slot = true;
                break;
            }
        }
        return $found_full_available_slot;
    }

    /**
     * Fetch end time according to sunset time option is selected in the system
     * @param five_days_sunset_time
     * @param manual_sunset_time_hr
     * @param manual_sunset_time_mins
     * @param sunset_time_mode
     * @param start_date_str
     */
    private function GetEndTime($five_days_sunset_time, $manual_sunset_time_hr, $manual_sunset_time_mins, $sunset_time_mode, $start_date_str, $shift_end_time) {
        if ( $sunset_time_mode == MANUAL_SUNSET_TIME ) {
            $end_time = DateTime::createFromFormat(DATETIME_24_HR, get24HrDateTime($start_date_str, $manual_sunset_time_hr, $manual_sunset_time_mins));
        }
        else if ( $sunset_time_mode == API_SUNSET_TIME ) {
            $specific_day_data = $this->sunset_time->FilterSunsetTimeByDate($five_days_sunset_time, $start_date_str);
            $end_time = DateTime::createFromFormat(DATETIME_24_HR, get24HrDateTime($start_date_str, $specific_day_data->EndTime));
        }

        if ( $end_time > $shift_end_time ) {
            $end_time = $shift_end_time;
        }
        return $end_time;
    }

    /**
     * Get index of first available slot
     * @param available_slots
     */
    public function GetAvailableSlotIndex($available_slots) {
        $first_available_slot_count = - 1;
        for ( $date_count = 0; $date_count < sizeof($available_slots); $date_count ++ ) {
            if ( !empty($available_slots[$date_count]->available_slots) ) {
                $is_holiday = false;
                foreach ($available_slots[$date_count]->holidays as $key => $holiday) {
                    if($holiday->date == $available_slots[$date_count]->date){
                        $is_holiday = true;
                        break;
                    }
                }
                if($is_holiday === false){
                    $first_available_slot_count = $date_count;
                    break;
                }
            }
        }
        return $first_available_slot_count;
    }

    /**
     * Get next five day available slot. Count starting from index.
     * @param available_slots
     * @param first_available_slot_count
     */
    public function GetNextNumOfDaysAvailableSlot($available_slots, $first_available_slot_count, $num_of_days) {
        $next_five_day_available_slots = array();
        for ( $date_count = $first_available_slot_count; $date_count < $first_available_slot_count + $num_of_days; $date_count ++ ) {
            array_push($next_five_day_available_slots, $available_slots [$date_count]);
        }
        return $next_five_day_available_slots;
    }

}
