<?php
/**
 * this class has methods that are related to orders
 * like fetch all orders of a worker,all scheduled orders,all orders,all orders by date range,
 * workers for calendar,all orders in string,sort todays orders for a worker,single order by worker
 * add,edit,update and delete order, add note
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class M_order extends MY_Model {

    const ORDER = "order";
    const CUSTOMER = "customer";
//     Temporary order with temporary mbr
    const TEMP_ORDER = "temp_order";

    public function __construct() {
        parent::__construct();
    }

    /**
     * @param string $workerID id of worker
     * fetch all orders of a worker from database
     * @return array or boolean
     */
    public function getAllOrders($workerID) {
        if ($workerID) {
            $sql = "SELECT * FROM `order` WHERE `order`.WorkerID = '$workerID' AND `order`.StatusID != '46b1e4fd15378'";
            $rows = $this->db->query($sql);
            $numberOfRecords = $rows->num_rows();
            if ($numberOfRecords > 0) {
                return $result = $rows->result_array();
            } else {
                return FALSE;
            }
        }
    }

    /**
     * @param string $workerID id of worker
     * fetch all todays scheduled orders of a worker from database
     * @return array or boolean
     */
    public function getAllTodaysScheduledOrders($workerID) {
        if ($workerID) {
            $status = $this->getStatusID(SCHEDULE);
            $sql = "SELECT * FROM `order` WHERE `order`.StatusID = '{$status["StatusID"]}' AND
                    `order`.WorkerID = '{$workerID}' AND DATE(`order`.Date) = DATE(NOW())";
            $rows = $this->db->query($sql);
            $numberOfRecords = $rows->num_rows();
            if ($numberOfRecords > 0) {
                return $result = $rows->result_array();
            } else {
                return FALSE;
            }
        }
    }

    /**
     * fetch all orders of current week from database
     * @return array or boolean
     */
    public function getAll() {
        $sql = "SELECT
                    `order`.OrderID,`order`.WorkerID,`order`.CustomerID,`order`.StatusID,`order`.TypeID,
                    `order`.`Name`,`order`.Detail,`order`.Duration,`order`.Date,customer.Longitude,customer.Latitude,
                    customer.City,(SELECT COUNT(*) FROM message_sent_recieved WHERE message_sent_recieved.
                    OrderID = `order`.OrderID AND message_sent_recieved.MessageStatus = '22377f6f23d49') AS MessageAlerts,worker.Color,
                    IF(`order`.Date > NOW(), '0', '1') AS Previous
                FROM `order`
                    INNER JOIN customer ON `order`.CustomerID = customer.CustomerID
                    INNER JOIN worker ON order.WorkerID = worker.WorkerID
                WHERE `order`.StatusID != '46b1e4fd15378' AND YEARWEEK(`order`.Date) = YEARWEEK(NOW())
                ORDER BY `order`.Date";
        $rows = $this->db->query($sql);
        $numberOfRecords = $rows->num_rows();
        if ($numberOfRecords > 0) {
            return $result = $rows->result_array();
        } else {
            return FALSE;
        }
    }

    /**
     * fetch all orders from database by date
     * @return array or boolean
     */
    public function getAllOrdersByDate($start, $end) {
        $sql = "SELECT
                    `order`.OrderID,`order`.WorkerID,`order`.CustomerID,`order`.StatusID,`order`.TypeID,
                    `order`.`Name`,`order`.Detail,`order`.Duration,`order`.Date,customer.Longitude,customer.Latitude,
                    customer.City,customer.isPhoneHomeLandline,(SELECT COUNT(*) FROM message_sent_recieved WHERE message_sent_recieved.
                    OrderID = `order`.OrderID AND message_sent_recieved.MessageStatus = '22377f6f23d49') AS MessageAlerts,worker.Color
                FROM `order`
                    INNER JOIN customer ON `order`.CustomerID = customer.CustomerID
                    INNER JOIN worker ON order.WorkerID = worker.WorkerID
                WHERE `order`.StatusID != '46b1e4fd15378' AND DATE(`order`.Date) >= '" . $start .
                "' AND DATE(`order`.Date) < '" . $end . "' ORDER BY `order`.Date";
        $rows = $this->db->query($sql);
        $numberOfRecords = $rows->num_rows();
        if ($numberOfRecords > 0) {
            return $result = $rows->result_array();
        } else {
            return FALSE;
        }
    }

    /**
     * fetch all orders from database
     * @return array or boolean
     */
    public function getAllByDateRange($workerID, $start, $end) {
        $sql = "SELECT
                    `order`.OrderID,`order`.`Name`,`order`.WorkerID,worker.`Name` AS Worker,
                    worker.Email,`order`.CustomerID,customer.CustomerName,`order`.StatusID,order_status.`Name` AS `Status`,
                    `order`.TypeID,order_type.`Name` AS Type,`order`.Detail,`order`.Date,`order`.Duration,
                    customer.Address1,customer.Address2,customer.Zip,customer.City,customer.Longitude,
                    customer.Latitude
                FROM `order`
                    INNER JOIN worker ON `order`.WorkerID = worker.WorkerID
                    INNER JOIN customer ON `order`.CustomerID = customer.CustomerID
                    INNER JOIN order_type ON `order`.TypeID = order_type.TypeID
                    INNER JOIN order_status ON `order`.StatusID = order_status.StatusID
                WHERE worker.WorkerID = '$workerID' AND DATE(`order`.Date) >= '$start'
                AND DATE(`order`.Date) < '$end' AND `order`.StatusID != '46b1e4fd15378' ORDER BY `order`.Date";
        $rows = $this->db->query($sql);
        $numberOfRecords = $rows->num_rows();
        if ($numberOfRecords > 0) {
            return $result = $rows->result_array();
        } else {
            return FALSE;
        }
    }

    /**
     * fetch all orders from database
     * @return array or boolean
     */
    public function javaWorker($workers) {
        foreach ((array) $workers as $key => $worker) {
            $javaWorkers[] = $key;
        }
        return $javaWorkers;
    }

    public function getMessageAlertOfOrder($orderID, $status) {
        $rows = $this->selectAllWhere("message_sent_recieved", array("OrderID" => $orderID, "MessageStatus" => $status));
        if ($rows->num_rows() > 0) {
            return $rows->result('array');
        } else {
            return false;
        }
    }

    /**
     * fetch all orders from database
     * @return array or boolean
     */
    public function creatOrderString($allOrders, $javaWorkers) {
        if ($allOrders) {
            foreach ((array) $allOrders as $key => $order) {
                $isHighlighted = $isMsgHighlighted = $isRejectedHighlighted = $isLateRejectedHighlighted = 0;
                $alertData = $this->getAlertOfOrder($order["OrderID"]);

                if ($alertData[0]["TypeID"] == "73dc597bd98e2") {
                    $isLateRejectedHighlighted = 1;
                } elseif ($order["StatusID"] == "56a5c03adf54c") {
                    $isRejectedHighlighted = 1;
                } elseif ($order["MessageAlerts"] > 0 && (empty($order["isPhoneHomeLandline"]) || $order["isPhoneHomeLandline"] != "1")) {
                    $isMsgHighlighted = 1;
                } elseif ($alertData) {
                    $isHighlighted = 1;
                }

                $userID = array_search($order["WorkerID"], $javaWorkers);
                $startDate = date("Y, n - 1, d, H", strtotime($order["Date"]));
                $endDate = date("Y, n - 1, d, H", strtotime($order["Date"]));
                $minute = date("i", strtotime($order["Date"]));
                $duration = date("i", strtotime($order["Date"])) + $order["Duration"];
                $color = $order["Color"];

                $array[] = array(
                    "id" => $order["OrderID"],
                    "userId" => $userID,
                    "start" => "new Date(" . $startDate . ", " . $minute . ")",
                    "end" => "new Date(" . $endDate . ", " . $duration . ")",
                    "title" => $order["Name"],
                    "color" => $color,
                    "isHighlighted" => $isHighlighted,
                    "isMsgHighlighted" => $isMsgHighlighted,
                    "isRejectedHighlighted" => $isRejectedHighlighted,
                    "isLateRejectedHighlighted" => $isLateRejectedHighlighted
                );
            }
            return (object) $array;
        } else {
            return FALSE;
        }
    }

    /**
     * fetch all orders from database
     * and sort them to show in order listing
     * @return array or boolean
     */
    public function sortAllOrders($workerID) {
        $allOrders = $this->getTodaysOrders($workerID);
        if ($allOrders) {
            foreach ($allOrders as $key => $order) {

                $alertData = $this->getAlertOfOrder($order["OrderID"]);
                $order["Alerts"] = $alertData;

                switch ($order["Status"]) {
                    case DONE: // separate completed orders in array
                        $sortedOrders[5][] = $order;
                        break;

                    case ABORT: // separate aborted orders in array
                        $sortedOrders[6][] = $order;
                        break;

                    case REJECTED: // separate start order in array
                        if ($alertData && $alertData[0]["TypeID"] == "73dc597bd98e2") {
                            $sortedOrders[0][] = $order;
                        } else {
                            $sortedOrders[1][] = $order;
                        }
                        break;

                    case START: // separate start order in array
                        $sortedOrders[2][] = $order;
                        break;

                    case ENROUTE: // separate en-route order in array
                        $sortedOrders[3][] = $order;
                        break;

                    default:
                        // default will be scheduled orders
                        $sortedOrders[4][] = $order;
                }
            }

            // sorting array by key
            ksort($sortedOrders);
            return $sortedOrders;
        }
    }

    /**
     * @param string $orderID id of order
     * fetch single order from database
     * @return array or boolean
     */
    public function getOrderByWorkerID($workerID, $date, $time) {
        if ($workerID) {
            // join tables order & customer to fetch all necessary data for orders
            $sql = "SELECT
                        `order`.OrderID,`order`.`Name`,`order`.WorkerID,worker.`Name` AS Worker,
                        worker.Email,`order`.CustomerID,`order`.StatusID,`order`.TypeID,`order`.Detail,
                        `order`.Date,`order`.Duration
                    FROM `order`
                        INNER JOIN worker ON `order`.WorkerID = worker.WorkerID
                    WHERE `order`.WorkerID = '{$workerID}'
                    AND DATE(`order`.Date) = '{$date}'
                    AND TIME(`order`.Date) = '{$time}'
                    AND `order`.StatusID != '46b1e4fd15378'";

            $rows = $this->db->query($sql);
            $numberOfRecords = $rows->num_rows();
            $result = $rows->first_row('array');

            // check if rows are more then 0
            if ($numberOfRecords > 0) {
                return $result;
            } else {
                return FALSE;
            }
        }
    }

    public function getOrderTimeWithDuration($Date = NULL) {
        if ($Date) {
            $condition = " WHERE DATE(`order`.Date) = '$Date'";
        } else {
            $condition = " WHERE DATE(`order`.Date) = CURDATE()";
        }
        $sql = "SELECT `order`.OrderID,`order`.Name,`order`.Duration,`order`.WorkerID,`order`.Date AS Time,
                ADDDATE(`order`.Date, INTERVAL `order`.Duration MINUTE) AS TimeDuration
                FROM `order`" . $condition . " AND `order`.StatusID != '46b1e4fd15378'";
        $rows = $this->db->query($sql);
        return $rows->result_array();
    }

    /**
     * @note this function is checking is it incident overlapping
     * or not in calendar after drop or extend
     * @param string $orders
     * @param string $workerID
     * @param string $orderID
     * @param string $start
     * @param string $end
     * @return boolean|string
     */
    public function checkOverlappingIncident($orders, $workerID, $orderID, $start, $end) {
        foreach ($orders as $order) {
            $errorFound = false;
            if ($workerID == $order["WorkerID"] && $orderID != $order["OrderID"]) {
                // checking if dropped in another incident
                if ($start >= $order["Time"] && $end <= $order["TimeDuration"]) {
                    $errorFound = true;
                }
                // checking if dropped incident overlaps another incident
                elseif ($start <= $order["Time"] && $end >= $order["TimeDuration"]) {
                    $errorFound = true;
                }
                // checking if dropped incident start time is smaller then antoher incident start time
                // and end time is greater then another incident start time
                // and end time is smaller then another incident end time
                elseif ($start < $order["Time"] && $end > $order["Time"] && $end < $order["TimeDuration"]) {
                    $errorFound = true;
                }
                // checking if dropped incident start time is greater then antoher incident start time
                // and start time is smaller then another incident start time
                // and end time is greater then another incident end time
                elseif ($start > $order["Time"] && $start < $order["TimeDuration"] && $end > $order["TimeDuration"]) {
                    $errorFound = true;
                }
            }
            if ($errorFound) {
                return BUSY_SLOT;
                break;
            }
        }
        return true;
    }

    public function getCustomerIDByName($name) {
        $this->db->select('CustomerID');
        $this->db->from('customer');
        $this->db->like('CustomerName', $name);
        $result = $this->db->get()->first_row('array');
        if ($result) {
            return $result['CustomerID'];
        } else {
            return FALSE;
        }
    }
    
    /**
     * This method used for self service automation. By using this method customers can create appointment by themselves
     * @return boolean
     */
    public function addOrderByCustomer($description = "", $worker_id, $customer_id, $start_time, $duration, $mbr_exist) {
    	$this->load->library("SelfAppointmentCreation");
    	$this->selfAppointmentCreation = new SelfAppointmentCreation();
    	
    	$service = TYPE_INSTALLATION;
    	$serviceAlias = $this->getCodeOFType($service);
    	$status = STATUS_SCHEDULE;
    	$customerData = $this->getCustomerDetail($customer_id);
    	if($service && $status && $customer_id && $worker_id && $duration) {
    		$AMPM = getAMPM($start_time->format('Y-m-d H:i:s'));
    		$appointment_name = $AMPM . " " . $customer_id . " " . $customerData[0]["CityCode"] . " " . $serviceAlias;
    		$data = array(
    				"OrderID" => uniqid(),
    				"Name" => $appointment_name,
    				"Detail" => $description,
    				"TypeID" => $service,
    				"StatusID" => $status,
    				"Date" => $start_time->format('Y-m-d H:i:s'),
    				"Duration" => $duration,
    				"WorkerID" => $worker_id,
    				"CustomerID" => $customer_id,
    				"IsProcessed" => 0
    		);
    		if($mbr_exist) {
    			$order_table = self::ORDER;
    			unset($data['IsProcessed']);
    		} else {
    			$data["AlertTitle"] = "create";
    			$order_table = self::TEMP_ORDER;
    		}
    		
    		if ($this->Insert($order_table, $data)) {
    			$this->addSingleActivity("8fc24e31a7af7", "40d64aeeb308b", $data["OrderID"], "order_created_by", "API");
    			return array("OrderID" => $data["OrderID"]);
//     			return TRUE;
    		} else {
    			return FALSE;
    		}
    		
    	}
    }

    /**
     * add order in database
     * @return boolean
     */
    public function addOrder() {
        $creatorID = $this->isAdminLogin();
        $description = str_replace(array("\n", "\r"), '', trim($this->input->post("description")));

        $service = $this->input->post("service");
        $serviceAlias = $this->getCodeOFType($service);

        $status = $this->input->post("status");
        if ($this->input->post("date")) {
            $date = formatDate($this->input->post("date"));
        } else {
            $date = NULL;
        }
        if ($this->input->post("time")) {
            $time = formatTime($this->input->post("time"));
        } else {
            $time = NULL;
        }

        $date = $date . " " . $time;
        $startTime = $date;
        $duration = $this->input->post("duration");
        $endTime = date("Y-m-d H:i:s", strtotime($startTime . "+" . $duration . " MINUTES"));
        $worker = $this->input->post("worker");
        $customerName = $this->input->post("customer");
        $customer = $this->getCustomerIDByName($customerName);
        $customerData = $this->getCustomerDetail($customer);

        if ($description && $service && $status && $this->input->post("date") && $time && $duration && $worker) {
            if ($customer) {
                $response = $this->getOrderByWorkerID($worker, formatDate($this->input->post("date")), formatTime($this->input->post("time")));
                if ($response) {
                    return ALREADY_HAS_APPOINTMENT;
                } else {
                    $orders = $this->getOrderTimeWithDuration();
                    $response = $this->checkOverlappingIncident($orders, $worker, NULL, $startTime, $endTime);
                    if ($response === true) {

                        $AMPM = getAMPM($date);
                        $data = array(
                            "OrderID" => uniqid(),
                            "Name" => $AMPM . " " . $customer . " " . $customerData[0]["CityCode"] . " " . $serviceAlias,
                            "Detail" => $description,
                            "TypeID" => $service,
                            "StatusID" => $status,
                            "Date" => $date,
                            "Duration" => $duration,
                            "WorkerID" => $worker,
                            "CustomerID" => $customer
                        );

                        $this->addSingleActivity("8fc24e31a7af7", "40d64aeeb308b", $data["OrderID"], "order_created_by", $this->isAdminLogin());

                        if ($this->Insert(self::ORDER, $data)) {

                            $addOrderActivity = array(
                                "OrderID" => $data["OrderID"],
                                "AssignedBy" => $creatorID,
                                "AssignedTo" => $worker
                            );

                            if ($this->addOrderAllActivities($addOrderActivity)) {
                                if (empty($customerData[0]["PhoneHome"]) || $customerData[0]["PhoneHome"] == "" || $customerData[0]["PhoneHome"] == "0") {
                                    $this->addSingleActivity("8fc24e31a7af7", "40d64aeeb308b", $data["OrderID"], "empty_phone_number", "yes");
                                }
                            }

                            if ($this->input->post("address-updated") || $this->input->post("phone-updated") || $this->input->post("email-updated")) {

                                if ($this->input->post("address-updated")) {
                                    $dataArray[] = $this->input->post("address-updated");
                                }

                                if ($this->input->post("phone-updated")) {
                                    $dataArray[] = $this->input->post("phone-updated");
                                }

                                if ($this->input->post("email-updated")) {
                                    $dataArray[] = $this->input->post("email-updated");
                                }

                                $activity = array(
                                    "OrderID" => $data["OrderID"],
                                    "AssignedBy" => $this->isAdminLogin(),
                                    "Name" => "customer_updated",
                                    "Message" => json_encode($dataArray),
                                );
                                $this->addMessageAllActivities($activity);
                            }
                        }

                        //if $status is schedule
                        if ($customerData[0]["isPhoneHomeLandline"] != "1" && $status == "56a5c03ab920f") {
                            //add message entry in database with pending status
                            if ($customerData[0]["PhoneHome"] != "" && $customerData[0]["PhoneHome"] != "0") {
                                $this->addMessageEntry($data["OrderID"]);
                            }
                        }
                        $orderData = $this->getOrder($data["OrderID"]);
                        if ($customerData[0]["PhoneHome"] != "" && $customerData[0]["PhoneHome"] != "0") {
                            $this->addAlert($orderData, "create", $endTime);
                        }

                        return TRUE;
                    } else {
                        return $response;
                    }
                }
            } else {
                return VALID_CUSTOMER;
            }
        } else {
            return "";
        }
    }

    public function addSingleActivity($actionID, $objectTypeID, $objectID, $Name, $Value) {
        $activityLogID = $this->orderActivity($actionID, $objectTypeID);

        $activityMetaArray = array(
            "MetaID" => strictUniqueID(),
            "ActivityLogID" => $activityLogID,
            "ObjectID" => $objectID,
            "Name" => $Name,
            "Value" => $Value
        );
        $this->Insert("activity_log_meta", $activityMetaArray);
    }

    /**
     * edit order in database
     * @return boolean
     */
    public function editOrder($orderID) {
        $creatorID = $this->isAdminLogin();
        $orderData = $this->getOrder($orderID);
        $meridium = false;
        $orderNameExplode = explode(" ", $orderData["Name"]);
        $customerData = $this->getCustomerDetail($orderData["CustomerID"]);
        $description = str_replace(array("\n", "\r"), '', trim($this->input->post("description")));
        $service = $this->input->post("service");
        $serviceAlias = $this->getCodeOFType($service);

        $status = $this->input->post("status");

        if ($this->input->post("date")) {
            $date = formatDate($this->input->post("date"));
        } else {
            $date = NULL;
        }
        if ($this->input->post("time")) {
            $time = formatTime($this->input->post("time"));
        } else {
            $time = NULL;
        }

        $date = $date . " " . $time;
        $startTime = $date;
        $duration = $this->input->post("duration");
        $endTime = date("Y-m-d H:i:s", strtotime($startTime . "+" . $duration . " MINUTES"));
        $worker = $this->input->post("worker");

        if ($description && $service && $status && $this->input->post("date") && $time && $duration && $worker) {
            $response = $this->getOrderByWorkerID($worker, formatDate($this->input->post("date")), formatTime($this->input->post("time")));

            if ($response) {
                if ($response["OrderID"] != $orderID) {
                    $error = ALREADY_HAS_APPOINTMENT;
                }
            }

            if (empty($error)) {

                $orderActualDate = date("Y-m-d", strtotime($orderData["Date"]));
                $orderPostedDate = date("Y-m-d", strtotime($date));

                // getting time convention (AM or PM) of edit incident
                // if it is edit in AM or PM time
                $orderNameExplode[0] = getAMPM($date);

                if ($orderActualDate != $orderPostedDate) {
                    $orders = $this->getOrderTimeWithDuration($orderPostedDate);
                } elseif (date("Y-m-d") != $orderPostedDate) {
                    $orders = $this->getOrderTimeWithDuration($orderPostedDate);
                } else {
                    $orders = $this->getOrderTimeWithDuration();
                }

                // check all possible senarios of incident overlapping in calendar
                $responseOverlapp = $this->checkOverlappingIncident($orders, $worker, $orderID, $startTime, $endTime);

                if ($responseOverlapp === true) {

                    $data = array(
                        "Name" => $orderNameExplode[0] . " " . $customerData[0]["CustomerID"] . " " . $customerData[0]["CityCode"] . " " . $serviceAlias,
                        "Detail" => $description,
                        "TypeID" => $service,
                        "StatusID" => $status,
                        "Date" => $date,
                        "Duration" => $duration,
                        "WorkerID" => $worker
                    );

                    if ($this->UpdateAll(self::ORDER, $data, array("OrderID" => $orderID))) {

                        if ($orderData["Date"] != $date) {
                            if ($status == "56a5c03ab920f" || $status == "56a5c03acf58c") {
                                $this->removeAlert($orderID);
                                if ($customerData[0]["PhoneHome"] != "" && $customerData[0]["PhoneHome"] != "0") {
                                    $this->addAlert($orderData, "change", $startTime);
                                }

                                if ($customerData[0]["isPhoneHomeLandline"] == "1") {
                                    $this->deleteMessageEntry($orderID);
                                } else {
                                    if ($this->getMessageEntry($orderID)) {
                                        if ($customerData[0]["PhoneHome"] != "" && $customerData[0]["PhoneHome"] != "0") {
                                            $this->updateMessageEntry($orderID);
                                        }
                                    } else {
                                        if ($customerData[0]["PhoneHome"] != "" && $customerData[0]["PhoneHome"] != "0") {
                                            $this->addMessageEntry($orderID);
                                        }
                                    }
                                }
                            }
                        }

                        $editOrderActivity = array(
                            "OrderID" => $orderID,
                            "AssignedBy" => $creatorID
                        );
                        $this->editOrderAllActivities($orderData, $data, $editOrderActivity, $meridium);
                    }
                    return TRUE;
                } else {
                    return $responseOverlapp;
                }
            } else {
                return $error;
            }
        } else {
            return "";
        }
    }
    
    /*
     * Update order date
     */
    public function UpdateOrderDate( $orderId, $start_date) {
    	$data = array(
    			'Date' => $start_date,
    	);
    	 
    	$this->db->where('OrderID', $orderId);
    	$result = $this->db->update('order', $data);
    	return $result;
    }
    
    /*
     * Update temp_order date
     */
    public function UpdateTempOrderDate( $orderId, $start_date) {
    	$data = array(
    			'Date' => $start_date,
    	);
    
    	$this->db->where('OrderID', $orderId);
    	$result = $this->db->update('temp_order', $data);
    	return $result;
    }
    
    /*
     * Update temp_order Alert Title
     */
    public function UpdateTempOrderAlertTitle( $orderId, $alertTitle) {
    	$data = array(
    			'AlertTitle' => $alertTitle,
    	);
    
    	$this->db->where('OrderID', $orderId);
    	$result = $this->db->update('temp_order', $data);
    	return $result;
    }

    /**
     * update order time in database
     * @return boolean
     */
    public function updateOrderTime($orderID, $data) {
        $order = $this->getOrder($orderID);
        if ($order["isPhoneHomeLandline"] == "1") {
            $this->deleteMessageEntry($orderID);
        } elseif ($order["StatusID"] == "56a5c03ab920f") {//if $status is schedule
            if ($this->getMessageEntry($orderID)) {
                if ($order["PhoneHome"] != "" && $order["PhoneHome"] != "0") {
                    $this->updateMessageEntry($orderID);
                }
            } else {
                //add message entry in database with pending status
                if ($order["PhoneHome"] != "" && $order["PhoneHome"] != "0") {
                    $this->addMessageEntry($orderID);
                }
            }
        }
        $this->UpdateAll(self::ORDER, $data, array("OrderID" => $orderID));
    }

    /**
     * update order time in database
     * @return boolean
     */
    public function addOrderDuration($orderID, $data) {
        $this->UpdateAll(self::ORDER, $data, array("OrderID" => $orderID));
    }

    /**
     * @note update order status in database
     * @return update
     */
    public function updateOrderStatus() {
        $orderID = $this->input->post("order-id");
        $nextOrderID = $this->input->post("next-order-enroute");

        if ($this->input->post("label") == "enroute-complete") {

            $this->updateOrderEnroute($orderID, DONE);
            $status = DONE;
            $this->session->set_userdata(array('msg' => INCIDENT_UPDATED));
        } elseif ($this->input->post("label") == "enroute-abort") {

            $this->updateOrderEnroute($orderID, ABORT);
            $status = ABORT;
            $this->session->set_userdata(array('msg' => INCIDENT_UPDATED));
        } elseif ($this->input->post("label") == "enroute-schedule") {

            $this->updateOrderEnroute($orderID, SCHEDULE);
            $status = SCHEDULE;
            $this->session->set_userdata(array('msg' => INCIDENT_UPDATED));
        } elseif ($this->input->post("label") == "cancel-enroute-complete") {

            $this->updateOrderCanecelEnroute($orderID, DONE, $nextOrderID);
            $status = DONE;
            $this->session->set_userdata(array('msg' => INCIDENT_UPDATED));
        } elseif ($this->input->post("label") == "cancel-enroute-abort") {

            $this->updateOrderCanecelEnroute($orderID, ABORT, $nextOrderID);
            $status = ABORT;
            $this->session->set_userdata(array('msg' => INCIDENT_UPDATED));
        } elseif ($this->input->post("label") == "cancel-enroute-schedule") {

            $this->updateOrderCanecelEnroute($orderID, SCHEDULE, $nextOrderID);
            $status = SCHEDULE;
            $this->session->set_userdata(array('msg' => INCIDENT_UPDATED));
        } elseif ($this->input->post("label") == "complete") {

            $this->updateOrderCanecelEnroute($orderID, DONE, $nextOrderID);
            $status = DONE;
            $this->session->set_userdata(array('msg' => INCIDENT_UPDATED));
        } elseif ($this->input->post("label") == "abort") {

            $this->updateOrderCanecelEnroute($orderID, ABORT, $nextOrderID);
            $status = ABORT;
            $this->session->set_userdata(array('msg' => INCIDENT_UPDATED));
        }

        return true;
    }

    /**
     * @param string $orderID id of order to make en-route
     * @param string $status status of order to update existing enroute order
     */
    public function updateOrderEnroute($orderID, $status) {
        $workerID = $this->isworkerLogin();

        //check if any order is en-route
        if ($this->getKeyOrderofToday($workerID, ENROUTE)) {
            $enrouteOrderID = $this->getKeyOrderofToday($workerID, ENROUTE);
        } else {
            //check if any order is start
            $enrouteOrderID = $this->getKeyOrderofToday($workerID, START);
        }
        $orderData = $this->getOrder($enrouteOrderID);
        $editOrderActivity = array(
            "OrderID" => $enrouteOrderID,
            "AssignedBy" => $this->isWorkerLogin()
        );
        $data = array(
            "Status" => $status
        );
        $this->editOrderAllActivities($orderData, $data, $editOrderActivity);
        $this->setStatus($enrouteOrderID, $status);
        if ($orderID) {
            $orderData = $this->getOrder($orderID);
            $editOrderActivity = array(
                "OrderID" => $orderID,
                "AssignedBy" => $this->isWorkerLogin()
            );
            $data = array(
                "Status" => ENROUTE
            );
            $this->editOrderAllActivities($orderData, $data, $editOrderActivity);
            $this->setStatus($orderID, ENROUTE);
        }
    }

    /**
     * @param string $orderID id of order to cancel en-route
     * @param string $status status of order to update existing enroute order
     * @param string $nextOrderID id of order to make en-route
     * @return update status of a existing enroute order
     */
    public function updateOrderCanecelEnroute($orderID, $status, $nextOrderID = NULL) {
        if ($orderID && $status) {
            if (!empty($nextOrderID)) {
                $orderData = $this->getOrder($nextOrderID);
                $this->setStatus($nextOrderID, ENROUTE);
                $editOrderActivity = array(
                    "OrderID" => $nextOrderID,
                    "AssignedBy" => $this->isWorkerLogin()
                );
                $data = array(
                    "Status" => ENROUTE
                );
                $this->editOrderAllActivities($orderData, $data, $editOrderActivity);
            }
            $orderData = $this->getOrder($orderID);
            $editOrderActivity = array(
                "OrderID" => $orderID,
                "AssignedBy" => $this->isWorkerLogin()
            );
            $data = array(
                "Status" => $status
            );
            $this->editOrderAllActivities($orderData, $data, $editOrderActivity);
            $this->setStatus($orderID, $status);
        }
    }

    public function checkDeletedOrder($orderID) {
        return $this->SelectCount("order", array("OrderID" => $orderID, "StatusID" => "46b1e4fd15378"));
    }

    /**
     * @param string $orderID id of order to update
     * @param string $status status of order to update
     * @return update status of a existing order
     */
    public function setStatus($orderID, $status) {
        $order = $this->getOrder($orderID);
        $shortCodes = $this->getShortCodes($order);
        $isDeleted = $this->checkDeletedOrder($orderID);

        if ($isDeleted == "0") {
            $statusDetail = $this->getStatusID($status);
            if ($status != SCHEDULE && $status != ENROUTE && $status != REJECTED) {
                $this->deleteMessageEntry($orderID);
                $this->removeAlert($orderID);
            } elseif ($status == ENROUTE && $order["isPhoneHomeLandline"] != "1") {
                $result = $this->getMessageEntry($orderID);
                $message = $this->getMessageBody("9634e9d91933d");
                $message = $this->replaceDateAndService($message, $shortCodes);
                $response = $this->sendSMS(FROM, $order["PhoneHome"], $message);

                if ($response) {
                    if ($response["Status"] != "fail") {

                        $response["LogID"] = uniqid();
                        $response["OrderID"] = $orderID;
                        $response["Date"] = date("Y-m-d H:i:s");

                        $this->Insert("message_log", $response);

                        if ($this->isWorkerLogin()) {
                            $activity = array(
                                "OrderID" => $orderID,
                                "AssignedBy" => $this->isWorkerLogin(),
                                "Name" => "enroute_sms_sent",
                                "Message" => $message,
                            );
                            $this->addMessageAllActivities($activity);
                        }
                    }
                }
            }
            if ($this->UpdateAll(self::ORDER, array("StatusID" => $statusDetail["StatusID"]), array("OrderID" => $orderID))) {
                return TRUE;
            } else {
                return FALSE;
            }
        } else {
            return FALSE;
        }
    }

    public function getOrderCustomerLatLng($orderID) {
        $sql = "SELECT
                    customer.Latitude,customer.Longitude,`order`.OrderID, `order`.Date
                FROM `order`
                    INNER JOIN customer ON `order`.CustomerID = customer.CustomerID
                WHERE `order`.OrderID = '{$orderID}'";
        $rows = $this->db->query($sql);
        $result = $rows->first_row('array');
        return $result;
    }

    /**
     * @note insert a note in database
     * @return boolean
     */
    public function addNote($label = NULL) {

        if ($this->input->post("label") == "enroute-complete" || $this->input->post("label") == "enroute-abort") {
            // get enroute or start order if set
            $workerID = $this->isworkerLogin();
            if ($this->getKeyOrderofToday($workerID, ENROUTE)) {
                $orderID = $this->getKeyOrderofToday($workerID, ENROUTE);
            } else {
                $orderID = $this->getKeyOrderofToday($workerID, START);
            }
        } else {
            $orderID = $this->input->post("order-id");
        }
        $data = array(
            "NoteID" => uniqid(),
            "Note" => e($this->input->post("note")),
            "OrderID" => $orderID,
            "WorkerID" => $this->isWorkerLogin(),
            "Label" => $label,
            "Date" => date("Y-m-d H:i:s"),
            "Read" => 0,
            "StatusID" => "44cde42d87552",
        );
        if ($this->Insert("note", $data)) {
            $addNoteActivity = array(
                "NoteID" => $data["NoteID"],
                "OrderID" => $data["OrderID"],
                "AssignedBy" => $this->isWorkerLogin(),
            );

            $this->addNoteAllActivities($addNoteActivity);
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function deleteOrder($orderID) {
        if ($orderID) {
            $this->UpdateAll("order", array("StatusID" => "46b1e4fd15378"), array("OrderID" => $orderID));
            $this->UpdateAll("note", array("StatusID" => "56b1e42d87332"), array("OrderID" => $orderID));
            $this->deleteMessageEntry($orderID);
            redirect(ADMIN_BASE_PATH . "calendar");
        }
    }
    
    public function deleteTempOrder($orderId) {
    	$this->db->where('IsProcessed', '1');
    	$this->db->delete('temp_order');
    }

    /**
     * check from database that is it alert is already sent or not
     * @param string $objectID
     * @param string $typeID
     * @param string $behaviorID
     * @param string $isVisible
     * @param string $date
     * @return boolean if false or return array if true
     */
    public function checkAlertSent($objectID, $typeID = NULL, $behaviorID = NULL, $isVisible = NULL, $date = NULL) {
        $where = array("ObjectID" => $objectID);

        if (!empty($typeID)) {
            $where["TypeID"] = $typeID;
        }
        if (!empty($objectID)) {
            $where["BehaviorID"] = $behaviorID;
        }
        if (!empty($isVisible)) {
            $where["isVisible"] = $isVisible;
        }
        if (!empty($date)) {
            $where["Date"] = $date;
        }

        $rows = $this->SelectAllWhere("alert", $where);

        if ($rows->num_rows() > 0) {
            return $rows->result('array');
        } else {
            return false;
        }
    }

    public function getAllsurveyHTML($surveys) {
        if ($surveys) {
            $x = 1;
            foreach ($surveys as $survey) {
                ?>
                <tr>
                    <td><?php echo $x++; ?></td>
                    <td><?php echo $survey->Name; ?></td>
                    <td><?php echo $survey->Email; ?></td>
                    <td>
                        <a data-id="<?php echo (!empty($survey->OrderID)) ? $survey->OrderID : false; ?>" data-app="admin-app" class="btn btn-primary send-to-email" title="Send Survey"><img src="<?php echo base_url() . "assets/admin/img/icon-send-survey.png" ?>" /></a>
                    </td>
                    <td><input type="checkbox" name="single-survey[]" value="<?php echo (!empty($survey->OrderID)) ? $survey->OrderID : false; ?>" /></td>
                </tr>
                <?php
            }
            ?>
            <?php
        } else {
            ?>
            <tr class="odd"><td valign="top" colspan="5" class="dataTables_empty">No data available in table</td></tr>
            <?php
        }
    }

}
