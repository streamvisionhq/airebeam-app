<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_day_appointments extends MY_Model{
	public $appointments;
	private $date;
	private $start_time;
	private $end_time;
	private $booked_slots;
	private $available_slots;
	private $min_commute_time;
	private $installation_time;
	private $last_available_start_time;
	private $holidays;
	private $max_appoints;
	
	public function __construct() {
		parent::__construct();
	}
	
	public function make_object($date, $booked_slots, $available_slots, 
								$min_commute_time, $installation_time,
								$last_available_start_time, $max_appoints, $start_time, 
								$end_time, $holidays_data, 
								$shift_end_time ) {
		$this->date = $date;
		$this->start_time = $start_time;
		$this->end_time = $end_time;
		$this->booked_slots = $booked_slots;
		$this->available_slots = $available_slots;
		$this->min_commute_time = $min_commute_time;
		$this->installation_time = $installation_time;
		$this->last_available_start_time = $last_available_start_time;
		$this->holidays = $holidays_data;
		$this->max_appoints = $max_appoints;
// 		$this->shift_end_time = $shift_end_time;
		
		$this->appointments = array(
				DATE_KEY                                                => $this->date,
				START_TIME_KEY						=> $this->start_time,
				END_TIME_KEY						=> $this->end_time,
				BOOKED_SLOTS_KEY 					=> $this->booked_slots,
				AVAILABLE_SLOTS_KEY                                     => $this->available_slots,
				MIN_COMMUTE_TIME_KEY                                    => $this->min_commute_time,
				INSTALLATION_TIME_KEY                                   => $this->installation_time,
				LAST_AVAILABLE_START_TIME_KEY                           => $this->last_available_start_time,
				MAX_APPOINTMENTS                                        => $this->max_appoints,
				HOLIDAYS_KEY						=> $this->holidays,
// 				SHIFT_END_TIME_KEY					=> $this->shift_end_time
		);
		$this->appointments = (object)$this->appointments;
	}
		
}