<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_holiday extends MY_Model{
	public $holiday;
	private $name;
	private $date;

	public function __construct() {
		parent::__construct();
	}

	public function make_object($name, $date) {
		$this->name = $name;
		$this->date = $date;
		$this->holiday = array(HOLIDAY_NAME_KEY => $this->name, HOLIDAY_DATE_KEY => $this->date);
		$this->holiday = (object)$this->holiday;
	}

}