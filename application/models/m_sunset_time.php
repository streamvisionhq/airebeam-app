<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_sunset_time extends MY_Model {
	public function __construct() {
		parent::__construct();
	}
	
	public function FetchSunsetTime($params) {
		$response = false;
		if( isset($params['lat']) && isset($params['lng']) && isset($params['date']) ){
			$this->load->model("M_curl");
			$this->curl = new M_curl();
// 			$url = 'https://api.sunrise-sunset.org/json?lat=36.7201600&lng=-4.4203400&date=2018-01-05&formatted=0';
			$url = 'https://api.sunrise-sunset.org/json?lat=' . 
					$params['lat'] . '&lng=' . $params['lng']. 
					'&date=' . $params['date'] . '&formatted=0';
			$response = $this->curl->curlExecute( $url );
			$response = json_decode($response, true);
		}
		return $response; 
	}
	
	/*
	 * Get last date of data
	 */
	public function getLastDate() {
		$query = $this->db->select_max('date');
		$query = $query->get('sunset_time');
		$result = $query->result();
		return $result;
	}
	
	/*
	 * Get sunset time by date range
	 */
	public function getDataByDateRange( $start_date,  $end_date ) {
		$query = $this->db->where('date >=', $start_date);
		$query = $this->db->where('date <=', $end_date);
		$query = $query->get('sunset_time');
		$result = $query->result();
		return $result;
	}
	/*
	 * Filter sunset time array
	 */
	public function FilterSunsetTimeByDate($time_array, $date) {
		$sunset_time_obj = NULL;
		foreach ($time_array as $time) {
			if ($time->Date == $date){
				$sunset_time_obj = $time;
				break;
			}
		}
		return $sunset_time_obj;
	}
	
	/*
	 * Get current date sunset time
	 */
	public function getDataByDate( $date ) {
		$query = $this->db->where('date', $date);
		$query = $query->get('sunset_time');
		$result = $query->result();
		return $result;
	}
	
	public function deleteSunsetTimeByDate($date) {
		$condtions_array = array('date <=' => $date);
		$query = $this->db->where($condtions_array);
		$result = $query->delete('sunset_time');
		return $result;
	}
	
	
	/**
	 * @param start_time
	 * @param current_date
	 * @param next_fifteen_date
	 * @param sunset_time_params
	 * @param response
	 * @param data
	 * @param result
	 */
	public function StoreInDB($start_time_config_value, $start_date, $end_date, $notify_email) {
		$error_flag = false;
		$this->load->model ( "M_admin" );
		$this->admin = new M_admin ();
		$prepare_sunset_data = array ();
		for($i = $start_date; $i <= $end_date; $i->modify ( '+1 day' )) {
			$sunset_time_params = array (
					'lat' => '34.0489281',
					'lng' => '-111.0937311',
					'date' => $i->format ( "Y-m-d" )
			);
			$response = $this->FetchSunsetTime ( $sunset_time_params );
			if( !is_array($response) ){
				$error_flag = true;
				break;
			}
			$sunset_time = $response ["results"] ["sunset"];
			$datetime = new DateTime($sunset_time);
			$timezone = 'America/Phoenix';
			$datetime_format_str = 'H:i:s';
			$datetime_str = convertToArizonaDateTime($datetime, $timezone, $datetime_format_str);
			$data = array (
					'StartTime' => $start_time_config_value,
					'EndTime' => $datetime_str,
					'Date' => $i->format ( "Y-m-d" )
			);
			array_push ( $prepare_sunset_data, $data );
		}
		if ($error_flag === false && count ( $prepare_sunset_data )) {
			$result = $this->admin->InsertSunsetTime ( $prepare_sunset_data );
			echo SUNSET_TIME_INSERT_SUCCESS;
		} else {
			$String = SUNSET_LOG_WARNING;
			$this->logSunsetTime($String);
			$subject = SUNSET_LOG_API_FAILED_MSG;
			$body    = SUNSET_LOG_API_EMAIL_MSG;
			$from	 = SUNSET_LOG_API_FROM;
			sendEmail($notify_email, $subject, $body, $from);
		}
	}
}