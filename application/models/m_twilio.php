<?php

/**
 * this class has methods that are related to admin
 * like change password,all workers detail,add edit worker,admin & CSR login
 * customer detail,all colors list for worker, assign a color to worker
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class M_twilio extends MY_Model {

    public function __construct() {
        parent::__construct();
    }

    public function updateMessageStatus($orderID, $status) {
        $postData = array("MessageStatus" => $status);
        $where = array("OrderID" => $orderID);
        $this->UpdateAll("message_sent_recieved", $postData, $where);
    }

    public function checkPhoneNumber($phone) {
        $this->load->library('twilio');
        return $this->twilio->lookupNumber($phone);
    }

    public function getPhoneNumberType($phone) {
        $phone = simplizePhone($phone);
        $isPhoneHomeLandline = "0";
        if($phone == "0"){
            return $isPhoneHomeLandline;
        }
        $res = $this->checkPhoneNumber($phone);
        if ($res->HttpStatus == "200") {
            $lookup_response = json_decode($res->ResponseText);
            $phone_type = $lookup_response->carrier->type;
            if ($phone_type == "landline") {
                $isPhoneHomeLandline = "1";
            }
        }
        return $isPhoneHomeLandline;
    }

}
