<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_worker_appointments extends MY_Model{
	public $appointments;
	private $technician_worker;
	private $booked_slots;
	
	public function __construct() {
		parent::__construct();
	}
	
	public function make_object($worker, $booked_slots) {
		$this->technician_worker = $worker;
		$this->booked_slots = $booked_slots;
		$this->appointments = array(WORKER_ID_KEY => $this->technician_worker->WorkerID, 
									WORKER_NAME_KEY => $this->technician_worker->Name, 
									WORKER_EMAIL_KEY => $this->technician_worker->Email,
									WORKER_PRIORITY_KEY => $this->technician_worker->Priority,
									WORKER_BOOKED_SLOTS_KEY => $booked_slots);
		$this->appointments = (object)$this->appointments;
	}
		
}