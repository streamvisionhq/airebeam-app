<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_appointment extends MY_Model{
	public $appointment;
	private $start_time;
	private $end_time;
	private $appointment_location;
	
	public function __construct() {
		parent::__construct();
	}
	
	public function make_object($start_time, $end_time, $appointment_location = false) {
		$this->start_time = $start_time;
		$this->end_time = $end_time;
		$this->appointment_location = $appointment_location;
		$this->appointment = array(START_TIME_KEY => $this->start_time, END_TIME_KEY => $this->end_time, 
				APPOINTMENT_LOCATION_KEY => $this->appointment_location);
		$this->appointment = (object)$this->appointment;
	}
		
}