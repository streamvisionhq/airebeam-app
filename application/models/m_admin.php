<?php

/**
 * this class has methods that are related to admin
 * like change password,all workers detail,add edit worker,admin & CSR login
 * customer detail,all colors list for worker, assign a color to worker
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class M_admin extends MY_Model {

    public function __construct() {
        parent::__construct();
    }

    public function ChangePassword() {
        $oldPassword = $this->input->post("oldpassword");
        $password = $this->input->post("password");
        $confirmPassword = $this->input->post("confirmpassword");

        // check if all three fields are filled
        if (!empty($oldPassword) && !empty($password) && !empty($confirmPassword)) {
            // check if password & confirm password are matched
            if ($password == $confirmPassword) {

                // if RoleID is in session
                if ($this->session->userdata('RoleID')) {

                    $name = $this->getRoleName($this->session->userdata('RoleID'));

                    if ($name == ADMINISTRATOR || $name == CSR || $name == WORKER || $name == INSITE) {
                        $workerID = $this->isAdminLogin();
                    } else {
                        $workerID = $this->isWorkerLogin();
                    }
                }

                $rows = $this->SelectAllWhere("worker", array("StatusID" => "44cde42d87552", "WorkerID" => $workerID));
                $numberOfRecords = $rows->num_rows();
                $result = $rows->first_row();
                if (password_verify($oldPassword, $result->Password)) {
                    $this->UpdateAll("worker", array("Password" => $this->passwordEncrypt($password)), array("StatusID" => "44cde42d87552", "WorkerID" => $workerID));
                } else {
                    return "Password not match";
                }
            } else {
                return "Password not match with confirm password";
            }
        } else {
            return ALL_REQUIRED;
        }
    }

    public function GetConfigSettings($key_names = null) {
        if (!is_null($key_names) && is_array($key_names)) {
            $result = $this->db->where_in('Name', $key_names)->get('config')->result();
        } else {
            $result = $this->db->get('config')->result();
        }
        return $result;
    }

    public function UpdateConfigSettings($data) {
        $this->db->update_batch('config', $data, 'Name');
        return true;
    }

    public function RemoveZip($zipCode = null) {
        $result = false;
        if (!is_null($zipCode)) {
            $result = $this->db->delete('zip_codes', array('ZipCodeId' => $zipCode));
        }
        return $result;
    }

    public function BatchRemoveZipPriorityMapping($zipIdArray) {
        $this->db->where_in('ZipCodeId', $zipIdArray);
        $result = $this->db->delete('zip_user_map');
        return $result;
    }

    public function RemoveZipPriorityMapping($zipCode = null) {
        if (!is_null($zipCode)) {
            $result = $this->db->delete('zip_user_map', array('ZipCodeId' => $zipCode));
        } else {
            $result = $this->db->delete('zip_user_map');
        }
        return $result;
    }

    public function InsertSunsetTime($sunsetTime) {
        $result = $this->db->insert_batch('sunset_time', $sunsetTime);
        return $result;
    }

    public function InsertZipUserMapping($userMapping) {
        $result = $this->db->insert_batch('zip_user_map', $userMapping);
        return $result;
    }

    public function UpdateZipCode($zipCodeId, $zipCode) {
        $data = array(
            'Code' => $zipCode,
        );

        $this->db->where('ZipCodeId', $zipCodeId);
        $result = $this->db->update('zip_codes', $data);
        return $result;
    }

    public function UpdateTempMbr($requestedId, $data) {
        $this->db->where('Id', $requestedId);
        $result = $this->db->update('mbr_request_log', $data);
        echo $this->db->last_query();
        return $result;
    }

    public function GetUnprocessedOrdersByCustomer($customer_id) {
        $this->db->select('*');
        $this->db->from('temp_order');
        $where_array = array('IsProcessed' => ORDER_NOT_PROCESSED, 'CustomerID' => $customer_id);
        $this->db->where($where_array);
        $result = $this->db->get()->result();
        return $result;
    }

    public function UpdateUnprocessedOrders($orderIds) {
        $data = array(
            'IsProcessed' => ORDER_PROCESSED,
        );

        $this->db->where_in('OrderID', $orderIds);
        $result = $this->db->update('temp_order', $data);
        return $result;
    }

    public function InsertUnProcessedOrders($data) {
        $result = $this->db->insert_batch('order', $data);
        return $result;
    }

    public function IsZipCodeExist($zipCode) {
        $this->db->select('Code');
        $this->db->from('zip_codes');
        $this->db->where('Code', $zipCode);
        $result = $this->db->get()->result();
        return $result;
    }

    public function IsWorkerExist($workerId) {
        $this->db->select('WorkerID');
        $this->db->from('worker');
        $this->db->where('WorkerID', $workerId);
        $result = $this->db->get()->result();
        return $result;
    }

    public function AllTechniciansMappingDetail() {
        $SCHEDULER_ROLES = config_item('SCHEDULER_ROLES');
        $sql = "SELECT
                    worker.WorkerID,worker.Email,worker.`Name`,worker.`Password`,worker.Date,
                    worker.RoleID,worker.Color,worker.APIKey,worker.SortOrder,role.`Name` AS Role,
		    zip_user_map .ZipCodeId, zip_user_map.Priority
                FROM worker
                INNER JOIN role ON worker.RoleID = role.RoleID
		INNER JOIN zip_user_map ON worker.WorkerID = zip_user_map.WorkerID
                WHERE worker.StatusID != '" . DELETE_STATUS . "' AND worker.RoleID = '" . $SCHEDULER_ROLES["Technician"] . "'";
        $rows = $this->db->query($sql);
        $numberOfRecords = $rows->num_rows();
        $result = $rows->result();
        if ($numberOfRecords > 0) {
            return $result;
        } else {
            return false;
        }
    }

    public function AllTechniciansDetail() {
        $SCHEDULER_ROLES = config_item('SCHEDULER_ROLES');
        $sql = "SELECT
                    worker.WorkerID,worker.Email,worker.`Name`,worker.`Password`,worker.Date,
                    worker.RoleID,worker.Color,worker.APIKey,worker.SortOrder,role.`Name` AS Role
                FROM worker
                    INNER JOIN role ON worker.RoleID = role.RoleID
                WHERE worker.StatusID != '" . DELETE_STATUS . "' AND worker.RoleID = '" . $SCHEDULER_ROLES["Technician"] . "'";
        $rows = $this->db->query($sql);
        $numberOfRecords = $rows->num_rows();
        $result = $rows->result();

        if ($numberOfRecords > 0) {
            return $result;
        } else {
            return false;
        }
    }

    public function AllTechniciansDetailByZipCode($zip_code) {
        $SCHEDULER_ROLES = config_item('SCHEDULER_ROLES');
        $sql = "SELECT 
    			worker.WorkerID,worker.Email,worker.`Name`,worker.`Password`,worker.Date,
                worker.RoleID,worker.Color,worker.APIKey,worker.SortOrder,role.`Name` AS Role,
    			zip_codes.Code, zip_codes.ZipCodeId, zip_user_map.Priority
				FROM worker
				INNER JOIN role ON worker.RoleID = role.RoleID 
				INNER JOIN zip_user_map ON worker.WorkerID = zip_user_map.WorkerID 
				INNER JOIN zip_codes ON zip_codes.ZipCodeId = zip_user_map.ZipCodeId 
				WHERE worker.StatusID != '" . DELETE_STATUS . "' AND 
				worker.RoleID = '" . $SCHEDULER_ROLES["Technician"] . "' AND
				zip_codes.Code = " . $zip_code . " AND zip_user_map.Priority > 0";

        $rows = $this->db->query($sql);
        $numberOfRecords = $rows->num_rows();
        $result = $rows->result();

        if ($numberOfRecords > 0) {
            return $result;
        } else {
            return false;
        }
    }

    public function AllZipDetail() {
        $sql = "SELECT
                    zip_codes.ZipCodeId, zip_codes.Code
                FROM zip_codes
                    ";
        $rows = $this->db->query($sql);
        $numberOfRecords = $rows->num_rows();
        $result = $rows->result();
        if ($numberOfRecords > 0) {
            return $result;
        } else {
            return false;
        }
    }

    public function AllWorkerDetail() {
        $sql = "SELECT
                    worker.WorkerID,worker.Email,worker.`Name`,worker.`Password`,worker.Date,
                    worker.RoleID,worker.Color,worker.APIKey,worker.SortOrder,role.`Name` AS Role
                FROM worker
                    INNER JOIN role ON worker.RoleID = role.RoleID
                WHERE worker.StatusID != '" . DELETE_STATUS . "'";
        $rows = $this->db->query($sql);
        $numberOfRecords = $rows->num_rows();
        $result = $rows->result();
        if ($numberOfRecords > 0) {
            return $result;
        } else {
            return false;
        }
    }

    public function AddZipCode() {
        $zipCode = $this->input->post("zip_code");
        if (!empty($zipCode)) {
            if (ctype_digit($zipCode)) {
                $Array = array(
                    "Code" => $zipCode,
                );
                if ($this->Insert("zip_codes", $Array)) {
                    return TRUE;
                } else {
                    return ZIP_CODE_FAILED;
                }
            } else {
                return ZIP_CODE_NUMBERS_ERROR;
            }
        }
    }

    public function InsertMBRRequestParams($address, $email, $zip, $phoneHome, $city, $state, $firstName, $lastName) {
        $Array = array(
            "Address" => $address,
            "Email" => $email,
            "Zip" => $zip,
            "PhoneHome" => $phoneHome,
            "City" => $city,
            "State" => $state,
            "FirstName" => $firstName,
            "LastName" => $lastName,
            "Request" => '',
        );
        if ($this->Insert("mbr_request_log", $Array)) {
            return TRUE;
        } else {
            return MBR_LOG_FAILED;
        }
    }

    public function GetMBRLogByTempId($temp_id) {
        $data = array('Id' => $temp_id);
        $this->db->select('*');
        $this->db->from('mbr_request_log');
        $this->db->where($data);
        $result = $this->db->get()->result();
        return $result;
    }

    public function GetMBRLogId($address, $zip, $phoneHome, $email) {
        $data = array('Address' => $address, 'Email' => $email);
        $this->db->select('*');
        $this->db->from('mbr_request_log');
        $this->db->where($data);
        $result = $this->db->get()->result();
        return $result;
    }

    public function AddWorker() {
        $SCHEDULER_ROLES = config_item('SCHEDULER_ROLES');
        $email = $this->input->post("email");
        $name = $this->input->post("name");
        $password = $this->input->post("password");
        $role = $this->input->post("role");
        $api = $this->input->post("api-key");
        $address = $role == $SCHEDULER_ROLES["Technician"] ? $this->input->post("address") : '';
        $lat = $role == $SCHEDULER_ROLES["Technician"] ? $this->input->post("lat") : 0;
        $lng = $role == $SCHEDULER_ROLES["Technician"] ? $this->input->post("lng") : 0;
        $place_icon = $role == $SCHEDULER_ROLES["Technician"] ? $this->input->post("place_icon") : '';
        $place_name = $role == $SCHEDULER_ROLES["Technician"] ? $this->input->post("place_name") : '';
        $place_address = $role == $SCHEDULER_ROLES["Technician"] ? $this->input->post("place_address") : '';

        if (!empty($email) && !empty($name) && !empty($password) && !empty($role)) {
            if (strlen($name) > 3 && strlen($name) < 50) {
                if (strlen($password) >= 6) {
                    $emailExists = $this->checkExistWorker($email);
                    if ($emailExists === false) {
                        if (!filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
                            $color = $this->assignColorToWorker();
                            $Array = array(
                                "WorkerID" => uniqid(),
                                "Email" => $email,
                                "Name" => $name,
                                "Password" => $this->passwordEncrypt($password),
                                "RoleID" => $role,
                                "Color" => $color,
                                "Date" => date("Y-m-d"),
                                "StatusID" => "44cde42d87552",
                                "APIKey" => $api,
                                "Address" => $address,
                                "Lat" => $lat != '' ? $lat : null,
                                "Lng" => $lng != '' ? $lng : null,
                                "PlaceIcon" => $place_icon != '' ? $place_icon : null,
                                "PlaceName" => $place_name != '' ? $place_name : null,
                                "PlaceAddress" => $place_address != '' ? $place_address : null
                            );
                            if ($this->Insert("worker", $Array)) {
                                return TRUE;
                            } else {
                                return WORKER_FAILED;
                            }
                        } else {
                            return "$email is not a valid email address";
                        }
                    } else {
                        return "$email is already Exists";
                    }
                } else {
                    return "Password should be greater then 6 characters";
                }
            } else {
                return "Full name should be between 3 to 50 characters";
            }
        } else {
            return ALL_REQUIRED;
        }
    }

    public function addOrderService() {
        if ($this->input->post("service") && $this->input->post("abbr")) {
            if (!$this->checkServiceByName($this->input->post("service"))) {
                if (!$this->checkServiceByAlias($this->input->post("abbr"))) {
                    $Array = array(
                        "TypeID" => uniqid(),
                        "Name" => $this->input->post("service"),
                        "Alias" => $this->input->post("abbr"),
                        "Date" => date("Y-m-d h:i:s"),
                        "StatusID" => "44cde42d87552"
                    );
                    if ($this->Insert("order_type", $Array)) {
                        return TRUE;
                    } else {
                        return SERVICE_FAILED;
                    }
                } else {
                    return SERVICE_CODE_ALREADY_EXISTS;
                }
            } else {
                return SERVICE_NAME_ALREADY_EXISTS;
            }
        } else {
            return ALL_REQUIRED;
        }
    }

    public function global_search($data) {
        $global = isset($data['global']) ? $data['global'] : 'null';
        $start_date = isset($data['start_date']) ? $data['start_date'] : null;
        $end_date = isset($data['end_date']) ? $data['end_date'] : null;
        $time_range_clause = '';
        if ($end_date != null && $start_date != null) {
            $time_range_clause = " AND `order`.`Date` BETWEEN '{$start_date}' AND '{$end_date}'";
        } else if ($start_date != null) {
            $time_range_clause = " AND `order`.`Date` >= '{$start_date}'";
        } else if ($end_date != null) {
            $time_range_clause = " AND `order`.`Date` <= '{$end_date}'";
        }
        $like_clause = '';
        if ($global != 'null') {
            $i = 0;
            foreach ($global as $key => $word) {
                if ($word != '') {
                    if ($i != 0) {
                        $like_clause .= ' OR ';
                    }
                    if ($i == 0) {
                        $like_clause .= 'WHERE (';
                    }
                    $like_clause .= "(`order_type`.Name LIKE '%{$word}%' OR `worker`.Name LIKE '%{$word}%' OR `customer`.CustomerName LIKE '%{$word}%' OR `order`.`Detail` LIKE '%{$word}%' "
                            . "OR `customer`.`Email` LIKE '%{$word}%' OR `customer`.`PhoneHome` LIKE '%{$word}%' OR `customer`.`Address1` LIKE '%{$word}%')";
                    if (!isset($global[$key + 1])) {
                        $like_clause .= ')';
                    }
                    $i++;
                }
            }
        }
        $sql = "SELECT `order`.`Name`  as 'orderName',`order`.`Date`,`order`.`OrderID`,`customer`.`City`,`order_type`.`Name`"
                . " from `order` inner join `customer` on `customer`.`CustomerID` = `order`."
                . "`CustomerID` inner join `order_type` on `order_type`.`TypeID` = `order`.`TypeID` inner join `worker` on `worker`.WorkerID = `order`.WorkerID"
                . " {$like_clause}";
        $sql .= $time_range_clause . ' order by `order`.`Date` DESC';
        $rows = $this->db->query($sql);
        $numberOfRecords = $rows->num_rows();
        $result = $rows->result();

        if ($numberOfRecords > 0) {
            return $result;
        } else {
            return false;
        }
    }

    public function addTown() {
        if ($this->input->post("town") && $this->input->post("abbr")) {
            if (!$this->checkTownByName($this->input->post("town"))) {
                if (!$this->checkTownByAlias($this->input->post("abbr"))) {
                    $Array = array(
                        "TownID" => uniqid(),
                        "Name" => $this->input->post("town"),
                        "Alias" => $this->input->post("abbr"),
                        "Date" => date("Y-m-d h:i:s"),
                        "StatusID" => "44cde42d87552"
                    );
                    if ($this->Insert("towns", $Array)) {
                        $this->UpdateAll("customer", array("CityCode" => $Array["Alias"]), array("City" => $Array["Name"]));
                        return TRUE;
                    } else {
                        return TOWN_FAILED;
                    }
                } else {
                    return TOWN_CODE_ALREADY_EXISTS;
                }
            } else {
                return TOWN_NAME_ALREADY_EXISTS;
            }
        } else {
            return ALL_REQUIRED;
        }
    }

    /**
     * @note get all order services
     * @return array if TRUE else false
     */
    public function getAllOrderServices() {
        $rows = $this->SelectAllWhere("order_type", array("StatusID" => "44cde42d87552"));
        if ($rows->num_rows() > 0) {
            return $rows->result();
        } else {
            return false;
        }
    }

    /**
     * @note get all towns
     * @return array if TRUE else false
     */
    public function getAllTowns() {
        $rows = $this->SelectAllWhere("towns", array("StatusID" => "44cde42d87552"));
        if ($rows->num_rows() > 0) {
            return $rows->result();
        } else {
            return false;
        }
    }

    /**
     * @note get single order services
     * @return array if TRUE else false
     */
    public function getOrderService($TypeID) {
        $rows = $this->SelectAllWhere("order_type", array("TypeID" => $TypeID));
        if ($rows->num_rows() > 0) {
            return $rows->first_row('array');
        } else {
            return false;
        }
    }

    /**
     * @note get single order services
     * @return array if TRUE else false
     */
    public function getTown($TownID) {
        $rows = $this->SelectAllWhere("towns", array("TownID" => $TownID));
        if ($rows->num_rows() > 0) {
            return $rows->first_row('array');
        } else {
            return false;
        }
    }

    public function checkTownByName($name) {
        $data = array("Name" => $name, "StatusID" => "44cde42d87552");
        $rows = $this->SelectAllWhere("towns", $data);
        if ($rows->num_rows() > 0) {
            return $rows->first_row('array');
        } else {
            return false;
        }
    }

    public function checkServiceByName($name) {
        $data = array("Name" => $name, "StatusID" => "44cde42d87552");
        $rows = $this->SelectAllWhere("order_type", $data);
        if ($rows->num_rows() > 0) {
            return $rows->first_row('array');
        } else {
            return false;
        }
    }

    public function checkTownByAlias($alias) {
        $data = array("Alias" => $alias, "StatusID" => "44cde42d87552");
        $rows = $this->SelectAllWhere("towns", $data);
        if ($rows->num_rows() > 0) {
            return $rows->first_row('array');
        } else {
            return false;
        }
    }

    public function checkServiceByAlias($alias) {
        $data = array("Alias" => $alias, "StatusID" => "44cde42d87552");
        $rows = $this->SelectAllWhere("order_type", $data);
        if ($rows->num_rows() > 0) {
            return $rows->first_row('array');
        } else {
            return false;
        }
    }

    public function editWorker($workerID) {
        $SCHEDULER_ROLES = config_item('SCHEDULER_ROLES');
        $email = $this->input->post("email");
        $name = $this->input->post("name");
        $password = $this->input->post("password");
        $role = $this->input->post("role");
        $roleName = $this->getRoleName($role);
        $api = $this->input->post("api-key");
        $address = $role == $SCHEDULER_ROLES["Technician"] ? $this->input->post("address") : '';
        $lat = $role == $SCHEDULER_ROLES["Technician"] ? $this->input->post("lat") : '';
        $lng = $role == $SCHEDULER_ROLES["Technician"] ? $this->input->post("lng") : '';
        $place_icon = $role == $SCHEDULER_ROLES["Technician"] ? $this->input->post("place_icon") : '';
        $place_name = $role == $SCHEDULER_ROLES["Technician"] ? $this->input->post("place_name") : '';
        $place_address = $role == $SCHEDULER_ROLES["Technician"] ? $this->input->post("place_address") : '';

        if (!empty($email) && !empty($name) && !empty($role)) {
            $wRows = $this->SelectAllWhere("worker", array("WorkerID" => $workerID));
            if ($wRows->num_rows() == 1) {
                $regex = '/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/';
                if (preg_match($regex, $email)) {
                    $result = $wRows->first_row();
                    if ($result->Email != $email) {
                        $emailExists = $this->checkExistWorker($email);
                    } else {
                        $emailExists = TRUE;
                    }
                    if (empty($emailExists) || $emailExists === TRUE) {
                        $array = array(
                            "Email" => $email,
                            "Name" => $name,
                            "Date" => date("Y-m-d"),
                            "RoleID" => $role,
                            "APIKey" => $api,
                            "SortOrder" => NULL,
                            "Address" => $address,
                            "Lat" => $lat,
                            "Lng" => $lng,
                            "PlaceIcon" => $place_icon,
                            "PlaceName" => $place_name,
                            "PlaceAddress" => $place_address
                        );
                        if ($this->UpdateAll("worker", $array, array("WorkerID" => $workerID))) {
                            if (!empty($password)) {
                                $passwordArray = array(
                                    "Password" => $this->passwordEncrypt($password)
                                );
                                $this->UpdateAll("worker", $passwordArray, array("WorkerID" => $workerID));
                            }
                            return TRUE;
                        } else {
                            return RECORD_NOT_UPDATED;
                        }
                    } else {
                        return "$email is already exists";
                    }
                } else {
                    return "$email is not a valid email address";
                }
            } else {
                return "$email is already Exists";
            }
        } else {
            return ALL_REQUIRED;
        }
    }

    public function editWorkerNumber($workerID) {
        $array = array("SortOrder" => NULL);
        if (preg_match("/^[0-9]+$/", $this->input->post("num"))) {
            if ($this->input->post("num") != "" && $this->input->post("num") != "0") {
                $row = $this->SelectAllWhere("worker", array("SortOrder" => $this->input->post("num"), "RoleID" => "56a5c10c469f7", "StatusID" => "44cde42d87552"));
                $array = array("SortOrder" => $this->input->post("num"));
                if ($row->num_rows() > 0) {
                    $error = "This number is already in use";
                }
            }
        } else {
            $error = "Value should be numeric";
        }
        if (empty($error)) {
            if ($this->UpdateAll("worker", $array, array("WorkerID" => $this->input->post("workerID")))) {
                return true;
            } else {
                $error = "Error";
            }
        } else {
            return $error;
        }
    }

    public function editOrderService($orderServiceID) {
        if ($this->input->post("service") && $this->input->post("abbr")) {
            $array = array(
                "Name" => $this->input->post("service"),
                "Alias" => $this->input->post("abbr"),
            );
            if ($this->UpdateAll("order_type", $array, array("TypeID" => $orderServiceID))) {
                return TRUE;
            } else {
                return RECORD_NOT_UPDATED;
            }
        } else {
            return ALL_REQUIRED;
        }
    }

    public function editTown($townID) {
        $town = $this->getTown($townID);
        if ($this->input->post("town") && $this->input->post("abbr")) {
            $array = array(
                "Name" => $this->input->post("town"),
                "Alias" => $this->input->post("abbr"),
            );
            if ($this->UpdateAll("towns", $array, array("TownID" => $townID))) {
                if ($town["Name"] == $array["Name"]) {
                    $this->UpdateAll("customer", array("CityCode" => $array["Alias"]), array("City" => $town["Name"]));
                } else {
                    $this->UpdateAll("customer", array("CityCode" => NULL), array("City" => $town["Name"]));
                    $this->UpdateAll("customer", array("CityCode" => $array["Alias"]), array("City" => $array["Name"]));
                }
                return TRUE;
            } else {
                return RECORD_NOT_UPDATED;
            }
        } else {
            return ALL_REQUIRED;
        }
    }

    public function getCustomerDetailByName($customerName) {
        $this->db->select('*');
        $this->db->from('customer AS c'); // I use aliasing make joins easier
        $this->db->join('service AS s', 'c.CustomerID = s.CustomerID', 'INNER');
        $this->db->where('c.CustomerName', $customerName);
        $array = $this->db->get()->result_array();
        if ($array) {
            return $array;
        } else {
            return false;
        }
    }

    public function searchCustomerByName($Keyword) {
        $this->db->select('CustomerName');
        $this->db->from('customer');
        $this->db->like('CustomerName', $Keyword);
        $result = $this->db->get()->result_array();
        if ($result) {
            return $result;
        } else {
            return false;
        }
    }

    /**
     * @note get login form values and match in database
     * if creadentials match then redirect to orders listing page
     * if not then show error
     * @return array
     */
    public function doAdminLogin() {
        if ($this->input->post("submit")) {

            $email = $this->input->post("email");
            $password = $this->input->post("password");
            $data = $this->checkExistWorker($email);
            if ($data) {
                if (password_verify($password, $data->Password)) {
                    if ($data->RoleID) {
                        $role = $this->getRole($data->WorkerID);
                        $items = array(
                            'AdminID' => $data->WorkerID,
                            "RoleID" => $role,
                        );
                        $this->session->set_userdata($items);
                        return array("response" => TRUE, "Msg" => SUCCSESS);
                    }
                } else {
                    return array("response" => false, "Msg" => ERROR);
                }
            } else {
                return array("response" => false, "Msg" => ERROR);
            }
        }
    }

    public function getAllColorsFromDB() {
        $sql = "SELECT worker.Color FROM worker";
        $rows = $this->db->query($sql);
        $result = $rows->result_array();
        foreach ($result as $color) {
            $colors[] = $color["Color"];
        }
        return $colors;
    }

    public function colorsList() {
        $colorArray = array(
            "rgb(153, 204, 255)", "rgb(1, 169, 169)", "rgb(102, 204, 102)", "rgb(0, 106, 215)",
            "rgb(238, 112, 0)", "rgb(211, 105, 0)", "rgb(2, 182, 112)", "rgb(255, 153, 255)",
            "rgb(153, 102, 204)", "rgb(102, 153, 255)", "rgb(56, 142, 142)", "rgb(0, 204, 0)",
            "rgb(102, 173, 66)", "rgb(160, 160, 0)", "rgb(255, 204, 102)", "rgb(255, 102, 102)",
            "rgb(116, 55, 1)", "rgb(204, 102, 255)", "rgb(51, 102, 255)", "rgb(0, 204, 255)",
            "rgb(51, 153, 0)", "rgb(255, 3, 80)", "rgb(204, 204, 51)", "rgb(255, 153, 0)",
            "rgb(204, 0, 204)", "rgb(153, 0, 204)", "rgb(51, 102, 204)",
            "rgb(51, 204, 204)", "rgb(102, 153, 51)", "rgb(102, 204, 102)", "rgb(153, 153, 0)",
            "rgb(255, 102, 0)", "rgb(153, 0, 0)", "rgb(153, 0, 153)", "rgb(102, 0, 153)",
            "rgb(0, 51, 153)", "rgb(0, 153, 153)", "rgb(51, 102, 0)", "rgb(102, 153, 102)",
            "rgb(102, 102, 0)", "rgb(153, 51, 0)", "rgb(102, 0, 0)"
        );
        return $colorArray;
    }

    public function assignColorToWorker() {
        $colors = $this->getAllColorsFromDB();
        $colorsArray = $this->colorsList();
        $checkValues = array_diff($colorsArray, $colors);
        reset($checkValues);
        $first = current($checkValues);
        return $first;
    }

    public function getAllMessages() {
        $data = array("MessageStatus" => "44cde42d87552");
        $rows = $this->SelectAllWhere("message", $data);
        if ($rows->num_rows() > 0) {
            return $rows->result();
        } else {
            return false;
        }
    }

    public function updateMessage($array, $messageID) {
        if ($this->UpdateAll("message", $array, array("MessageTypeID" => $messageID))) {
            return true;
        } else {
            return false;
        }
    }

}
