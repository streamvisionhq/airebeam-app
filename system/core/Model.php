<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class CI_Model {

    protected $worker;

    public function __construct() {
        log_message('info', 'Model Class Initialized');
    }

    public function __get($key) {
        return get_instance()->$key;
    }

    /**
     * @name $Insert
     * Description: Insert data into the table required params
     * @param string $tbl table name in string
     * @param array $PostData  data for save
     * @return boolean 
     */
    function Insert($tbl, $PostData) {
        $this->db->update("update_table", array("LastUpdated" => date("Y-m-d H:i:s")));
        return $this->db->insert($tbl, $PostData);
    }

    /**
     * @name $SelectAll
     * @description select all the data from the table
     * @param string $tbl table name in string
     * @return mysql result array of arrays 
     */
    function SelectAll($tbl) {
        return $this->db->get($tbl);
    }

    /**
     * 
     * @param string $tbl table name in string
     * @param array $where filter by Entity coloum name with value
     * @param int $limit (Nullable not required)limit the records
     * @param int $offset (Nullable not required) pointer the  
     * @return mysql result array of arrays
     */
    function SelectAllWhere($tbl, $where, $limit = NULL, $offset = NULL) {
        return $this->db->get_where($tbl, $where, $limit, $offset);
    }

    /**
     * 
     * @param string $tbl table name in string
     * @param array $columns Entity columns name in string
     * @return mysql result array of arrays
     */
    function SelectSelected($tbl, $columns) {
        $this->db->select($columns);
        return $this->db->get($tbl);
    }

    /**
     * 
     * @param string $tbl table name in string
     * @param array $columns Entity Columns name
     * @param array $where filter by Entity coloum name with value
     * @return mysql result array of arrays
     */
    function SelectSelectedWhere($tbl, $columns, $where) {
        $this->db->select($columns);
        return $this->db->get_where($tbl, $columns, $where);
    }

    /**
     * 
     * @param string $tbl table name
     * @return int count of total records in a table
     */
    function SelectCount($tbl, $where = NULL) {
        if (isset($where)) {
            $this->db->where($where);
        }
        return $this->db->count_all_results($tbl);
    }

    /**
     * 
     * @param string $tbl table name
     * @param array $postData key value pair of Entity coloum with value
     * @param array $where update where selected colum with values
     * @return array Mysql result array
     */
    function UpdateAll($tbl, $postData, $where = NULL) {
        $this->db->update("update_table", array("LastUpdated" => date("Y-m-d H:i:s")));
        if (isset($where)) {
            $this->db->where($where);
        }
        return $this->db->update($tbl, $postData);
    }

    /**
     * 
     * @param string or Array $tbl table name
     * @param array $where
     * @note if you want to delete data from multiple table pass table name array
     *       as 1 parameter otherwise table name in string
     * @return boolean Status isDeleted of Not
     */
    function Delete($tbl, $where) {
        $this->db->update("update_table", array("LastUpdated" => date("Y-m-d H:i:s")));
        if (isset($where)) {
            $this->db->where($where);
        }
        return $this->db->delete($tbl);
    }

    /**
     * @param string $str
     * @note remove comma from string
     * @return boolean
     */
    public function RemoveComma($str) {
        return str_replace("'", "", $str);
    }

    /**
     * @note logout for worker
     */
    public function workerLogout() {
        $array_items = array('WorkerID' => '');
        $this->session->unset_userdata($array_items);
    }

    /**
     * @note logout for admin
     */
    public function adminLogout() {
        $array_items = array('AdminID' => '');
        $this->session->unset_userdata($array_items);
    }

    /**
     * @param string $Email
     * @note check if worker is exists with this email
     * @return array if success else return false
     */
    public function checkExistWorker($Email) {
        if ($Email) {
            $Rows = $this->SelectAllWhere("worker", array("Email" => $Email, "StatusID" => "44cde42d87552"));
            $NumberOfRecords = $Rows->num_rows();
            $Result = $Rows->first_row();
            if ($NumberOfRecords > 0) {
                return $Result;
            } else {
                return FALSE;
            }
        }
    }
    
    public function logTime($String) {
        $date = date("Y-m-d");
        $file = fopen('assets/logs-' . $date . '.txt', 'a');
        fwrite($file, $String . " " . date("y-m-d h:i:s A") . "\r\n");
        fclose($file);
    }

    /**
     * @param string $workerID
     * @note get worker detail by WorkerID
     * @return array if TRUE else FALSE
     */
    public function workerDetail($workerID) {
        if ($workerID) {
            $rows = $this->SelectAllWhere("worker", array("WorkerID" => $workerID));
            $numberOfRecords = $rows->num_rows();
            $result = $rows->first_row('array');
            if ($numberOfRecords > 0) {
                return $result;
            } else {
                return FALSE;
            }
        }
    }

    /**
     * @note get all roles
     * @return array if TRUE else FALSE
     */
    public function getRoles() {
        $rows = $this->SelectAll("role");
        $result = $rows->result_array();
        foreach ($result as $r) {
            $key = $r["RoleID"];
            $array[$key] = $r["Name"];
        }
        return $array;
    }

    public function getRoleName($roleID) {
        $roleRow = $this->SelectAllWhere("role", array("RoleID" => $roleID));
        $result = $roleRow->first_row('array');
        return $result["Name"];
    }

    /**
     * @param string $workerID
     * @note get role of a worker
     * @return array if TRUE else FALSE
     */
    public function getRole($workerID) {
        $rows = $this->SelectAllWhere("worker", array("WorkerID" => $workerID));
        $result = $rows->first_row('array');
        if ($result) {
            $roleRow = $this->SelectAllWhere("role", array("RoleID" => $result["RoleID"]));
            $result = $roleRow->first_row('array');
            return $result["RoleID"];
        } else {
            return FALSE;
        }
    }

    /**
     * @note get all order services
     * @return array if TRUE else FALSE
     */
    public function getServices() {
        $rows = $this->SelectAllWhere("order_type", array("StatusID" => "44cde42d87552"));
        $result = $rows->result_array();
        foreach ($result as $r) {
            $key = $r["TypeID"];
            $array[$key] = $r["Name"] . " - " . $r["Alias"];
        }
        if (isset($array)) {
            return $array;
        } else {
            return FALSE;
        }
    }

    /**
     * @note get all order statuses
     * @return array if TRUE else FALSE
     */
    public function getStatuses() {
        $sql = "SELECT * FROM order_status WHERE StatusID != '46b1e4fd15378'";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        foreach ($result as $r) {
            $key = $r["StatusID"];
            $array[$key] = $r["Name"];
        }
        if (isset($array)) {
            return $array;
        } else {
            return FALSE;
        }
    }

    public function getServiceTypeNameByID($typeID) {
        $row = $this->db->query("SELECT order_type.`Name` FROM order_type WHERE TypeID='$typeID' AND StatusID='44cde42d87552'");
        $result = $row->first_row("array");
        if (isset($result["Name"])) {
            return $result["Name"];
        }
    }

    /**
     * @param string $idStatus
     * @note get status detail by $idStatus
     * @return array if TRUE else FALSE
     */
    public function getStatus($idStatus) {
        $rows = $this->SelectAllWhere("order_status", array("StatusID" => $idStatus));
        $result = $rows->first_row('array');
        if ($result) {
            return $result;
        } else {
            return FALSE;
        }
    }

    /**
     * @param string $status
     * @note get status detail by status name
     * @return array if TRUE else FALSE
     */
    public function getStatusID($status) {
        $rows = $this->SelectAllWhere("order_status", array("Name" => $status));
        $result = $rows->first_row('array');
        if ($result) {
            return $result;
        } else {
            return FALSE;
        }
    }

    /**
     * @note get all workers
     * @return array if TRUE else FALSE
     */
    public function getWorkers() {
        $rows = $this->db->query("SELECT * FROM worker WHERE RoleID = '56a5c10c469f7' AND  StatusID = '44cde42d87552' ORDER BY SortOrder DESC, SortOrder ASC");
        $result = $rows->result_array();
        foreach ($result as $r) {
            $key = $r["WorkerID"];
            $array[$key] = $r["Name"];
        }
        if (isset($array)) {
            return $array;
        } else {
            return FALSE;
        }
    }

    /**
     * @note get worker detail
     * @return array if TRUE else FALSE
     */
    public function getWorkersDetail() {
        $rows = $this->SelectAllWhere("worker", array("RoleID" => "56a5c10c469f7"));
        $result = $rows->result_array();
        if (isset($result)) {
            return $result;
        } else {
            return FALSE;
        }
    }

    /**
     * @note get all customers
     * @return array if TRUE else FALSE
     */
    public function getCustomers() {
        $sql = "SELECT
                    customer.CustomerID,customer.CustomerName,customer.Email,
                    customer.Address1,customer.Address2,customer.City,customer.Zip,
                    customer.Date,customer.PhoneHome
                FROM customer";
        $rows = $this->db->query($sql);
        $result = $rows->result_array();
        foreach ($result as $r) {
            $key = $r["CustomerID"];
            $array[$key] = $r["CustomerName"];
        }
        if (isset($array)) {
            return $array;
        } else {
            return FALSE;
        }
    }

    /**
     * @note get all notes
     * @return array if TRUE else FALSE
     */
    public function getAllNotes() {
        $sql = "SELECT 
                    note.NoteID,note.Note,note.Date,note.OrderID,`order`.`Name`,note.Label,worker.`Name` AS Worker
                FROM note
                    INNER JOIN `order` ON note.OrderID = `order`.OrderID
                    INNER JOIN worker ON `order`.WorkerID = worker.WorkerID ORDER BY Date DESC";
        $rows = $this->db->query($sql);
        $result = $rows->result_array();
        if (isset($result)) {
            return $result;
        } else {
            return FALSE;
        }
    }

    /**
     * @note count all notes for pagination
     * @return string
     */
    public function countAllNotes() {
        $sql = "SELECT COUNT(note.NoteID) AS Total FROM note WHERE StatusID !='56b1e42d87332' ";
        $rows = $this->db->query($sql);
        $result = $rows->first_row('array');
        return $result["Total"];
    }

    /**
     * @note get notes by limit
     * @return array if TRUE else FALSE
     */
    public function getLimitNotes($records_per_page, $page) {
        $sql = "SELECT
                    note.NoteID,note.Note,note.Date,note.OrderID,`order`.`Name`,note.Label,worker.`Name` AS Worker
                FROM note
                    INNER JOIN `order` ON note.OrderID = `order`.OrderID
                    INNER JOIN worker ON note.WorkerID = worker.WorkerID
                WHERE note.StatusID != '56b1e42d87332'                    
                ORDER BY Date DESC LIMIT $records_per_page OFFSET $page";
        $rows = $this->db->query($sql);
        $result = $rows->result_array();
        if (isset($result)) {
            return $result;
        } else {
            return FALSE;
        }
    }

    /**
     * @note update all unread notes to read
     */
    public function updateAllNewNotes() {
        echo CLEAR_NEW_NOTES;
        $this->UpdateAll("note", array("Read" => 1), array("Read" => 0));
    }

    public function getAllNewNotesNumber() {
        $rows = $this->SelectAllWhere("note", array("Read" => 0, "StatusID" => "44cde42d87552"));
        if ($rows->num_rows() > 0) {
            return $rows->num_rows();
        } else {
            return FALSE;
        }
    }

    public function getAllAlertForCustomerCall() {
        $rows = $this->SelectAllWhere("alert", array("isVisible" => 1));
        if ($rows->num_rows() > 0) {
            return $rows->result('array');
        } else {
            return FALSE;
        }
    }

    // Failed Sms
    public function getAllFailedSMSForCustomer(){        
        $sql = "SELECT
                    alert.AlertID,alert.Message,alert.BehaviorID,alert.Date,`order`.`Name`,`order`.`OrderID`,`order`.`CustomerID`, `alert_log`.`Content`, `customer_notification_log`.`IsNotified`
                FROM alert
                    INNER JOIN `order` ON alert.ObjectID = `order`.OrderID
                    INNER JOIN `alert_log` ON alert.ObjectID = `alert_log`.AlertOrderID
                    INNER JOIN `customer_notification_log` ON alert.ObjectID = `customer_notification_log`.OrderID
                    WHERE alert.TypeID = '7c3d597bd72e4' AND alert.isVisible = 1
                    ORDER BY Date DESC";
        $rows = $this->db->query($sql);
        $result = $rows->result_array();
        if (isset($result)) {
            return $result;
        } else {
            return FALSE;
        }
    }

    public function getCustomerNotificationStatus($orderId, $customerId){
        $rows = $this->SelectAllWhere("customer_notification_log", array(
            "CustomerID" => $customerId,
            "OrderID" => $orderId
        ));
        if ($rows->num_rows() > 0) {
            return $rows->result('array');
        } else {
            return FALSE;
        }
    }

    public function getAllNewFailureSmsNumber() {
        $rows = $this->SelectAllWhere("alert_log", array("isRead" => 0));
        if ($rows->num_rows() > 0) {
            return $rows->num_rows();
        } else {
            return FALSE;
        }
    }
    
    /**
     * @note update all unread failure sms to read
     */
    public function updateAllNewFailureSms() {
        echo CLEAR_NEW_FAILURE_SMS;
        $this->UpdateAll("alert_log", array("isRead" => 1), array("isRead" => 0));
    }

    public function getNotesByOrder($orderID) {
        $sql = "SELECT
                    note.NoteID,note.Note,note.Date,note.OrderID,`order`.`Name`,note.Label,worker.`Name` AS Worker
                FROM note
                    INNER JOIN `order` ON note.OrderID = `order`.OrderID
                    INNER JOIN worker ON note.WorkerID = worker.WorkerID WHERE `order`.OrderID = '$orderID' ORDER BY Date DESC";
        $rows = $this->db->query($sql);
        $result = $rows->result_array();
        if (isset($result)) {
            return $result;
        } else {
            return FALSE;
        }
    }

    public function getNoteByID($noteID) {
        $sql = "SELECT
                    note.NoteID,note.Note,note.Date,note.OrderID,`order`.`Name`,note.Label,worker.`Name` AS Worker
                FROM note
                    INNER JOIN `order` ON note.OrderID = `order`.OrderID
                    INNER JOIN worker ON note.WorkerID = worker.WorkerID WHERE note.NoteID = '$noteID' ORDER BY Date DESC";
        $rows = $this->db->query($sql);
        $result = $rows->first_row('array');
        if ($result) {
            return $result;
        } else {
            return FALSE;
        }
    }

    public function getDurations() {
        $array = array("30", "60", "90", "120", "150", "180", "210", "240", "270", "300", "330", "360", "390", "420", "450", "480", "510", "540", "570", "600");
        if (isset($array)) {
            return $array;
        } else {
            return FALSE;
        }
    }

    /**
     * @note get privilage for a role
     * @return array if TRUE else FALSE
     */
    public function getPrivilages($roleID) {
        $sql = "SELECT
                    permission.Slug
                FROM privilage
                    INNER JOIN permission ON privilage.PermissionID = permission.PermissionID
                WHERE RoleID = '{$roleID}'";
        $rows = $this->db->query($sql);
        $result = $rows->result_array();
        foreach ($result as $priv) {
            $array[] = $priv["Slug"];
        }
        if (!empty($array)) {
            return $array;
        } else {
            return FALSE;
        }
    }

    public function getCodeOFType($val) {
        $rows = $this->SelectAllWhere("order_type", array("TypeID" => $val));
        $result = $rows->first_row('array');
        if(!empty($result["Alias"])){
            return $result["Alias"];
        }else{
            return false;
        }
    }

    public function getCustomerDetail($customerID) {
        $sql = "SELECT
                    customer.CustomerID,customer.CustomerName,customer.Email,
                    customer.Address1,customer.Address2,customer.City,customer.CityCode,customer.Zip,
                    customer.Date,customer.PhoneHome,customer.isPhoneHomeLandline
                FROM customer
                WHERE customer.CustomerID = '{$customerID}'";
        $rows = $this->db->query($sql);
        $array = $rows->result_array();
        if ($array) {
            return $array;
        } else {
            return FALSE;
        }
    }

    public function getCustomerDetailByEmail($email) {
        $sql = "SELECT
                    customer.CustomerID,customer.CustomerName,customer.Email,
                    customer.Address1,customer.Address2,customer.City,customer.CityCode,customer.Zip,
                    customer.Date,customer.PhoneHome
                FROM customer
                WHERE customer.Email = '{$email}'";
        $rows = $this->db->query($sql);
        if ($rows->num_rows() > 0) {
            return $rows->first_row();
        } else {
            return false;
        }
    }

    public function getCustomerDetailByPhone($phone) {
        $sql = "SELECT
                    customer.CustomerID,customer.CustomerName,customer.Email,
                    customer.Address1,customer.Address2,customer.City,customer.CityCode,customer.Zip,
                    customer.Date,customer.PhoneHome,customer.isPhoneHomeLandline
                FROM customer
                WHERE REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(customer.PhoneHome,'(',''),')',''),'-',''),' ',''),'+92',''),'+1','') LIKE '%{$phone}%'";
        $rows = $this->db->query($sql);
        if ($rows->num_rows() > 0) {
            return $rows->first_row();
        } else {
            return false;
        }
    }

    public function getCityByName($name) {
        $rows = $this->SelectAllWhere("towns", array("Name" => $name));
        if ($rows->num_rows() > 0) {
            return $rows->first_row("array");
        } else {
            return false;
        }
    }

    public function getSurveyByID($order_id) {
        $rows = $this->SelectAllWhere("survey_queue", array("OrderID" => $order_id));
        if ($rows->num_rows() > 0) {
            return $rows->result("array");
        } else {
            return false;
        }
    }

    public function getOrdersCountByServiceID($serviceID, $period = false, $from = false, $to = false) {
        $condition = null;
        $sql = 'SELECT * FROM `order` WHERE TypeID = "' . $serviceID . '" AND StatusID != "46b1e4fd15378" ';
        if ($period == "last_week") {
            $condition .= 'AND Date BETWEEN DATE_SUB(DATE_SUB(CURDATE(), INTERVAL WEEKDAY(CURDATE()) DAY),INTERVAL 7 DAY) 
            AND DATE_SUB(DATE_SUB(CURDATE(), INTERVAL WEEKDAY(CURDATE()) DAY),INTERVAL 2 DAY)';
        } elseif ($period == "this_week") {
            $condition .= 'AND Date BETWEEN DATE_SUB(CURDATE(), INTERVAL WEEKDAY(CURDATE()) DAY) 
            AND DATE_ADD(DATE_SUB(CURDATE(), INTERVAL WEEKDAY(CURDATE()) DAY),  INTERVAL 5 DAY)';
        } elseif ($period == "next_week") {
            $condition .= 'AND Date BETWEEN DATE_ADD(DATE_SUB(CURDATE(), INTERVAL WEEKDAY(CURDATE()) DAY),INTERVAL 7 DAY) 
            AND DATE_ADD(DATE_SUB(CURDATE(), INTERVAL WEEKDAY(CURDATE()) DAY),INTERVAL 12 DAY)';
        } elseif ($period == "last_month") {
            $condition .= 'AND Date BETWEEN DATE_SUB(CAST(DATE_FORMAT(NOW() ,"%Y-%m-01") as DATE),INTERVAL 1 MONTH) 
            AND DATE_ADD(DATE_SUB(CAST(DATE_FORMAT(NOW() ,"%Y-%m-01") as DATE),INTERVAL 1 MONTH),INTERVAL 1 MONTH)';
        } elseif ($period == "this_month") {
            $condition .= 'AND Date BETWEEN CAST(DATE_FORMAT(NOW() ,"%Y-%m-01") as DATE) 
            AND LAST_DAY(NOW())';
        } elseif ($period == "next_month") {
            $condition .= 'AND Date BETWEEN DATE_ADD(CAST(DATE_FORMAT(NOW() ,"%Y-%m-01") as DATE),INTERVAL 1 MONTH) 
            AND LAST_DAY(DATE_ADD(CAST(DATE_FORMAT(NOW() ,"%Y-%m-01") as DATE),INTERVAL 1 MONTH))';
        }

        if ($from && $to) {
            $condition .= 'AND Date BETWEEN "' . $from . ' 00:00:00" AND "' . $to . ' 23:59:59"';
        } elseif ($from) {
            $condition .= 'AND Date > "' . $from . ' 00:00:00"';
        } elseif ($to) {
            $condition .= 'AND Date < "' . $to . ' 23:59:59"';
        }


        $rows = $this->db->query($sql . $condition);
        if ($rows->num_rows() > 0) {
            return $rows->num_rows();
        } else {
            return "0";
        }
    }

    /**
     * add initial activity on actions like
     * add,edit,delete in activity_log table
     * @param string $actionID
     * @param string $objectTypeID
     * @return activity id on success or  false if fails
     */
    public function surveyActivity($actionID, $objectTypeID) {
        $activityLogID = strictUniqueID();
        $activityArray = array(
            "ActivityLogID" => $activityLogID,
            "ActionID" => $actionID,
            "ObjectTypeID" => $objectTypeID,
            "Date" => date("Y-m-d H:i:s")
        );
        if ($this->Insert("activity_log", $activityArray)) {
            return $activityLogID;
        } else {
            return false;
        }
    }

    /**
     * calls surveyActivity and addSurveyActivityMeta functions
     * for send survey activity
     * @param array $addSurveyActivity
     */
    public function addSurveyAllActivities($addSurveyActivity) {
        $actionID = "9175eec88598e"; //id of action in this case it is "send"
        $objectTypeID = "9175eec88598e"; //id of object type in this case it is "order"

        if ($activityLogID = $this->surveyActivity($actionID, $objectTypeID)) {
            $this->addSurveyActivityMeta($activityLogID, $addSurveyActivity);
        }
    }

    /**
     * add all add order activity meta in activity_log_meta table
     * like who created order and who is assigned on it
     * @param string $activityLogID
     * @param string $addOrderActivity
     */
    public function addSurveyActivityMeta($activityLogID, $addSurveyActivity) {
        if (isset($addSurveyActivity["Name"])) {
            $name = $addSurveyActivity["Name"];
        } else {
            $name = "survey_email_sent";
        }
        if (isset($addSurveyActivity["Value"])) {
            $value = $addSurveyActivity["Value"];
        } else {
            $value = "yes";
        }
        $activityMetaArray[] = array(
            "MetaID" => strictUniqueID(),
            "ActivityLogID" => $activityLogID,
            "ObjectID" => $addSurveyActivity["OrderID"],
            "Name" => $name,
            "Value" => $value
        );
        $activityMetaArray[] = array(
            "MetaID" => strictUniqueID(),
            "ActivityLogID" => $activityLogID,
            "ObjectID" => $addSurveyActivity["OrderID"],
            "Name" => "assigned_by",
            "Value" => $addSurveyActivity["AssignedBy"]
        );
        foreach ($activityMetaArray as $array) {
            $this->Insert("activity_log_meta", $array);
        }
    }

    public function removeSurveyFromQueue($orderID) {
        $this->Delete("survey_queue", array("OrderID" => $orderID));
    }

    public function getQueueSurveysNumber() {
        $rows = $this->db->query("SELECT * FROM `order`
            INNER JOIN survey_queue ON `order`.OrderID = survey_queue.OrderID
            INNER JOIN customer ON `order`.CustomerID = customer.CustomerID
            WHERE customer.Email != '0' AND customer.Email != ''");
        if ($rows->num_rows > 0) {
            return $rows->num_rows();
        } else {
            return false;
        }
    }

    public function getAllQueuedSurveys() {
        $rows = $this->db->query("SELECT * FROM `order`
            INNER JOIN survey_queue ON `order`.OrderID = survey_queue.OrderID
            INNER JOIN customer ON `order`.CustomerID = customer.CustomerID
            WHERE customer.Email != '0' AND customer.Email != ''");
        if ($rows->num_rows > 0) {
            return $rows->result();
        } else {
            return false;
        }
    }

}
