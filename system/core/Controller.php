<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class CI_Controller {

    private static $instance;
    public $data = array();
    protected $curl;
    protected $admin;
    protected $order;
    protected $twil;
    protected $worker;
    protected $extend;
    protected $Browser;

    public function __construct() {
        self::$instance = & $this;

        foreach (is_loaded() as $var => $class) {
            $this->$var = & load_class($class);
        }

        $this->load = & load_class('Loader', 'core');
        $this->load->initialize();
        log_message('info', 'Controller Class Initialized');
    }

    public static function &get_instance() {
        return self::$instance;
    }

    /**
     * full raw push insert into log table
     * @param json encode object $emeraldPush
     */
    public function logEmeraldPush($emeraldPush) {

        $this->load->model("m_admin");
        $this->admin = new M_admin();

        $postData = array(
            "LogID" => uniqid(),
            "Content" => simple_encrypt(json_encode($emeraldPush)),
            "CustomerID" => $emeraldPush->CustomerID,
            "Date" => date("Y-m-d H:i:s"),
        );

        $this->admin->Insert("log", $postData);
        // need to add log here
    }

    /**
     * full raw push insert into log table
     * @param json encode object $emeraldPush
     */
    public function logRecievedMessagePush($data) {

        $this->load->model("m_admin");
        $this->admin = new M_admin();

        $postData = array(
            "LogID" => uniqid(),
            "Content" => json_encode($data),
            "Date" => date("Y-m-d H:i:s"),
        );

        $this->admin->Insert("message_log", $postData);
    }

    /**
     * check if customer is already exists in database
     * it is using in customer & mbr_service_validate controllers
     * modify or delete this function will effect in controller
     * mentioned above.
     * @param string $customerID
     * @return boolean
     */
    public function checkExistsCustomer($customerID) {
        $this->load->model("m_admin");
        $this->admin = new M_admin();

        $row = $this->admin->SelectAllWhere("customer", array("CustomerID" => $customerID));
        $num = $row->num_rows();
        if ($num > 0) {
            $result = $row->first_row('array');
            return $result;
        } else {
            return FALSE;
        }
    }

    /**
     * check if service is already exists in database
     * @param string $serviceID
     * @return boolean
     */
    public function checkExistsService($serviceID) {
        $this->load->model("m_admin");
        $this->admin = new M_admin();

        $row = $this->admin->SelectAllWhere("service", array("ServiceID" => $serviceID));
        $num = $row->num_rows();
        if ($num > 0) {
            $result = $row->first_row('array');
            return $result;
        } else {
            return FALSE;
        }
    }
    
    public function checkExistsServiceCustomer($customerID = false, $title = false) {
        $this->load->model("m_admin");
        $this->admin = new M_admin();

        $row = $this->admin->SelectAllWhere("service", array("CustomerID" => $customerID, "Title" => $title));
        $num = $row->num_rows();
        if ($num > 0) {
            $result = $row->first_row('array');
            return $result;
        } else {
            return FALSE;
        }
    }

    /**
     * fetch service full deyail from emerald
     * @param string $accountID
     * @return array $array
     */
    public function getServiceFromEmerald($accountID) {
        $this->load->model("m_curl");
        $this->curl = new M_curl();


        $endPointURL = EMERALD_REQUEST_URL . "aceroute_billing_detail.ews";
        $array = array(
            "emerald_user" => EMERALD_USER,
            "emerald_password" => EMERALD_PASSWORD,
            "ID" => $accountID
        );

        // hit post curl to fetch service xml from emerald
        $result = $this->curl->curlExecutePost($endPointURL, $array);
        libxml_use_internal_errors(true);
        $xml = simplexml_load_string($result);

        if (false === $xml || $xml == "No matching id" ) {
            // need to add log here
            return FALSE;
        } else {
            // need to add log here
            foreach ($xml as $key => $val) {
                $array[$key] = simplize($val);
            }
            return $array;
        }
    }

    /**
     * Get unprocessed order ids
     * @param array $unprocessed_orders
     */
    public function getUnprocessedOrderIds( $unprocessed_orders ) {
    	$unprocessed_order_ids = array();
    	foreach ($unprocessed_orders as $unprocessed_order) {
    		array_push($unprocessed_order_ids, $unprocessed_order->OrderID);
    	}
    	return $unprocessed_order_ids;
    }

    /**
     * add customer into database
     * @param object $emeraldPush
     * @param array $service
     * @return boolean or update
     */
    public function addCustomer($emeraldPush, $service) {
        if ($emeraldPush && $service) {
            $credit_card = $homePhone = $workPhone = $fax = $state = "";
            $this->load->model("m_admin");
            $this->admin = new M_admin();
            $this->load->model("M_order");
            $this->order = new M_order();

            $this->load->model("m_curl");
            $this->curl = new M_curl();

            $this->load->model("M_twilio");
            $this->twil = new M_twilio();

            if ($service["address2"] == NULL || $service["address2"] == "0") {
                $service["address2"] = "";
            }

            $address = urldecode($service["address1"] . " " . $service["zip"] . " " . $service["city"] . ", AZ USA");
            $this->admin->logTime($service["address1"] . " " . $service["zip"] . " " . $service["city"] . ", AZ USA");
            $arrayAddress = $this->curl->convertAddressToLatLong($address);
            $cityCode = $this->getCodeOFCity($service["city"]);
            if (!$cityCode) {
                $cityCode = NULL;
            }

            //url to fetch credit card details from backoffic endpoint for airebeam customer portal
            $url = EMERALD_REQUEST_URL . "backoffice/mbrdetail.ews?mbr=" . $emeraldPush->CustomerID;
            $detail = $this->curl->fetchBackOfficeMBRDetails($url);

            $is_landline = "0";

            if (!empty($detail->MBR)) {
                $credit_card = simplize($detail->MBR->LASTFOUR);
                $homePhone = simplizePhone(simplize($detail->MBR->PHONEHOME));
                $workPhone = simplizePhone(simplize($detail->MBR->PHONEWORK));
                $fax = simplizePhone(simplize($detail->MBR->PHONEFAX));
                $state = simplize($detail->MBR->STATE);
            }

            $is_landline = $this->twil->getPhoneNumberType($homePhone);

            // make customer array from service detail & service push to insert in database
            $customerArray = array(
                "CustomerID" => $emeraldPush->CustomerID,
                "CustomerName" => $emeraldPush->CustomerID . " - " . $emeraldPush->FirstName . " " . $emeraldPush->LastName,
                "Email" => $service["email"],
                "PhoneHome" => $homePhone,
                "PhoneWork" => $workPhone,
                "Fax" => $fax,
                "Address1" => $service["address1"],
                "Address2" => $service["address2"],
                "City" => $service["city"],
                "State" => $state,
                "CityCode" => $cityCode,
                "Zip" => $service["zip"],
                "LastAction" => $emeraldPush->Action,
                "Longitude" => $arrayAddress["longitude"],
                "Latitude" => $arrayAddress["latitude"],
                "CreditCardLastFour" => $credit_card,
                "isPhoneHomeLandline" => $is_landline,
                "TermsAccepted" => "0",
                "isPasswordReset" => "1",
                "Date" => date("Y-m-d H:i:s")
            );

            if ($this->admin->Insert("customer", $customerArray)) {
                // log in assets/logs.txt
                $this->admin->logTime("customer inserted in database succesfully.");
                
                // Update temporary mbr and Process Temporary Order
            	$mbr_log = $this->admin->GetMBRLogId($customerArray["Address1"], $customerArray["Zip"], $customerArray["PhoneHome"], $service["email"]);
                if ( sizeof($mbr_log) && $this->admin->UpdateTempMbr($mbr_log[0]->Id, ['Mbr' => $customerArray["CustomerID"]]) ) {
                	$this->admin->logTime("customer temporary mbr has been updated in database succesfully.");
                	$customer_temp_id = $mbr_log[0]->Id;
                	$mbr = $customerArray["CustomerID"];
                	$unprocessed_orders = $this->admin->GetUnprocessedOrdersByCustomer($customer_temp_id);
                	if( sizeof($unprocessed_orders) ) {
	                	$unprocessed_order_ids = $this->getUnprocessedOrderIds( $unprocessed_orders );
	                	if( $this->admin->UpdateUnprocessedOrders($unprocessed_order_ids) ) {
	                		foreach ($unprocessed_orders as $unprocessed_order) {
	                			$description = $unprocessed_order->Detail;
	                			$worker_id = $unprocessed_order->WorkerID;
	                			$start_time_obj = new DateTime( $unprocessed_order->Date );
	                			$duration = $unprocessed_order->Duration;
	                			$mbr_exist = true;
	                			$alertTitle = $unprocessed_order->AlertTitle;

	                			$response = $this->order->addOrderByCustomer($description, $worker_id, $mbr, $start_time_obj, $duration, $mbr_exist);
                				if( $response && array_key_exists("OrderID", $response) ) {
                                                    $prev_date = date('m/d/y', (strtotime('-1 days', strtotime($start_time_obj->format('Y-m-d')))));
                                                    $this->updateBillDay($mbr, $start_time_obj->format('d'), $prev_date);
                                                    
                					$this->admin->logTime( TEMP_ORDERS_INSERTED );
                					$orderId = $response["OrderID"];
                					$orderData = $this->order->getOrderWithoutService($orderId);
                					
                					if ( $orderData["isPhoneHomeLandline"] != "1" ) {
                						//add message entry in database with pending status
                						if ($orderData["PhoneHome"] != "" && $orderData["PhoneHome"] != "0") {
                							$this->order->addMessageEntry($orderData["OrderID"]);
                						}
                					}
                					
                					$this->order->removeAlert($orderId);
                					if ($orderData["PhoneHome"] != "" && $orderData["PhoneHome"] != "0") {
                						$end_time_obj = clone $start_time_obj;
                						$end_time_obj->add( new DateInterval ( 'PT' . $duration . 'M' ) );
                						$this->order->addAlert($orderData, $alertTitle, $end_time_obj->format('Y-m-d H:i:s'));
                						
                						$this->admin->logTime( "Alert added on order" );
                					}

                					
                					
                					
                				} else {
                					$this->admin->logTime( TEMP_ORDERS_NOT_INSERTED );
                				}
	                		}

	                	} else {
	                		$this->admin->logTime( TEMP_ORDERS_NOT_UPDATED );
	                	}
                	}
//                 	Delete Temporary order
                	if( sizeof($unprocessed_orders) ) {
                		$this->order->deleteTempOrder($unprocessed_order_ids);
                		$this->admin->logTime( "Temp Order is deleted" );
                	}
                	
                } else {
                	$this->admin->logTime("customer temporary mbr has not been updated in database succesfully.");
                }
                return TRUE;
            } else {
                // log in assets/logs.txt
                $this->admin->logTime("customer not inserted in database succesfully.");
                return FALSE;
            }
        } else {

            // log in assets/logs.txt
            $this->admin->logTime("customer not inserted in database succesfully.");
            return FALSE;
        }
    }

    /**
     * update customer on every push recieve from emerald
     * @param object $emeraldPush
     * @param array $service
     * @return boolean
     */
    public function updateCustomer($emeraldPush, $service) {
        if ($emeraldPush && $service) {
            $detail = $credit_card = $homePhone = $workPhone = $fax = $state = "";
            $this->load->model("m_admin");
            $this->admin = new M_admin();

            $this->load->model("m_curl");
            $this->curl = new M_curl();

            $this->load->model("M_twilio");
            $this->twil = new M_twilio();

            if ($service["address2"] == NULL || $service["address2"] == "0") {
                $service["address2"] = "";
            }

            $address = urldecode($service["address1"] . " " . $service["zip"] . " " . $service["city"] . ", AZ, USA");
            $arrayAddress = $this->curl->convertAddressToLatLong($address);

            $cityCode = $this->getCodeOFCity($service["city"]);
            if (!$cityCode) {
                $cityCode = NULL;
            }

            //url to fetch credit card details from backoffic endpoint for airebeam customer portal
            $url = EMERALD_REQUEST_URL . "backoffice/mbrdetail.ews?mbr=" . $emeraldPush->CustomerID;
            $detail = $this->curl->fetchBackOfficeMBRDetails($url);
            $this->curl->logTime("back office detail:" . json_encode($detail));

            if (!empty($detail->MBR)) {
                $credit_card = simplize($detail->MBR->LASTFOUR);
                $homePhone = simplizePhone(simplize($detail->MBR->PHONEHOME));
                $workPhone = simplizePhone(simplize($detail->MBR->PHONEWORK));
                $fax = simplizePhone(simplize($detail->MBR->PHONEFAX));
                $state = simplize($detail->MBR->STATE);
            }

            $is_landline = $this->twil->getPhoneNumberType($homePhone);
            $this->curl->logTime("twilio lookup api response:" . json_encode($is_landline));

            // make customer array to update in database
            $customerArray = array(
                "CustomerName" => $emeraldPush->CustomerID . " - " . $emeraldPush->FirstName . " " . $emeraldPush->LastName,
                "Email" => $service["email"],
                "PhoneHome" => $homePhone,
                "PhoneWork" => $workPhone,
                "Fax" => $fax,
                "Address1" => $service["address1"],
                "Address2" => $service["address2"],
                "City" => $service["city"],
                "State" => $state,
                "CityCode" => $cityCode,
                "Zip" => $service["zip"],
                "LastAction" => $emeraldPush->Action,
                "Longitude" => $arrayAddress["longitude"],
                "Latitude" => $arrayAddress["latitude"],
                "CreditCardLastFour" => $credit_card,
                "isPhoneHomeLandline" => $is_landline,
                "Date" => date("Y-m-d H:i:s")
            );

            if ($this->curl->UpdateAll("customer", $customerArray, array("CustomerID" => $emeraldPush->CustomerID))) {
                // log in assets/logs.txt
                $this->curl->logTime("customer updated in database");
                return TRUE;
            } else {
                // log in assets/logs.txt
                $this->curl->logTime("customer not updated in database");
                return FALSE;
            }
        } else {
            // log in assets/logs.txt
            $this->curl->logTime("customer not updated in database");
            return FALSE;
        }
    }

    /**
     * add service into database
     * @param object $emeraldPush
     * @param array $service
     * @return boolean or update
     */
    public function addService($emeraldPush, $service) {
        $this->load->model("m_admin");
        $this->admin = new M_admin();
        // make service array from service detail & service push to insert in database
        $serviceArray = array(
            "ServiceID" => $emeraldPush->AccountID,
            "Title" => $emeraldPush->ServiceType,
            "FirstName" => $service["firstname"],
            "LastName" => $service["lastname"],
            "Phone" => $service["phone"],
            "Status" => $service["status"],
            "Note" => "",
            "CustomerID" => $emeraldPush->CustomerID,
            "UserName" => $service["username"],
            "Date" => date("Y-m-d H:i:s")
        );
        if ($this->admin->Insert("service", $serviceArray)) {
            // log in assets/logs.txt
            $this->admin->logTime("service " . $emeraldPush->AccountID . " inserted in database");
            return TRUE;
        } else {
            // log in assets/logs.txt
            $this->admin->logTime("service " . $emeraldPush->AccountID . " not inserted in database");
            return FALSE;
        }
    }

    /**
     * update customer on every push recieve from emerald
     * @param object $emeraldPush
     * @param array $service
     * @return boolean
     */
    public function updateService($emeraldPush, $service) {
        if ($emeraldPush && $service) {
            $this->load->model("m_admin");
            $this->admin = new M_admin();

            // make service array from service detail & service push to insert in database
            $serviceArray = array(
                "Title" => $emeraldPush->ServiceType,
                "FirstName" => $service["firstname"],
                "LastName" => $service["lastname"],
                "Phone" => $service["phone"],
                "Status" => $service["status"],
                "CustomerID" => $emeraldPush->CustomerID,
                "UserName" => $service["username"],
                "Date" => date("Y-m-d H:i:s")
            );

            if ($this->admin->UpdateAll("service", $serviceArray, array("ServiceID" => $emeraldPush->AccountID))) {
                return TRUE;
                // log in assets/logs.txt
                $this->admin->logTime("service " . $emeraldPush->AccountID . " updated in database");
            } else {
                // log in assets/logs.txt
                $this->admin->logTime("service " . $emeraldPush->AccountID . " not updated in database");
                return FALSE;
            }
        } else {
            // log in assets/logs.txt
            $this->admin->logTime("service " . $emeraldPush->AccountID . " not updated in database");
            return FALSE;
        }
    }

    public function getCodeOFCity($val) {
        $this->load->model("m_admin");
        $this->admin = new M_admin();
        $cityRow = $this->admin->getCityByName($val);
        if ($cityRow) {
            return $cityRow["Alias"];
        }
    }
    
    public function updateBillDay($customerID, $bill_day, $prev_date){
        $this->load->model("M_curl");
        $this->curl = new M_curl();
        $parameters = array(
              "emerald_user" => EMERALD_API_USER,
              "emerald_password" => EMERALD_API_PASSWORD,
              "action" => "mbr_modify",
              "CustomerID" => $customerID,
              "BillDay" => $bill_day,
              "TransHoldDate" => $prev_date,
        );
        $this->curl->updateMBR($customerID, $parameters);
    }

}
