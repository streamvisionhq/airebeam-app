/**
 * JS for all popups elements: Cancel popup, Toggle into view incident popup to edit incident
 * Show messages 
 * This is for the user accessible section, not the admin section.
 */
var xhrAppointment;
$(document).ready(function () {

  $("#modal-editIncident").on('show.bs.modal', function () {
    var popupInterval = setInterval(function () {
      if ($("#edit_popup_cont").is(":visible") === true) {
	var headerHeight = $("#modal-editIncident").find(".modal-header").height();
	$("#modal-editIncident").find(".modal-body").css("max-height", $("#modal-editIncident").height() - headerHeight - 60);
	clearInterval(popupInterval);
      }
    }, 1);
  });

  $("#modal-1").on('show.bs.modal', function () {
    var headerHeight = $(this).find(".modal-header").height();
    $(this).find(".modal-body").css("max-height", $(this).height() - headerHeight - 100);
  });

  $("input[name=edit-incident]").click(function () {
    var form = $(this).closest("form").serialize();
    var orderID = $("#editIncidentForm input[name='orderID']").val();
    $.ajax({
      type: "POST",
      url: base_url + "ajax/order/editOrder",
      data: form,
      success: function (data) {
	if (data == "success") {
	  $('#modal-editIncident .btn-remove').click();
	  removeMarkers();
	  $calendar.weekCalendar('refresh');
	} else {
	  $('#modal-editIncident #error-php').html(data);
	  $(".modal-body").scrollTop(0);
	}
	$("input[name=edit-incident]").removeAttr("disabled");
      },
      error: function () {
	$("input[name=edit-incident]").removeAttr("disabled");
      }
    });
  });

  $(".notes_edit_popup").click(function () {
    $("input[name=edit-incident]").attr('name', 'edit-incident-note');
  });

  $('.btn-activity').click(function () {
    if ($('.activity-log').length == 0) {
      var calID = $('.btn-activity').attr("data-id");
      $.ajax({
	url: base_url + "ajax/order/getAllActivitiesOfOrder/" + calID,
	type: 'POST',
	async: false,
	cache: false,
	success: function (data) {
	  if (data) {
	    var obj = $.parseJSON(data);
	    $("#editIncidentForm #editIncidentNotes").html(obj.notesHtml);
	    $(".text-note").html("Activity Log");
	  }
	}
      });
      $('.activity-log').fadeIn();

    } else {
      $('.activity-log').remove();
      $(".text-note").html("Notes");
      $(".modal-body").scrollTop(0);
    }
  });

  $(document).on("click", ".alert-editIncident", function () {
    var $this = $(this);
    var orderID = $this.attr("data-eventid");
    $('.btn-activity').attr("data-id", orderID);
    $.post(base_url + "ajax/order/getOrderDetail/" + orderID, {detail: 1}, function (data) {
      if (data) {
	var obj = $.parseJSON(data);
	$(".alertbox-notification-appointment").html(obj.alertHtml);
	if (obj.customerEmail == "" || obj.customerEmail == "0") {
	  $("#email-error").html(obj.emailErrorHtml);
	  $("#email-error").parent().show();
	} else {
	  $("#email-error").html("");
	  $("#email-error").parent().hide();
	}
	if (obj.customerPhone == "" || obj.customerPhone == "0") {
	  $("#phone-error").html(obj.phoneErrorHtml);
	  $("#phone-error").parent().show();
	} else {
	  $("#phone-error").html("");
	  $("#phone-error").parent().hide();
	}
	$("#editIncidentForm select[name=service]").html(obj.servicesHtml);
	$("#editIncidentForm select[name=status]").html(obj.statusesHtml);
	$("#editIncidentForm select[name=duration]").html(obj.durationsHtml);
	$("#editIncidentForm select[name=worker]").html(obj.workersHtml);
	$("#editIncidentForm input[name=name]").val(obj.orderName);
	$("#editIncidentForm textarea[name=description]").val(obj.orderDetail);
	$("#editIncidentForm input[name=date]").val(obj.orderDate);
	$("#editIncidentForm input[name=time]").val(obj.orderTime);
	$("#editIncidentForm input[name=customer]").val(obj.customerName);
	$("#editIncidentForm input[name=address]").val(obj.customerAddress);
	$("#editIncidentForm input[name=email]").val(obj.customerEmail);
	$("#editIncidentForm input[name=phone]").val(obj.customerPhone);
	$("#editIncidentForm #editIncidentNotes").html(obj.notesHtml);
	$("#editIncidentForm input[name='orderID']").val(obj.orderID);
	$("#edit_popup_cont").show();
      }
    });
    getCustomerDataByOrderID(orderID);
  });

  $(".note-editIncident").click(function () {
    var $this = $(this);
    var orderID = $this.attr("data-eventid");
    $('.btn-activity').attr("data-id", orderID);
    $.post(base_url + "ajax/order/getOrderDetail/" + orderID, {detail: 1}, function (data) {
      if (data) {
	var obj = $.parseJSON(data);
	$(".alertbox-notification-appointment").html(obj.alertHtml);
	if (obj.customerEmail == "" || obj.customerEmail == "0") {
	  $("#email-error").html(obj.emailErrorHtml);
	  $("#email-error").parent().show();
	} else {
	  $("#email-error").html("");
	  $("#email-error").parent().hide();
	}
	if (obj.customerPhone == "" || obj.customerPhone == "0") {
	  $("#phone-error").html(obj.phoneErrorHtml);
	  $("#phone-error").parent().show();
	} else {
	  $("#phone-error").html("");
	  $("#phone-error").parent().hide();
	}
	$("#editIncidentForm select[name=service]").html(obj.servicesHtml);
	$("#editIncidentForm select[name=status]").html(obj.statusesHtml);
	$("#editIncidentForm select[name=duration]").html(obj.durationsHtml);
	$("#editIncidentForm select[name=worker]").html(obj.workersHtml);
	$("#editIncidentForm input[name=name]").val(obj.orderName);
	$("#editIncidentForm textarea[name=description]").val(obj.orderDetail);
	$("#editIncidentForm input[name=date]").val(obj.orderDate);
	$("#editIncidentForm input[name=time]").val(obj.orderTime);
	$("#editIncidentForm input[name=customer]").val(obj.customerName);
	$("#editIncidentForm input[name=address]").val(obj.customerAddress);
	$("#editIncidentForm input[name=email]").val(obj.customerEmail);
	$("#editIncidentForm input[name=phone]").val(obj.customerPhone);
	$("#editIncidentForm #editIncidentNotes").html(obj.notesHtml);
	$("#editIncidentForm input[name='orderID']").val(obj.orderID);
	$("#edit_popup_cont").show();
      }
    });
    getCustomerDataByOrderID(orderID);
  });
  //MSGs
  $(".alert-contsuccess").fadeIn("slow");

  $(".alert-contsuccess").delay(5000).fadeOut("slow");

  $(document.body).on("click", ".alert-warning", function () {
    $(this).fadeIn("slow");
  });

  $(document.body).on("click", ".alert-nthsuccess", function () {
    $(".alert-warning").fadeOut("slow");
    $(this).fadeIn("slow");
    $(this).delay(5000).fadeOut("slow");
  });

  $(document.body).on("click", ".alert .close", function () {
    $("#gritter-notice-wrapper").fadeOut("slow");
    $("#gritter-notice-wrapper-success").fadeOut("slow");
    return false;
  });

  $(document.body).on("click", "input[name=edit-incident-note]", function () {
    var $this = $(this);
    var form = $this.closest("form").serialize();
    var orderID = $("#editIncidentForm input[name='orderID']").val();
    $.ajax({
      type: "POST",
      url: base_url + "ajax/order/editOrder",
      data: form,
      success: function (data) {
	if (data == "success") {
	  $("#modal-editIncident .btn-remove").click();
	  location.reload();
	} else {
	  $("#modal-editIncident #error-php").html(data);
	  $(".modal-body").scrollTop(0);
	}
      }
    });
  });


  $(document.body).on('keydown','input[name=global_search_text], input[name=start_date], input[name=end_date]', function (event) {
    if (event.which == 13) {
      event.preventDefault();
      $("#global_search_button").click();
    }
  });


  // DatePicker
  $(document.body).on("change", ".datepick, .timepick", function () {
    var $this = $(this);
    if ($this.val() != "") {
      $this.focusout();
    }
  });

  // View Incident Popup Toggle into Edit Incident Popup
  $(document.body).on("click", ".btn-edit", function () {
    var $this = $(this);
    $(".view-popup .modal-body .jquery-validation, .view-popup .modal-body textarea, .view-popup .modal-body select").removeAttr("disabled");
    $("#editIncidentForm input[name='customer']").attr("disabled", "disabled");
    $("#editIncidentForm input[name='name']").attr("disabled", "disabled");
    $this.children().addClass("icon-eye-open");
    $this.children().removeClass("icon-edit");
    $this.attr("title", "View Appointment");
    $this.addClass("btn-view");
    $this.removeClass("btn-edit");
    $(".view-popup #myModalLabel").text("Edit Appointment");
    $(".modal-footer .btn-primary").css("display", "inline-block");
  });

  $(document.body).on("click", ".btn-view", function () {
    var $this = $(this);
    $(".view-popup .modal-body .jquery-validation, .view-popup .modal-body textarea, .view-popup .modal-body select").attr("disabled", "disabled");

    $(".view-popup .modal-body .control-group").each(function () {
      if ($this.hasClass("error") || $this.hasClass("success")) {
	$this.removeClass("error");
	$this.removeClass("success");
	$this.children(".help-block").remove();
      }
    });

    $this.children().removeClass("icon-eye-open");
    $this.children().addClass("icon-edit");
    $this.attr("title", "Edit Appointment");
    $this.addClass("btn-edit");
    $this.removeClass("btn-view");
    $(".view-popup #myModalLabel").text("View Appointment");
    $("#editIncidentForm .modal-footer .btn-primary").css("display", "none");
    // $(".view-popup .modal-body .jquery-validation").val('');
  });

  $('.btn-remove').click(function () {
    $(".modal-body").scrollTop(0);
    $(".text-note").html("Notes");
    $('.activity-log').fadeOut();
    $("#email-error").html("");
    $("#email-error").parent().hide();
    $("#phone-error").html("");
    $("#phone-error").parent().hide();
  });

  //on Done POPUP
  $("#modal-a3").on('show.bs.modal', function () {
    setTimeout(function () {
      $(".modal-backdrop").next().addClass("modal-bg");
    }, 0);
  });

  $(document.body).on("click", ".cancel-done", function () {
    $("#modal-a3").removeClass("in", function () {
      $(this).css("display", "none");
      $(this).attr("aria-hidden", "true");
    });
    $('.popup-done').unbind('click');
    $(".modal-bg").remove();
  });

  $('.modal').on('shown.bs.modal', function () {
    // do something...
    $('body').css('overflow', 'hidden');
    $(".loading_customer").hide();
  });

  $('.modal').on('hidden.bs.modal', function () {
    // do something...
    $('body').css('overflow', 'auto');
    if (xhrAppointment !== undefined && xhrAppointment.abort() !== undefined) {
      xhrAppointment.abort();
    }
  });

  // On Cancel POPUP Button
  $(".modal").on("hidden", function () {

    $("#error-php").html("");
    $(".view-popup .modal-body .jquery-validation, .view-popup .modal-body textarea, .view-popup .modal-body select").attr("disabled", "disabled");
    var $btnPrimary = $(".view-popup .modal-header .btn-primary");
    $btnPrimary.children().removeClass("icon-eye-open");
    $btnPrimary.children().addClass("icon-edit");
    $btnPrimary.attr("title", "Edit Appointment");
    $btnPrimary.addClass("btn-edit");
    $btnPrimary.removeClass("btn-view");

    $("#email-error").html("");
    $("#email-error").parent().hide();
    $("#phone-error").html("");
    $("#phone-error").parent().hide();

    $("#addIncidentForm .modal-body .null-val").val("");
    $(".view-popup #myModalLabel").text("View Appointment");
    $(".view-popup .modal-footer .btn-primary").css("display", "none");
    $(".view-popup .control-group").each(function () {
      var $this = $(this);
      if ($this.hasClass("error") || $this.hasClass("success")) {
	$this.removeClass("error");
	$this.removeClass("success");
	$this.children(".help-block").remove();
      }
    });

    $("#addIncidentForm .control-group").each(function () {
      var $this = $(this);
      if ($this.hasClass("error") || $this.hasClass("success") || $this.hasClass("c-add")) {
	$this.children("input").val('');
	$this.children("textarea").val('');
	$this.children("select").val('');
	$this.children("select[name='duration']").prop('selectedIndex', 1);
	$this.children("select[name='worker']").prop('selectedIndex', 0);
	$this.removeClass("error");
	$this.removeClass("success");
	$this.children(".help-block").remove();
      }
    });

  });

  $(document.body).on("click", ".ui-menu-item", function () {
    getCustomerAddress($("#customer_add").val());
  });

  $(document.body).bind("keydown", function (e) {
    if (e.keyCode == 13) {
      if (e.target.id == "customer_add") {
	getCustomerAddress($("#customer_add").val());
      }
    }
  });

  /**
   * Create Incident
   */
  $("input[name=add-incident]").click(function () {
    $(".modal-body").scrollTop(0);
    var form = $(this).closest("form").serialize();
    var resetForm = $(this).closest("form");
    $.ajax({
      type: "POST",
      url: base_url + "ajax/order/addOrder",
      data: form,
      success: function (data) {
	var obj = $.parseJSON(data);
	if (obj.reponse == "error") {
	  $('#error-php').html(obj.showmsg.error);
	  $("input[name=add-incident]").removeAttr("disabled");
	} else {
	  $(".btn-remove").click();
	  $('#error-php').html('');
	  $(resetForm).trigger('reset');
	  $(".control-group").removeClass('error');
	  $(".control-group").removeClass('success');
	  $(".control-group").children('.help-block').remove();
	  $(".control-group").children('input').val('');
	  $(".control-group").children('textarea').val('');
	  $(".control-group").children("select").val('');
	  $(".control-group").children('select[name="duration"]').val('60');
	  $("input[name=add-incident]").removeAttr("disabled");
	  $calendar.weekCalendar('refresh');
	}
      },
      error: function () {
	$("input[name=add-incident]").removeAttr("disabled");
      }
    });
  });

  $("input[name=all-survey]").click(function () {
    if ($(this).is(":checked") != true) {
      $("input[name='single-survey[]']").removeProp("checked");
    } else {
      $("input[name='single-survey[]']").prop("checked", "checked");
    }
  });

  $(document.body).on("click", ".send-sms", function () {
    $(".send-sms").attr("disabled", "disabled");
    $(".btn-remove").trigger('click');
    sendSMS($(this).data("id"));
    location.reload(true);    
  });

  $('#paginated_table').DataTable();
  $(document.body).on("click", ".notify-customer", function () {
    $this = $(this);
    var customer_id = $this.attr('data-customer-id');
    var order_id = $this.attr('data-order-id');
    $.ajax({
      url: base_url + "ajax/alert/smsFailNotification/",
      type: "POST",
      async: false,
      cache: false,
      data: {
        order: order_id,
        customer: customer_id
      },
      success: function () {
        location.reload(true);        
      }
    });   
  });

  $(document.body).on("click", ".cancel-send-sms", function () {
    $(".cancel-send-sms").attr("disabled", "disabled");
    $(".btn-remove").trigger('click');
    cancelSMS($(this).data("id"));
  });

  $(document.body).on("click", ".force-reset", function () {
    $this = $(this);
    $(".force-reset-hit").attr("disabled", true);
    var customerID = $this.attr("data-id");
    $.post(base_url + "ajax/customer/forcePasswordReset/" + customerID, function (data) {
      if (data != "failed") {
	$(".password-activity").html(data);
      }
    });
  });

});

function getCustomerAddress(customerName) {
  var emptyFields = [];
  $.post(base_url + "ajax/customer/getCustomerAddress", {customerName: customerName}, function (data) {
    if (data) {
      var address = "";
      var obj = $.parseJSON(data);
      var serviceID = obj[0].ServiceID;
      var customerID = obj[0].CustomerID;

      if (obj[0].Address1 == "0" || obj[0].Address1 == "" || obj[0].Address1 == null) {
      } else {
	address = address.concat(obj[0].Address1);
      }

      if (obj[0].Address2 == "0" || obj[0].Address2 == "" || obj[0].Address2 == null) {
      } else {
	address = address.concat(", " + obj[0].Address2 + "");
      }

      if (obj[0].City == "0" || obj[0].City == "" || obj[0].City == null) {
      } else {
	address = address.concat(", " + obj[0].City + "");
      }

      if (obj[0].Zip == "0" || obj[0].Zip == "" || obj[0].Zip == null) {
      } else {
	address = address.concat(", " + obj[0].Zip + "");
      }

      if (obj[0].PhoneHome == "0" || obj[0].PhoneHome == "" || obj[0].PhoneHome === null) {
	emptyFields.push("PhoneHome");
      } else {
	$("input[name=phone]").val(obj[0].PhoneHome);
      }

      if (obj[0].Email == "0" || obj[0].Email == "" || obj[0].Email == null) {
	emptyFields.push("Email");
      } else {
	$("input[name=email]").val(obj[0].Email);
      }

      $("input[name=address]").val(address);
      checkCustomerUpdates(customerID, serviceID, emptyFields);
    }
  });
}

function sendSMS(orderID) {
  $.ajax({
    url: base_url + "ajax/twilioBase/send/" + orderID,
    type: "POST",
    async: false,
    cache: false,
    success: function (data) {
      if (data) {
	var obj = $.parseJSON(data);
	if (obj.status == "success") {
	  $(".alert-nthsuccess").trigger('click');
	  $(".btn-remove").trigger('click');
	  $('#message-success').text(obj.message);
	  removeMarkers();
	  $calendar.weekCalendar('refresh');
	} else {
	  $(".alert-warning").trigger('click');
	  $('#schedule-error').text(obj.message);
	}
      }
    }
  });
}

function cancelSMS(orderID) {
  $.ajax({
    url: base_url + "ajax/alert/removeAlertNotification/" + orderID,
    type: "POST",
    async: false,
    cache: false,
    success: function (data) {
    }
  });
}

function removeItem(value, array) {
  var i = array.indexOf(value);
  if (i != -1) {
    array.splice(i, 1);
  }
  return array;
}

function getCustomerDataByOrderID(orderID) {
  $.ajax({
    url: base_url + "ajax/order/getOrderDetail/" + orderID,
    type: "POST",
    success: function (data) {
      var obj = $.parseJSON(data);
      getCustomerAddress(obj.customerName);
    }
  });
}

function checkCustomerUpdates(customerID, serviceID, emptyFields) {
  $(".loading_customer").show();
  xhrAppointment = $.post(base_url + "ajax/customer/getCustomerUpdates", {customerID: customerID, serviceID: serviceID}, function (data) {
    if (data != "Already updated") {
      var obj = $.parseJSON(data);

      $("input[name=customer]").val(obj.FullName);

      if (obj.AddressUpdated == "yes") {
	$("input[name=address]").val(obj.Address);
	$("input[name=updated]").val("yes");
	$("input[name=address-updated]").val("Address");
      }

      if (obj.PhoneHome != "" && obj.PhoneHome != "0" && obj.PhoneHome !== undefined) {
	if (obj.PhoneUpdated == "yes") {
	  $("input[name=phone]").val(obj.PhoneHome);
	  $("input[name=updated]").val("yes");
	  $("input[name=phone-updated]").val("PhoneHome");
	} else {
	  $("input[name=phone]").val(obj.PhoneHome);
	}
	emptyFields = removeItem('PhoneHome', emptyFields);
      } else {
	emptyFields.push("PhoneHome");
      }

      if (obj.Email != "" && obj.Email != "0" && obj.Email !== undefined) {
	if (obj.EmailUpdated == "yes") {
	  $("input[name=email]").val(obj.Email);
	  $("input[name=updated]").val("yes");
	  $("input[name=email-updated]").val("Email");
	} else {
	  $("input[name=email]").val(obj.Email);
	}
	emptyFields = removeItem('Email', emptyFields);
      } else {
	emptyFields.push("Email");
      }

      $(".loading_customer").hide();

    } else if (data == "Already updated") {
      $(".loading_customer").hide();
    }

    if ($.inArray("PhoneHome", emptyFields) !== -1) {
      $("input[name=phone]").val("");
      if ($("#phone-error").parent().is(":visible") !== true) {
	$("#phone-error").html("Phone number is empty");
	$("#phone-error").parent().show();
	var modalBody = $("#phone-error").parent().parent().next().find(".modal-body").height();
	$("#phone-error").parent().parent().next().find(".modal-body").css("max-height", modalBody - 45);
      }
      if ($("#phone-error-edit").parent().is(":visible") !== true) {
	$("#phone-error-edit").html("Phone number is empty");
	$("#phone-error-edit").parent().show();
	var modalBody = $("#phone-error-edit").parent().parent().next().find(".modal-body").height();
	$("#phone-error-edit").parent().parent().next().find(".modal-body").css("max-height", modalBody - 45);
      }
    } else {
      if ($("#phone-error").parent().is(":visible") === true) {
	$("#phone-error").html("");
	$("#phone-error").parent().hide();
	var modalBody = $("#phone-error").parent().parent().next().find(".modal-body").height();
	$("#phone-error").parent().parent().next().find(".modal-body").css("max-height", modalBody + 45);
      }
      if ($("#phone-error-edit").parent().is(":visible") === true) {
	$("#phone-error-edit").html("");
	$("#phone-error-edit").parent().hide();
	var modalBody = $("#phone-error-edit").parent().parent().next().find(".modal-body").height();
	$("#phone-error-edit").parent().parent().next().find(".modal-body").css("max-height", modalBody + 45);
      }
    }

    if ($.inArray("Email", emptyFields) !== -1) {
      $("input[name=email]").val("");
      if ($("#email-error").parent().is(":visible") !== true) {
	$("#email-error").html("Email address is empty");
	$("#email-error").parent().show();
	var modalBody = $("#email-error").parent().parent().next().find(".modal-body").height();
	$("#email-error").parent().parent().next().find(".modal-body").css("max-height", modalBody - 45);
      }
      if ($("#email-error-edit").parent().is(":visible") !== true) {
	$("#email-error-edit").html("Email address is empty");
	$("#email-error-edit").parent().show();
	var modalBody = $("#email-error-edit").parent().parent().next().find(".modal-body").height();
	$("#email-error-edit").parent().parent().next().find(".modal-body").css("max-height", modalBody - 45);
      }
    } else {
      if ($("#email-error").parent().is(":visible") === true) {
	$("#email-error").html("");
	$("#email-error").parent().hide();
	var modalBody = $("#email-error").parent().parent().next().find(".modal-body").height();
	$("#email-error").parent().parent().next().find(".modal-body").css("max-height", modalBody + 40);
      }
      if ($("#email-error-edit").parent().is(":visible") === true) {
	$("#email-error-edit").html("");
	$("#email-error-edit").parent().hide();
	var modalBody = $("#email-error-edit").parent().parent().next().find(".modal-body").height();
	$("#email-error-edit").parent().parent().next().find(".modal-body").css("max-height", modalBody + 40);
      }
    }

  });
}
