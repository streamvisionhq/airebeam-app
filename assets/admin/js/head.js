/**
 * this file adds autocomplete ajax for customer name
 */
$(function() {
    $("#customer_add").autocomplete({
        source: base_url + 'ajax/customer/searchCustomerByName',
    });
    $("#customer_add").on("keyup", function(event, ui) {
        var addedCustomer = $(this).val();
        if (addedCustomer == "") {
            $("#modal-1 input[name=address]").val("");
            $("#modal-1 input[name=phone]").val("");
            $("#modal-1 input[name=email]").val("");
        }
    });
    $("#customer_search").autocomplete({
        source: base_url + 'ajax/customer/searchCustomerByName',
    });
});