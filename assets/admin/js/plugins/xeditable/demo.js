$(function() {
    $('.order-value').editable({
        validate: function(value) {
            var textBox = jQuery(this);
            var workerID = textBox.attr("data-worker-id");
            jQuery.post(base_url + "ajax/worker/sortWorker/", {num: value, workerID: workerID}, function(data) {
                if(data != "success"){
                    jQuery(".editable-error-block").text(data);
                }else{
                    window.location.href = base_url+"app";
                }
            });
            return ' ';
        }
    });
});
