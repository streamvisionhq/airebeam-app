/**
 * 
 * JS for google.maps.Map and calendar
 */
var $calendar;
var map;
var customers;
var customersJavascript;
var mStart;
var mEnd;
var markersdata = [];
var totalMarkerClick = 1;

function pan(orderID) {
    jQuery.post(base_url + "ajax/map/getNewLngLat", {orderID: orderID}, function (data) {
        if (data) {
            var obj = jQuery.parseJSON(data);
            var panPoint = new google.maps.LatLng(obj["Latitude"], obj["Longitude"]);
            map.panTo(panPoint);

            var markerLatitude = Math.floor(obj["Latitude"]);
            var markerLongitude = Math.ceil(obj["Longitude"]);
            for (i = 0; i < markersdata.length; i++) {
                var mapContent = markersdata[i].labelContent;
                if (mapContent.indexOf("<span class='map_orderID'>" + obj["OrderID"] + "</span>") != -1) {
                    google.maps.event.trigger(markersdata[i], 'click');
                }
            }
        }
    });
}

function markerClick() {
    this.setZIndex(google.maps.Marker.MAX_ZINDEX + totalMarkerClick);
    totalMarkerClick++;
}

function markerDoubleClick() {
    $(".marker-click").attr("data-eventid", this.id);
    $(".marker-click.alert-editIncident").click();
}

function initMap(start, end) {
    var mapCenter = {lat: 32.5, lng: -111.7};
    map = new google.maps.Map(document.getElementById('map'), {
        center: mapCenter,
        maxZoom: 17,
        minZoom: 3,
        zoom: 10,
        zoomControlOptions: {
            position: google.maps.ControlPosition.RIGHT_TOP
        },
        streetViewControlOptions: {
            position: google.maps.ControlPosition.RIGHT_TOP
        },
    });
    // Set a marker at the center of the map.
    setMarkers(map, start, end);
}

function removeMarkers() {
    for (var i = 0; i < markersdata.length; i++) {
        markersdata[i].setMap(null);
    }
    markersdata = [];
}

function setMarkers(map, start, end) {
    if (typeof (start) !== 'undefined') {
        if (start.isTrusted != true) {
            mStart = start;
            mEnd = end;
        }
    }
    $.ajax({
        url: base_url + "ajax/map/getMarkersData",
        type: 'POST',
        async: false,
        cache: false,
        data: {
            'start': mStart,
            'end': mEnd,
        },
        success: function (data) {
            if (data) {
                var obj = jQuery.parseJSON(data);
                Object.keys(obj).forEach(function (key) {
                    customers = obj[key]["customer"];
                    customersJavascript = obj[key]["customerFull"];
                    // Marker sizes are expressed as a Size of X,Y where the origin of the image
                    // (0,0) is located in the top left of the image.
                    // Origins, anchor positions and coordinates of the marker increase in the X
                    // direction to the right and in the Y direction down.
                    var image = {
                        url: 'img/demo/user-avatar.jpg',
                    };
                    // Shapes define the clickable region of the icon. The type defines an HTML
                    // <area> element 'poly' which traces out a polygon as a series of X,Y points.
                    // The final coordinate closes the poly by connecting to the first coordinate.
                    for (var i = 0; i < customers.length; i++) {
                        var customer = customers[i];
                        var cJavascript = customersJavascript[i];
                        var keyExists = "no";
                        Object.keys(markersdata).forEach(function (key) {
                            if (customer[3] == markersdata[key].id) {
                                keyExists = "yes";
                            }
                        });
                        if (keyExists == "no") {
                            marker = new MarkerWithLabel({
                                position: {lat: parseFloat(customer[1]), lng: parseFloat(customer[2])},
                                map: map,
                                raiseOnDrag: true,
                                labelContent: "<div class='markerbgcolor' style='background:" + cJavascript[6] + ";'><span class='sqnum'>" + cJavascript[0] + "</span>" + cJavascript[4] + "<span>" + cJavascript[5] + "</span><span class='map_orderID'>" + cJavascript[7] + "</span></div><div class='markerarrow' style='border-top-color:" + cJavascript[6] + ";'></div>",
                                labelClass: "labels", // the CSS class for the label
                                labelInBackground: false,
                                icon: pinSymbol(cJavascript[6]),
                                draggable: false,
                                animation: google.maps.Animation.DROP,
                                size: new google.maps.Size(0, 0),
                                title: customer[0],
                                id: customer[3],
                                zIndex: customer[4]
                            });
                            google.maps.event.addListener(marker, "click", markerClick);
                            google.maps.event.addListener(marker, 'dblclick', markerDoubleClick);
                            $(".map_orderID");
                            $("#modal-editIncident").on('show.bs.modal', function () {
                                var popupInterval = setInterval(function () {
                                    if (jQuery("#edit_popup_cont").is(":visible") === true) {
                                        var headerHeight = $(this).find(".modal-header").height();
                                        $(this).find(".modal-body").css("max-height", $(this).height() - headerHeight - 60);
                                        clearInterval(popupInterval);
                                    }
                                }, 1);
                            });
                            markersdata.push(marker);
                        }
                    }
                    // end loop
                });
            }
        }
    });
}

function pinSymbol(color) {
    return {
        path: 'M0.545,33.217c0-10.229,0-20.459,0-30.69C0.569,2.5,0.606,2.479,0.615,2.45c0.452-1.409,1.129-1.903,2.631-1.903c15.201,0,30.401,0,45.603,0		c0.163,0,0.326-0.003,0.489,0.009c0.863,0.063,1.537,0.472,1.875,1.262c0.19,0.447,0.271,0.977,0.273,1.469		c0.014,9.723,0.01,19.445,0.01,29.168c0,1.912-0.877,2.797-2.775,2.797c-5.061,0-10.122,0.005-15.183-0.01		c-0.44,0-0.724,0.139-1.009,0.471c-1.997,2.312-4.014,4.604-6.029,6.901c-0.976,1.111-2.258,1.109-3.217,0		c-2.006-2.33-4.015-4.651-6.002-6.996c-0.288-0.34-0.573-0.457-1.009-0.453c-4.37,0.042-8.741,0.058-13.111,0.089		c-1.074,0.009-1.896-0.392-2.375-1.383C0.686,33.665,0.625,33.437,0.545,33.217z',
        fillColor: color,
        fillOpacity: 0,
        strokeColor: '#000',
        strokeWeight: 0,
        scale: 1
    };
}

function getIncidents(start, end) {
    var currentRequest = null;
    currentRequest = $.ajax({
        url: base_url + "ajax/calendar/getCalendarData",
        type: 'POST',
        data: {start: start, end: end},
        async: false,
        cache: false,
        beforeSend: function () {
            if (currentRequest != null) {
                currentRequest.abort();
            }
        },
        success: function (data) {
            if (data) {
                var obj = jQuery.parseJSON(data);
                events = obj.events;
                if (events) {
                    Object.keys(events).forEach(function (key) {
                        events[key]["start"] = eval(events[key]["start"]);
                        events[key]["end"] = eval(events[key]["end"]);
                    });
                } else {
                    events = [];
                }
            }
        }
    });
    return events;
}

function renderEvent(calEvent, $event) {
    if (calEvent.isLateRejectedHighlighted == 1) {
        $event.find(".wc-time").css({
            backgroundColor: '#cc0000',
            'border-bottom': '1px solid #cc0000'
        });
        $event.find(".icon-trash").before('<i title="Appointment Rejected" class="icon rejected-appt icon-rejected"></i>');
    } else if (calEvent.isRejectedHighlighted == 1) {
        $event.find(".wc-time").css({
            backgroundColor: '#cc0000',
            'border-bottom': '1px solid #cc0000'
        });
        $event.find(".icon-trash").before('<i title="Appointment Rejected" class="icon rejected-appt icon-rejected"></i>');
    } else if (calEvent.isMsgHighlighted == 1) {
        $event.find(".wc-time").css({
            backgroundColor: '#cc0000',
            'border-bottom': '1px solid #cc0000'
        });
        $event.find(".icon-trash").before('<i title="Send SMS" class="icon icon-send"></i>');
    } else if (calEvent.isHighlighted == 1) {
        $event.find(".wc-time").css({
            backgroundColor: '#cc0000',
            'border-bottom': '1px solid #cc0000'
        });
    }
    $event.find(".icon-send").bind("click", function () {
        jQuery(this).unbind("click");
        sendSMS(calEvent.id);
    });
    $event.bind("click", ".icon-trash", function () {
        jQuery("#deleteIncident").attr("data-event", calEvent.id);
    });
    $event.bind("dblclick", function () {
        calendar_edit_entry(calEvent, $event);
    });
}

function sendSMS(calEventID) {
    $.ajax({
        url: base_url + "ajax/twilioBase/send/" + calEventID,
        type: "POST",
        async: false,
        cache: false,
        success: function (data) {
            if (data) {
                var obj = jQuery.parseJSON(data);
                if (obj.status == "success") {
                    $(".alert-nthsuccess").trigger('click');
                    $(".btn-remove").trigger('click');
                    $('#message-success').text(obj.message);
                    removeMarkers();
                    $calendar.weekCalendar('refresh');
                } else {
                    $(".alert-warning").trigger('click');
                    $('#schedule-error').text(obj.message);
                }
            }
        }
    });
}

function newEvent(calEvent, $event, FreeBusyManager, calendar) {
    calendar_new_entry(calEvent, $event);
}

function resizeEvent(newCalEvent, oldCalEvent, $event) {
    var orderID = newCalEvent.id;
    var oldEnd = new Date(oldCalEvent.end);
    var newEnd = new Date(newCalEvent.end);
    var addTime = (newEnd - oldEnd);
    var userID = newCalEvent.userId;

    var oldYear = oldCalEvent.start.getFullYear();
    var oldMonth = oldCalEvent.start.getMonth() + 1;
    var oldDay = oldCalEvent.start.getDate();

    var newYear = newCalEvent.start.getFullYear();
    var newMonth = newCalEvent.start.getMonth() + 1;
    var newDay = newCalEvent.start.getDate();

    var startTime = oldYear + "-" + oldMonth + "-" + oldDay + " " + oldCalEvent.start.getHours() + ":" + oldCalEvent.start.getMinutes();
    var endTime = newYear + "-" + newMonth + "-" + newDay + " " + newCalEvent.end.getHours() + ":" + newCalEvent.end.getMinutes();

    $.ajax({
        type: 'POST',
        url: base_url + "ajax/order/updateOrderNewTime",
        async: false,
        data: {detail: JSON.stringify({orderID: orderID, userID: userID, addTime: addTime, startTime: startTime, endTime: endTime})},
        success: function (data) {
            var obj = jQuery.parseJSON(data);
            if (obj.reponse == "error") {
                $(".alert-warning").trigger('click');
                $('#schedule-error').text(obj.msg);
            } else {
                $(".alert-warning").fadeOut("slow");
            }
        }
    });
    removeMarkers();
    $calendar.weekCalendar('refresh');
}

function dropEvent(newCalEvent, oldCalEvent, $event) {
    if (newCalEvent.id) {
        var orderID = newCalEvent.id;
        var userID = newCalEvent.userId;

        var startYear = oldCalEvent.start.getFullYear();
        var startMonth = oldCalEvent.start.getMonth() + 1;
        var startDay = oldCalEvent.start.getDate();

        var endYear = newCalEvent.start.getFullYear();
        var endMonth = newCalEvent.start.getMonth() + 1;
        var endDay = newCalEvent.start.getDate();

        var oldTime = startYear + "-" + startMonth + "-" + startDay + " " + oldCalEvent.start.getHours() + ":" + oldCalEvent.start.getMinutes();
        var newTime = endYear + "-" + endMonth + "-" + endDay + " " + newCalEvent.start.getHours() + ":" + newCalEvent.start.getMinutes();

        $.ajax({
            type: 'POST',
            url: base_url + "ajax/order/updateOrder",
            data: {detail: JSON.stringify({orderID: orderID, userID: userID, oldTime: oldTime, newTime: newTime})},
            success: function (data) {
                removeMarkers();
                $calendar.weekCalendar('refresh');
                var obj = jQuery.parseJSON(data);
                if (obj.reponse == "error") {
                    $(".alert-warning").trigger('click');
                    $('#schedule-error').text(obj.msg);
                } else {
                    $(".alert-warning").fadeOut("slow");
                }
            }
        });
    }
}

function getOrderDetail(calEvent) {
    jQuery('.btn-activity').attr("data-id", calEvent.id);
    $.ajax({
        type: "POST",
        url: base_url + "ajax/order/getOrderDetail/" + calEvent.id,
        async: false,
        detail: 1,
        success: function (data) {
            if (data) {
                var obj = jQuery.parseJSON(data);
                jQuery(".alertbox-notification-appointment").html(obj.alertHtml);
                if (obj.customerEmail == "" || obj.customerEmail == "0") {
                    jQuery("#email-error-edit").html(obj.emailErrorHtml);
                    jQuery("#email-error-edit").parent().show();
                } else {
                    jQuery("#email-error-edit").html("");
                    jQuery("#email-error-edit").parent().hide();
                }
                if (obj.customerPhone == "" || obj.customerPhone == "0") {
                    jQuery("#phone-error-edit").html(obj.phoneErrorHtml);
                    jQuery("#phone-error-edit").parent().show();
                } else {
                    jQuery("#phone-error-edit").html("");
                    jQuery("#phone-error-edit").parent().hide();
                }
                jQuery("#editIncidentForm select[name=service]").html(obj.servicesHtml);
                jQuery("#editIncidentForm select[name=status]").html(obj.statusesHtml);
                jQuery("#editIncidentForm select[name=duration]").html(obj.durationsHtml);
                jQuery("#editIncidentForm select[name=worker]").html(obj.workersHtml);
                jQuery("#editIncidentForm input[name=name]").val(obj.orderName);
                jQuery("#editIncidentForm textarea[name=description]").val(obj.orderDetail);
                jQuery("#editIncidentForm input[name=date]").val(obj.orderDate);
                jQuery("#editIncidentForm input[name=time]").val(obj.orderTime);
                jQuery("#editIncidentForm input[name=customer]").val(obj.customerName);
                jQuery("#editIncidentForm input[name=address]").val(obj.customerAddress);
                jQuery("#editIncidentForm input[name=email]").val(obj.customerEmail);
                jQuery("#editIncidentForm input[name=phone]").val(obj.customerPhone);
                if (obj.isPhoneHomeLandline) {
                    jQuery("#editIncidentForm input[name=cell-type]").attr("checked", "checked");
                } else {
                    jQuery("#editIncidentForm input[name=cell-type]").removeAttr("checked");
                }
                jQuery("#editIncidentForm #editIncidentNotes").html(obj.notesHtml);
                jQuery("#editIncidentForm input[name='orderID']").val(obj.orderID);
                jQuery("#edit_popup_cont").show();
            }
        },
        error: function () {
            jQuery("input[name=edit-incident]").removeAttr("disabled");
        }
    });

    $(".alert-warning").fadeOut("slow");
    $('#modal-editIncident').modal({
        keyboard: false,
        backdrop: 'static'
    });
    $("#modal-editIncident").on('show.bs.modal', function () {
        var popupInterval = setInterval(function () {
            if (jQuery("#edit_popup_cont").is(":visible") === true) {
                var headerHeight = $(this).find(".modal-header").height();
                $(this).find(".modal-body").css("max-height", $(this).height() - headerHeight - 60);
                clearInterval(popupInterval);
            }
        }, 1);
    });
}

function calendar_edit_entry(calEvent, $event) {
    jQuery('.modal-body #error-php').html('');
    getCustomerDataByOrderID(calEvent.id);
    getOrderDetail(calEvent);
}

function calendar_new_entry(calEvent, $event) {
    $(".alert-warning").fadeOut("slow");
    jQuery("input[name=customer]").val("");
    var d = calEvent.start;
    var year = d.getFullYear();
    var month = d.getMonth() + 1;
    var day = d.getDate();
    var hours = d.getHours();
    var minutes = d.getMinutes();

    var orderStart = year + "-" + month + "-" + day + " " + hours + ":" + minutes;
    jQuery.post(base_url + "ajax/order/getPrefilledData", {detail: JSON.stringify({userID: calEvent.userId, start: orderStart})}, function (data) {
        if (data) {
            var obj = jQuery.parseJSON(data);
            jQuery("select[name=worker]").html(obj.workers);
            jQuery("input[name=date]").val(obj.date);
            jQuery("input[name=time]").val(obj.time);
        }
    });
    $('#modal-1').modal({
        keyboard: false,
        backdrop: 'static'
    });
    $("#modal-1").on('show.bs.modal', function () {
        var headerHeight = $(this).find(".modal-header").height();
        $(this).find(".modal-body").css("max-height", $(this).height() - headerHeight - 100);
    });
    $('.btn-remove').click(function () {
        $(".control-group").children('select[name="worker"]').prop('selectedIndex', 0);
        $calendar.weekCalendar('removeEvent', calEvent.id);
    });
}

$.noConflict();
(function ($) {
    $(document).ready(function () {
        var d = new Date();
        var year = d.getFullYear();
        var month = d.getMonth();
        var day = d.getDate();
        var events;
        var workers;

        $.ajax({
            url: base_url + "ajax/calendar/getCalendarWorkersData",
            type: 'POST',
            async: false,
            cache: false,
            success: function (data) {
                if (data) {
                    var obj = jQuery.parseJSON(data);
                    workers = obj.workerString;
                }
            }
        });

        initMap();
        $calendar = $('#calendar').weekCalendar({
            timeslotsPerHour: 2,
            scrollToHourMillis: 0,
            newEventText: 'New Incident',
            businessHours: {start: 8, end: 24, limitDisplay: true},
            timeslotHeight: 32,
            height: function ($calendar) {
                jQuery("#map").css("height", $(window).height() + 17 + "px");
                return $(window).height();
            },
            eventResize: function (newCalEvent, oldCalEvent, $event) {
                resizeEvent(newCalEvent, oldCalEvent, $event);
            },
            eventRender: function (calEvent, $event) {
                renderEvent(calEvent, $event);
            },
            eventClick: function (calEvent, $event) {
                //Single Click select MAP
                var orderID = calEvent.id;
                pan(orderID);
            },
            eventDrop: function (newCalEvent, oldCalEvent, $event) {
                dropEvent(newCalEvent, oldCalEvent, $event);
            },
            eventNew: function (calEvent, $event, FreeBusyManager, calendar) {
                newEvent(calEvent, $event, FreeBusyManager, calendar);
            },
            data: function (start, end, callback) {
                setMarkers(map, start, end);
                callback({
                    options: {
                        defaultFreeBusy: {
                            free: true
                        }
                    },
                    events: getIncidents(start, end)
                });
            },
            eventHeader: function (calEvent, calendar) {
                return '';
            },
            users: workers,
            showAsSeparateUser: false,
            displayOddEven: true,
            displayFreeBusys: true,
            allowCalEventOverlap: true,
            daysToShow: 1,
            switchDisplay: {'1 day': 1, '3 next days': 3, 'work week': 5, 'full week': 7},
            headerSeparator: ' ',
            useShortDayNames: true,
            // I18N
            dateFormat: 'd F y'
        });

        jQuery('#deleteIncident').click(function () {
            var eventID = jQuery(this).attr('data-event');
            $.ajax({
                type: 'POST',
                url: base_url + "ajax/calendar/deleteEvent",
                data: {orderID: eventID},
                success: function (data) {
                    removeMarkers();
                    $calendar.weekCalendar('removeEvent', eventID);
                    $calendar.weekCalendar('refresh');
                }
            });
        });

        var timer = window.setInterval(function () {
            var pageSlug = jQuery("meta[name=page]").attr("content");
            $.ajax({
                type: 'POST',
                url: base_url + "ajax/calendar/checktablesUpdated/" + pageSlug,
                success: function (data) {
                    if (data != "Not Updated") {
                        removeMarkers();
                        $calendar.weekCalendar('refresh');
                        getNewNotes();
                        getAlerts();
                        getSurveyNumber();
                    }
                }
            });
        }, 5000);

    });
})(jQuery);