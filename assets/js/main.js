/**
 * JS for all popups elements: done, abort, enroute, cancel enroute
 * and also start button dropdown.
 * This is for the user accessible section, not the admin section.
 */

jQuery(document).ready(function() {

    var Click = 0;
    var fancyBox = jQuery('.fancybox-close');

    jQuery('body').click(function(evt) {
        ClickEvent = evt.target;
        if (jQuery(ClickEvent).hasClass("f-click") !== true) {
            jQuery(".onclick-hide").slideUp("fast", function() {
                var $this = jQuery(this);
                $this.parent().removeClass("mobile-ul");
                $this.parent().css("border-radius", "100px");
            });
            Click = 0;
        }
    });

    $(document.body).on("click", ".first-enroute", function() {
        var href = $(this).attr("href");
        $(".first-enroute").attr("href","javascript:void();");
        window.location.href = href;
        return;
    });

    jQuery('.detail-btn').click(function() {
        jQuery('#cus-loading-overlay span').text("Please wait, your request is being processed");
    });

    jQuery('.loadOverlaySetEnroute').click(function() {
        var overlay = jQuery('#cus-loading-overlay');
        var header = jQuery('.header');

        checkInternetAjax(function(isOnline) {
            if (isOnline == true) {
                if (jQuery(window).width() > 480) {
                    overlay.css({"top": "114px", "z-index": "1"});
                    header.css({"position": "fixed", "width": "97%", "z-index": "1", "top": "0", "padding": "36px 20px"});
                } else {
                    overlay.css({"top": "70px", "z-index": "1"});
                    header.css({"position": "fixed", "width": "92%", "z-index": "1"});
                }
            }
        });
    });

    jQuery(".f-click").click(function() {
        var $this = jQuery(this);
        var onclickHide = jQuery(".onclick-hide");
        Click = Click + 1;
        if (Click > 2) {
            Click = 1;
        }
        if (Click == 1) {
            onclickHide.slideDown("fast");
            $this.parent().parent().css("border-radius", "25px 25px 0px 0px");
            $this.parent().parent().addClass("mobile-ul");
        } else {
            var orderID = $this.attr("data-id");
            var url = base_url + "orders/start/" + orderID;
            checkInternetAjax(function(isOnline) {
                if (isOnline == true) {
                    window.location.href = url;
                } else {
                    setIntervalCheck();
                }
            });
            onclickHide.slideUp("fast", function() {
                var $this = jQuery(this);
                $this.parent().removeClass("mobile-ul");
                $this.parent().css("border-radius", "100px");
            });
            return false;
        }
    });

    if (jQuery(".success-msg").is(":visible") == true) {
        jQuery(".success-msg").delay(3500).fadeOut("slow");
    }

    jQuery(".Error span").click(function() {
        jQuery(".Error").fadeOut("slow");
    });

    jQuery(".enroute-done").click(function() {

        var enrouteDone = jQuery(".enroute-done-form");
        var enrouteNote = jQuery(".enroute-note-form");
        var popupTextArea = jQuery(".popup-textarea");
        var enrouteDoneForm = jQuery(".enroute-done-form input[name=Status]");
        var enrouteNoteForm = jQuery(".enroute-note-form input[name=Status]");

        if (enrouteNote.is(":visible") == true) {
            enrouteNote.slideUp(500);
            jQuery(".error").addClass("display-none");
            enrouteNoteForm.val("");
            popupTextArea.val("");
            enrouteDone.delay(800).slideDown(500);
            enrouteDoneForm.val("Done");
        } else {
            if (enrouteDone.is(":visible") == true) {
                enrouteDone.slideUp(500);
                enrouteDoneForm.val("");
                popupTextArea.val("");
            } else {
                enrouteDone.slideDown(500);
                enrouteDoneForm.val("Done");
            }
        }
    });

    jQuery(".enroute-note").click(function() {
        var enrouteDone = jQuery(".enroute-done-form");
        var enrouteNote = jQuery(".enroute-note-form");
        var popupTextArea = jQuery(".popup-textarea");
        var enrouteNoteInput = jQuery(".enroute-note-form input[name=Status]");
        var enrouteDoneInput = jQuery(".enroute-done-form input[name=Status]");

        if (enrouteDone.is(":visible") == true) {
            enrouteDone.slideUp(500);
            enrouteDoneInput.val("");
            popupTextArea.val("");
            enrouteNote.delay(800).slideDown(500);
            enrouteNoteInput.val("Report");
        } else {
            if (enrouteNote.is(":visible") == true) {
                enrouteNote.slideUp(500);
                jQuery(".error").addClass("display-none");
                enrouteNoteInput.val("");
                popupTextArea.val("");
            } else {
                enrouteNote.slideDown(500);
                enrouteNoteInput.val("Report");
            }
        }
    });

    jQuery(".enroute-schedule").click(function() {
        jQuery(".enroute-done-form").slideUp(500);
        jQuery(".enroute-note-form").slideUp(500);
        var $this = jQuery(this);
        checkInternetAjax(function(isOnline) {
            if (isOnline == true) {
                $this.parent().parent().find('.enroute-schedule-form').submit();
            } else {
                fancyBox.click();
                setIntervalCheck();
            }
        });
    });

    jQuery(".anchor-no-internet").click(function(e) {
        e.preventDefault();
        var $this = $(this);
        checkInternetAjax(function(isOnline) {
            if (isOnline == true) {
                fancyBox.click();
                window.location = $this.attr('href');
            } else {
                setIntervalCheck();
            }
        });
    });

    jQuery(".cancel-enroute-done").click(function() {
        setCancelEnrouteFormParam("cancel-enroute-complete", "Done", "bg-green");
        jQuery("#cancel-enroute-form textarea").removeClass("display-none");
        jQuery("#cancel-enroute-form textarea").attr("placeholder", "Enter note");
        jQuery("#cancel-enroute-form .error").addClass("display-none");
    });

    jQuery(".cancel-enroute-abort").click(function() {
        setCancelEnrouteFormParam("cancel-enroute-abort", "Abort", "bg-red");
        jQuery("#cancel-enroute-form textarea").removeClass("display-none");
        jQuery("#cancel-enroute-form textarea").attr("placeholder", "Enter note (required)");
        jQuery("#cancel-enroute-form .error").addClass("display-none");
    });

    jQuery(".cancel-enroute-schedule").click(function() {
        jQuery("#cancel-enroute-form .error").addClass("display-none");
        jQuery("#cancel-enroute-form textarea").addClass("display-none");
        setCancelEnrouteFormParam("cancel-enroute-schedule", "Reschedule", "bg-blue");
    });

});

function checkInternetAjax(callback) {

    $.ajax({
        type: "HEAD",
        url: base_url + "ajax/checkInternet/checkInternetConnection",
        async: true,
        error: function() {
            if ($.isFunction(callback)) {

                jQuery('#cus-loading-overlay').hide();
                jQuery('.cus-overlay').hide();
                jQuery(".error-msg").show();
                callback(false);
            }
        },
        success: function() {
            if ($.isFunction(callback)) {
                callback(true);
            }
        }
    }, 1000);
}

/**
 * check internet is connected or not on every 5th second
 * @param {object} $this
 */
function setIntervalCheck($this) {
    var timer = window.setInterval(function() {
        $.ajax({
            type: "HEAD",
            url: base_url + "ajax/checkInternet/checkInternetConnection",
            async: true,
            error: function() {
            },
            success: function() {
                jQuery(".error-msg").hide();
                $this.closest("form").submit();
                jQuery(".popup-textarea").val("");
                clearInterval(timer);
            }
        }, 1000);
    }, 5000);
}

/**
 * set parameters in cancel enroute popup in mobile app according to situtation
 * @param {string} label,button name, class name
 */
function setCancelEnrouteFormParam(label, buttonName, className) {
    var $cancelEnroute = jQuery("#cancel-enroute-form");
    if ($cancelEnroute.is(":visible") == true) {
        $cancelEnroute.slideUp(300);
        $cancelEnroute.delay(150).slideDown("slow");
    } else {
        $cancelEnroute.slideUp(300);
        $cancelEnroute.delay(150).slideDown("slow");
    }

    jQuery("#cancel-enroute-form input[name=label]").val(label);
    jQuery("#cancel-enroute-form input[type=button]").val(buttonName);

    if (className == "bg-green") {
        var removeClasses = "bg-red bg-blue";
    } else if (className == "bg-red") {
        var removeClasses = "bg-green bg-blue";
    } else {
        var removeClasses = "bg-red bg-green";
    }

    jQuery("#cancel-enroute-form input[type=button]").addClass(className).removeClass(removeClasses);

}