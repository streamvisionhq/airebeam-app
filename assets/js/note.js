/**
 * checking if is there any new note is arrived
 * so set local storage value of all number of notes
 */

jQuery(document).ready(function() {
    var pageSlug = jQuery("meta[name=page]").attr("content");
    var timer = window.setInterval(function() {
        $.ajax({
            type: 'POST',
            url: base_url + "ajax/calendar/checktablesUpdated/"+pageSlug,
            success: function(data) {
                if (data != "Not Updated") {
                    if (typeof $calendar != "undefined") {
                        $calendar.weekCalendar('refresh');
                    }
                    getNewNotes();
                    getAlerts();
                    getSurveyNumber();
                    getSurveyHtml();
                    getNewFailureSms();
                }
            }
        });
    }, 3000);

});

jQuery(document).on("click", ".submit_alert", function() {
    var id = jQuery("#modal-a3").attr("data-id");
    jQuery.post(base_url + "ajax/alert/AlertActionPerform/" + id, function(data) {
        if (data) {
            location.reload();
        }
    });
    getAlerts();
    jQuery('.btn-remove').click();
});

jQuery(document).on("click", ".append-alert-id", function() {
    var id = jQuery(this).attr("data-id");
    jQuery("#modal-a3").attr("data-id", id);
});

function getNewNotes() {
    jQuery.post(base_url + "ajax/note/getNotes", function(data) {
        data = parseInt(data);
        if (data) {
            jQuery("#new-notes-number").html(data);
            localStorage.setItem('newNotes', data);
        }
    });
}

function getSurveyNumber() {
    jQuery.post(base_url + "ajax/survey/getSurveys", function(data) {
        data = parseInt(data);
        if (data) {
            jQuery("#pending-survey-number").html(data);
            localStorage.setItem('queuedSurveys', data);
        } else {
            jQuery("#pending-survey-number").html("");
            localStorage.removeItem('queuedSurveys');
        }
    });
}

function getSurveyHtml() {
    jQuery.post(base_url + "ajax/survey/getAllSurveys", function(data) {
        if (data) {
            jQuery(".survey-list").html(data);
        }
    });
}

function getAlerts() {
    jQuery.post(base_url + "ajax/alert/getAlerts", function(data) {
        if (data) {
            jQuery(".alertbox-notification").html(data);
        } else {
            jQuery(".alertbox-notification").html("");
        }
    });
}

function getNewFailureSms() {
    jQuery.post(base_url + "ajax/alert/getFailureSms", function(data) {
        data = parseInt(data);
        if (data) {
            jQuery("#new-failure-sms-number").html(data);
            localStorage.setItem('newFailureSms', data);
        }
    });
}