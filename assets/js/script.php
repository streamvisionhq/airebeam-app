<?php
/**
 * this file uses php session variable
 * and show popup for email send or fill now options
 */
header("Content-type: application/javascript");
session_start();
?>
jQuery(document).ready(function() {
<?php
if (isset($_SESSION["done_order"])) {
    ?>
    $.ajax({
        type: "POST",
        url: base_url + "ajax/order/getOrderDetail/<?php echo $_SESSION["done_order"]; ?>",
        async: false,
        detail: 1,
        success: function(data) {
            if (data) {
                var obj = jQuery.parseJSON(data);
                jQuery('#order-popup-survey .send-sms-for-survey').attr('data-customerID',obj.customerID);                
            }
        }
    });
    jQuery('.order-popup-survey').trigger('click');
    jQuery('#order-popup-survey .send-sms-for-survey').attr('data-id','<?php echo $_SESSION["done_order"]; ?>');
    <?php
}
unset($_SESSION["done_order"]);
?>
});