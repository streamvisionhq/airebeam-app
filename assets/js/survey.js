/**
 * JS for mark read surveys, send survey on email and get number of new surveys
 */

jQuery(document).ready(function() {
    jQuery('#all-surveys').on('submit', function(e) {
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: base_url + "ajax/order/sendAllSurveyToEmail/",
            data: jQuery('#all-surveys').serialize(),
            success: function(data) {
                var obj = jQuery.parseJSON(data);
                if (obj.status == "success") {
                    getSurveyNumber();
                    jQuery(".survey-list").html(obj.response);
                }
            },
            error: function() {
            }
        });
    });

    jQuery(document).on("click", ".send-to-email", function() {
        var orderID = jQuery(this).attr("data-id");
        var app = jQuery(this).attr("data-app");
        if (app == "mobile-app") {
            localStorage.setItem('surveysUpdated', "yes");
        }
        if (orderID) {
            $.ajax({
                type: "POST",
                url: base_url + "ajax/order/sendSurveyToEmail/",
                data: {orderID: orderID, app: app},
                success: function(data) {
                    location.reload();
                },
                error: function() {
                }
            });
        }
    });

    function getSurveyNumber() {
        jQuery.post(base_url + "ajax/survey/getSurveys", function(data) {
            data = parseInt(data);
            if (data) {
                jQuery("#pending-survey-number").html(data);
                localStorage.setItem('queuedSurveys', data);
            } else {
                jQuery("#pending-survey-number").html("");
                localStorage.removeItem('queuedSurveys');
            }
        });
    }

});