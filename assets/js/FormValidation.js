/**
 * validates email and password required fields
 */
$(document).ready(function() {
    jQuery.validator.setDefaults({
        debug: false,
        success: "valid"
    });
    $('#login-form').validate({
        rules: {
            email:
                    {
                        required: true,
                        email: true
                    },
            password:
                    {
                        required: true
                    },
        },
        errorElement: 'span',
        errorClass: 'help-block',
    })
});