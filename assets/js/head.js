/**
 * initiates a variables isConnected
 * load a img so if internet goes down this image is already loaded
 * add's fancy box dialog close event action
 */
var isConnected = null;
$(document).ready(function() {
    var img = new Image();
    img.src = base_url + "assets/img/close-icon.png";

    $('.fancybox').fancybox({
        afterClose: function() {
            jQuery(".error").css("display", "none");
            jQuery("#cancel-enroute-form").css("display", "none");
            jQuery(".enroute-done-form,.enroute-note-form,.enroute-schedule-form").css("display", "none");
            var is_survey = (jQuery(this).attr('href') == '#order-popup-survey') ? true : false;
            if (is_survey) {
                var orderID = jQuery(".send-sms-for-survey").attr("data-id");
                $.ajax({
                    type: "POST",
                    url: base_url + "ajax/order/addPendingSurvery/" + orderID,
                    success: function(data) {
                        window.location.reload();
                    },
                    error: function() {
                    }
                });
                
            }
        },
        beforeClose: function() {
            var emailExists = jQuery('#order-popup-survey .fill-now').attr('data-email');
            if (emailExists == "No") {
                var orderID = jQuery('#order-popup-survey .fill-now').attr('data-id');
                jQuery.ajax({
                    type: "POST",
                    url: base_url + "ajax/order/addOrderEmailActivity/" + orderID,
                    async: false,
                    detail: 1,
                    success: function(data) {}
                });
            }
        }
    });
});