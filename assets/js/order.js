/**
 * JS for add, edit incident submit form
 */

jQuery(document).ready(function() {
    /**
     * jquery for mobile app popup forms submit
     */
    jQuery("input[name=status]").click(function() {
        var $this = jQuery(this);
        $this.attr("disabled", "disabled");
        var customOverloading = jQuery('#cus-loading-overlay');
        checkInternetAjax(function(isOnline) {
            var label = jQuery($this.closest("form").find("input[name=label]"));
            var orderID = jQuery($this.closest("form").find("input[name=order-id]"));
            label = jQuery(label[0]).val();
            orderID = jQuery(orderID[0]).val();
            if (label == "complete" || label == "enroute-complete" || label == "cancel-enroute-complete" || label == "abort" || label == "cancel-enroute-abort" || label == "enroute-abort" || label == "detail") {
                var textArea = jQuery($this.closest("form").find("textarea[name=note]"));
                textArea = jQuery(textArea[0]).val();
                if (textArea == "") {
                    jQuery(".error").removeClass("display-none").text("Error! Note is required.");
                    jQuery(".error").show();
                    $this.removeAttr("disabled");
                    return false;
                }
            }

            if (isOnline == true) {
                customOverloading.show();
                jQuery('.cus-overlay').show();
                jQuery('.fancybox-close').click();
                // process all hits when order is completed
                if (label == "complete" || label == "enroute-complete" || label == "cancel-enroute-complete") {
                    var form = $this.closest("form").serialize();
                    localStorage.setItem('surveysUpdated', "yes");
                    $.ajax({
                        type: "POST",
                        url: base_url + "ajax/order/doneOrder",
                        data: form,
                        success: function(data) {
                        },
                        error: function() {
                        }
                    });
                }
                $this.closest("form").submit();

            } else {
                jQuery('.fancybox-close').click();
                jQuery(".error-msg").show();
                setIntervalCheck($this);
            }
        });
    });

    /**
     * prevent default behavior of add incident form submit
     */
    jQuery("#addIncidentForm").submit(function() {
        return false;
    });

    /**
     * prevent default behavior of edit incident form submit
     */
    jQuery("#editIncidentForm").submit(function() {
        return false;
    });

    // First send SMS if SMS failed then send email
    jQuery(".send-sms-for-survey").click(function() {
        jQuery(this).attr('data-email','');
        jQuery.fancybox.close();
        var orderID = jQuery(this).attr("data-id");
        if (orderID) {
            $.ajax({
                type: "POST",
                url: base_url + "ajax/order/sendSurveySms/" + orderID,
                success: function(data) {
                    window.location.reload();
                },
                error: function() {
                }
            });
        }
    });

});